@extends('frontOffice.layout')


<style>
    #wrapper {
        padding-top: 0px!important;
    }
</style>
@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('404 Not Found') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li>{{ __('404 Not Found') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>


    <div class="container">

        <div class="row">
            <div class="col-xl-12">

                <section id="not-found" class="center margin-top-50 margin-bottom-25">
                    <h2>404 <i class="icon-line-awesome-question-circle"></i></h2>
                    <p>{{ __("We're sorry, but the page you were looking for doesn't exist") }}</p>
                </section>

                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <div class="intro-banner-search-form not-found-search margin-bottom-50">

                            <!-- Button -->
                            <div style="width: 100%" class="intro-search-button">
                                <a href="{{ url()->previous() }}" style="width: 100%; color: white; text-align: center" class="button ripple-effect btn-block">{{ __('Back to home') }}</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- Container / End -->


    <!-- Spacer -->
    <div class="margin-top-70"></div>
@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
