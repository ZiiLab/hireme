<!-- Basic Page Needs
================================================== -->
<title>{{ $title ?? 'Not Found' }}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="api-base-url" content="{{ url('') }}" />
@yield('meta')
<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{asset('frontOffice/css/style.css')}}">
<link rel="stylesheet" href="{{asset('frontOffice/css/colors/blue.css')}}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="icon" href="{{asset(\App\Modules\User\Models\Media::where('type' , 102)->first() ? \App\Modules\User\Models\Media::where('type' , 102)->first()->link : '')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />


<script src="{{asset('frontOffice/js/jquery-3.4.1.min.js')}}"></script>



<style>

    .disabled-area {
        pointer-events: none;
     cursor: default;
    background-color: #ddd!important;
    color: #888!important;
    }
    #navigation ul a {
        padding: 5px;
    }
    .accordionHandler {
        position: absolute;
        top: 0;
        right: 0;
        height: 36px;
        width: 36px;
        padding: 0;
        color: #fff;
        background-color: #2a41e8;
        border-radius: 4px;
        margin: 6px;
        font-size: 19px;
        text-align: center;
        line-height: 36px;
    }

    input[type=date]::-webkit-inner-spin-button,
        /* up */
    input[type=date]::-webkit-outer-spin-butto {
        display: none;
    }
     .loader,
     .loader:before,
     .loader:after {
         background: #2a41e8;
         -webkit-animation: load1 1s infinite ease-in-out;
         animation: load1 1s infinite ease-in-out;
         width: 1em;
         height: 4em;
     }
    .loader {
        color: #2a41e8;
        text-indent: -9999em;
        margin: 88px auto;
        position: relative;
        font-size: 11px;
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }
    .loader:before,
    .loader:after {
        position: absolute;
        top: 0;
        content: '';
    }
    .loader:before {
        left: -1.5em;
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }
    .loader:after {
        left: 1.5em;
    }
    @-webkit-keyframes load1 {
        0%,
        80%,
        100% {
            box-shadow: 0 0;
            height: 4em;
        }
        40% {
            box-shadow: 0 -2em;
            height: 5em;
        }
    }
    @keyframes load1 {
        0%,
        80%,
        100% {
            box-shadow: 0 0;
            height: 4em;
        }
        40% {
            box-shadow: 0 -2em;
            height: 5em;
        }
    }
</style>
