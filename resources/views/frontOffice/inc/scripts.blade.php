<script src="{{asset('frontOffice/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>


<script src="{{asset('frontOffice/js/mmenu.min.js')}}"></script>
<script src="{{asset('frontOffice/js/tippy.all.min.js')}}"></script>
<script src="{{asset('frontOffice/js/simplebar.min.js')}}"></script>
<script src="{{asset('frontOffice/js/bootstrap-slider.min.js')}}"></script>

<script src="{{asset('frontOffice/js/snackbar.js')}}"></script>
<script src="{{asset('frontOffice/js/clipboard.min.js')}}"></script>
{{-- <script src="{{asset('frontOffice/js/counterup.min.js')}}"></script> --}}
<script src="{{asset('frontOffice/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('frontOffice/js/slick.min.js')}}"></script>
<script src="{{asset('frontOffice/js/custom.js')}}"></script>
<script src="{{asset('frontOffice/js/form.js')}}"></script>
<link rel="stylesheet" href="{{ asset('frontOffice/css/jquery-confirm.min.css') }}">
<script src="{{ asset('js/jquery-confirm.min.js') }}"></script>
<script src="//js.pusher.com/3.1/pusher.min.js"></script>
<script src="{{asset('js/sharer.js')}}" charset="utf-8"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="{{asset('frontOffice/js/validation.js')}}"></script>

<script src="{{asset('frontOffice/js/bootstrap-select.min.js')}}"></script>

<script>
    @if(Session::has('message'))

    Snackbar.show({
        text: '{{ Session::get('message') }}',
        pos: 'bottom-right',
        showAction: false,
        actionText: "Dismiss",
        duration: 3000,
        textColor: '#fff',
        backgroundColor: '{{ Session::get('color') }}'
    });

    @endif
</script>

<script type="text/javascript">
        Dropzone.autoDiscover = false;

function deleteForm(path, params, method) {
  method = method || "post";

  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", path);

  var csrfField = document.createElement("input");
  csrfField.setAttribute("type", "hidden");
  csrfField.setAttribute("name", "_token");
  csrfField.setAttribute("value", $('meta[name="csrf-token"]').attr('content'));
  form.appendChild(csrfField);

  for(var key in params) {
      if(params.hasOwnProperty(key)) {
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", key);
          hiddenField.setAttribute("value", params[key]);

          form.appendChild(hiddenField);
       }
  }

  document.body.appendChild(form);
  form.submit();
}

$('#snackbar-user-status label').click(function() { 
   if($('.updateStatus').hasClass('status-online')) {
    $('.updateStatus').removeClass('status-online');
  }else {
    $('.updateStatus').addClass('status-online');
  }
  
  Snackbar.show({
    text: '{{ __('Your status has been changed!') }}',
    pos: 'bottom-center',
    showAction: false,
    actionText: "Dismiss",
    duration: 3000,
    textColor: '#fff',
    backgroundColor: '#383838'
  }); 


}); 

</script>


<script>
  $(document).ready(function() {
   $(".toggle-password").on('click', function(event) {
  event.preventDefault();
  if($('input[name="password"]').attr("type") == "text"){
      $('input[name="password"]').attr('type', 'password');
      $('.toggle-password').addClass( "fa-eye-slash" );
      $('.toggle-password').removeClass( "fa-eye" );
  }else if($('input[name="password"]').attr("type") == "password"){
      $('input[name="password"]').attr('type', 'text');
      $('.toggle-password').removeClass( "fa-eye-slash" );
      $('.toggle-password').addClass( "fa-eye" );
  }
});
});
</script>