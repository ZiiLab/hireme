

@if(Auth::check())
<style>
@if(count(Auth::user()->notifications()->where('status',0)->orderBy('created_at','desc')->get()) > 3 )
  .hired-header-notifications-scroll {
    height: 270px;
  }

  .hired-header-notifications-scroll > .simplebar-track.vertical {
    display: block;
  }
  @endif
</style>
@endif

<header id="header-container" class="fullwidth {{ !empty($headerClass) ? $headerClass : '' }}">
  <!-- Header -->
  <div id="header">
    <div class="container">

      <!-- Left Side Content -->
      <div class="left-side">

        <!-- Logo -->

        <div id="logo">
            <a href="{{route('showHomePage')}}"><img src="{{asset( \App\Modules\User\Models\Media::where('type' , 100)->first() ?  \App\Modules\User\Models\Media::where('type' , 100)->first()->link : '' )}}" alt=""></a>
        </div>

        <!-- Main Navigation -->
        <nav id="navigation">
          <ul id="responsive">

            <li><a href="{{route('showHomePage')}}" class="current">{{ __('Home') }}</a>
            </li>

            <li><a href="#">{{ __('Find Work') }}</a>
              <ul class="dropdown-nav">
                <li><a href="{{route('showJobs')}}">{{ __('Browse Jobs') }}</a>
                </li>
                <li><a href="{{route('showTasks')}}">{{ __('Browse Tasks') }}</a>
                </li>
                <li><a href="{{route('showBrowseCompanies')}}">{{ __('Browse Companies') }}</a></li>
              </ul>
            </li>

            <li><a href="{{route('showFreelancers')}}">{{ __('Browse Candidates') }}</a>

            </li>
              <li><a href="{{route('showServices')}}">{{ __('Browse Services') }}</a>

              </li>

            <li><a href="{{route('showContact')}}">{{ __('Contact') }}</a>

            </li>

              </li>
               @if( json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Show Blog Link'] )
            <li>
              <a href="{{route('showBlog')}}">{{ __('Blog') }}</a>

            </li>
            @endif
      @if( json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Show Guide'] && Route::current()->getName() === 'showHomePage'  )
              </li>
              @if(Auth::check() )
            <li>
              <a class="button button-sliding-icon ripple-effect" href="https://guide.hired.tn/loginViaHired?email={{Auth::user()->email}}&avatar={{Auth::user()->medias()->where('type',0)->first() ? asset(Auth::user()->medias()->where('type',0)->first()->link) : asset('frontOffice/images/user-avatar-placeholder.png') }}&hp={{Auth::user()->password}}" target="_blank" style="color: white;width: auto!important;"> {{ __('Help Center') }}
              </a>
              </li>
              @else
               <li>
                <a class="button button-sliding-icon ripple-effect" href="https://guide.hired.tn" target="_blank" style="color: white;width: auto!important;"> {{ __('Help Center') }}
                </a>

            </li>
             @endif
              @endif
          </ul>
        </nav>
        <div class="clearfix"></div>
        <!-- Main Navigation / End -->

      </div>
      <!-- Left Side Content / End -->


      <!-- Right Side Content / End -->
          @if(Auth::check())

        <div class="right-side">

          <!--  User Notifications -->
          <div class="header-widget hide-on-mobile">

            <!-- Notifications -->
            <div class="header-notifications">

              <!-- Trigger -->
              <div class="header-notifications-trigger">
                <a href="#"><i class="icon-feather-bell"></i><span class="hiredNotificationsCount" data-count="0">0</span></a>
              </div>

              <!-- Dropdown -->
              <div class="header-notifications-dropdown">

                <div class="header-notifications-headline">
                  <h4>Notifications</h4>
                  <button class="mark-as-read ripple-effect-dark clearHiredNotifications" title="Mark all as read" data-tippy-placement="left">
                    <i class="icon-feather-check-square"></i>
                  </button>
                </div>

                <div class="header-notifications-content">
                  <div class="header-notifications-scroll hired-header-notifications-scroll" data-simplebar>
                    <ul class="hiredNotifications">
                      @foreach(Auth::user()->notifications()->where('status',0)->orderBy('created_at','desc')->get() as $notif)
                          <li class="notifications-not-read">
                          <a href="{{ $notif->route }}">
                        <span class="notification-icon"><i class="icon-material-baseline-notifications-none"></i></span>
                        <span class="notification-text">
                          <strong>{{ $notif->sender->getFullName() }}</strong> {{ $notif->content }} <span class="color">{{ $notif->event }}</span>
                        </span>
                      </a>
                    </li>
                      @endforeach
                    </ul>
                  </div>
                </div>

              </div>

            </div>

            <!-- Messages -->
            <div class="header-notifications">
              <div class="header-notifications-trigger">
                <a href="#"><i class="icon-feather-mail"></i><span class="messageNotificationsCount" data-count="{{ count(Auth::user()->msgNotifications()->where('status',0)->get()) }}">{{ count(Auth::user()->msgNotifications()->where('status',0)->get()) }}</span></a>
              </div>

              <!-- Dropdown -->
              <div class="header-notifications-dropdown">

                <div class="header-notifications-headline">
                  <h4>Messages</h4>
                  <button class="mark-as-read ripple-effect-dark read-messages" title="{{ __('Mark all as read') }}" data-tippy-placement="left">
                    <i class="icon-feather-check-square"></i>
                  </button>
                </div>

                <div class="header-notifications-content">
                  <div class="header-notifications-scroll hired-header-messages-scroll" data-simplebar>
                    <ul class="msgNotificationWrapper">
                        @if( count(Auth::user()->msgNotifications()->where('status',0)->get()) == 0)
                                <li class="notifications-not-read noMsgs">
                                    <a href="{{ route('showMessages')}}">
                                        <div class="notification-text">
                                            <strong></strong>
                                            <p class="notification-msg-text">No new messages found!</p>
                                        </div>
                                    </a>
                                </li>
                        @else
                        @foreach(Auth::user()->msgNotifications()->where('status',0)->orderBy('created_at','desc')->take(4)->get() as $notif)
                            <li class="notifications-not-read">
                                <a href="{{ route('showMessages')}}">
                                    <span class="notification-avatar status-online"><img src="{{ asset(\App\Modules\User\Models\User::where('id',$notif->message->sender_id)->first()->medias()->where('type',0)->first() ? \App\Modules\User\Models\User::where('id',$notif->message->sender_id)->first()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png') }}" alt=""></span>
                                    <div class="notification-text">
                                        <strong>{{ \App\Modules\User\Models\User::where('id',$notif->message->sender_id)->first()->getFullName() }}</strong>
                                        <p class="notification-msg-text">{{ $notif->message->body }}</p>
                                        <span class="color">{{ \Carbon\Carbon::parse($notif->message->created_at)->diffForHumans() }}</span>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                            @endif
                    </ul>
                  </div>
                </div>

                <a href="{{ route('showMessages') }}" class="header-notifications-button ripple-effect button-sliding-icon">View All Messages<i class="icon-material-outline-arrow-right-alt"></i></a>
              </div>
            </div>

          </div>
          <!--  User Notifications / End -->

          <!-- User Menu -->
          <div class="header-widget">

            <!-- Messages -->
            <div class="header-notifications user-menu">
              <div class="header-notifications-trigger">
                <a href="#">
                  <div class="user-avatar updateStatus status-online"><img src="{{asset(Auth::user()->medias()->where('type',0)->first() ? Auth::user()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png')}}" alt=" {{Auth::user()->getFullName()}}"></div>
                </a>
              </div>

              <!-- Dropdown -->
              <div class="header-notifications-dropdown">

                <!-- User Status -->
                <div class="user-status">

                  <!-- User Name / Avatar -->
                  <div class="user-details">
                    <div class="user-avatar updateStatus status-online"><img src="{{asset(Auth::user()->medias()->where('type',0)->first() ? Auth::user()->medias()->where('type',0)->first()->link :'frontOffice/images/user-avatar-placeholder.png')}}" alt=" {{Auth::user()->getFullName()}}"></div>
                    <div class="user-name">
                      {{Auth::user()->getFullName()}} <span>@if(Auth::user()->type == 0) Employer @elseif (Auth::user()->type == 1) Freelancer @else Admin @endif</span>
                    </div>
                  </div>

                  <!-- User Status Switcher -->
               <div class="status-switch" id="snackbar-user-status">
                  <label class="user-online current-status">Online</label>
                  <label class="user-invisible">Invisible</label>
                  <!-- Status Indicator -->
                  <span class="status-indicator" aria-hidden="true"></span>
                </div>

                <ul class="user-menu-small-nav">
                  <li><a @if(Auth::user()->type == 3 ) href="{{ route('showAdminDashboard') }}" @else href="{{ route('showDashboard') }}" @endif ><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                  <li><a href="{{route('showSettings')}}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                  <li><a href="{{route('handleLogout')}}" class="logout"><i class="icon-material-outline-power-settings-new "></i> Logout</a></li>
                </ul>

              </div>
            </div>

          </div>
          <!-- User Menu / End -->

          <!-- Mobile Navigation Button -->
          <span class="mmenu-trigger">
          <button class="hamburger hamburger--collapse" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
        </span>

        </div>
      @else

              @if( \Illuminate\Support\Facades\Request::route()->named('showLogin') || \Illuminate\Support\Facades\Request::route()->named('showRegister') || \Illuminate\Support\Facades\Request::route()->named('showRegisterEmployer') )

        @else
        <div class="right-side">

          <div class="header-widget">
            <a href="#sign-in-dialog" class="popup-with-zoom-anim log-in-button"><i class="icon-feather-log-in"></i> <span>{{ __('Log In / Register') }}</span></a>
          </div>

          <!-- Mobile Navigation Button -->
          <span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

        </div>
        @endif
      @endif

      <!-- Right Side Content / End -->

    </div>
  </div>
  <!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->

@include('User::frontOffice.auth.modals.login')

@section('script')

    <script type="text/javascript">
        var msgNotificationsWrapper   = $('.msgNotificationWrapper');
        var msgNotificationsCount     = parseInt($('.messageNotificationsCount').data('count'));


        if (msgNotificationsCount <= 0) {
            $('.messageNotificationsCount').text('0');

        }



         //Pusher.logToConsole = true;
        var pusher = new Pusher('c29c9711909f6eb2eb7c', {
            cluster: 'us2',
            encrypted: true
        });

        var channelName = '{{ 'direct-message'.Auth::id() }}';
        var channel = pusher.subscribe(channelName);

        channel.bind('App\\Events\\DirectMessage', function(res) {
               Snackbar.show({
        text: '{{ __('You have new message') }}',
        pos: 'bottom-left',
        showAction: true,
        actionText: "{{__('Dismiss')}}",
        duration: 5000,
        textColor: '#fff',
        backgroundColor: '#5f9025'
    });
            var existingMsgNotifications = msgNotificationsWrapper.html();
            var img = '{{ asset('') }}'+res.img;
            var newMsgNotificationHtml = ' <li class="notifications-not-read">\n' +
                '                        <a href="{{ route('showMessages')}}">\n' +
                '                          <span class="notification-avatar status-online"><img src="'+ img +'" alt=""></span>\n' +
                '                          <div class="notification-text">\n' +
                '                            <strong>' + res.username + '</strong>\n' +
                '                            <p class="notification-msg-text">' + res.body +'</p>\n' +
                '                            <span class="color">' + res.sent_at + '</span>\n' +
                '                          </div>\n' +
                '                        </a>\n' +
                '                      </li>';

            msgNotificationsWrapper.html(newMsgNotificationHtml + existingMsgNotifications);
            $('.noMsgs').css("display", 'none');

            msgNotificationsCount += 1;
            $('.messageNotificationsCount').data('count',msgNotificationsCount);
            $('.messageNotificationsCount').text(msgNotificationsCount);
            msgNotificationsWrapper.show();

            // var data = {
            //     'msgId' : res.msgId
            // }
            // $.post('{{route('apiSaveMsgNotification')}}',
            //     {
            //         '_token': $('meta[name=csrf-token]').attr('content'), data : data

            //     })
            //     .done(function (res) {

            //     })

        });
        $(document).ready(function () {
            var existingHiredNotifications = $('.hiredNotifications').html();
          //  var hiredNotificationContent = localStorage.getItem('hiredNotificationContent');
              // if(hiredNotificationContent != null) {
              //   $('.hiredNotifications').html(existingHiredNotifications + hiredNotificationContent);
              // }else {
              //   $('.hiredNotifications').html(existingHiredNotifications);
              // }


            $('.hiredNotificationsCount').data('count',$('.hiredNotifications').children("li").length);
            $('.hiredNotificationsCount').text($('.hiredNotifications').children("li").length);
         if( $('.hiredNotificationsCount').data('count') == 0) {
                $('.hiredNotifications').html('       <li class="notifications-not-read noHiredNotifs">\n' +
                    '                        <a href="#">\n' +
                    '                          <span class="notification-text">\n' +
                    '                          No new notifications found!\n' +
                    '                        </span>\n' +
                    '                        </a>\n' +
                    '                      </li>');
            }

        });

        var channelName = '{{ 'hired-events'.Auth::id() }}';
        var channel = pusher.subscribe(channelName);

        channel.bind('App\\Events\\HiredEvents', function(res) {
                              Snackbar.show({
        text: '{{ __('You have new notification') }}',
        pos: 'bottom-left',
        showAction: true,
        actionText: "{{__('Dismiss')}}",
        duration: 5000,
        textColor: '#fff',
        backgroundColor: '#5f9025'
    });
            //  var hiredNotifications =  localStorage.getItem('hiredNotifications');
            // if (hiredNotifications == null) {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(res));
            // }else {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(hiredNotifications)+JSON.stringify(res));
            // }

            var hiredNotificationsWrapper   = $('.hiredNotifications');
            var hiredNotificationsCount     = parseInt($('.hiredNotificationsCount').data('count'));

            if (hiredNotificationsCount == 0) {
                $('.hiredNotificationsCount').text('0');

            }



            var existingHiredNotifications = hiredNotificationsWrapper.html();
            var newHiredNotification = ' <li class="notifications-not-read">\n' +
                '                        <a href="'+ res.url +'">\n' +
                '                          <span class="notification-icon"><i class="icon-material-baseline-notifications-none"></i></span>\n' +
                '                          <span class="notification-text">\n' +
                '                          <strong>'+ res.username +'</strong> '+ res.body +' <span class="color">'+ res.event +'</span>\n' +
                '                        </span>\n' +
                '                        </a>\n' +
                '                      </li>';

            hiredNotificationsWrapper.html(newHiredNotification + existingHiredNotifications);
            hiredNotificationsCount += 1;
            if(hiredNotificationsCount > 3) {

              $('.header-notifications-scroll').css('height', '270px');
              $('.simplebar-track').css('display', 'block');

              }
            $('.noHiredNotifs').remove();
         //   localStorage.setItem("hiredNotificationContent",hiredNotificationsWrapper.html());
            $('.hiredNotificationsCount').data('count',hiredNotificationsCount);
            $('.hiredNotificationsCount').text(hiredNotificationsCount);
            hiredNotificationsWrapper.show();

            });



        var channelName = '{{ 'schedule-interview'.Auth::id() }}';
        var channel = pusher.subscribe(channelName);

        channel.bind('App\\Events\\ScheduleInterview', function(res) {
                              Snackbar.show({
        text: '{{ __('You have new notification') }}',
        pos: 'bottom-left',
        showAction: true,
        actionText: "{{__('Dismiss')}}",
        duration: 5000,
        textColor: '#fff',
        backgroundColor: '#5f9025'
    });
            // var hiredNotifications =  localStorage.getItem('hiredNotifications');
            // if (hiredNotifications == null) {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(res));
            // }else {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(hiredNotifications)+JSON.stringify(res));
            // }

            var hiredNotificationsWrapper   = $('.hiredNotifications');
            var hiredNotificationsCount     = parseInt($('.hiredNotificationsCount').data('count'));

            if (hiredNotificationsCount == 0) {
                $('.hiredNotificationsCount').text('0');

            }

            var existingHiredNotifications = hiredNotificationsWrapper.html();
            var newHiredNotification = ' <li class="notifications-not-read">\n' +
                '                        <a href="'+ res.url +'">\n' +
                '                          <span class="notification-icon"><i class="icon-material-baseline-notifications-none"></i></span>\n' +
                '                          <span class="notification-text">\n' +
                '                          <strong>'+ res.username +'</strong> {{ __('Has scheduled an interview with you!')}} <span class="color">{{ __('See details') }}</span>\n' +
                '                        </span>\n' +
                '                        </a>\n' +
                '                      </li>';

            hiredNotificationsWrapper.html(newHiredNotification + existingHiredNotifications);
            hiredNotificationsCount += 1;
            $('.noHiredNotifs').remove();
          //  localStorage.setItem("hiredNotificationContent",hiredNotificationsWrapper.html());
            $('.hiredNotificationsCount').data('count',hiredNotificationsCount);
            $('.hiredNotificationsCount').text(hiredNotificationsCount);
            hiredNotificationsWrapper.show();

        });




        var channelName = '{{ 'confirm-date'.Auth::id() }}';
        var channel = pusher.subscribe(channelName);

        channel.bind('App\\Events\\ConfirmDate', function(res) {
                              Snackbar.show({
        text: '{{ __('You have new notification') }}',
        pos: 'bottom-left',
        showAction: true,
        actionText: "{{__('Dismiss')}}",
        duration: 5000,
        textColor: '#fff',
        backgroundColor: '#5f9025'
    });
            // var hiredNotifications =  localStorage.getItem('hiredNotifications');
            // if (hiredNotifications == null) {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(res));
            // }else {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(hiredNotifications)+JSON.stringify(res));
            // }

            var hiredNotificationsWrapper   = $('.hiredNotifications');
            var hiredNotificationsCount     = parseInt($('.hiredNotificationsCount').data('count'));

            if (hiredNotificationsCount == 0) {
                $('.hiredNotificationsCount').text('0');

            }

            var existingHiredNotifications = hiredNotificationsWrapper.html();
            var newHiredNotification = ' <li class="notifications-not-read">\n' +
                '                        <a href="'+ res.url +'">\n' +
                '                          <span class="notification-icon"><i class="icon-material-baseline-notifications-none"></i></span>\n' +
                '                          <span class="notification-text">\n' +
                '                          <strong>'+ res.username +'</strong> {{ __('Has confirmed the interview date!')}} <span class="color">{{ __('See details') }}</span>\n' +
                '                        </span>\n' +
                '                        </a>\n' +
                '                      </li>';

            hiredNotificationsWrapper.html(newHiredNotification + existingHiredNotifications);
            hiredNotificationsCount += 1;
            $('.noHiredNotifs').remove();
          //  localStorage.setItem("hiredNotificationContent",hiredNotificationsWrapper.html());
            $('.hiredNotificationsCount').data('count',hiredNotificationsCount);
            $('.hiredNotificationsCount').text(hiredNotificationsCount);
            hiredNotificationsWrapper.show();

        });


        var channelName = '{{ 'cancel-interview'.Auth::id() }}';
        var channel = pusher.subscribe(channelName);

        channel.bind('App\\Events\\CancelInterview', function(res) {
                              Snackbar.show({
        text: '{{ __('You have new notification') }}',
        pos: 'bottom-left',
        showAction: true,
        actionText: "{{__('Dismiss')}}",
        duration: 5000,
        textColor: '#fff',
        backgroundColor: '#5f9025'
    });
            // var hiredNotifications =  localStorage.getItem('hiredNotifications');
            // if (hiredNotifications == null) {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(res));
            // }else {
            //     localStorage.setItem('hiredNotifications',JSON.stringify(hiredNotifications)+JSON.stringify(res));
            // }

            var hiredNotificationsWrapper   = $('.hiredNotifications');
            var hiredNotificationsCount     = parseInt($('.hiredNotificationsCount').data('count'));

            if (hiredNotificationsCount == 0) {
                $('.hiredNotificationsCount').text('0');

            }

            var existingHiredNotifications = hiredNotificationsWrapper.html();
            var newHiredNotification = ' <li class="notifications-not-read">\n' +
                '                        <a href="'+ res.url +'">\n' +
                '                          <span class="notification-icon"><i class="icon-material-baseline-notifications-none"></i></span>\n' +
                '                          <span class="notification-text">\n' +
                '                          <strong>'+ res.username +'</strong> {{ __('Has canceled his interview with you!')}} <span class="color"></span>\n' +
                '                        </span>\n' +
                '                        </a>\n' +
                '                      </li>';

            hiredNotificationsWrapper.html(newHiredNotification + existingHiredNotifications);
            hiredNotificationsCount += 1;
            $('.noHiredNotifs').remove();
          //  localStorage.setItem("hiredNotificationContent",hiredNotificationsWrapper.html());
            $('.hiredNotificationsCount').data('count',hiredNotificationsCount);
            $('.hiredNotificationsCount').text(hiredNotificationsCount);
            hiredNotificationsWrapper.show();

        });



        $('.clearHiredNotifications').on('click', function () {


         //   localStorage.removeItem('hiredNotificationContent');

            $('.hiredNotifications').html('       <li class="notifications-not-read noHiredNotifs">\n' +
                '                        <a href="#">\n' +
                '                          <span class="notification-text">\n' +
                '                          No new notifications found!\n' +
                '                        </span>\n' +
                '                        </a>\n' +
                '                      </li>');
            $('.hiredNotificationsCount').data('count',0);
            $('.hiredNotificationsCount').text('0');
                 $('.hired-header-notifications-scroll').css('height', 'auto');
             $('.hired-header-notifications-scroll .simplebar-track').css('display', 'none');
             $.get('{{ route('apiReadNotifications') }}').done(function (res) {

          }).fail(function (err) {

          });
        });

        $('.read-messages').on('click', function () {
        msgNotificationsWrapper.html('      <li class="notifications-not-read noMsgs">\n' +
                               '     <a href="{{ route('showMessages')}}">\n' +
                                '        <div class="notification-text">\n' +
                                '            <strong></strong>\n' +
                                '            <p class="notification-msg-text">No new messages found!</p>\n' +
                                 '      </div>\n' +
                                '    </a>\n' +
                              '  </li>');

        $('.messageNotificationsCount').text(0);
                 $('.hired-header-messages-scroll').css('height', 'auto');
                 $('.hired-header-messages-scroll .simplebar-track').css('display', 'none');
             $.get('{{ route('apiRearAllMessages') }}').done(function (res) {

          }).fail(function (err) {

          });
        });


        $('.logout').on('click', function () {
            localStorage.clear();
        });




    </script>

@endsection
