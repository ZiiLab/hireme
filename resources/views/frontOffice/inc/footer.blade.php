<!-- Footer
================================================== -->
<div id="footer">

  <!-- Footer Top Section -->
  <div class="footer-top-section">
    <div class="container">
      <div class="row">
        <div class="col-xl-12">

          <!-- Footer Rows Container -->
          <div class="footer-rows-container">

            <!-- Left Side -->
            <div class="footer-rows-left">
              <div class="footer-row">
                <div class="footer-row-inner footer-logo">
                    <a href="{{ route('showHomePage') }}"> <img src="{{asset(\App\Modules\User\Models\Media::where('type' , 101)->first() ? \App\Modules\User\Models\Media::where('type' , 101)->first()->link : '')}}" alt="Hired.tn" title="Hired.tn"></a>
                </div>
              </div>
            </div>

            <!-- Right Side -->
            <div class="footer-rows-right">

              <!-- Social Icons -->
              <div class="footer-row">
                <div class="footer-row-inner">
                  <ul class="footer-social-links">
                    <li>
                      <a href="{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Facebook'] }}" target="_blank" title="Facebook" data-tippy-placement="bottom" data-tippy-theme="light">
                        <i class="icon-brand-facebook-f"></i>
                      </a>
                    </li>

                    <li>
                      <a href="{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Linkedin'] }}" target="_blank" title="LinkedIn" data-tippy-placement="bottom" data-tippy-theme="light">
                        <i class="icon-brand-linkedin-in"></i>
                      </a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
              </div>

              <!-- Language Switcher -->
                @if( json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Show Translations'] )
              <div class="footer-row">
                <div class="footer-row-inner">
                  <select class="selectpicker language-switcher" data-selected-text-format="count" data-size="5">
                      @if(Session::has('locale'))
                          <option data-lg='en' @if(Session::get('locale') == 'en') selected @endif >English</option>
                          <option data-lg='fr' @if(Session::get('locale') == 'fr') selected @endif>Français</option>
                          @else
                          <option data-lg='en' selected>English</option>
                          <option data-lg='fr'>Français</option>
                      @endif


                  </select>
                </div>
              </div>
              @endif


            </div>

          </div>
          <!-- Footer Rows Container / End -->
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Top Section / End -->

  <!-- Footer Middle Section -->
  <div class="footer-middle-section">
    <div class="container">
      <div class="row">

        <!-- Links -->
        <div class="col-xl-2 col-lg-2 col-md-3">
          <div class="footer-links">
            <h3>{{ __('For Candidates') }}</h3>
            <ul>
              <li><a href="{{ route('showJobs') }}"><span>{{ __('Browse Jobs') }}</span></a></li>
              <li><a href="{{ route('showTasks') }}"><span>{{ __('Browse Tasks') }}</span></a></li>
              <li><a href="{{ route('showBrowseCompanies') }}"><span>{{ __('Browse Companies') }}</span></a></li>
            </ul>
          </div>
        </div>

        <!-- Links -->
        <div class="col-xl-2 col-lg-2 col-md-3">
          <div class="footer-links">
            <h3>For Employers</h3>
            <ul>
              <li><a href="{{ route('showFreelancers') }}"><span>{{ __('Browse Candidates') }}</span></a></li>
              <li><a href="{{ route('showServices') }}"><span>{{ __('Browse Services') }}</span></a></li>
            </ul>
          </div>
        </div>

        <!-- Links -->
        <div class="col-xl-2 col-lg-2 col-md-3">
          <div class="footer-links">
            <h3>Helpful Links</h3>
            <ul>
              <li><a href="{{ route('showContact') }}"><span>{{ __('Contact') }}</span></a></li>
              <li><a href="{{ route('showPrivacyPage') }}"><span>{{ __('Privacy Policy') }}</span></a></li>
              <li><a href="{{ route('showTermsPage') }}"><span>{{ __('Terms of Use') }}</span></a></li>
            </ul>
          </div>
        </div>

        <!-- Links -->
        <div class="col-xl-2 col-lg-2 col-md-3">
          <div class="footer-links">
            <h3>{{ __('Account') }}</h3>
            <ul>
             @if(Auth::check())

             <li><a href="{{ route('showDashboard') }}"><span>{{__('Dashboard')}}</span></a></li>
              <li><a href="{{ route('showSettings') }}"><span>{{__('Settings')}}</span></a></li>
              <li><a href="{{ route('handleLogout') }}"><span>{{__('Log Out')}}</span></a></li>

             @else

              <li><a href="{{ route('showLogin') }}"><span>{{__('Log In')}}</span></a></li>
              <li><a href="{{ route('showRegister') }}"><span>{{__('Sign Up')}}</span></a></li>

              @endif
            </ul>
          </div>
        </div>

        <!-- Newsletter -->
        <div class="col-xl-4 col-lg-4 col-md-12">
          <h3><i class="icon-feather-mail"></i> {{ __('Sign Up For a Newsletter') }}</h3>
          <p>{{__('Weekly breaking news, analysis and cutting edge advices on job searching.')}}</p>
          <form action="javascript:void(0)" method="post" id="newsletter-form" class="newsletter">
            <input type="email" id="subEmail" name="email" placeholder="Enter your email address">
            <button type="submit" id="subBtn" form="newsletter-form"><i class="icon-feather-arrow-right"></i></button>
          </form>
            <small id="subEmail-error"></small>

        </div>

      </div>
    </div>
  </div>
  <!-- Footer Middle Section / End -->

  <!-- Footer Copyrights -->
  <div class="footer-bottom-section">
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
           {!! json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Content']['Copyrights'] !!}
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Copyrights / End -->

</div>
<!-- Footer / End -->

<script>
    $( "#newsletter-form" ).submit(function( event ) {
        event.preventDefault();
        $('#subBtn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('#subBtn').css('opacity','0.5');
        $('#subBtn').css('cursor','not-allowed');
        $("#subBtn").attr("disabled", true);

        if($('#subEmail').val() == '') {
            $('#subEmail-error').text('Please enter a valid email!');
            $('#subBtn').html('<i class="icon-feather-arrow-right"></i>')
            $('#subBtn').css('opacity','1');
            $('#subBtn').css('cursor','pointer');
            $("#subBtn").attr("disabled", false);
        }else {

        var data = {
            'email' : $('#subEmail').val()
        }

        $.post('{{route('apiSubscribeToNewsletter')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                $('#subEmail-error').text('');
                $('#subEmail').val('');
                $('#subBtn').html('<i class="icon-feather-arrow-right"></i>')
                $('#subBtn').css('opacity','1');
                $('#subBtn').css('cursor','pointer');
                $("#subBtn").attr("disabled", false);
                if(res.exist) {
                        Snackbar.show({
                            text: 'Email already subscribed, thank you.',
                            pos: 'bottom-center',
                            showAction: false,
                            actionText: "Dismiss",
                            duration: 3000,
                            textColor: '#fff',
                            backgroundColor: '#8fbc8f'
                        });

                }else {

                        Snackbar.show({
                            text: 'Thank you, you are now subscribed to our awesome newsletters.',
                            pos: 'bottom-center',
                            showAction: false,
                            actionText: "Dismiss",
                            duration: 3000,
                            textColor: '#fff',
                            backgroundColor: '#8fbc8f'
                        });

                }

            })
            .fail(function (res) {
                $('#subEmail-error').text('');
                $('#subEmail').val('');
                $('#subBtn').html('<i class="icon-feather-arrow-right"></i>')
                $('#subBtn').css('opacity','1');
                $('#subBtn').css('cursor','pointer');
                $("#subBtn").attr("disabled", false);
                    Snackbar.show({
                        text: 'Something went wrong, please try again!',
                        pos: 'bottom-center',
                        showAction: false,
                        actionText: "Dismiss",
                        duration: 3000,
                        textColor: '#fff',
                        backgroundColor: '#f00000'
                });
            })
        }
    });

    $('.language-switcher').on('change', function(e) {
      e.preventDefault();
      var lg = $(this).children("option:selected").data('lg');
      window.location.href = '{{ url('/locale').'/' }}'+lg
    })
</script>
