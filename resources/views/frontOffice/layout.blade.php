<!doctype html>
<html lang="en">
<head>
      @include('frontOffice.inc.head')
 
</head>

<body  class="{{ !empty($class) ? $class : '' }}">
<style>
    small {
        color:red;
    }
</style>
	<div id="wrapper">
          @yield('header')
          @yield('content')
       
          @yield('footer')
  </div>

  @include('frontOffice.inc.scripts')

@yield('script')

</body>
</html>
