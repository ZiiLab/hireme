
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('jquery-confirm');
require('jsoneditor');
window.Vue = require('vue');
window.Vue.config.devtools = false;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chat-app', require('./components/ChatApp.vue'));
Vue.component('channels', require('./components/Channels.vue'));
Vue.component('channel', require('./components/Channel.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('search', require('./components/Search.vue'));
Vue.component('message', require('./components/Message.vue'));
Vue.component('message-composer', require('./components/MessageComposer.vue'));

const app = new Vue({
    el: '#app'
});
