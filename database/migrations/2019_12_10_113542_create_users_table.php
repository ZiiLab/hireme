<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title'); // 1 Admin // 2 User
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 30)->nullable();
            $table->string('last_name', 30)->nullable();
            $table->tinyInteger('type')->nullable(); // 0 employer / 1 freelancer
            $table->string('email');
            $table->date('birthday')->nullable();
            $table->string('password', 60)->nullable();
            $table->string('phone')->nullable();
            $table->integer('status'); // 0 mail invalide / 1 mail valid
            $table->string('image')->nullable();
            $table->string('validation')->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('industry');
            $table->string('size')->nullable();
            $table->string('phone');
            $table->string('fax')->nullable();
            $table->string('country');
            $table->string('website')->nullable();
            $table->dateTime('founded_at')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('zip')->nullable();
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            $table->string('country')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tagline');
            $table->text('abt_me');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('competences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('rate');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hour_rate');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });



        Schema::create('socials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->string('link');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('employments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position');
            $table->string('company_name')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->tinyInteger('present')->nullable();  // 0 false / 1 true
            $table->text('description')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_title');
            $table->string('project_description')->nullable();
            $table->string('link')->nullable();
            $table->dateTime('realized_at')->nullable();
            $table->string('type')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('progresses', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('account')->default(0);
            $table->tinyInteger('profile')->default(0);
            $table->tinyInteger('skills')->default(0);
            $table->tinyInteger('files')->default(0);
            $table->tinyInteger('links')->default(0);
            $table->tinyInteger('emps')->default(0);
            $table->tinyInteger('rate')->default(0);
            $table->tinyInteger('intro')->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });








    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('infos');
        Schema::dropIfExists('competences');
        Schema::dropIfExists('skills');
        Schema::dropIfExists('preferences');
        Schema::dropIfExists('socials');
        Schema::dropIfExists('employments');
        Schema::dropIfExists('portfolios');
        Schema::dropIfExists('medias');
        Schema::dropIfExists('companies');
        Schema::dropIfExists('progresses');
    }
}
