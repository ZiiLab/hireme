<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHiredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->float('min');
            $table->float('max');
            $table->tinyInteger('type'); // 0 fixed / 1 hourly
            $table->timestamps();
        });

        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->default(1); // 0 unpublished / 1  published / 2 in progress / 3 completed
            $table->longText('description');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('budget_id')->unsigned();
            $table->foreign('budget_id')->references('id')->on('budgets')->onDelete('cascade');
            $table->integer('address_id')->unsigned()->nullable();
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('task_categories')->onDelete('cascade');
            $table->timestamps();
        });




        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');
            $table->float('rate');
            $table->integer('delivery_time');
            $table->tinyInteger('type'); // 0 days / 1 hours
            $table->integer('bidder_id')->unsigned()->nullable();
            $table->foreign('bidder_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('task_id')->unsigned()->nullable();
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('accepted_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned()->nullable();
            $table->foreign('bid_id')->references('id')->on('bids')->onDelete('cascade');
            $table->integer('task_id')->unsigned()->nullable();
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('type'); // 0 fulltime  /1 parttime / 2 internship / 3 temporary
            $table->tinyInteger('status')->default(1); // 0 unpublished /1 published /2 finished
            $table->longText('description');
            $table->float('min_salary');
            $table->float('max_salary');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('address_id')->unsigned()->nullable();
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('job_categories')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('job_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->integer('job_id')->unsigned()->nullable();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('status')->default(1); // 0 unpublished / 1  published
            $table->longText('description');
            $table->longText('requirements');
            $table->float('min');
            $table->float('max');
            $table->tinyInteger('delay'); //days
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('task_categories')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('task_id')->unsigned()->nullable();
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
            $table->integer('service_id')->unsigned()->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('interviews', function (Blueprint $table) {
            $table->increments('id');
            $table->text('note')->nullable();
            $table->tinyInteger('type');  // 0 physical / 1 online
            $table->tinyInteger('status');  // 0 pending / 1 accepted
            $table->integer('interviewer')->unsigned();
            $table->foreign('interviewer')->references('id')->on('users')->onDelete('cascade');
            $table->integer('interviewed')->unsigned();
            $table->foreign('interviewed')->references('id')->on('users')->onDelete('cascade');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('interview_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->tinyInteger('status');  // 0 pending / 1 accepted
            $table->integer('interview_id')->unsigned()->nullable();
            $table->foreign('interview_id')->references('id')->on('interviews')->onDelete('cascade');
            $table->timestamps();
        });







    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hireds');
    }
}
