<?php

use Illuminate\Database\Seeder;
use App\Modules\General\Models\JobCategory;
use App\Modules\General\Models\TaskCategory;
use App\Modules\User\Models\Media;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()



    {
       $jc1 = JobCategory::create([
            'label' => 'Web / Software Dev'
        ]);

        $jc2 = JobCategory::create([
            'label' => 'Data Science / Analytics'
        ]);
        $jc3 = JobCategory::create([
            'label' => 'Accounting / Consulting'
        ]);
        $jc4 = JobCategory::create([
            'label' => 'Writing & Translations'
        ]);
        $jc5 = JobCategory::create([
            'label' => 'Sales & Marketing'
        ]);
        $jc6 = JobCategory::create([
        'label' => 'Graphics & Design'
         ]);
        $jc7 = JobCategory::create([
        'label' => 'Degital Marketing'
         ]);

        $jc8 = JobCategory::create([
            'label' => 'Education & Training'
        ]);

        $tc1 = TaskCategory::create([
            'label' => 'Admin Support'
        ]);
        $tc2 = TaskCategory::create([
            'label' => 'Customer Service'
        ]);
        $tc3 = TaskCategory::create([
        'label' => 'Data Analytics'
          ]);
        $tc4 = TaskCategory::create([
        'label' => 'Design & Creative'
         ]);
        $tc5 = TaskCategory::create([
        'label' => 'Legal'
            ]);
        $tc6 = TaskCategory::create([
            'label' => 'Software Development'
        ]);
        $tc7 = TaskCategory::create([
            'label' => 'Writing'
        ]);   $tc1 = TaskCategory::create([
        'label' => 'Translation'
        ]);
        $tc8 = TaskCategory::create([
        'label' => 'Sales & Marketing'
    ]);

        Media::create([
            'link' => 'storage/uploads/general/logoBlack.png' ,
            'type' => '100'
        ]);
        Media::create([
           'link' => 'storage/uploads/general/logoWhite.png',
           'type' =>  '101'
        ]);

        Media::create([
           'link' => 'storage/uploads/general/icon.svg',
           'type' =>  '102'
        ]);


    
  }
}
