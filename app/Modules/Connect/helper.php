<?php

use App\Modules\Connect\Models\HiredNotifications;


if (! function_exists('saveNotification')) {


    function saveNotification($senderId, $receiverId, $type, $content,$event,$route)
    {
        
    	HiredNotifications::create([
    	'type' =>$type,
        'event' =>$content,
        'content' =>$event,
        'route' =>$route,
        'sender_id' =>$senderId,
        'receiver_id' =>$receiverId,
        'status' => 0
    	]);
    }
}