<?php

namespace App\Modules\Connect\Models;

use Illuminate\Database\Eloquent\Model;

class HiredNotifications extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hired_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',  // 0 hired / 1 confirm date / 2 schedule interview
        'event',
        'content',
        'route',
        'sender_id',
        'receiver_id',
        'status'

    ];


    public function sender()
    {
        return $this->belongsTo('App\Modules\User\Models\User','sender_id','id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Modules\User\Models\User','receiver_id','id');
    }


}
