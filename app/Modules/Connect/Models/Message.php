<?php

namespace App\Modules\Connect\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'status',
        'sender_id',
        'receiver_id',
        'channel_id'

    ];


    public function channel()
    {
        return $this->belongsTo('App\Modules\Connect\Models\Channel');
    }


}
