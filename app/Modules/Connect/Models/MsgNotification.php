<?php

namespace App\Modules\Connect\Models;

use Illuminate\Database\Eloquent\Model;

class MsgNotification extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'msg_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status',
        'user_id',
        'message_id'

    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\Connect\Models\Channel');
    }

    public function message()
    {
        return $this->belongsTo('App\Modules\Connect\Models\Message');
    }


}
