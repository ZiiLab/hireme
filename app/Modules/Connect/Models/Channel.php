<?php

namespace App\Modules\Connect\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'channels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id',
        'receiver_id'

    ];


    public function messages()
    {
        return $this->hasMany('App\Modules\Connect\Models\Message');
    }


}
