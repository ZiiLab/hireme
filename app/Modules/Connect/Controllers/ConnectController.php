<?php

namespace App\Modules\Connect\Controllers;

use App\Events\NewMessage;
use App\Events\DirectMessage;
use App\Modules\Connect\Models\Message;
use App\Modules\Connect\Models\MsgNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Input;
use App\Modules\Connect\Models\Channel;
use App\Modules\User\Models\User;
use Session;

class ConnectController extends Controller
{

    /**
     * @desc get user chat channels
     *
     * @route /channels
     */
    public function getUserChannels()
    {
        return response()->json(Auth::user()->channels());
    }

    public function getChannelMessages(Request $request) {

        $channel = Channel::find($request->get('channelId'));
        $offset = $request->get('offset');
        $messages = [];

        foreach ($channel->messages()->orderBy('created_at','desc')->skip($offset * 10)->take(10)->get() as $message) {
            $messages[] = [
                'message' => [
                        'id' => $message->id,
                        'body' => $message->body ,
                        'sent_at' => $message->created_at->format('Y/m/d H:i:s') ,
                        'status' => $message->status
                            ],
                'sender' => [
                        'id' => $message->sender_id,
                        'avatar' => User::where('id',$message->sender_id)->first()->medias()->where('type',0)->first() ? User::where('id',$message->sender_id)->first()->medias()->where('type',0)->first()->link :'frontOffice/images/user-avatar-placeholder.png' ,
                        'fullName' => User::where('id',$message->sender_id)->first()->getFullName()
                            ],
                'receiver' => [
                       'id' => $message->receiver_id,
                       'avatar' => User::where('id',$message->receiver_id)->first()->medias()->where('type',0)->first() ? User::where('id',$message->receiver_id)->first()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png',
                      'fullName' => User::where('id',$message->receiver_id)->first()->getFullName()
                ]
            ];
        }

        return response()->json($messages);
    }

    public function handlePushMessage(Request $request) {
        $message = Message::create([
            'body' => $request->body,
            'status' => 0,
            'sender_id' => Auth::id(),
            'receiver_id' => $request->receiverId,
            'channel_id' => $request->channelId
        ]);

        $messageToReturn = [
            'message' => [
                'id' => $message->id,
                'body' => $message->body ,
                'sent_at' => $message->created_at->format('Y/m/d H:i:s') ,
                'status' => $message->status
            ],
            'sender' => [
                'id' => $message->sender_id,
                'avatar' => User::where('id',$message->sender_id)->first()->medias()->where('type',0)->first() ? User::where('id',$message->sender_id)->first()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png',
                'fullName' => User::where('id',$message->sender_id)->first()->getFullName()
            ],
            'receiver' => [
                'id' => $message->receiver_id,
                'avatar' => User::where('id',$message->receiver_id)->first()->medias()->where('type',0)->first() ? User::where('id',$message->receiver_id)->first()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png',
                'fullName' => User::where('id',$message->receiver_id)->first()->getFullName()
            ]
        ];

        event(new NewMessage($message));

        return response()->json($messageToReturn);
    }

          public function handleUpdateMessageStatus(Request $request) {
              $message= Message::find($request->get('id'));
              $message->status = 1;
              $message->save();

              return response()->json(['status' => 200]);
          }

          public function handleUpdateChannelMessagesStatus(Request $request) {
            $channel= Channel::find($request->get('id'));
            foreach($channel->messages as $msg) {
              $msg->status = 1;
              $msg->save();
            }
            return response()->json(['status' => 200]);

          }

          public function handleSearchChannels(Request $request) {
              $query = $request->get('query');
              $channels =  Channel::where("sender_id",Auth::id())->orWhere('receiver_id',Auth::id())->orderBy('created_at','desc')->get();
              $channelsToReturn = [];
              if($query == '') { $query = ' '; }
              foreach ($channels as $channel) {
                        $user1= User::where('id',$channel->sender_id)->first();
                        $user2 = User::where('id',$channel->receiver_id)->first();
                        $fullName1 = stripos($user1->getFullName(), $query);
                        $email1 = stripos($user1->email, $query);
                        $fullName2 = stripos($user2->getFullName(), $query);
                        $email2 = stripos($user2->email, $query);

                        if($fullName1 !== false || $email1 !== false || $fullName2 !== false || $email2 !== false) {
                          $message = $channel->messages()->latest()->first();
                          $sender = User::where('id', $message->sender_id)->first();
                          $senderToReturn = ['id' => $sender->id,'fullName' => $sender->getFullName(),'avatar' => $sender->medias()->where('type',0)->first() ? $sender->medias()->where('type',0)->first()->link:'frontOffice/images/user-avatar-placeholder.png' ];
                          $receiver = User::where('id', $message->receiver_id)->first();
                          $receiverToReturn = ['id' => $receiver->id,'fullName' => $receiver->getFullName(),'avatar' => $receiver->medias()->where('type',0)->first() ? $receiver->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png' ];
                          $channelsToReturn[] = ['id' => $channel->id,'sender' => $senderToReturn ,'receiver' => $receiverToReturn, 'message' => $message,'unread' => count($channel->messages()->where('status', 0)->get())];

                        }
                                  }

              return response()->json($channelsToReturn);

          }


              public function apiSendDirectMessage(Request $request)
              {
                    $body = $request->data['body'];
                    $receiverId = $request->data['receiverId'];
                    $channel = Channel::where('sender_id',Auth::id())->where('receiver_id', $receiverId)->first();
                    $channel2 = Channel::where('sender_id',$receiverId)->where('receiver_id', Auth::id() )->first();
                    if($channel || $channel2) {
                        if($channel) {
                             $message =  Message::create([
                          'body' => $body,
                          'status' => 0,
                          'sender_id' => Auth::id(),
                          'receiver_id' => $receiverId,
                          'channel_id' => $channel->id
                        ]);
                            event(new DirectMessage(Auth::user(),$message,$receiverId));

                                $receiver = User::find($receiverId);
                                $receiver->msgNotifications()->create([
                                   'message_id' => $message->id
                                 ]);
                            return response()->json(['status' => 200]);
                        }else {
                             $message =  Message::create([
                          'body' => $body,
                          'status' => 0,
                          'sender_id' => Auth::id(),
                          'receiver_id' => $receiverId,
                          'channel_id' => $channel2->id
                        ]);
                            event(new DirectMessage(Auth::user(),$message,$receiverId));
                                $receiver = User::find($receiverId);
                                $receiver->msgNotifications()->create([
                                   'message_id' => $message->id
                                 ]);
                            return response()->json(['status' => 200]);
                        }


                    }else {
                        $channel = Channel::create([
                          'sender_id' => Auth::id(),
                          'receiver_id' => $receiverId
                        ]);
                        $message =  Message::create([
                          'body' => $body,
                          'status' => 0,
                          'sender_id' => Auth::id(),
                          'receiver_id' => $receiverId,
                          'channel_id' => $channel->id
                        ]);
                        event(new DirectMessage(Auth::user(),$message,$receiverId));                            
                        $user = User::where('id',$receiverId)->first();
                            $user->msgNotifications()->create([
                                   'message_id' => $message->id
                                 ]);
                        sendMail('Hired::mails.message',
                            $user->email,
                            ['user' => $user] ,
                            'New Message');
                        return response()->json(['status' => 200]);
                    }
              }

              public function apiSaveMsgNotification(Request $request) {

                   Auth::user()->msgNotifications()->create([
                       'message_id' => $request->data['msgId']
                   ]);

                   return response()->json(['status' => 200]);

              }

              public function handleDeleteConversation() {
                  $data   =  Input::all();
                  $convId =  $data['convId'];

        $convo = Channel::find($convId);
        if(!$convo) {
          Session::flash('message', 'Some things went wrong !');
          Session::flash('color', '#de5959');
          return back();
        }

          foreach ($convo->messages as $msg) {
            $msg->delete();
          }
          Session::flash('message', 'Conversation Deleted !');
          Session::flash('color', '#5f9025');

          return back();
              }


              public function apiReadNotifications() {
                     Auth::user()->notifications()->update(['status' => 1]);
                     return response()->json(['status' => 200]);
              }

               public function apiRearAllMessages() {
                    $messages = Message::where('receiver_id',Auth::id())->get();
                    foreach ($messages as $msg) {
                     $msg->update(['status' => 1]);
                    }
                    Auth::user()->msgNotifications()->update(['status' => 1]);
                     return response()->json(['status' => 200]);
              }


}
