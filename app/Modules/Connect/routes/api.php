<?php

Route::group(['module' => 'Connect', 'middleware' => ['api'], 'namespace' => 'App\Modules\Connect\Controllers'], function() {

    Route::resource('Connect', 'ConnectController');

});
