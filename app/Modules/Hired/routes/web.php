<?php

Route::group(['module' => 'Hired', 'middleware' => ['web'], 'namespace' => 'App\Modules\Hired\Controllers'], function() {

    Route::get('/job/{jobTitle}', 'HiredController@showJobDetails')->name('showJobDetails');
    Route::get('/task/{taskTitle}', 'HiredController@showTaskDetails')->name('showTaskDetails');
    Route::get('/service/{serviceName}', 'HiredController@showServiceDetails')->name('showServiceDetails');
    Route::get('/api/get-project-medias', 'HiredController@apiGetProjectMedias')->name('apiGetProjectMedias');
    Route::get('/sendMassMailsFr/{n?}', 'HiredController@sendMassMailsFr')->name('sendMassMailsFr');
    Route::get('/sendMassMailsEn/{n?}', 'HiredController@sendMassMailsEn')->name('sendMassMailsEn');
    Route::get('/finishProfileMassMails/{n}', 'HiredController@finishProfileMassMails')->name('finishProfileMassMails');


    Route::group(['module' => 'Hired', 'middleware' => ['user']], function() {

        Route::post('/tasks/add', 'HiredController@handleAddTask')->name('handleAddTask');
        Route::get('/tasks/update/{id}', 'HiredController@showUpdateTask')->name('showUpdateTask');
        Route::post('/tasks/update/{id}', 'HiredController@handleUpdateTask')->name('handleUpdateTask');
        Route::post('/task-delete', 'HiredController@HandleDeleteTask')->name('HandleDeleteTask');

        Route::post('/bid-delete', 'HiredController@HandleDeleteBid')->name('HandleDeleteBid');
        Route::post('/updateBid', 'HiredController@handleUpdateBid')->name('handleUpdateBid');
        Route::get('/deleteBidder', 'HiredController@handleDeleteBidder')->name('handleDeleteBidder');
        Route::post('/acceptBid', 'HiredController@handleAcceptBidderOffer')->name('handleAcceptBidderOffer');
        Route::post('/api/bid', 'HiredController@apiHandleBidOnTask')->name('apiHandleBidOnTask');
        Route::post('/book-service','HiredController@handleBookService')->name('handleBookService');
        Route::post('/book-delete','HiredController@handleDeleteBooking')->name('handleDeleteBooking');
        Route::get('/submit-review','HiredController@handleSubmitReview')->name('handleSubmitReview');


        Route::group(['module' => 'Hired', 'middleware' => ['employer']], function() {
            Route::post('/job/add', 'HiredController@handlePostJob')->name('handlePostJob');
            Route::post('/deleteJob', 'HiredController@handleDeleteJob')->name('handleDeleteJob');
            Route::get('/jobs/update/{id}', 'HiredController@showUpdateJob')->name('showUpdateJob');
            Route::Post('/jobs/update/{id}', 'HiredController@handleUpdateJob')->name('handleUpdateJob');
            Route::post('/deleteApplication', 'HiredController@handleDeleteApplication')->name('handleDeleteApplication');
            Route::post('/api/scheduleInterview', 'HiredController@handleScheduleInterview')->name('handleScheduleInterview');

        });

        Route::group(['module' => 'Hired', 'middleware' => ['freelancer']], function() {
            Route::post('/apply', 'HiredController@handleApplyForJob')->name('handleApplyForJob');
            Route::get('/manage-services','HiredController@showManageServices')->name('showManageServices');
            Route::get('/post-service','HiredController@showPostService')->name('showPostService');
            Route::post('/post-service','HiredController@handlePostService')->name('handlePostService');
            Route::get('/update-service/{serviceId}','HiredController@showUpdateService')->name('showUpdateService');
            Route::post('/update-servic/{serviceId}','HiredController@handleUpdateService')->name('handleUpdateService');
            Route::post('/delete-service','HiredController@handleDeleteService')->name('handleDeleteService');
            Route::get('/manage-bookings/{serviceId?}','HiredController@showManageBookings')->name('showManageBookings');
            Route::get('/portfolio', 'HiredController@showPortfolio')->name('showPortfolio');
            Route::post('/add-project', 'HiredController@handleAddProject')->name('handleAddProject');
            Route::post('/delete-project', 'HiredController@handleDeleteProject')->name('handleDeleteProject');
            Route::post('/upload-images', 'HiredController@handleUploadMiltupleImages')->name('handleUploadMiltupleImages');

        });

    });
});
