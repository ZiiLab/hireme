<?php

namespace App\Modules\Hired\Services;


use App\Modules\General\Models\EmailAlert;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SearchAlerts {

    public static function handleSendDailySearchAlerts() {
        $alerts = EmailAlert::where('created_at','>=',Carbon::now()->subDays(7))->get();

        foreach ($alerts as $alert) {
            $data = searchAlertsMatching($alert->type,$alert->query);
            if($data != false) {
                $ids = '';
                foreach ($data as $row) {
                    $ids .= '_'.$row->id;
                }

                $subject = '';
                $message = '';
                $btnMessage = '';
                switch ($alert->type) {
                    case 'job' :
                        $message = 'New and fresh jobs that matches your preferences, browse the new jobs by clicking the button below';
                        $btnMessage = 'Browse New Jobs';
                        $subject = 'New Jobs Matching Your Recent Searches';
                        break;
                    case 'task' :
                        $message = 'New and fresh tasks that matches your preferences, browse the new tasks by clicking the button below';
                        $btnMessage = 'Browse New Tasks';
                        $subject = 'New Jobs Matching Your Recent Searches';
                        break;
                    case 'service' :
                        $subject = 'New Services Matching Your Recent Searches';
                        $message = 'New and fresh services that matches your preferences, browse the new services by clicking the button below';
                        $btnMessage = 'Browse New Services';
                        break;
                    case 'freelancer' :
                        $subject = 'New Freelancer profiles Matching Your Recent Searches';
                        $message = 'New and fresh profiles that matches your preferences, browse the new freelancers by clicking the button below';
                        $btnMessage = 'Browse New Freelancers Profiles';
                        break;
                }
                sendMail('Hired::mails.alert',
                    $alert->user->email,
                    [
                        'user' => $alert->user,
                        'type' => $alert->type,
                        'data' => $ids,
                        'message' => $message,
                        'btnMessage' => $btnMessage
                    ],
                    $subject);
            }
        }
    }
}
