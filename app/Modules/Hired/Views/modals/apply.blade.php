<!-- Make an Offer Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">{{__('Job Application')}}</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{__('Apply for')}} '{{ $job->title }}'</h3>
                </div>

                <!-- Form -->
                <form action="javascript:void(0)"  id="application-form" method="post" enctype="multipart/form-data">

                    <textarea name="textarea" id="noteBody" rows="6" cols="10" placeholder="{{__('Motivation letter or notes...')}} ({{__('Optional')}})" class="with-border"></textarea>
                    <div class="switchToUpload">
                    @if(count(Auth::user()->medias()->where('type',5)->get()) == 0)
                    <div class="uploadButton margin-top-25">
                        <input class="uploadButton-input" type="file" accept="application/pdf" id="upload" />
                        <label class="uploadButton-button ripple-effect attach" for="upload">{{__('Attach CV')}}</label>
                        <span class="uploadButton-file-name">{{__('Allowed file type: pdf')}} <br> {{__('Max. file size: 10 MB.')}}</span>
                    </div>
                    <small id="fileErrors"></small>
                     @else
                        <div class="hasFiles">

                            <div class="section-headline margin-top-25 margin-bottom-12">
                                <h5>{{__('Attach cv from uploaded files')}}</h5>
                            </div>

                            <select class="selectpicker" id="oldCv" title="Select file">
                                @foreach(Auth::user()->medias()->where('type',5)->get() as $file)
                                <option value="{{ $file->id }}">{{ $file->label }}</option>
                                    @endforeach
                            </select>
                        </div>

                    @endif
                    </div>
                    @if(count(Auth::user()->medias()->where('type',5)->get()) > 0)
                        <div style="margin-top: 15px" >
                            <div class="switches-list">
                                <div class="switch-container">
                                    <label class="switch"><input  id="switchFileUpload" type="checkbox" ><span class="switch-button"></span>{{__('Upload new cv instead?')}}</label>
                                </div>
                            </div>
                        </div>
                        @endif
                </form>

                <!-- Button -->
                <button id="submit-application-btn" class="button margin-top-35 full-width button-sliding-icon ripple-effect" form="application-form" type="submit">{{__('Apply')}}<i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>
            <!-- Login -->

        </div>
    </div>
</div>
<script>
    // $(document).on('click','.attach', function () {
    //          $('.popup-tab-content').css('padding','50px 0px');

    // });

    $('#switchFileUpload').on('change', function () {
            if($(this).is(':checked')) {
                $('.hasFiles').hide();
                $('#oldCv').val('');
                $('.switchToUpload').append('  <div class="uploadButton margin-top-25">\n' +
                    '                        <input class="uploadButton-input" type="file" accept="application/pdf" id="upload" />\n' +
                    '                        <label class="uploadButton-button ripple-effect attach" for="upload">Attach CV</label>\n' +
                    '                        <span class="uploadButton-file-name">Allowed file type: pdf <br> Max. file size: 10 MB.</span>\n' +
                    '                    </div>');
            } else {
                $('.hasFiles').show();
                $('#oldCv').val('').change();
                files = null;
                $('.uploadButton').remove();

            }


    });
      var files = null;
    $(document).on('change','#upload', function () {
         files = $(this)[0].files;
        if((files[0].size / 1048576).toFixed(2) > 10 ) {
            $('#fileErrors').text('Maximum file size is 10 MB').fadeIn('slow').delay(2000).fadeOut();
        }else {
          $('.uploadButton-file-name').html(files[0].name);
        }
      });

      $('#application-form').submit( function (event) {
          event.preventDefault();
          $('#submit-application-btn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
          $('#submit-application-btn').css('opacity','0.5');
          $('#submit-application-btn').css('cursor','not-allowed');
          $("#submit-application-btn").attr("disabled", true);

          var formData = new FormData();

          formData.append("jobId", '{{ $job->id }}');

          if(files != null) {
            formData.append("cv", files[0]);
            formData.append("upload_file", true);
          }
          if($('#oldCv').length != 0 ) {
          if($('#oldCv').val() != "") {
              formData.append("oldCv", $('#oldCv').val());
          }}

          if($('#noteBody').val() != '') {
              formData.append("note", $('#noteBody').val());
          }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: '{{ route('handleApplyForJob') }}',

                    success: function (data) {
                             if(data.status == 404) {
                              $('.apply-now-button').hide();
                              $('.mfp-close').click();
                        Snackbar.show({
                            text: '{{ __("This job is not available for now!") }}',
                            pos: 'bottom-center',
                            showAction: false,
                            actionText: "Dismiss",
                            duration: 3000,
                            textColor: '#fff',
                            backgroundColor: '#f00000'
                        });
                    }else {

                      $('.apply-now-button').hide();
                      $('.appliedNotif').show();
                      Snackbar.show({
                          text: 'Application submitted successfuly',
                          pos: 'bottom-center',
                          showAction: false,
                          actionText: "Dismiss",
                          duration: 3000,
                          textColor: '#fff',
                          backgroundColor: '#5f9025'
                      });
                        $('.mfp-close').click()
                    }
                    },
                    error: function (error) {
                      $('#submit-application-btn').html('Apply<i class="icon-material-outline-arrow-right-alt"></i>')
                      $('#submit-application-btn').css('opacity','1');
                      $('#submit-application-btn').css('cursor','pointer');
                      $("#submit-application-btn").attr("disabled", false);
                      Snackbar.show({
                          text: 'Something went wrong, please try again!',
                          pos: 'bottom-center',
                          showAction: false,
                          actionText: "Dismiss",
                          duration: 3000,
                          textColor: '#fff',
                          backgroundColor: '#f00000'
                      });
                        $('.mfp-close').click()
                    },
                    async: true,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    timeout: 60000
                });
      });



</script>
<!-- Make an Offer Popup / End -->
