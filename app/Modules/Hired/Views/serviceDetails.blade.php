@extends('frontOffice.layout', ['title' => 'Service | '.$service->name])
@section('meta')
    <meta property="og:url" content="{{ route('showServiceDetails', $service->getServiceParamName())}}">
    <meta property="og:description" content="{{$service->name}}">
    <meta property="og:site_name" content="Hired.tn">
     <meta property="twitter:creator" content="{{$service->user->getFullName()}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$service->name}}">
    <meta property="twitter:description" content="{{$service->description}}">
  
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">
    <meta name="description" content="{{ $service->description }}">
    <meta name="keywords" content="{{ $service->description.','.$service->name }}">

@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <div class="single-page-header" data-background-image="{{ asset('frontOffice/images/single-job.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-details">
                                <h3>{{ $service->name }}</h3>
                                <h5>{{ $service->user->info ? $service->user->info->tagline : '' }}</h5>
                                <ul>
                                    <li><a href="{{ route('showFreelancerDetails', $service->user->getFreelancerUsername()) }}"><i class="icon-feather-user"></i>{{ $service->user->getFullName() }}</a></li>
                                    @if(count($service->user->getAllRatings($service->user->id,'desc','App\Modules\User\Models\User')) < 3)
                                        <li><span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span></li>
                                    @else
                                        <li><div class="star-rating" data-rating="{{ $service->user->averageRating(2) }}"></div></li>
                                    @endif
                                    @if($service->user->address)
                                        <li> <img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($service->user->address->country).'.svg') }}" alt="{{ $service->user->address->country }}"> </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="right-side">
                            <div class="salary-box">
                                <div class="salary-type">{{__('Service Cost')}}</div>
                                <div class="salary-amount" style="font-size : 25px!important">{{ $service->min.' TND - '.$service->max.' TND' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{__('Service Requirements')}}</h3>

                    <p>{{ $service->requirements }}</p>
                </div>

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{__('Service Description')}}</h3>

                    <p>{{ $service->description }}</p>
                </div>


                @if(count($service->medias()->where('type',7)->get()) != 0)
                    <div class="sidebar-widget">
                        <h3>Samples</h3>
                        <div class="attachments-container">
                            @foreach($service->medias()->where('type',7)->get() as $file)
                                <a href="{{ asset($file->link) }}" class="attachment-box ripple-effect" download><span>{{$file->label}}</span></a>
                            @endforeach
                        </div>
                    </div>
                @endif
                          @if(count($service->skills) != 0)
                    <div class="single-page-section">
                        <h3>Skills Included</h3>
                        <div class="task-tags">
                            @foreach($service->skills as $skill)
                                <span>{{$skill->label}}</span>
                            @endforeach
                        </div>
                    </div>
                    @endif
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">{{__('Similar Services')}}</h3>

                    <!-- Listings Container -->
                    <div class="listings-container grid-layout">

                        @foreach(getSimilarServices($service) as $sService)
                        <!-- Job Listing -->
                        <a href="{{ route('showServiceDetails', $sService->getServiceParamName()) }}" class="job-listing">

                            <!-- Job Listing Details -->
                            <div class="job-listing-details">
                                <!-- Logo -->
                                <div class="job-listing-company-logo">
                                    <img src="{{ asset($sService->user->medias()->where('type',0)->first()->link) }}" alt="{{ $sService->user->getFullName() }}">
                                </div>

                                <!-- Details -->
                                <div class="job-listing-description">
                                    <h4 class="job-listing-company">{{ $sService->user->getFullName() }}</h4>
                                    <h3 class="job-listing-title">{{ $sService->name  }}</h3>
                                </div>
                            </div>

                            <!-- Job Listing Footer -->
                            <div class="job-listing-footer">
                                <ul>
                                    <li><i class="icon-material-outline-access-alarm"></i> {{__('Delivered In')}} {{$sService->delay.' Days'}}</li>
                                    <li><i class="icon-material-outline-business-center"></i> {{ \App\Modules\General\Models\TaskCategory::where('id',$sService->category_id)->first()->label }}</li>
                                    <li><i class="icon-material-outline-local-atm"></i> {{ $sService->min.' TND - '.$sService->max.' TND' }}</li>
                                    <li><i class="icon-material-outline-access-time"></i> {{ \Carbon\Carbon::parse($sService->created_at)->diffForHumans() }}</li>
                                </ul>
                            </div>
                        </a>
                            @endforeach
                    </div>
                    <!-- Listings Container / End -->

                </div>
            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">

                    @if(Auth::check())
                        @if(Auth::user()->id == $service->user->id)
                          @elseif(booked($service))
                            <div class="countdown green margin-bottom-35 bookedNotif">{{__('Booked')}} {{booked($service)}}</div>
                            @else

                               @if(eligibleCandidate(Auth::user())) 
                            <div style="display : none" class="countdown green margin-bottom-35 bookedNotif">{{__('Service booked successfully')}}</div>
                            <a  href="#small-dialog" class="apply-now-button popup-with-zoom-anim book-now-button">{{__('Book Now')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
                            @else
                            <a  href="#" id="notEligible" class="apply-now-button popup-with-zoom-anim book-now-button">{{__('Book Now')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
                             <div style="display: none" class="countdown yellow margin-bottom-35 notEligible">{{__('You must update your profile information in order to be able to book services.')}} <br> <a href="{{ route('showProfile') }}">{{ __('Update Profile Now') }}</a> </div>
                            @endif

                        @endif

                    @else
                        <a href="#sign-in-dialog" class="apply-now-button popup-with-zoom-anim book-now-button">{{__('Book Now')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
                @endif

                <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <div class="job-overview">
                            <div class="job-overview-headline">{{__('Service Summary')}}</div>
                            <div class="job-overview-inner">
                                <ul>
                                    <li>
                                        <i class="icon-material-outline-access-alarm"></i>
                                        <span>{{__('Delivered In')}}</span>
                                        <h5>{{$service->delay.' Days'}}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-business-center"></i>
                                        <span>{{__('Service Category')}}</span>
                                        <h5>{{ \App\Modules\General\Models\TaskCategory::where('id',$service->category_id)->first()->label }}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-local-atm"></i>
                                        <span>{{__('Cost')}}</span>
                                        <h5>{{ $service->min.' TND - '.$service->max.' TND' }}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-access-time"></i>
                                        <span>{{__('Date Posted')}}</span>
                                        <h5>{{ \Carbon\Carbon::parse($service->created_at)->diffForHumans() }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <h3>{{__('Bookmark or Share')}}</h3>

                        <!-- Bookmark Button -->
                        @if(Auth::check() && Auth::id() != $service->user->id)
                            <button class="bookmark-button margin-bottom-25  {{ Auth::user()->hasBookmarked($service) ? 'bookmarked' : '' }}">
                                <span class="bookmark-icon"></span>
                                <span class="bookmark-text">{{__('Bookmark')}}</span>
                                <span class="bookmarked-text">{{__('Bookmarked')}}</span>
                            </button>
                    @endif
                    <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>

                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>{{__('Interesting?')}} <strong>{{__('Share It!')}}</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"
                                           data-sharer="facebook" data-title="{{$service->name}}" data-url="{{route('showServiceDetails', $service->getServiceParamName())}}"
                                        ><i class="icon-brand-facebook-f"></i></a></li>

                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"
                                           data-sharer="twitter"  data-title="{{$service->name}}" data-url="{{route('showServiceDetails', $service->getServiceParamName())}}"
                                        ><i class="icon-brand-twitter"></i></a></li>

                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"
                                           data-sharer="linkedin"  data-url="{{route('showServiceDetails', $service->getServiceParamName())}}"
                                        ><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>
    @include('User::frontOffice.auth.modals.login')
    @if(Auth::check())
        @include('Hired::modals.bookService')
    @endif
    <script>
        $('#authCheckPopup').on('click' , function () {
            $.confirm({
                icon: 'fa fa-warning',
                title: 'Authentication is required!',
                type: 'orange',
                columnClass: 'col-xl-6 col-xl-offset-4',
                content: '       <h3 style="margin-top : 20px; margin-bottom: 10px"> You must be logged in to apply for a job.</h3>\n' +
                    '                            <a href="{{ route('showLogin') }}"> Already have an account? </a>\n' +
                    '                            <a href="{{ route('showRegister') }}"> Register now! </a>',
                buttons: {
                    cancel: function () {
                    }
                }
            });
        })

        $('.bookmark-button').on('click', function () {
            $.get('{{ route('handleBookmark') }}?type=service&id='+{{ $service->id }}).done(function(res) {
                if(res.bookmarked) {
                    $('.bookmark-button').addClass('bookmarked');
                }else {
                    $('.bookmark-button').removeClass('bookmarked');
                }
            }).fail(function(error) {

            });
        });

            $('#notEligible').on('click' , function (e) {
            e.preventDefault();
                $('.notEligible').show();
        });
    </script>

@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
