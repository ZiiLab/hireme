<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class AcceptedBid extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accepted_bids';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bid_id',
        'task_id'
    ];

    public function task()
    {
        return $this->hasOne('App\Modules\Hired\Models\Task');
    }

    public function bid()
    {
        return $this->hasOne('App\Modules\Hired\Models\Bid','id','bid_id');
    }

}
