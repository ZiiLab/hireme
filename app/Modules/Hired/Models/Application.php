<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'media_id',
        'user_id',
        'job_id'
    ];

    public function candidate()
    {
        return $this->belongsTo('App\Modules\User\Models\User','user_id','id');
    }

    public function job()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Job');
    }

    public function media() {

        return $this->hasOne('App\Modules\User\Models\Media','id','media_id');
    }


}
