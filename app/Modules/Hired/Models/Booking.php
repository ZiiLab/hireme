<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'media_id',
        'user_id',
        'service_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Modules\User\Models\User','user_id','id');
    }

    public function service()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Service');
    }

    public function media() {

        return $this->hasOne('App\Modules\User\Models\Media','id','media_id');
    }


}
