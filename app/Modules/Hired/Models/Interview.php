<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\User\Models\User;


class Interview extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'interviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'note',
        'type',
        'status',
        'interviewer',
        'interviewed',
        'job_id'
    ];

    public function dates()
    {
        return $this->hasMany('App\Modules\Hired\Models\InterviewDate');
    }

    public function interviewerUser()
    {
        return User::where('id',$this->interviewer)->first();
    }

    public function interviewedUser()
    {
        return User::where('id',$this->interviewed)->first();
    }

     public function job()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Job','job_id','id');
    }



}
