<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class JobTag extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_tags';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'job_id'
    ];

    public function job()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Job');
    }

}
