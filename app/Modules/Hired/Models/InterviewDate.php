<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class InterviewDate extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'interview_dates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'status',
        'interview_id',
    ];

    protected $dates = [
        'date'
    ];

    public function interview()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Interview');
    }


}
