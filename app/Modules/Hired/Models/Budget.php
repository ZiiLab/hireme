<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'budgets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'min',
        'max',
        'type'
    ];

    public function task()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Task');
    }

}
