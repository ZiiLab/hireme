<?php

namespace App\Modules\Hired\Models;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bids';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rate',
        'delivery_time',
        'type',
        'bidder_id',
        'task_id'
    ];

    public function task()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Task');
    }

    public function bidder()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function accepted() {
        return $this->hasOne('App\Modules\Hired\Models\AcceptedBid');

    }

}
