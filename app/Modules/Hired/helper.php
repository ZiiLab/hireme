<?php
use App\Modules\Hired\Models\Job;
use App\Modules\Hired\Models\Service;
use App\Modules\Hired\Models\Task;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 *	Hired Helper
 */

if (! function_exists('eligibleCandidate')) {


     function eligibleCandidate($user)
     {
        if ($user->type == 0) {
          return true;
        }
        else if($user->type == 1 && $user->status == 2) {
          return true;
        }else 
        return false;
     }

 }



 if (! function_exists('applied')) {


     function applied($job)
     {
       if($job->applications()->where('user_id',Auth::id())->first()) {
        $date = $job->applications()->where('user_id',Auth::id())->first()->created_at;
        return Carbon::parse($date)->diffForHumans();
       }else
        return false;

     }

 }

if (! function_exists('booked')) {


    function booked($service)
    {
        if($service->bookings()->where('user_id',Auth::id())->first()) {
            $date = $service->bookings()->where('user_id',Auth::id())->first()->created_at;
            return Carbon::parse($date)->diffForHumans();
        }else
            return false;

    }

}

if (! function_exists('getSimilarJobs')) {


    function getSimilarJobs($job)
    {
        $jobs = Job::where('category_id', $job->category_id)->where('type', $job->type)->get();

        $filtredJobs = $jobs->filter(function ($item) use ($job) {
            if($item->id != $job->id)
                return $item;
        })->values();


        if(count($filtredJobs) != 0) {
            return $filtredJobs;
        }else {
            $jobs =  Job::where('category_id', $job->category_id)->get();
            $filtredJobs = $jobs->filter(function ($item) use ($job) {
                if($item->id != $job->id)
                    return $item;
            })->values();
            return $filtredJobs;

        }

    }

}

if (! function_exists('getSimilarServices')) {


    function getSimilarServices($service)
    {
        $services = Service::where('category_id', $service->category_id)->get();

        $filtredServices = $services->filter(function ($item) use ($service) {
            if($item->id != $service->id)
                return $item;
        })->values();


        if(count($filtredServices) != 0) {
            return $filtredServices;
        }else {

            return Service::where('id','!=', $service->id)->take(2)->get();
        }

    }

}

if (! function_exists('searchAlertsMatching')) {


    function searchAlertsMatching($type,$query)
    {
        $request = json_decode($query);
        switch ($type) {
            case 'job' :
                $salary = explode(',', $request->salaryRange);
                $jobs  = Job::where('created_at', '>=', Carbon::now()->subDay())->where(function ($q) use ($salary) {
                    $q->whereBetween('min_salary',$salary)->orWhereBetween('max_salary',$salary);
                });


                if (isset($request->lat)) {
                    $sqlDistance = DB::raw
                    ('
              ( 6371 * acos
                  ( cos
                      ( radians
                          (' . $request->lat . ')
                      )
                  * cos
                      ( radians
                          ( lat )
                      )
                  * cos
                      ( radians
                          ( lon )
                      - radians
                          (' . $request->lon . ')
                      )
                  + sin
                      ( radians
                          (' . $request->lat  . ')
                      )
                  * sin
                      ( radians
                          ( lat )
                      )
                  )
              )
            ');
                    $jobs->when($sqlDistance != null, function ($query) use ($sqlDistance,$request){
                        $query->whereHas('address', function ($subQuery) use ($sqlDistance,$request) {
                            $subQuery->addSelect(DB::raw("{$sqlDistance} AS distance"));
                            $subQuery->havingRaw("distance <= ?", [(int)$request->range]);
                        });
                    })
                        ->with('company')
                        ->with('address');


                }

                if(isset($request->key)) {
                    $jobs->where('title', 'like', '%' . $request->key . '%');
                }

                if(isset($request->cat)) {
                    $jobs->whereIn('category_id', $request->cat);
                }

                if (isset($request->type)) {
                    $jobs->whereIn('type', $request->type);
                }

                if(count($jobs->get()) != 0) {
                    return $jobs->get();
                }else return false;

            case 'task' :

                $tasks  = Task::where('created_at', '>=', Carbon::now()->subDay())->where('status',1);

                if (isset($request->lat)) {
                    $sqlDistance = DB::raw
                    ('
              ( 6371 * acos
                  ( cos
                      ( radians
                          (' . $request->lat . ')
                      )
                  * cos
                      ( radians
                          ( lat )
                      )
                  * cos
                      ( radians
                          ( lon )
                      - radians
                          (' . $request->lon . ')
                      )
                  + sin
                      ( radians
                          (' . $request->lat  . ')
                      )
                  * sin
                      ( radians
                          ( lat )
                      )
                  )
              )
            ');
                    $tasks->when($sqlDistance != null, function ($query) use ($sqlDistance,$request){
                        $query->whereHas('address', function ($subQuery) use ($sqlDistance,$request) {
                            $subQuery->addSelect(DB::raw("{$sqlDistance} AS distance"));
                            $subQuery->havingRaw("distance <= ?", [(int)$request->range]);
                        });
                    })
                        ->with('user')
                        ->with('address');

                }
                if(isset($request->key)) {
                    $tasks->where('name', 'like', '%' . $request->key . '%');
                }

                if(isset($request->cat)) {
                    $tasks->whereIn('category_id', $request->cat);
                }

                if (isset($request->type)) {
                    $tasks->whereHas('budget', function ($subQuery) use($request,$types) {
                        $subQuery->whereIn('type',$request->types);
                    });
                }

                if(count($tasks->get()) != 0) {
                    return $tasks->get();
                }else return false;

            case 'service' :

                $cost = explode(',',$request->costRange);

                $services  = Service::where('created_at', '>=', Carbon::now()->subDay())->where(function ($q) use ($cost) {
                    $q->whereBetween('min',$cost)->orWhereBetween('max',$cost);
                });

                if(isset($request->key)) {
                    $services->where('name', 'like', '%' . $request->key . '%');
                }

                if(isset($request->cat)) {
                    $services->whereIn('category_id', $request->cat);
                }

                if (isset($request->skills)) {
                    $skills = $request->skills;
                    $services->whereHas('skills', function ($subQuery) use ($skills) {
                        $subQuery->whereIn('label',$skills);
                    });
                }

                if(count($services->get()) != 0) {
                    return $services->get();
                }else return false;

            case 'freelancer' :

                $freelancers  = User::where('created_at', '>=', Carbon::now()->subDay())->where('type',1)->where('status',2);

                if (isset($request->lat)) {
                    $sqlDistance = DB::raw
                    ('
              ( 6371 * acos
                  ( cos
                      ( radians
                          (' . $request->lat . ')
                      )
                  * cos
                      ( radians
                          ( lat )
                      )
                  * cos
                      ( radians
                          ( lon )
                      - radians
                          (' . $request->lon . ')
                      )
                  + sin
                      ( radians
                          (' . $request->lat  . ')
                      )
                  * sin
                      ( radians
                          ( lat )
                      )
                  )
              )
            ');
                    $freelancers->when($sqlDistance != null, function ($query) use ($sqlDistance,$request){
                        $query->whereHas('address', function ($subQuery) use ($sqlDistance,$request) {
                            $subQuery->addSelect(DB::raw("{$sqlDistance} AS distance"));
                            $subQuery->havingRaw("distance <= ?", [(int)$request->range]);
                        });
                    })
                        ->with('address');


                }

                if(isset($request->key)) {
                    $freelancers->whereHas('info', function ($subQuery) use ($request) {
                        $subQuery->where('tagline', 'like', '%' . $request->key . '%');
                    });
                }

                if (isset($request->skills)) {
                    $skills = $request->skills;
                    $freelancers->whereHas('skills', function ($subQuery) use ($skills) {
                        $subQuery->whereIn('label',$skills);
                    });
                }

                if(count($freelancers->get()) != 0) {
                    return $freelancers->get();
                }else return false;
        }
    }

}


if (! function_exists('scheduled')) {


    function scheduled($job, $userId)
    {
      $int =  $job->interviews()->where('interviewed', $userId)->first();

      if($int) {
        return true;
      }else 
      return false;
    }

}



