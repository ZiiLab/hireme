<?php

namespace App\Modules\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = true;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'comments';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'full_name',
      'email',
      'content',
      'user_id',
      'article_id',
      'comment_id'
  ];

  public function user()
  {
      return $this->belongsTo('App\Modules\User\Models\User');
  }
  public function article()
  {
      return $this->belongsTo('App\Modules\Blog\Models\Article');
  }

  public function comments(){
    return  $this->hasMany('App\Modules\Blog\Models\Comment');
}


}
