@extends('frontOffice.layout',['class' => 'gray','title' => 'Manage Posts'])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

    <div class="dashboard-container">
        @include('General::admin.sideBar')
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <div class="dashboard-headline">
                    <h3>Manage Posts</h3>

                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Blog</a></li>
                            <li>Posts</li>
                        </ul>
                    </nav>
                </div>
                <div class="row">

                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-business-center"></i> Posts Listings</h3>
                            </div>

                            <div class="content">
                                <ul class="dashboard-box-list">
                                    @if(count($posts) !=0 )
                                        @foreach($posts as $post)
                                            <li>
                                                <!-- Job Listing -->
                                                <div class="job-listing">

                                                    <!-- Job Listing Details -->
                                                    <div class="job-listing-details">


                                                        <a href="#" class="job-listing-company-logo">
                                                            <img src="{{ asset($post->medias()->where('type',10)->first()->link) }}" alt="{{$post->title}}">
                                                        </a>

                                                        <!-- Details -->
                                                        <div class="job-listing-description">
                                                            <h3 class="job-listing-title"><a href="#">{{ $post->title }}</a> <span class="dashboard-status-button updateStatus  @if($post->status == 0 || $post->status == 1) green @else red @endif" data-id="{{ $post->id }}"  style="cursor: pointer">  @if($post->status == 0 || $post->status == 1) Published @else Unpublished @endif</span>  </h3>

                                                            <!-- Job Listing Footer -->
                                                            <div class="job-listing-footer">
                                                                <ul>
                                                                    <li><i class="icon-material-outline-date-range"></i> Posted on {{$post->created_at->format('d M, Y') .' By '.$post->user->getFullName()}}</li>
                                                                    <li><i class="icon-line-awesome-certificate"></i> {{$post->category->name}}</li>
                                                                    <li><i class="icon-feather-tag"></i>
                                                                        @foreach($post->tags as $tag)
                                                                            <span class="dashboard-status-button" style="background-color: #333333;color: white;">{{ $tag->name }}</span>
                                                                            @endforeach
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Buttons -->
                                                <div class="buttons-to-right always-visible">
                                                    <a href="{{route('showUpdatePost', Crypt::encrypt($post->id))}}" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                                                    <span class="bookmark-icon {{ $post->status == 0 || $post->status == 2 ? 'bookmarked' : '' }} updateFeatureStatus" data-id="{{ $post->id }}" style="display: inline-block; position: relative; top: 0;right: 0;height: 30px;width: 30px;line-height: 17px"></span>
                                                </div>
                                            </li>
                                        @endforeach
                                    @else
                                        <li>
                                            <h3>Theres no posts yet,   <a href="{{ route('showAddArticle') }}">Create one?</a> </h3>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on("click", ".deleteCategory", function(e) {
            var cId = $(this).data('id');
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure you want to delete this category ?',
                buttons: {
                    cancel: function () {
                    },
                    somethingElse: {
                        text: 'Yes',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){
                            deleteForm('{{route('handleDeleteCategory')}}', {
                                cId : cId
                            }, 'post');
                        }
                    }
                }
            });
        });

        $(document).on('click', '.updateStatus', function (e) {
            e.preventDefault();
            var elm = $(this);
            $.get('{{ route('apiUpdatePostStatus') }}?id='+$(this).data('id')).done(function (res) {
                    if(elm.hasClass('green')) {
                        elm.removeClass('green');
                        elm.text('Unpublished');
                        elm.addClass('red');
                    } else {
                        elm.removeClass('red');
                        elm.text('Published');
                        elm.addClass('green');
                    }
            })
        })

        $(document).on('click', '.updateFeatureStatus', function (e) {
            e.preventDefault();
            var elm = $(this);
            $.get('{{ route('apiUpdatePostFeaturedStatus') }}?id='+$(this).data('id')).done(function (res) {
              if(elm.hasClass('bookmarked')) {
                  elm.removeClass('bookmarked');
              } else {
                  elm.addClass('bookmarked');

              }
            })
        })

    </script>
@endsection
