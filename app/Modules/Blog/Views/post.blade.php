
@extends('frontOffice.layout',['title' => $post->title])

@section('meta')
    <meta property="og:url" content="{{route('showPost',$post->getArticleParamTitle())}}">
    <meta property="og:site_name" content="{{$post->title}}">
    <meta property="og:image" content="{{asset($post->medias()->where('type',10)->first()->link)}}">
    <meta property="og:image:type" content="image/jpg">
    <meta property="twitter:creator" content="{{$post->user->getFullName()}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$post->title}}">
    <meta property="twitter:description" content="{{$post->content}}">
    <meta property="twitter:image:src" content="{{asset($post->medias()->where('type',10)->first()->link)}}">
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">


@endsection
@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Blog</h2>
                    <span>{{ $post->title }}</span>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">Home</a></li>
                            <li><a href="{{ route('showBlog') }}">Blog</a></li>
                            <li>Blog Post</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- Post Content -->
    <div class="container">
        <div class="row">

            <!-- Inner Content -->
            <div class="col-xl-8 col-lg-8">
                <!-- Blog Post -->
                <div class="blog-post single-post">

                    <!-- Blog Post Thumbnail -->
                    <div class="blog-post-thumbnail">
                        <div class="blog-post-thumbnail-inner">
                            <span class="blog-item-tag">{{ $post->category->name }}</span>
                            <img  src="{{ asset($post->medias()->where('type',10)->first()->link) }}" alt="{{ $post->title }}">
                        </div>
                    </div>

                    <!-- Blog Post Content -->
                    <div class="blog-post-content">
                        <h3 class="margin-bottom-10">{{ $post->title }}</h3>

                        <div class="blog-post-info-list margin-bottom-20">
                            @foreach($post->tags as $tag)
                                <span class="blog-post-info">{{ $tag->name }}</span>
                                @endforeach
                        </div>
                        <div class="blog-post-info-list margin-bottom-20">
                            <span class="blog-post-info">{{ $post->created_at->format('d M, Y') }}</span>
                            <a href="#"  class="blog-post-info scrollToComments">{{ count($post->comments) }} Comments</a>
                        </div>

                        {!! $post->content !!}
                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>Interesting? <strong>Share It!</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"
                                           data-sharer="facebook" data-title="{{$post->title}}" data-url="{{route('showPost',$post->getArticleParamTitle())}}"
                                        ><i class="icon-brand-facebook-f"></i></a></li>

                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"
                                           data-sharer="twitter"  data-title="{{$post->title}}" data-url="{{route('showPost',$post->getArticleParamTitle())}}"
                                        ><i class="icon-brand-twitter"></i></a></li>

                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"
                                           data-sharer="linkedin"  data-url="{{route('showPost',$post->getArticleParamTitle())}}"
                                        ><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Blog Post Content / End -->

                <!-- Blog Nav -->
                <ul id="posts-nav" class="margin-top-0 margin-bottom-40">
                    @if(\App\Modules\Blog\Models\Article::where('id', '>', $post->id)->orderBy('id','asc')->first())
                    <li class="next-post">
                        <a href="{{ route('showPost', \App\Modules\Blog\Models\Article::where('id', '>', $post->id)->orderBy('id','asc')->first()->getArticleParamTitle()) }}">
                            <span>Next Post</span>
                            <strong>{{ \App\Modules\Blog\Models\Article::where('id', '>', $post->id)->orderBy('id','asc')->first()->title }}</strong>
                        </a>
                    </li>
                    @endif
                        @if(\App\Modules\Blog\Models\Article::where('id', '<', $post->id)->orderBy('id','desc')->first())
                    <li class="prev-post">
                        <a href="{{ route('showPost', \App\Modules\Blog\Models\Article::where('id', '<', $post->id)->orderBy('id','desc')->first()->getArticleParamTitle()) }}">
                            <span>Previous Post</span>
                            <strong>{{ \App\Modules\Blog\Models\Article::where('id', '<', $post->id)->orderBy('id','desc')->first()->title }}</strong>
                        </a>
                    </li>
                        @endif
                </ul>

                <!-- Related Posts -->
               {{-- <div class="row">

                    <!-- Headline -->
                    <div class="col-xl-12">
                        <h3 class="margin-top-40 margin-bottom-35">Related Posts</h3>
                    </div>

                    <!-- Blog Post Item -->
                    <div class="col-xl-6">
                        <a href="pages-blog-post.html" class="blog-compact-item-container">
                            <div class="blog-compact-item">
                                <img src="images/blog-02a.jpg" alt="">
                                <span class="blog-item-tag">Recruiting</span>
                                <div class="blog-compact-item-content">
                                    <ul class="blog-post-tags">
                                        <li>29 June 2019</li>
                                    </ul>
                                    <h3>How to "Woo" a Recruiter and Land Your Dream Job</h3>
                                    <p>Appropriately empower dynamic leadership skills after business portals. Globally myocardinate interactive.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Blog post Item / End -->

                    <!-- Blog Post Item -->
                    <div class="col-xl-6">
                        <a href="pages-blog-post.html" class="blog-compact-item-container">
                            <div class="blog-compact-item">
                                <img src="images/blog-03a.jpg" alt="">
                                <span class="blog-item-tag">Marketing</span>
                                <div class="blog-compact-item-content">
                                    <ul class="blog-post-tags">
                                        <li>10 June 2019</li>
                                    </ul>
                                    <h3>11 Tips to Help You Get New Clients Through Cold Calling</h3>
                                    <p>Compellingly embrace empowered e-business after user friendly intellectual capital. Interactively front-end.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- Blog post Item / End -->
                </div>--}}
                <!-- Related Posts / End -->


                <!-- Comments -->
                <div class="row commentsSection">
                    <div class="col-xl-12">
                        <section class="comments">
                            <h3 class="margin-top-45 margin-bottom-0">Comments (<span class="comments-amount">{{ count($post->comments) }}</span>)</h3>

                            <ul class="firstLevel">
                                @foreach($post->comments()->where('comment_id',null)->get() as $comment)
                                <li>
                                    <div class="avatar"><img src="{{ asset( $comment->user ? $comment->user->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png') }}" alt="{{ $comment->user ? $comment->user->getFullName() : $comment->full_name }}"></div>
                                    <div class="comment-content"><div class="arrow-comment"></div>
                                        <div class="comment-by">{{ $comment->user ? $comment->user->getFullName() : $comment->full_name }}<span class="date">{{ $comment->created_at->format('d M, Y') }}</span>
                                            <a href="#" class="reply" data-comment="{{ $comment->id }}" data-class="secondLevel-{{$comment->id}}"><i class="fa fa-reply"></i> Reply</a>
                                        </div>
                                        <p>{{ $comment->content }}</p>
                                    </div>

                                    <ul class="secondLevel-{{$comment->id}}">
                                        @foreach($comment->comments as $comment)
                                        <li>
                                            <div class="avatar"><img src="{{ asset( $comment->user ? $comment->user->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png') }}" alt="{{ $comment->user ? $comment->user->getFullName() : $comment->full_name }}"></div>
                                            <div class="comment-content"><div class="arrow-comment"></div>
                                                <div class="comment-by">{{ $comment->user ? $comment->user->getFullName() : $comment->full_name }}<span class="date">{{ $comment->created_at->format('d M, Y') }}</span>
                                                </div>
                                                <p>{{ $comment->content }}</p>
                                            </div>
                                        </li>
                                        @endforeach

                                    </ul>

                                </li>
                                @endforeach

                            </ul>

                        </section>
                    </div>
                </div>
                <!-- Comments / End -->


                <!-- Leava a Comment -->
                <div class="row addCommentSection">
                    <div class="col-xl-12">

                        <h3 class="margin-top-35 margin-bottom-30">Add Comment</h3>

                        <!-- Form -->
                        <form method="post" action="javascript:void(0)" id="add-comment">

                       @if(!Auth::check())
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-material-outline-account-circle"></i>
                                            <input type="text" class="input-text" name="commentname" id="namecomment" placeholder="Name" required/>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="input-with-icon-left no-border">
                                            <i class="icon-material-baseline-mail-outline"></i>
                                            <input type="text" class="input-text" name="emailaddress" id="emailaddress" placeholder="Email Address" required/>
                                        </div>
                                    </div>
                                </div>
                           @endif

                            <textarea  name="comment-content" id="commentContent" cols="30" rows="5" placeholder="Comment" required></textarea>
                        </form>

                        <!-- Button -->
                        <button class="button button-sliding-icon ripple-effect margin-bottom-40" type="submit" id="add-comment-btn" form="add-comment">Add Comment <i class="icon-material-outline-arrow-right-alt"></i></button>

                    </div>
                </div>
                <!-- Leava a Comment / End -->

            </div>
            <!-- Inner Content / End -->


            <div class="col-xl-4 col-lg-4 content-left-offset">
                <div class="sidebar-container">

                    <!-- Location -->
                    <div class="sidebar-widget margin-bottom-40">
                        <div class="input-with-icon">
                            <input id="searchPosts" type="text" placeholder="Search">
                            <i class="icon-material-outline-search"></i>
                        </div>
                    </div>

                    <!-- Widget -->
                    <div class="sidebar-widget">

                        <h3>Related Posts</h3>
                        <ul class="widget-tabs">
                            @foreach($related as $post)
                            <li>
                                <a href="{{ route('showPost', $post->getArticleParamTitle()) }}" class="widget-content active">
                                    <img src="{{ asset($post->medias()->where('type',10)->first()->link) }}" alt="{{ $post->title }}">
                                    <div class="widget-text">
                                        <h5>{{ $post->title }}</h5>
                                        <span>{{  $post->created_at->format('d M, Y') }}</span>
                                    </div>
                                </a>
                            </li>

                        @endforeach
                        </ul>

                    </div>
                    <!-- Widget / End-->

{{--
                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Social Profiles</h3>
                        <div class="freelancer-socials margin-top-25">
                            <ul>
                                <li><a href="#" title="Dribbble" data-tippy-placement="top"><i class="icon-brand-dribbble"></i></a></li>
                                <li><a href="#" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                <li><a href="#" title="Behance" data-tippy-placement="top"><i class="icon-brand-behance"></i></a></li>
                                <li><a href="#" title="GitHub" data-tippy-placement="top"><i class="icon-brand-github"></i></a></li>
                            </ul>
                        </div>
                    </div>--}}

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Tags</h3>
                        <div class="task-tags">
                            @foreach(\App\Modules\Blog\Models\Tag::all() as $tag)
                                <a href="{{ route('showBlog').'?tag='.$tag->name }}"><span>{{ $tag->name }}</span></a>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- Spacer -->
    <div class="padding-top-40"></div>
    <script>
        $('#searchPosts').keyup(function(e){
            if(e.keyCode == 13)
            {
                var key = $(this).val();
                window.location.href = '{{ route('showBlog').'?key=' }}'+key;
            }
        });

        $( "#add-comment" ).on('submit',function( event ) {
            event.preventDefault();
            $('#add-comment-btn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
            $('#add-comment-btn').css('opacity','0.5');
            $('#add-comment-btn').css('cursor','not-allowed');
            $("#add-comment-btn").attr("disabled", true);

            var data = {
                'commentContent' : $('#commentContent').val(),
                'post' : '{{ $post->id }}'
            }

            if ($( "#namecomment" ).length != 0) {
                data.name = $('#namecomment').val(),
                data.email =   $('#emailaddress').val()
            }

            data.comment = $('#subComment').val();

            $.post('{{route('apiHandlePostComment')}}',
                {
                    '_token': $('meta[name=csrf-token]').attr('content'), data : data

                })
                .done(function (res) {
                    var count = parseInt($('.comments-amount').text());
                    $('#add-comment-btn').html('Add Comment <i class="icon-material-outline-arrow-right-alt"></i>')
                    $('#add-comment-btn').css('opacity','1');
                    $('#add-comment-btn').css('cursor','pointer');
                    $("#add-comment-btn").attr("disabled", false);
                    if(res.status == 200) {
                        count++;
                        $('.comments-amount').text(count);
                        $('#commentContent').val('');
                        $('#namecomment').val('');
                        $('#emailaddress').val('');
                        Snackbar.show({
                            text: 'Comment posted',
                            pos: 'bottom-right',
                            showAction: false,
                            actionText: "Dismiss",
                            duration: 3000,
                            textColor: '#fff',
                            backgroundColor: '#05ff84'
                        });

                        if(res.level == 'one') {

                            $('.firstLevel').append('    <li>\n' +
                                '                                    <div class="avatar"><img src="'+ res.comment.img +'" alt="'+ res.comment.name +'"></div>\n' +
                                '                                    <div class="comment-content"><div class="arrow-comment"></div>\n' +
                                '                                        <div class="comment-by">'+ res.comment.name +'<span class="date">'+ res.comment.name +'</span>\n' +
                                '                                            <a href="#" class="reply" data-comment="' + res.fC + '"><i class="fa fa-reply"></i> Reply</a>\n' +
                                '                                        </div>\n' +
                                '                                        <p>'+ res.comment.content +'</p>\n' +
                                '                                    </div>\n' +
                                '                                </li>');
                        }else {
                            $('.secondLevel-'+res.comment.id).append(' <li>\n' +
                                '                                            <div class="avatar"><img src="'+ res.comment.img +'" alt="'+ res.comment.name +'"></div>\n' +
                                '                                            <div class="comment-content"><div class="arrow-comment"></div>\n' +
                                '                                                <div class="comment-by">'+ res.comment.name +'<span class="date">'+ res.comment.name +'</span>\n' +
                                '                                                </div>\n' +
                                '                                                <p>'+ res.comment.content +'</p>\n' +
                                '                                            </div>\n' +
                                '                                        </li>');
                        }

                        $( ".subComments" ).remove();

                    }

                })
                .fail(function (res) {
                    $('#add-comment-btn').html('Add Comment <i class="icon-material-outline-arrow-right-alt"></i>')
                    $('#add-comment-btn').css('opacity','1');
                    $('#add-comment-btn').css('cursor','pointer');
                    $("#add-comment-btn").attr("disabled", false);

                    Snackbar.show({
                        text: 'Something went wrong, please try again',
                        pos: 'bottom-right',
                        showAction: false,
                        actionText: "Dismiss",
                        duration: 3000,
                        textColor: '#fff',
                        backgroundColor: '#ff0017'
                    });
                })


        });

        $(document).on('click', '.reply', function (e) {
            e.preventDefault();
            var comment = $(this).data('comment');
                $( ".subComments" ).remove();

                $('<input>').attr({
                    type: 'hidden',
                    id: 'subComment',
                    value : comment,
                    class: 'subComments'
                }).appendTo('form');


            var offset = -80;
            $('html, body').animate({
                scrollTop: $(".addCommentSection").offset().top + offset
            }, 1000);
        });

        $('.scrollToComments').on('click', function (e) {
            e.preventDefault();
            var offset = -80;
            $('html, body').animate({
                scrollTop: $(".commentsSection").offset().top + offset
            }, 1000);
        });
    </script>
@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
