@foreach($standard as $post)
    <!-- Blog Post -->
    <a href="{{ route('showPost', $post->getArticleParamTitle()) }}" class="blog-post">
        <!-- Blog Post Thumbnail -->
        <div class="blog-post-thumbnail">
            <div class="blog-post-thumbnail-inner">
                <span class="blog-item-tag">{{ $post->category->name }}</span>
                <img src="{{ asset($post->medias()->where('type',10)->first()->link) }}" alt="{{ $post->title }}">
            </div>
        </div>
        <!-- Blog Post Content -->
        <div class="blog-post-content">
            <span class="blog-post-date">{{  $post->created_at->format('d M, Y') }}</span>
            <h3>{{ $post->title }}</h3>
        </div>
        <!-- Icon -->
        <div class="entry-icon"></div>
    </a>
@endforeach