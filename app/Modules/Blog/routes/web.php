<?php

Route::group(['module' => 'Blog', 'middleware' => ['web'], 'namespace' => 'App\Modules\Blog\Controllers'], function() {


        Route::get('/blog', 'BlogController@showBlog')->name('showBlog');
        Route::get('/blog/{param}', 'BlogController@showPost')->name('showPost');
        Route::post('/api/post-comment', 'BlogController@apiHandlePostComment')->name('apiHandlePostComment');

    Route::group(['module' => 'Blog', 'prefix' => 'admin/blog', 'middleware' => ['admin']], function() {

        Route::get('/categories', 'BlogController@showManageCategories')->name('showManageCategories');
        Route::post('/add-category', 'BlogController@handleAddCategory')->name('handleAddCategory');
        Route::post('/delete-category', 'BlogController@handleDeleteCategory')->name('handleDeleteCategory');
        Route::get('/add-post', 'BlogController@showAddArticle')->name('showAddArticle');
        Route::post('/add-post', 'BlogController@handleAddPost')->name('handleAddPost');
        Route::get('/posts', 'BlogController@showManagePosts')->name('showManagePosts');
        Route::get('/update-post/{param}', 'BlogController@showUpdatePost')->name('showUpdatePost');
        Route::post('/update-post/{param}', 'BlogController@handleUpdatePost')->name('handleUpdatePost');
        Route::get('/api/update-post-status', 'BlogController@apiUpdatePostStatus')->name('apiUpdatePostStatus');
        Route::get('/api/update-post-feature-status', 'BlogController@apiUpdatePostFeaturedStatus')->name('apiUpdatePostFeaturedStatus');

    });
});
