<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'medias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'link',
        'label',
        'thumbnail',
        'user_id',
        'portfolio_id',
        'company_id',
        'task_id',
        'service_id',
        'type'  // 0 profile img / 1 banner img / 2 intro video / 3 task files / 4 job files / 5 freelancer files / 6 application cv / 7 service attachments / 8 book files / 9 portfolio / 10 post featured image
    ];


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    public function portfolio()
    {
        return $this->belongsTo('App\Modules\User\Models\Portfolio');
    }


    public function company()
    {
        return $this->belongsTo('App\Modules\User\Models\Company');
    }

    public function task()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Task');
    }

    public function service()
    {
        return $this->belongsTo('App\Modules\Hired\Models\Service');
    }
}
