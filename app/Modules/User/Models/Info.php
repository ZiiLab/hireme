<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Info extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'infos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tagline',
        'abt_me',
        'user_id',
        'company_id'

    ];


    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User');
    }

    public function campany()
    {
        return $this->hasOne('App\Modules\User\Models\Campany');
    }


}
