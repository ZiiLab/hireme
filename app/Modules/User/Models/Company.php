<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeBookmarked;
use Codebyray\ReviewRateable\Contracts\ReviewRateable;
use Codebyray\ReviewRateable\Traits\ReviewRateable as ReviewRateableTrait;

class Company extends Model implements ReviewRateable
{
    use CanBeBookmarked;
    use ReviewRateableTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'industry',
        'size',
        'phone',
        'fax',
        'founded_at',
        'country',
        'website',
        'user_id'
    ];

    protected $dates = [
        'founded_at'
    ];

    public function user(){
        return $this->belongsTo('App\Modules\User\Models\User');
    }
    public function medias(){
        return $this->hasMany('App\Modules\User\Models\Media');
    }
    public function info()
    {
        return $this->hasOne('App\Modules\User\Models\Info');
    }
    public function address(){
        return $this->hasOne('App\Modules\User\Models\Address');
    }

    public function tasks() {
        return $this->hasMany('App\Modules\Hired\Models\Task');
    }

    public function jobs() {
        return $this->hasMany('App\Modules\Hired\Models\Job');
    }

    public function socials() {
        return $this->hasMany('App\Modules\User\Models\Social');
    }

    public function getCompanyParamName() {
        $name = str_replace(' ', '-', $this->name);
        $name = str_replace('?', '', $name);
        $name = str_replace('/', '&', $name);
        $id = $this->id * 33 + 9371;
        return $name.'-'.$id;
    }
}
