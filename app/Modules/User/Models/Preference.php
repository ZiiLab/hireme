<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Preference extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'preferences';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hour_rate',
        'user_id'

    ];


    public function user()
    {
        return $this->hasOne('App\Modules\User\Models\User');
    }

}
