<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address',
        'city',
        'province',
        'zip',
        'lat',
        'lon',
        'country',
        'user_id',
        'compnay_id'


    ];

    public function user(){
        return $this->belongsTo('App\Modules\User\Models\User');
    }
    public function campnay(){
        return $this->belongsTo('App\Modules\User\Models\Campany');
    }

    public function task(){
        return $this->belongsTo('App\Modules\Hired\Models\Task');
    }

}
