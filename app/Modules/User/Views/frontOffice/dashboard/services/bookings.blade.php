@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Manage Service Bookings')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('Manage Bookings') }}</h3>
                    <span class="margin-top-7" style="width: 60%">{{ __('Bookings for') }} <a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}">{{ $service->name }}</a></span>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Manage Bookings') }}</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                           <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-supervisor-account"></i> {{ count($service->bookings) }} {{ __('Bookings') }}</h3>
                                {{--    <div class="sort-by">
                                       <select class="selectpicker hide-tick">
                                           <option>Highest First</option>
                                           <option>Lowest First</option>
                                           <option>Fastest First</option>
                                       </select>
                                   </div>
                               </div>--}}

                            <div class="content">
                                <ul class="dashboard-box-list">
                                    @if(count($service->bookings) == 0)
                                        <li>
                                            <h3>{{ __('No bookings on this service yet.') }}</h3>
                                        </li>
                                    @endif
                                    @foreach($service->bookings as $booking)
                                        <li>
                                            <!-- Overview -->
                                            <div class="freelancer-overview manage-candidates">
                                                <div class="freelancer-overview-inner">

                                                    <!-- Avatar -->
                                                    <div class="freelancer-avatar">
                                                        <a href="#"><img src="{{asset($booking->client->medias()->where('type',0)->first() ? $booking->client->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png' )}}" alt="{{ $booking->client->getFullName() }}"></a>
                                                    </div>

                                                    <!-- Name -->
                                                    <div class="freelancer-name">
                                                        <h4><a href="#">{{$booking->client->getFullName()}} </a></h4>

                                                        <!-- Details -->
                                                        <span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i> <span class="__cf_email__" data-cfemail="660207100f0226031e070b160a034805090b">[email&#160;protected]</span></a></span>

                                                        <!-- Rating -->
                                                        <div class="freelancer-rating">
                                                 @if(count($booking->client->getAllRatings($booking->client->id,'desc','App\Modules\User\Models\User')) < 3)
                                                <span class="company-not-rated">{{ __('Minimum of 3 votes required') }}</span>
                                                    @else
                                                   <div class="star-rating" data-rating="{{ $booking->client->averageRating(1)[0] }}"></div>
                                                @endif
                                                        </div>


                                                        <!-- Buttons -->
                                                        <div class="buttons-to-right always-visible margin-top-25 margin-bottom-0">
                                                            <a href="#small-dialog-2" class="popup-with-zoom-anim button dark ripple-effect sendDirectMessage" data-name="{{ $booking->client->getFullName() }}" data-id="{{$booking->client->id}}"><i class="icon-feather-mail"></i> Send Message</a>
                                                            @if($booking->media_id)
                                                                <a href="{{ asset($booking->media->link) }}" class="button ripple-effect" download><i class="icon-feather-file-text"></i> {{ __('Download File') }}</a>
                                                            @endif
                                                            @if($booking->note)
                                                                <a href="#" data-note="{{ $booking->note }}" class="button ripple-effect showNote"><i class="icon-feather-eye"></i> {{ __('Note') }}</a>
                                                            @endif

                                                            <a href="#!" class="button gray ripple-effect ic delete-booking" data-id="{{ $booking->id }}" title="Remove Bid" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row / End -->


            </div>
        </div>
        <!-- Dashboard Content / End -->
    </div>
    <script>
        $('.delete-booking').on('click',function () {
            var bookingId = $(this).data('id');
            $.confirm({
                title: '{{ __('Confirm!') }}',
                content: '{{ __('Are you sure you want to delete this booking?') }}',
                buttons: {
                    cancel: function () {
                    },
                    somethingElse: {
                        text: 'Yes',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){
                            deleteForm('{{route('handleDeleteBooking')}}', {
                                bookingId : bookingId
                            }, 'post');
                        }
                    }
                }
            });
        });

        $('.sendDirectMessage').on('click', function () {
            $('#bidder_name').text($(this).data('name'));
            $('#receiverId').val($(this).data('id'));
        });

        $('.showNote').on('click', function () {
            $.alert({
                title: 'Booking note',
                content: $(this).data('note'),
                columnClass: 'medium',
                buttons: {
                    somethingElse: {
                        text: 'Close',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){

                        }
                    }
                }
            });
        })
    </script>

    @include('User::frontOffice.dashboard.modals.sendDirectMessageBiddersPopup')
@endsection
