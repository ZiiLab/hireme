@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Update Service').' '.$service->name])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->
        <style>
            .toggle {
                display: none;
            }
        </style>

        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('Update a Service') }}</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Update a Service') }}</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->

                <form action="{{route('handleUpdateService', Crypt::encrypt($service->id))}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">

                        <!-- Dashboard Box -->
                        <div class="col-xl-12">
                            <div class="dashboard-box margin-top-0">

                                <!-- Headline -->


                                <div class="headline">
                                    <h3><i class="icon-feather-folder-plus"></i> {{ __('Update:') }} {{ $service->name }}</h3>
                                </div>

                                <div class="content with-padding padding-bottom-10">
                                    <div class="row">

                                        <div class="col-xl-8">
                                            <div class="submit-field">
                                                <h5>{{ __('What you can do?') }}</h5>
                                                <input name="name" type="text" value="{{ $service->name }}" class="with-border" placeholder="{{ __('e.g. I can translate...') }}">
                                                @if ($errors->has('name'))
                                                    <small>{{ $errors->first('name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="submit-field">
                                                <h5>{{ __('Category') }}</h5>
                                                <select name="category" class="selectpicker with-border" data-size="7" title="{{ __('Select Category') }}">
                                                    @foreach (\App\Modules\General\Models\TaskCategory::all() as $category)
                                                        <option value="{{$category->id}}">{{$category->label}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('category'))
                                                    <small>{{ $errors->first('category') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>{{ __('What skills are involved?') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Up to 5 skills that best describe your project') }}"></i> <small id="maxSkills"></small></h5>
                                                <div class="keywords-container">
                                                    <div class="keyword-input-container">
                                                        <input  type="text" id="skillsInput" class="keyword-input with-border" placeholder="{{ __('Add Skills') }}"/>
                                                        @if ($errors->has('skills'))
                                                            <small>{{ $errors->first('skills') }}</small>
                                                        @endif
                                                        @php
                                                            $oldSkills = '';
                                                            foreach($service->skills as $skill)
                                                            $oldSkills .=$skill->label.',';
                                                        @endphp
                                                        <input value="{{  substr($oldSkills, 0, -1) }}" type="hidden" id="serviceSkills" name="skills">
                                                        <a class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></a>
                                                    </div>
                                                    <div class="keywords-list">
                                                            @foreach($service->skills as $skill)
                                                                <span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>{{ $skill->label }}</span></span>
                                                            @endforeach
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-xl-8">
                                            <div class="submit-field">
                                                <h5>{{ __('For how much?') }}</h5>
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <div class="input-with-icon">
                                                            <input value="{{ $service->min  }}" name="min" class="with-border" type="text" placeholder="{{ __('Minimum') }}">
                                                            <i class="currency">TND</i>
                                                        </div>
                                                        @if ($errors->has('min'))
                                                            <small>{{ $errors->first('min') }}</small>
                                                        @endif
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <div class="input-with-icon">
                                                            <input value="{{ $service->max }}" name="max" class="with-border" type="text" placeholder="{{ __('Maximum') }}">
                                                            <i class="currency">TND</i>
                                                        </div>
                                                        @if ($errors->has('max'))
                                                            <small>{{ $errors->first('max') }}</small>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-xl-4">
                                            <div class="submit-field">
                                                <h5>{{ __('For how long') }} ({{ __('days') }})? </h5>
                                                <input type="number" value="{{ $service->delay }}" name="delay" min="1" placeholder="1">
                                                @if ($errors->has('delay'))
                                                    <small>{{ $errors->first('delay') }}</small>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="submit-field">
                                                <h5>{{ __('What are the requirements you need?') }}</h5>
                                                <textarea name="requirements" cols="30" rows="5" class="with-border">{{ $service->requirements  }}</textarea>
                                                @if ($errors->has('requirements'))
                                                    <small>{{ $errors->first('requirements') }}</small>
                                                @endif

                                                <h5 style="margin-top: 10px">Describe Your Service</h5>
                                                <textarea name="description" cols="30" rows="5" class="with-border">{{ $service->description }}</textarea>
                                                @if ($errors->has('description'))
                                                    <small>{{ $errors->first('description') }}</small>
                                                @endif
                                                <div class="uploadButton margin-top-30">
                                                    <input name="attachments[]" class="uploadButton-input" type="file" id="upload" accept=".pdf, .csv,.xls,.xlsx, image/*" multiple/>
                                                    <label class="uploadButton-button ripple-effect" for="upload">{{ __('Upload Files') }} ({{ __('Samples') }})</label>

                                                    @if(count($errors) > 0)
                                                        <span style="font-size: 18px" class="uploadButton-file-name">{{ __('Please check your files, you may need to re-upload them!') }}</span>

                                                    @else
                                                        <span class="uploadButton-file-name">{{ __('Images or documents that might be helpful in describing your project') }} <small id="uploadErrors"> &nbsp;({{ __('Each file size must be less than 10 MB') }})</small></span>
                                                    @endif
                                                </div>
                                                <div id="attachments" class="attachments-container margin-top-0 margin-bottom-0">
                                                    @if(count($service->medias()->where('type',7)->get()) > 0)
                                                        @foreach($service->medias()->where('type',7)->get() as $file)
                                                            <div class="attachment-box ripple-effect">
                                                                <span>{{ $file->label }}</span>
                                                                <button class="remove-attachment" data-id="{{$file->id}}" data-tippy-placement="top" title="Remove"></button>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                @if ($errors->has('attachments'))
                                                    <small>{{ $errors->first('attachments') }}</small>
                                                @endif
                                                @for($i = 0; $i<3 ; $i++ )
                                                    @if ($errors->has('attachments.'.$i))
                                                        <small>{{ 'File '.($i +1 ).' :'. $errors->first('attachments.'.($i +1)).' | ' }}</small>
                                                    @endif
                                                @endfor
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> {{ __('Submit') }} </button>
                        </div>

                    </div>
                    <!-- Row / End -->
                </form>

                @include('frontOffice.inc.small-footer')

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>

    <script>

        $(document).ready(function() {
            var oldCategory = '{{ $service->category_id }}';

            if(oldCategory !== '') {
                $('select').val(oldCategory).change();
            }
        });

        $('#skillsInput').focus(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

        });

        $('#skillsInput').focusout(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    $('form').submit();

                }
            });
        });
        $(".keywords-container").each(function() {
            var skills = []  ;
            skills = $('#serviceSkills').val().split(',');

            var keywordInput = $(this).find(".keyword-input");
            var keywordsList = $(this).find(".keywords-list");

            function addKeyword() {
                if(skills.length < 6 ) {
                    var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
                    keywordsList.append($newKeyword).trigger('resizeContainer');
                    skills.push(keywordInput.val());
                    $('#serviceSkills').val(skills);
                }else {
                    $('#maxSkills').text('You can just add 6 skills !').fadeIn('slow').delay(2000).fadeOut();
                }
                keywordInput.val("");
            }
            keywordInput.on('keyup', function(e) {
                if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $('.keyword-input-button').on('click', function() {
                if ((keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $(document).on("click", ".keyword-remove", function() {
                $(this).parent().addClass('keyword-removed');
                function removeFromMarkup() {
                    skills = $.grep(skills, function(value) {
                        return value !=  $(".keyword-removed").text();
                    });
                    $('#serviceSkills').val(skills);
                    $(".keyword-removed").remove()
                }
                setTimeout(removeFromMarkup, 500);
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            keywordsList.on('resizeContainer', function() {
                var heightnow = $(this).height();
                var heightfull = $(this).css({
                    'max-height': 'auto',
                    'height': 'auto'
                }).height();
                $(this).css({
                    'height': heightnow
                }).animate({
                    'height': heightfull
                }, 200);
            });
            $(window).on('resize', function() {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            $(window).on('load', function() {
                var keywordCount = $('.keywords-list').children("span").length;
                if (keywordCount > 0) {
                    keywordsList.css({
                        'height': 'auto'
                    }).height();
                }
            });
        });

        var length = $("#attachments").children().length;
        $('#upload').on('change', function () {
            $('#attachments').html('');
            var files = $(this)[0].files;
            if(files  > 2) {

                $('#uploadErrors').text( ' Only 3 files are allowed !').fadeIn('slow').delay(3000).fadeOut();

            } else
                $.each(files, function (i, file) {

                    $('#attachments').append('<div class="attachment-box ripple-effect">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t<span>'+ file.name +'</span> \n' +

                        '\t\t\t\t\t\t\t\t\t\t\t\t</div>')
                    length++;
                })
        });

        $('.remove-attachment').on('click' ,  function (e) {
            e.preventDefault();
            var parent = $(this).parent()
            $.get("{{ route('apiHandleDeleteFreelancerFile')}}?id="+$(this).data('id')).done(function (res) {
                if(res.status == 200) {
                    parent.css('display', 'none');
                    length--;
                }

            });
        })

    </script>


@endsection
