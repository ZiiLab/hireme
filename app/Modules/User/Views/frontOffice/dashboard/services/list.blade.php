@extends('frontOffice.layout',['class' => 'gray','title' => __('Manage Services')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
            ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('Manage Services') }}</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Manage Services') }}</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-assignment"></i> {{ __('My Services') }}</h3>
                            </div>

                            <div class="content">
                                <ul class="dashboard-box-list">
                                    @if(count(Auth::user()->services) > 0)
                                        @foreach(Auth::user()->services()->where('status',1)->get() as $service)
                                            <li>

                                            @php

                                                $endDate = Carbon\Carbon::parse($service->created_at)->addDays(20);
                                                $left = $endDate->diffInDays(Carbon\Carbon::now());
                                                $leftInHours = null;
                                                if($left == 0 ) {
                                                     $leftInHours = $endDate->diffInHours(Carbon\Carbon::now());
                                                }
                                            @endphp
                                            <!-- Job Listing -->
                                                <div class="job-listing width-adjustment">

                                                    <!-- Job Listing Details -->
                                                    <div class="job-listing-details">

                                                        <!-- Details -->
                                                        <div class="job-listing-description">
                                                            <h3 class="job-listing-title"><a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}">{{$service->name}}</a> @if($leftInHours) <span class="dashboard-status-button yellow">{{ __('Expiring') }}</span> @endif</h3>

                                                            <!-- Job Listing Footer -->
                                                            <div class="job-listing-footer">
                                                                <ul>
                                                                    <li><i class="icon-material-outline-access-time"></i>  {{ $leftInHours ? $leftInHours.' Hours Left' : $left.' Days Left' }} </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Task Details -->
                                                <ul class="dashboard-task-info">
                                                    <li><strong>{{ count($service->bookings) }}</strong><span>{{ __('Bookings') }}</span></li>
                                                </ul>

                                                <!-- Buttons -->
                                                <div class="buttons-to-right always-visible">
                                                    <a href="{{ route('showManageBookings',Crypt::encrypt($service->id)) }}" class="button ripple-effect"><i class="icon-material-outline-supervisor-account"></i> {{ __('Manage Bookings') }} <span class="button-info">{{ count($service->bookings) }}</span></a>
                                                    <a href="{{ route('showUpdateService', Crypt::encrypt($service->id)) }}" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
                                                    <a href="#" class="button gray ripple-effect ico deleteService " title="Remove" data-tippy-placement="top" data-id="{{ $service->id }}"><i class="icon-feather-trash-2" ></i></a>

                                                </div>
                                            </li>
                                        @endforeach
                                    @else
                                        <li>
                                            <h3>{{ __('You have no services yet,') }} <a href="{{ route('showPostService') }}">{{ __('Create one?') }}</a></h3>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row / End -->

                @include('frontOffice.inc.small-footer')

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>

    <script>
        $(document).on("click", ".deleteService", function(e) {
            var serviceId = $(this).data('id');
            $.confirm({
                title: '{{ __('Confirm!') }}',
                content: '{{ __('Are you sure you want to delete this service?') }}',
                buttons: {
                    cancel: function () {
                    },
                    somethingElse: {
                        text: 'Yes',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){
                            deleteForm('{{route('handleDeleteService')}}', {
                                serviceId : serviceId
                            }, 'post');
                        }
                    }
                }
            });
        });

    </script>
@endsection
