 @if(count($medias) == 0)
  <div style="text-align: center;">
            <h3>{{ __('Theres no attached images for this project!') }}</h3>
  </div>
   @else

    <div class="slideshow-container">
       
       

        @foreach($medias as $key => $media)
            <div class="mySlides fade" @if($key == 0) style="display: block" @endif>
                <div class="numbertext">{{ $key+1 }} / {{ count($medias) }}</div>
                <img src="{{ asset($media->link) }}">
                {{--
                        <div class="text">{{ $media->label }}</div>
                --}}
            </div>
        @endforeach
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>

    <div style="text-align:center">
        @foreach($medias as $key => $media)
            <span class="dot {{ $key == 0 ? 'activeDot' : ''  }}" onclick="currentSlide({{ $key+1 }})"></span>
        @endforeach
    </div>

    @endif


