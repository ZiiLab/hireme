<style>
    * {box-sizing: border-box}
    .mySlides {display: none}
    img {vertical-align: middle;}

    /* Slideshow container */
    .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
    }

    /* Next & previous buttons */
    .prev, .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        padding: 16px;
        margin-top: -22px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover, .next:hover {
        background-color: rgba(0,0,0,0.8);
    }

    /* Caption text */


    /* Number text (1/3 etc) */
    .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* The dots/bullets/indicators */
    .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
    }

   .activeDot, .dot:hover {
        background-color: #717171;
    }

    /* Fading animation */
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 1.5s;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @-webkit-keyframes fade {
        from {opacity: .4}
        to {opacity: 1}
    }

    @keyframes fade {
        from {opacity: .4}
        to {opacity: 1}
    }

    /* On smaller screens, decrease text size */
    @media only screen and (max-width: 300px) {
        .prev, .next,.text {font-size: 11px}
    }
</style>
<div class="content" >
    <p>{{ __('Click on any project to see its images gallery!') }}</p>
    <ul class="dashboard-box-list">

        <div class="tasks-list-container listings-container tasks-grid-layout ">

        @foreach($portfolios as $project)
            <!-- Task -->
                <a href="#" class="task-listing showGalery" data-id="{{ $project->id }}" >
                    <div class="task-listing-bid">
                        <div class="task-listing-bid-inner">
                            <div class="task-offers">
                                <h3 style="word-break: break-word"><strong>{{ $project->project_title }}</strong></h3>
                                <ul class="task-icons">
                                    <li><i class="icon-line-awesome-tag"></i> {{ $project->type }}</li>
                                    <li><i class="icon-material-outline-access-time"></i> {{ $project->realized_at->format('Y/m/d') }}</li>
                                </ul>
                            </div>
                             <div style="display: flex; align-items:center;width: fit-content">
                                @if($project->link)
                                <span style="width: fit-content;" class="button button-sliding-icon ripple-effect externelLink" data-link="{{ $project->link }}" style="width: 30%!important;margin-right: 15px">{{ __('External Link ') }}<i class="icon-material-outline-arrow-right-alt"></i></span>

                            @endif
                           
                            </div>   
                        </div>
                    </div>
                    <div class="task-listing-details">
                        <div class="task-listing-description">
                            <h4 class="task-listing-title" style="word-break: break-word">
                                    {{ $project->project_description  }}
                                </h4>
                         
                        </div>
                    </div>
                  
                </a>

            @endforeach
        </div>
    </ul>
</div>
<script>

$('.externelLink').on('click', function (e) {
e.stopPropagation();
e.preventDefault();
window.open($(this).data('link'), '_blank');
});


  $('.showGalery').on('click', function (e) {
           e.preventDefault();
           var projectId = $(this).data('id');
           $.confirm({
               title: 'Galery',
               columnClass: 'col-xl-6',
               containerFluid: true,
               content: function () {
                   var self = this;
                   return $.ajax({
                       url: '{{ route('apiGetProjectMedias') }}?id='+projectId,
                       dataType: 'json',
                       method: 'get'
                   }).done(function (response) {
                          self.setContent(response.html);
                   }).fail(function(){
                       self.setContent('Something went wrong.');
                   });
               },
               buttons: {
                   close: function () {
                   }
               }
           });
       });


</script>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" activeDot", "");
    }
    if(typeof(slides[slideIndex-1]) !== 'undefined') {
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " activeDot";
    }
    
    
}
</script>
