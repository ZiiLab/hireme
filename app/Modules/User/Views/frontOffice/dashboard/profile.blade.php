@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Profile')])
@section('header')
@include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

<div class="dashboard-container">


    @include('User::frontOffice.dashboard.inc.sidebar')

    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner">

            <div class="dashboard-headline">
                <h3>{{ __('Profile') }}</h3>

                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                        <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                        <li>{{ __('Profile') }}</li>
                    </ul>
                </nav>
            </div>

            <div class="row">
              @if(Auth::user()->type == 1)
                @include('User::frontOffice.dashboard.inc.progress')
              @endif  
              @include('User::frontOffice.dashboard.inc.settingsFreelancer')
              @include('User::frontOffice.dashboard.inc.employmentsList')

            </div>
            @include('frontOffice.inc.small-footer')

        </div>
    </div>
    <!-- Dashboard Content / End -->

    </div>

  @endsection
