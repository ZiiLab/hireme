@extends('frontOffice.layout',['class' => 'gray','title' => __('Bookmarks')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('Bookmarks') }}</h3>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('Bookmarks') }}</li>
        </ul>
      </nav>
    </div>

    <!-- Row -->
    <div class="row">

      <!-- Dashboard Box Jobs -->
      <div class="col-xl-6">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-business-center"></i> {{ __('Bookmarked Jobs') }} </h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">

                @foreach (Auth::user()->bookmarks(App\Modules\Hired\Models\Job::class)->get() as $job)
              <li>
                <!-- Job Listing -->
                <div class="job-listing">

                  <!-- Job Listing Details -->
                  <div class="job-listing-details">

                    <!-- Logo -->
                    <a href="{{ route('showCompanyDetails' , $job->company->getCompanyParamName()) }}" class="job-listing-company-logo">
                      <img src="{{asset($job->company->medias()->where('type',0)->first() ? $job->company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png')}}" alt="{{$job->company->name}}">
                    </a>

                    <!-- Details -->
                    <div class="job-listing-description">
                      <h3 class="job-listing-title"><a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}">{{ $job->title }}</a></h3>

                      <!-- Job Listing Footer -->
                      <div class="job-listing-footer">
                        <ul>
                            <li><i class="icon-material-outline-location-on"></i>{{ $job->company->address->city }}</li>
                            <li><i class="icon-material-outline-business-center"></i> {{ jobTypeToString($job->type) }} </li>
                            <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($job->created_at)->diffForHumans() }} </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Buttons -->
                <div class="buttons-to-right">
                  <a href="#" class="button red ripple-effect ico removeBookmark" data-id="{{ $job->id }}" data-type="job" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                </div>
              </li>
                    @endforeach
            </ul>
          </div>
        </div>
      </div>
      <!-- Dashboard Box Task -->
      <div class="col-xl-6">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-assignment"></i> {{ __('Bookmarked Tasks') }}</h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">


                @foreach (Auth::user()->bookmarks(App\Modules\Hired\Models\Task::class)->get() as $task)
                <li>
                  <!-- Job Listing -->
                  <div class="job-listing">

                    <!-- Job Listing Details -->
                    <div class="job-listing-details">

                      <!-- Logo -->
                      <a @if($task->company) href="{{ route('showCompanyDetails', $task->company->getCompanyParamName()) }}" @else href="{{ $task->user->type == 1 ? route('showFreelancerDetails', $task->user->getFreelancerUsername()) : '#' }}" @endif  class="job-listing-company-logo">
                        <img src="{{asset($task->company ? $task->company->medias()->where('type',0)->first()->link : $task->user->medias()->where('type',0)->first()->link)}}" alt="">
                      </a>

                      <!-- Details -->
                      <div class="job-listing-description">
                        <h3 class="job-listing-title"><a href="{{ route('showTaskDetails', $task->getTaskParamName()) }}">{{$task->name}}</a></h3>

                        <!-- Job Listing Footer -->
                        <div class="job-listing-footer">
                          <ul>
                            <li><i class="icon-line-awesome-tag"></i> {{ $task->category->label }} </li>
                          <li><i class="icon-line-awesome-money"></i> {{$task->budget->min.' TND'}} - {{$task->budget->max.' TND'}}</li>
                            <li><i class="icon-line-awesome-flag"></i> {{$task->type == 0 ? 'Fixed Rate' : 'Hourly Rate'}}</li>
                            <li><i class="icon-material-outline-access-time"></i>{{ \Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Buttons -->
                  <div class="buttons-to-right">
                    <a href="#" class="button red ripple-effect ico removeBookmark" data-id="{{ $task->id }}" data-type="task" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                  </div>
                </li>
              @endforeach




            </ul>
          </div>
        </div>
      </div>

      <!-- Dashboard Box Freelancers -->
      <div class="col-xl-6">
        <div class="dashboard-box">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-face"></i> {{ __('Bookmarked Candidates') }} </h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">
              @foreach (Auth::user()->bookmarks(App\Modules\User\Models\User::class)->get() as $freelancer)
                <li>

                  <!-- Overview -->
                  <div class="freelancer-overview">
                    <div class="freelancer-overview-inner">

                      <!-- Avatar -->
                      <div class="freelancer-avatar">
{{--
                        <div class="verified-badge"></div>
--}}

                        <a href="{{ route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}"><img src="{{$freelancer->medias()->where('type',0)->first()  ? $freelancer->medias()->where('type',0)->first()->link  : 'frontOffice/images/user-avatar-placeholder.png' }}" alt="{{$freelancer->getFullName()}} "></a>
                        </div>

                      <!-- Name -->
                      <div class="freelancer-name">
                        <h4><a href="{{ route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}">{{$freelancer->getFullName()}} </a> <img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($freelancer->address->country).'.svg') }}" alt="{{ $freelancer->address->country }}"> <span style="font-size: 15px;
    cursor: auto;
}">{{ $freelancer->address->country }}</span> </h4>
                        <span>{{$freelancer->info->tagline}}</span>
                        <!-- Rating -->
                        <div class="freelancer-rating">
                            @if(count($freelancer->getAllRatings($freelancer->id,'desc','App\Modules\User\Models\User')) < 3)
                                <span class="company-not-rated">{{ __('Minimum of 3 votes required') }}</span>
                            @else
                                <div class="star-rating" data-rating="{{ $freelancer->averageRating(1)[0] }}"></div>
                            @endif
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Buttons -->
                  <div class="buttons-to-right">
                    <a href="#" class="button red ripple-effect ico removeBookmark" data-id="{{ $freelancer->id }}" data-type="user" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                  </div>
                </li>

              @endforeach
            </ul>
          </div>
        </div>
      </div>
      <!-- Dashboard Box Companies -->
      <div class="col-xl-6">
        <div class="dashboard-box">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-business"></i> {{ __('Bookmarked Companies') }} </h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">
            @foreach (Auth::user()->bookmarks(App\Modules\User\Models\Company::class)->get() as $company)
              <li>
                <!-- Overview -->
                <div class="freelancer-overview">
                  <div class="freelancer-overview-inner">

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                      {{--<div class="verified-badge"></div>--}}
                      <a href="{{ route('showCompanyDetails',$company->getCompanyParamName()) }}"><img src="{{asset($company->medias()->where('type',0)->first() ? $company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png' )}}" alt="{{$company->name}}"></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                      <h4><a href="{{ route('showCompanyDetails',$company->getCompanyParamName()) }}">{{$company->name}} <img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($company->country).'.svg') }}" alt="{{$company->country}}" title="{{$company->country}}" data-tippy-placement="top"></a></h4>
                      <span>{{$company->info->tagline}}</span>
                      <!-- Rating -->
                      <div class="freelancer-rating">
                          @if(count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company')) < 3)
                              <span class="company-not-rated">Minimum of 3 votes required</span>
                          @else
                              <div class="star-rating" data-rating="{{ $company->averageRating(1)[0] }}"></div>
                          @endif
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Buttons -->
                <div class="buttons-to-right">
                  <a href="#" class="button red ripple-effect ico removeBookmark" data-id="{{ $company->id }}" data-type="company" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                </div>
              </li>
            @endforeach

            </ul>
          </div>
        </div>
      </div>

        <div class="col-xl-6">
            <div class="dashboard-box">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-line-awesome-tasks"></i> {{ __('Bookmarked Services') }} </h3>
                </div>

                <div class="content">
                    <ul class="dashboard-box-list">


                        @foreach (Auth::user()->bookmarks(App\Modules\Hired\Models\Service::class)->get() as $service)
                            <li>
                                <!-- Job Listing -->
                                <div class="job-listing">

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Logo -->
                                        <a href="{{ route('showFreelancerDetails', $service->user->getFreelancerUsername()) }}" class="job-listing-company-logo">
                                            <img src="{{asset($service->user->medias()->where('type',0)->first() ? $service->user->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png')}}" alt="{{ $service->user->getFullName() }}">
                                        </a>

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title"><a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}">{{$service->name}}</a></h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-line-awesome-tag"></i> {{ $service->category->label }} </li>
                                                    <li><i class="icon-line-awesome-money"></i> {{$service->min.' TND'}} - {{$service->max.' TND'}}</li>
                                                    <li><i class="icon-material-outline-access-alarm"></i>{{'Delivered In '.$service->delay.' Days'}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Buttons -->
                                <div class="buttons-to-right">
                                    <a href="#" class="button red ripple-effect ico removeBookmark" data-id="{{ $service->id }}" data-type="task" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                </div>
                            </li>
                        @endforeach




                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- Row / End -->

      @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->
</div>

  <script>
      $('.removeBookmark').on('click', function () {
          var id = $(this).data('id');
          var type = $(this).data('type');
          var elm = $(this).parent().parent();
          $.confirm({
              title: '{{ __('Confirm!') }}',
              content: '{{ __('Are you sure you want to delete this bookmark?') }}',
              buttons: {
                  cancel: function () {
                  },
                  somethingElse: {
                      text: 'Yes',
                      btnClass: 'button ripple-effect',
                      keys: ['enter', 'shift'],
                      action: function(){
                            $.get('{{ route('handleBookmark') }}?id='+id+'&type='+type).done(function (res) {
                                    elm.remove();
                            })
                      }
                  }
              }
          });
      })
  </script>


@endsection
