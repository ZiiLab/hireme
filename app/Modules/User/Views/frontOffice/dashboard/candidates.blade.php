
@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Manage Candidates')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('Manage Candidates') }}</h3>
      <span class="margin-top-7" style="width: 60%">{{ __('Job Applications for') }} <a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}">{{$job->title}}</a></span>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
          <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
          <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('Manage Candidates') }}</li>
        </ul>
      </nav>
    </div>

    <!-- Row -->
    <div class="row">

      <!-- Dashboard Box -->
      <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-supervisor-account"></i>{{ count($job->applications) }}</h3>
          </div>

          <div class="content">
            <ul class="dashboard-box-list">

                @if(count($job->applications) == 0)
                  <li>
                    <h3>{{ __('No application has been submitted for') }} <a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}">{{$job->title}}</a> {{ __('yet.') }}</h3>
                  </li>
                @else

              @foreach ($job->applications as $application)

              <li>
                <!-- Overview -->
                <div class="freelancer-overview manage-candidates">
                  <div class="freelancer-overview-inner">

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                      <a href="{{ route('showFreelancerDetails',$application->candidate->getFreelancerUsername()) }}"><img alt="{{ $application->candidate->getFullName() }}" src="{{asset($application->candidate->medias()->where('type',0)->first() ? $application->candidate->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png')}}" alt="{{ $application->candidate->getFullName() }}"></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                      <h4><a href="{{ route('showFreelancerDetails',$application->candidate->getFreelancerUsername()) }}">{{$application->candidate->getFullName()}}</a></h4>

                      <!-- Details -->
                      <span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i> <span class="__cf_email__" data-cfemail="681b01060c11280d10090518040d460b0705">[email&#160;protected]</span></a></span>
                      <span class="freelancer-detail-item"><i class="icon-feather-phone"></i> {{$application->candidate->phone ?? 'No phone number available'}}</span>

                      <!-- Rating -->
                      <div class="freelancer-rating">
                        @if(count($application->candidate->getAllRatings($application->candidate->id,'desc','App\Modules\User\Models\User')) < 3)
                                            <span class="company-not-rated">{{__('Minimum of 3 votes required')}}</span>
                                        @else
                                            <div class="star-rating" data-rating="{{ $application->candidate->averageRating(1)[0] }}"></div>
                                        @endif
                      </div>

                      <!-- Buttons -->
                      <div class="buttons-to-right always-visible margin-top-25 margin-bottom-5">
                        @if($application->media_id)
                            <a href="{{ asset($application->media->link) }}" class="button ripple-effect" download><i class="icon-feather-file-text"></i> {{ __('Download CV') }}</a>
                        @endif
                        @if($application->note)
                            <a href="#" data-note="{{ $application->note }}" class="button ripple-effect showNote"><i class="icon-feather-eye"></i> {{ __('Note')}}</a>
                        @endif
                        <a href="#small-dialog-2" class="popup-with-zoom-anim button dark ripple-effect sendDirectMessage" data-name="{{ $application->candidate->getFullName() }}" data-id="{{$application->candidate->id}}"><i class="icon-feather-mail"></i> {{ __('Send Message')}}</a>


                        <span id="scheduleInterview-{{ $application->candidate->id }}">
                          @if(scheduled($job, $application->candidate->id))

                         <a href="{{ route('showCalendar') }}" class="button ripple-effect" >{{ __('An interview has been scheduled') }}</a>

                        @else
                        <a href="#small-dialog-1"  class="popup-with-zoom-anim button dark ripple-effect scheduleInterview" data-id="{{ $application->candidate->id }}" data-name="{{ $application->candidate->getFullName() }}" ><i class="icon-feather-calendar"></i> {{ __('Schedule an interview')}}</a>
                        @endif
                        </span>

                        <a href="#" class="button gray ripple-effect ico deleteApplication" data-id="{{ $application->id }}" title="Remove Candidate" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            @endforeach
          @endif
            </ul>
          </div>
        </div>
      </div>

    </div>
    <!-- Row / End -->

      @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->

</div>


@include('User::frontOffice.dashboard.modals.sendDirectMessageBiddersPopup')
@include('User::frontOffice.dashboard.modals.scheduleInterview')

  <script>
  $(document).on("click", ".deleteApplication", function(e) {
      e.preventDefault();
     var applicationId = $(this).data('id');
      $.confirm({
          title: '{{ __('Confirm!') }}',
          content: '{{ __('Are you sure you want to delete this application?') }}',
          buttons: {
              cancel: function () {
              },
              somethingElse: {
                  text: 'Yes',
                  btnClass: 'button ripple-effect',
                  keys: ['enter', 'shift'],
                  action: function(){
                    deleteForm('{{route('handleDeleteApplication')}}', {
                      applicationId : applicationId
                    }, 'post');
                }
              }
          }
      });
  });

  $('.sendDirectMessage').on('click', function () {
      $('#bidder_name').text($(this).data('name'));
      $('#receiverId').val($(this).data('id'));
  });

  $('.showNote').on('click', function () {
    $.alert({
    title: 'Condidate note',
    content: $(this).data('note'),
    columnClass: 'medium',
    buttons: {
    somethingElse: {
        text: 'Close',
        btnClass: 'button ripple-effect',
        keys: ['enter', 'shift'],
        action: function(){

        }
    }
}
});
  });


  $('.scheduleInterview').on('click', function() {
     $('.mfp-ready').show();
    $('#fName').text($(this).data('name'));
    $('#uId').val($(this).data('id'));

  });

  </script>
@endsection
