@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Companies')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <style>

        .setting-wrapper {
            position: relative;
            width: 150px;
            height: 150px;
            border-radius: 4px;
            overflow: hidden;
            box-shadow: none;
            margin: 0 10px 30px 0;
            transition: all .3s ease
        }

        .setting-wrapper:hover {
            transform: scale(1.05);
            cursor: pointer
        }

        .setting-wrapper .setting-profile-pic {
            height: 100%;
            width: 100%;
            transition: all .3s ease;
            object-fit: cover
        }

        .setting-wrapper .setting-profile-pic:after {
            font-family: Feather-Icons;
            content: "\e9f1";
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            line-height: 120px;
            position: absolute;
            font-size: 60px;
            background: #f0f0f0;
            color: #aaa;
            text-align: center
        }

        .setting-wrapper .setting-upload-button {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%
        }

        .setting-wrapper .setting-file-upload {
            opacity: 0;
            pointer-events: none;
            position: absolute
        }


    </style>



    <div class="dashboard-container">

    @include('User::frontOffice.dashboard.inc.sidebar')

        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <div class="dashboard-headline">
                    <h3>{{ __('Companies') }}</h3>

                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Companies') }}</li>
                        </ul>
                    </nav>
                </div>


                <div class="row">

    @include('User::frontOffice.dashboard.inc.settingsEmployer')
                </div>
                <!-- Row / End -->

                @include('frontOffice.inc.small-footer')

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>


    <script>

        function settingSwitcher() {
            var readURL = function(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('.setting-profile-pic').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            };
            $(".setting-file-upload").on('change', function() {
                readURL(this);
            });
            $(".setting-upload-button").on('click', function() {
                $(".setting-file-upload").click();
            });
        }


        settingSwitcher();



    </script>

@endsection
