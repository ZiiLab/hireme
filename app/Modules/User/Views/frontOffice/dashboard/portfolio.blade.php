@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Portfolio')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')


    <style>
        * {box-sizing: border-box}
        .mySlides {display: none}
        img {vertical-align: middle;}

        /* Slideshow container */
        .slideshow-container {
            max-width: 1000px;
            position: relative;
            margin: auto;
        }
        .dropzone {
            border: 2px solid #2a41e8!important;
        }

        .dz-message span {
            font-size: 20px;
            color: black;
        }
        /* Next & previous buttons */
        .prev, .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover, .next:hover {
            background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */


        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
        }

       .activeDot, .dot:hover {
            background-color: #717171;
        }

        /* Fading animation */
        .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 1.5s;
        }

        @-webkit-keyframes fade {
            from {opacity: .4}
            to {opacity: 1}
        }

        @keyframes fade {
            from {opacity: .4}
            to {opacity: 1}
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
            .prev, .next,.text {font-size: 11px}
        }

    </style>
    <div class="dashboard-container">


    @include('User::frontOffice.dashboard.inc.sidebar')


        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <div class="dashboard-headline">
                    <h3>{{ __('My Portfolio') }}</h3>

                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('My Portfolio') }}</li>
                        </ul>
                    </nav>
                </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-feather-folder-plus"></i> {{ __('Add New Project') }} </h3>
                            <a href="#" class="accordionHandler ripple-effect slidePortfolio" style="top: 10px!important; left:200px;"><i class="icon-material-outline-add" style="color:#fff;"></i></a>
                        </div>


                        <div id="portfolioAccordion" style="display: none" class="content with-padding padding-bottom-10">
                            <form action="{{ route('handleAddProject') }}" method="post" enctype="multipart/form-data">
                                @csrf
                            <div class="row">

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('Project Title') }}</h5>
                                        <input name="title" value="{{ old('title') }}" type="text" class="with-border">
                                        @if ($errors->has('title'))
                                            <small>{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('Project Category') }}</h5>
                                        <select id="projectCategory" name="type" class="selectpicker with-border" data-size="7" title="{{ __('Select Project Category') }}">
                                            <option value="Website">Website</option>
                                            <option value="Web App">Web App</option>
                                            <option value="Mobile App">Mobile App</option>
                                            <option value="Branding">Branding</option>
                                            <option value="Logo Design">Logo Design</option>
                                            <option value="Graphic Design">Graphic Design</option>
                                        </select>
                                        @if ($errors->has('type'))
                                            <small>{{ $errors->first('type') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('Realized At') }}</h5>
                                        <input type="date" name="realized_at" value="{{ old('realized_at')  }}">
                                        @if ($errors->has('realized_at'))
                                            <small>{{ $errors->first('realized_at') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>{{ __('External Link') }} ({{ __('Optional') }})  <i class="help-icon" data-tippy-placement="right" title="{{ __('Add a direct link to your project') }}"></i></h5>
                                        <input type="text" name="link" value="{{ old('link') }}">
                                        @if ($errors->has('link'))
                                            <small>{{ $errors->first('link') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-xl-8">
                                    <div class="submit-field">
                                        <h5>{{ __('Project Description') }}</h5>
                                        <textarea name="desc" id="" cols="30" rows="3">{{ old('desc') }}</textarea>
                                        @if ($errors->has('desc'))
                                            <small>{{ $errors->first('desc') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    {{-- <div class="uploadButton">
                                        <input name="images[]" class="uploadButton-input" type="file" accept="image/*" id="upload" multiple/>
                                        <label class="uploadButton-button ripple-effect" for="upload" style="width: 20%">{{ __('Upload Images') }}</label>
                                    </div>
                                    <span>{{ __('Maximum image size: 10 MB | 6 images are allowed.') }}</span>
                                    <span id="uploadErrors" style="color: red"></span>
                                    @if ($errors->has('images'))
                                        <small>{{ $errors->first('images') }}</small>
                                    @endif --}}
                                    <div class="needsclick dropzone" id="dropzone">
                                    
                                        </div>
                                        {{-- <div>
                                            <input class="btn btn-danger" type="submit">
                                        </div> --}}
                                </div>
                                <div class="col-md-12">
                                    <div id="images" class="attachments-container margin-top-30 margin-bottom-0">

                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> {{ __('Save Project') }}</button>
                                </div>

                            </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-feather-grid"></i> {{ __('Projects List') }}</h3>
                            </div>

                            <div class="content" style="padding: 25px">
                                <ul class="dashboard-box-list">

                                    <div class="tasks-list-container listings-container tasks-grid-layout">
                                        @if(count(Auth::user()->portfolios) != 0)
                                        @foreach(Auth::user()->portfolios as $project)
                                            <!-- Task -->
                                                <a href="#" class="task-listing showGalery" data-id="{{ $project->id }}" >


                                                    
                                                    <div class="task-listing-bid">
                                                        <div class="task-listing-bid-inner">
                                                            <div class="task-offers">
                                                                <h3 style="word-break: break-word"><strong>{{ $project->project_title }}</strong></h3>
                                                                <ul class="task-icons">
                                                                    <li><i class="icon-line-awesome-tag"></i> {{ $project->type }}</li>
                                                                    <li><i class="icon-material-outline-access-time"></i> {{ $project->realized_at->format('Y/m/d') }}</li>
                                                                </ul>
                                                            </div>
                                                             <div style="display: flex; align-items:center;width: fit-content">
                                                                @if($project->link)
                                                                <span style="width: fit-content;" class="button button-sliding-icon ripple-effect externelLink" data-link="{{ $project->link }}" style="width: 30%!important;margin-right: 15px">{{ __('External Link ') }}<i class="icon-material-outline-arrow-right-alt"></i></span>

                                                            @endif
                                                            <i style="margin-left:20px; font-size:25px" class="fa fa-edit deleteProject" data-id="{{ $project->id }}"></i>
                                                            <i style="margin-left:20px; font-size:25px" class="fa fa-trash deleteProject" data-id="{{ $project->id }}"></i>
                                                            </div>   
                                                        </div>
                                                    </div>

                                                    
                                                    <div class="task-listing-details">
                                                        <div class="task-listing-description">
                                                            <h4 class="task-listing-title" style="word-break: break-word">
                                                                    {{ $project->project_description  }}
                                                                </h4>
                                                         
                                                        </div>
                                                    </div>

                                                </a>

                                        @endforeach
                                            @else

                                        <h3>{{ __('You have no projects yet!, start adding projects to your portfolio above.') }}</h3>
                                            @endif
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                @include('frontOffice.inc.small-footer')

            </div>

        </div>


    </div>

    <script>

        @if($errors->any())
        $('#portfolioAccordion').slideToggle('slow');
        $('.slidePortfolio i').removeClass('icon-material-outline-add');
        $('.slidePortfolio i').addClass('icon-line-awesome-minus');
        @endif


        // $('#upload').on('change', function () {
        //     $('#images').html('');
        //     var files = $(this)[0].files;
        //     if(files.length > 6) {
        //         $('#uploadErrors').text( ' Only 6 images are allowed!').fadeIn('slow').delay(3000).fadeOut();
        //     } else
        //         $.each(files, function (i, file) {
        //             var size = file.size / 1024 / 1024;
        //             if(size > 10) {
        //                 $('#uploadErrors').text(file.name+' is too large!').fadeIn('slow').delay(3000).fadeOut();
        //             }
        //             var reader = new FileReader();
        //             reader.onload = function (e) {
        //                 $('#images').append(' <div class="col-xl-3">\n' +
        //                     '                                            <div class="attachment-box ripple-effect">\n' +
        //                     '                                                <img src="'+ e.target.result +'" alt="'+ file.name +'">\n' +
        //                     '                                            </div>\n' +
        //                     '                                        </div>');
        //             }
        //             reader.readAsDataURL(file);
        //         })
        // });
  
       $(document).ready(function () {
           var oldCategory = '{{ old('type') }}';
           if(oldCategory != '') {
               $('#projectCategory').val(oldCategory).change();

           }
       });

       $('.externelLink').on('click', function (e) {
           e.stopPropagation();
           e.preventDefault();
           window.open($(this).data('link'), '_blank');
       });
       $('.showGalery').on('click', function (e) {
           e.preventDefault();
           var projectId = $(this).data('id');
           $.confirm({
               title: 'Galery',
               columnClass: 'col-xl-6',
               containerFluid: true,
               content: function () {
                   var self = this;
                   return $.ajax({
                       url: '{{ route('apiGetProjectMedias') }}?id='+projectId,
                       dataType: 'json',
                       method: 'get'
                   }).done(function (response) {
                          self.setContent(response.html);
                   }).fail(function(){
                       self.setContent('Something went wrong.');
                   });
               },
               buttons: {
                   close: function () {
                   }
               }
           });
       });

        $('.slidePortfolio').on('click', function (e) {
            e.preventDefault();
            if($('.slidePortfolio i').hasClass('icon-material-outline-add')) {
                $('.slidePortfolio i').removeClass('icon-material-outline-add');
                $('.slidePortfolio i').addClass('icon-line-awesome-minus');
            }else {
                $('.slidePortfolio i').addClass('icon-material-outline-add');
                $('.slidePortfolio i').removeClass('icon-line-awesome-minus');
            }
            $('#portfolioAccordion').slideToggle('slow');
        });

        $('.deleteProject').on('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            var pId = $(this).data('id');
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure you want to delete this project?',
                buttons: {
                    cancel: function () {
                    },
                    somethingElse: {
                        text: 'Yes',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){
                            deleteForm('{{route('handleDeleteProject')}}', {
                                pId : pId
                            }, 'post');
                        }
                    }
                }
            });
        })

    </script>
    <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" activeDot", "");
            }
            if(typeof(slides[slideIndex-1]) !== 'undefined') {
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " activeDot";
    }
        }




    </script>
<script>

    $(document).ready(function() {

   var uploadedDocumentMap = {}
   $("#dropzone").dropzone({

 
    url: '{{ route('handleUploadMiltupleImages') }}',
    maxFilesize: 2, // MB
    maxFiles: 6,
    addRemoveLinks: true,
    dictDefaultMessage: "Click or drag and drop images here to upload (max image sie 2MB | 6 images are allowed) ",
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {

        var value = 
            {
              name: response.name,
              url: response.link
            }
        
      $('form').append('<input type="hidden" name="images[]" id="'+ response.name +'" value="' + encodeURIComponent(JSON.stringify(value)) + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="images[]"][id="' + name + '"]').remove()
    },
    init: function () {

        this.on("maxfilesexceeded", function(file){
        alert("You have exceeded the maximum of 6 images to upload!");
    });
  
    //   @if(isset($project) && $project->document)
    //     var files =
    //       {!! json_encode($project->document) !!}
    //     for (var i in files) {
    //       var file = files[i]
    //       this.options.addedfile.call(this, file)
    //       file.previewElement.classList.add('dz-complete')
    //       $('form').append('<input type="hidden" name="images[]" value="' + file.file_name + '">')
    //     }
    //   @endif
    
    }
}) 
  
  
  
  
  })
    </script>
   
    @endsection
