<!-- Send Direct Message Popup
================================================== -->
<div id="small-dialog-2" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab2">{{ __('Send Message') }}</a></li>
		</ul>

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab2">

				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>{{ __('Direct Message To') }} <span id="bidder_name"></span></h3>
				</div>

				<!-- Form -->
				<form method="post" id="send-pm">
					<textarea name="body" id="msgBody" cols="10" placeholder="Message" class="with-border" required></textarea>
                    <input type="hidden" id="receiverId" name="receiverId">
				</form>

                <span id="messageSent" style="color: green"></span>

				<!-- Button -->
				<button class="button sendBtn full-width button-sliding-icon ripple-effect" type="submit" form="send-pm">{{ __('Send') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>




		</div>
	</div>
</div>

<script>
    $(document).ready( function () {
        $('.sendBtn').attr('disabled' , true);
        $('.sendBtn').css('opacity' , '0.5');
        $('.sendBtn').css('cursor' , 'not-allowed');
    });
    var oldMsgVal ='';
    $('#msgBody').on('change keyup' , function () {
          if($(this).val() == oldMsgVal ) {
              $('.sendBtn').attr('disabled' , true);
              $('.sendBtn').css('opacity' , '0.5');
              $('.sendBtn').css('cursor' , 'not-allowed');
          }else {
              $('.sendBtn').attr('disabled' , false);
              $('.sendBtn').css('opacity' , '1');
              $('.sendBtn').css('cursor' , 'pointer');
          }
    })


    $( "#send-pm" ).submit(function( event ) {
        event.preventDefault();
        $('.sendBtn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>');
        $('.sendBtn').attr('disabled' , true);
        $('.sendBtn').css('opacity' , '0.5');
        $('.sendBtn').css('cursor' , 'not-allowed');
        var data = {
            'body' : $('#msgBody').val(),
            'receiverId' : $('#receiverId').val()
        }

        $.post('{{route('apiSendDirectMessage')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                if(res.status == 200) {
                        $('#messageSent').text('Messege sent!').fadeIn('slow').delay(3000).fadeOut();;
                        $('#msgBody').val('');
                    $('.sendBtn').html('Send <i class="icon-material-outline-arrow-right-alt"></i>');
                    $('.sendBtn').attr('disabled' , true);
                    $('.sendBtn').css('opacity' , '0.5');
                    $('.sendBtn').css('cursor' , 'not-allowed');
                }

            })
            .fail(function (res) {
                $('.sendBtn').attr('disabled' , false);
                $('.sendBtn').css('opacity' , '1');
                $('.sendBtn').css('cursor' , 'pointer');
                $('.sendBtn').html('Send <i class="icon-material-outline-arrow-right-alt"></i>');
            })


    })
</script>
