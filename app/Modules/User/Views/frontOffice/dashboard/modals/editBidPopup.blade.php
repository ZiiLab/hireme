
<!-- Edit Bid Popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab">{{ __('Edit Bid') }}</a></li>
		</ul>

		<div class="popup-tabs-container">
            <form action="{{route('handleUpdateBid')}}" method="post">
            @csrf
			<!-- Tab -->
			<div class="popup-tab-content" id="tab">

					<!-- Bidding -->
					<div class="bidding-widget">
						<!-- Headline -->
						<span class="bidding-detail" id="bidType">{{ __('Set your') }} <strong></strong></span>

						<!-- Price Slider -->
						<div class="bidding-value">TND <span id="biddingVal"></span></div>
						<input name="rate" class="bidding-slider" id="bidding-slider" type="text" value="" data-slider-handle="custom" data-slider-currency="TND" data-slider-min="10" data-slider-max="60" data-slider-value="" data-slider-step="1" data-slider-tooltip="show" />

						<!-- Headline -->
						<span class="bidding-detail margin-top-30">{{ __('Set your') }} <strong>{{ __('delivery time') }}</strong></span>

						<!-- Fields -->
						<div class="bidding-fields">
							<div class="bidding-field">
								<input type="hidden" name="id" id="bid" value="">
								<!-- Quantity Buttons -->
								<div class="qtyButtons with-border">
									<div class="qtyDec"></div>
									<input type="text" name="delivery_time" id="delivery_time" value="">
									<div class="qtyInc"></div>
								</div>
							</div>
							<div class="bidding-field">
								<select name="type" class="selectpicker default with-border" id="bidType">
									<option  value="0">Days</option>
									<option  value="1">Hours</option>
								</select>
							</div>
						</div>
				</div>

				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit"> {{ __('Save Changes') }} <i class="icon-material-outline-arrow-right-alt"></i></button>
			</div>
            </form>

		</div>
	</div>
</div>
<!-- Edit Bid Popup / End -->
