<!-- Bid Acceptance Popup
================================================== -->
<div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab1">{{ __('Accept Offer') }}</a></li>
		</ul>

		<div class="popup-tabs-container">
            <form action="{{route('handleAcceptBidderOffer')}}" method="post">
                @csrf
			<!-- Tab -->
			<div class="popup-tab-content" id="tab">

				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>{{ __('Accept Offer From') }} <span id="bidderName"></span></h3>
					<div id="bidDelivery" class="bid-acceptance margin-top-15">

					</div>

				</div>

					<div class="radio">
						<input id="radio-1" name="radio" type="radio" required>
						<label for="radio-1"><span class="radio-label"></span>  {{ __('I have read and agree to the Terms and Conditions') }}</label>
					</div>
                <input type="hidden" id="bid_id" name="bid_id">
                <input type="hidden" id="task_id" name="task_id">
				<!-- Button -->
				<button style="cursor: not-allowed; opacity: 0.5" class="margin-top-15 button full-width button-sliding-icon ripple-effect" disabled type="submit" id="acceptTerms">{{ __('Accept') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

			</div>
            </form>
		</div>
	</div>
</div>
<!-- Bid Acceptance Popup / End -->

<script>
        $('#radio-1').on('click' , function () {
                $('#acceptTerms').attr('disabled', false);
                $('#acceptTerms').css('opacity','1');
                $('#acceptTerms').css('cursor','pointer');
        })
</script>
