<style>
    .unstyled {
        -webkit-appearance: none;
    }
    .unstyled::-webkit-inner-spin-button
     {
        display: none;
        -webkit-appearance: none;
    }
</style>
<div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs" >

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">{{ __('Schedule Interview') }}</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __('Schedule an interview with') }} <span id="fName"></span></h3>
                </div>

                <!-- Form -->
                <form method="post" action="javascript:void(0)" id="interview-form">
               <div class="col-xl-12" style="margin-bottom: 10px">
                   <h4>{{ __('Set your preferred dates') }}</h4>
               </div>
                <div >
                    <div class="row dates" style="margin-left: 0px">
                    <div class="col-xl-10 ">
                            <input class="unstyled date" type="datetime-local"  min="{{date('Y-m-d').'T00:00'}}"  required>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <a href="#" class=" button  ripple-effect addDate" style="text-align: center;margin-top: 0px!important;
    margin-bottom: 15px;
    padding: 5px 5px 5px 5px;
    height: 35px;" > {{ __('Set another date') }}</a>
                    </div>
                    <div class="col-xl-6" style="margin-bottom: 15px">
                        <small id="maxDates"></small>
                    </div>
                </div>

                <div class="col-xl-12" style="margin-bottom: 10px">
                    <div class="switches-list">
                        <div class="switch-container">
                            <label class="switch"><input type="checkbox" id="type" ><span class="switch-button"></span> {{ __('Online Interview?') }}</label>
                        </div>
                    </div>
                </div>

                        <input type="hidden" id="uId">
                  <div class="col-xl-12">
                      <textarea name="textarea" id="noteBody" cols="10" placeholder="{{ __('Add some notes, e.g mention some another details or put a meeting platform to use if online etc ...') }}" class="with-border"></textarea>
                  </div>
                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect subBtn" type="submit" form="interview-form">{{ __('Submit') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>

<script>
    $('.addDate').on('click', function (e) {
        e.preventDefault();
        if($(":input[type='datetime-local']").length < 5){
            $('.dates').append('     <div class="col-xl-10 appendedDates">\n' +
                '                            <input class="unstyled date" type="datetime-local"   min="{{date('Y-m-d').'T00:00'}}" required>\n' +
                '                        </div>\n' +
                '                        <div class="col-xl-2">\n' +
                '                            <a href="#" class=" gray ripple-effect ico deleteDate"><i class="icon-feather-delete" style="    color: red;\n' +
                '    font-size: 25px;"></i></a>\n' +
                '\n' +
                '                        </div> ');
        }else {
            $('#maxDates').text('Only 5 dates are allowed').fadeIn('slow').delay(2000).fadeOut();
            $('.addDate').hide();
        }

    });

    $(document).on('click', '.deleteDate', function (e) {
        e.preventDefault();
        $(this).parent().prev().remove();
        $(this).parent().remove();
        if($(":input[type='datetime-local']").length < 5) {
            $('.addDate').show();
        }

    });

    $( "#interview-form" ).submit(function( event ) {
        event.preventDefault();
        $('.subBtn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('.subBtn').css('opacity','0.5');
        $('.subBtn').css('cursor','not-allowed');
        $(".subBtn").attr("disabled", true);
        var dates = [];
        var type = 0;

        $('.date').each(function (i,v) {
            dates.push($(this).val());
        });

        if($('#type').is(":checked")) {
            type = 1;
        }

        var data = {
            'dates' : dates,
            'note' : $("#noteBody").val(),
            'type' : type,
            'uId' : $('#uId').val(),
            'jId' : '{{ $job->id }}'
        }

        $.post('{{route('handleScheduleInterview')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {  
                $('#scheduleInterview-'+$('#uId').val()).find('a').remove();
 $('#scheduleInterview-'+$('#uId').val()).append('<a href="{{ route('showCalendar') }}" class="button ripple-effect" >{{ __(' Interview Scheduled') }}</a>');

                

                $("#noteBody").val('');
                $("#uId").val('');
                $("input[type=datetime-local]").val("");
                $('#type').prop('checked', false); 
                $('.appendedDates').remove();

        
                window.location.href = '{{ route('redirectAfterScheduleInterview') }}?job={{ Crypt::encrypt($job->id) }}'      
            })
            .fail(function (res) {
                                Snackbar.show({
        text: '{{ __('Something went wrong, please try again.') }}',
        pos: 'bottom-right',
        showAction: false,
        actionText: "Dismiss",
        duration: 3000,
        textColor: '#fff',
        backgroundColor: '#de5959'
    });
                $(".subBtn").html('Submit <i class="icon-material-outline-arrow-right-alt"></i>')
                $(".subBtn").css('opacity','1');
                $(".subBtn").css('cursor','pointer');
                $(".subBtn").attr("disabled", false);
            })
    });






























</script>

