<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

  <!--Tabs -->
  <div class="sign-in-form">

    <ul class="popup-tabs-nav">
      <li><a href="#tab">{{ __('Add Note') }}</a></li>
    </ul>

    <div class="popup-tabs-container">

      <!-- Tab -->
      <div class="popup-tab-content" id="tab">

        <!-- Welcome Text -->
        <div class="welcome-text">
          <h3>{{ __('Do Not Forget') }} 😎</h3>
        </div>

        <!-- Form -->
        <form method="post" action="javascript:void(0)" id="add-note-form">

          <select class="selectpicker with-border default margin-bottom-20" id="priority" data-size="7" title="Priority">
            <option value="0" selected>{{ __('Low Priority') }}</option>
            <option value="1">{{ __('Medium Priority') }}</option>
            <option value="2">{{ __('High Priority') }}</option>
          </select>

          <textarea name="textarea" id="noteBody" cols="10" placeholder="{{ __('Note') }}" class="with-border" required></textarea>

        </form>

        <!-- Button -->
        <button style="cursor:not-allowed; opacity: 0.5;" disabled class="button full-width button-sliding-icon ripple-effect addBtn" type="submit" form="add-note-form">{{ __('Add Note') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

      </div>

    </div>
  </div>
</div>
<!-- Apply for a job popup / End -->


<script>
    var oldNoteVal ='';
    $('#noteBody').on('change keyup' , function () {
        if($(this).val() == oldNoteVal ) {
            $('.addBtn').attr('disabled' , true);
            $('.addBtn').css('opacity' , '0.5');
            $('.addBtn').css('cursor' , 'not-allowed');
        }else {
            $('.addBtn').attr('disabled' , false);
            $('.addBtn').css('opacity' , '1');
            $('.addBtn').css('cursor' , 'pointer');
        }
    });
    var countNotes = $('.dashboard-note').length;
    $( "#add-note-form" ).submit(function( event ) {
        event.preventDefault();
        $('.addBtn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('.addBtn').css('opacity','0.5');
        $('.addBtn').css('cursor','not-allowed');
        $(".addBtn").attr("disabled", true);

        var data = {
            'priority' : $('#priority').val(),
            'body' : $('#noteBody').val()
        }

        $.post('{{route('apiHandleAddNote')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                $('.noNotes').hide();
                    $('.notesList').prepend('      <div class="dashboard-note">\n' +
                        '          <p>'+ res.body +'</p>\n' +
                        '          <div class="note-footer">\n' +
                        '            <span class="note-priority '+ res.class +'">' + res.priority + '</span>\n' +
                        '            <div class="note-buttons">\n' +
                        '              <a href="#" title="Remove" data-tippy-placement="top" class="deleteNote" data-id="' + res.id + '"<><i class="icon-feather-trash-2"></i></a>\n' +
                        '            </div>\n' +
                        '          </div>\n' +
                        '        </div>');
                countNotes++;
                    if($('.dashboard-note').length > 10) {
                        $('.notesList').children().last().remove();
                    }
                    if(countNotes > 9) {
                        $('.maxNotesNotif').show();
                        $('.showAddNote').hide();
                    }
                    $('.mfp-close').click();
                    $('#noteBody').val('');
                    $('#priority').change();
                $('.addBtn').html('Add Note <i class="icon-material-outline-arrow-right-alt"></i>')
                $('.addBtn').css('opacity','1');
                $('.addBtn').css('cursor','pointer');
                $(".addBtn").attr("disabled", false);
            })
            .fail(function (res) {
                $('.addBtn').html('Add Note <i class="icon-material-outline-arrow-right-alt"></i>')
                $('.addBtn').css('opacity','1');
                $('.addBtn').css('cursor','pointer');
                $(".addBtn").attr("disabled", false);
            })
    });

    $(document).on('click', '.deleteNote', function () {
            var noteId = $(this).data('id');
             var elem = $(this).closest('div[class^="dashboard-note"]');

              $.get('{{ route('apiHandleDeleteNote') }}?id='+noteId).done(function (res) {
                if(res.deletedWithNextNote) {
                   elem.remove();
                   countNotes--;
                    $('.maxNotesNotif').hide();
                    $('.showAddNote').show();

                    $('.notesList').append('      <div class="dashboard-note">\n' +
                        '          <p>'+ res.nextNote.body +'</p>\n' +
                        '          <div class="note-footer">\n' +
                        '            <span class="note-priority '+ res.nextNote.class +'">' + res.nextNote.priority + '</span>\n' +
                        '            <div class="note-buttons">\n' +
                        '              <a href="#" title="Remove" data-tippy-placement="top" class="deleteNote" data-id="' + res.nextNote.id + '"<><i class="icon-feather-trash-2"></i></a>\n' +
                        '            </div>\n' +
                        '          </div>\n' +
                        '        </div>');
                }else if(res.deleted) {
                    countNotes--;
                    $('.maxNotesNotif').hide();
                    $('.showAddNote').show();
                    elem.remove();
                }

            }).fail(function (res) {

            })

    });







</script>
