@extends('frontOffice.layout',['class' => 'gray','title' => __('Manage Bidders')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('Manage Bidders') }}</h3>
      <span class="margin-top-7" style="width: 60%">{{ __('Bids for') }} <a href="{{ route('showTaskDetails', $task->getTaskParamName()) }}">{{ $task->name }}</a></span>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
          <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
          <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('Manage Bidders') }}</li>
        </ul>
      </nav>
    </div>

    <!-- Row -->
    <div class="row">

      <!-- Dashboard Box -->
      <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
          <div class="headline">
            <h3><i class="icon-material-outline-supervisor-account"></i> {{ count($task->bids) }} {{ __('Bidders') }}</h3>
       {{--     <div class="sort-by">
              <select class="selectpicker hide-tick">
                <option>Highest First</option>
                <option>Lowest First</option>
                <option>Fastest First</option>
              </select>
            </div>--}}
          </div>

          <div class="content">
            <ul class="dashboard-box-list">
              @if(count($task->bids) == 0)
                <li>
                  <h3>{{ __('No bidders on this task yet.') }}</h3>
                </li>
              @endif
           @foreach($task->bids as $bid)
              <li>
                <!-- Overview -->
                <div class="freelancer-overview manage-candidates">
                  <div class="freelancer-overview-inner">

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                      <a href="{{ route('showFreelancerDetails',$bid->bidder->getFreelancerUsername() ) }}"><img src="{{asset($bid->bidder->medias()->where('type',0)->first() ? $bid->bidder->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png' )}}" alt="{{ $bid->bidder->getFullName() }}"></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                      <h4><a href="{{ route('showFreelancerDetails',$bid->bidder->getFreelancerUsername() ) }}">{{$bid->bidder->getFullName()}} </a></h4>

                      <!-- Details -->
                      <span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i> <span class="__cf_email__" data-cfemail="660207100f0226031e070b160a034805090b">[email&#160;protected]</span></a></span>
                       <span class="freelancer-detail-item"><i class="icon-feather-phone"></i> {{$bid->bidder->phone ?? __('No phone number available')}}</span>


                      <!-- Rating -->
                      <div class="freelancer-rating">
                          @if(count($bid->bidder->getAllRatings($bid->bidder->id,'desc','App\Modules\User\Models\User')) < 3)
                                            <span class="company-not-rated">{{ __('Minimum of 3 votes required') }}</span>
                                        @else
                                            <div class="star-rating" data-rating="{{ $bid->bidder->averageRating(1)[0] }}"></div>
                                        @endif
                      </div>

                      <!-- Bid Details -->
                      <ul class="dashboard-task-info bid-info">
                        <li><strong>{{ $bid->rate }} TND</strong><span>{{$bid->task->type == 0 ? 'Fixed Price' : 'Hourly Price'}}</span></li>
                        <li><strong>{{$bid->type == 0 ? $bid->delivery_time.' Days' :  $bid->delivery_time.' Hours' }}</strong><span>{{ __('Delivery Time') }}</span></li>
                      </ul>

                      <!-- Buttons -->
                      <div class="buttons-to-right always-visible margin-top-25 margin-bottom-0">
                        <a href="#small-dialog-1"  data-bidder-name="{{ $bid->bidder->getFullName() }}" data-bid-id="{{$bid->id}}" data-task-id="{{$bid->task->id}}" data-bid-rate="{{ $bid->rate }}" data-bid-delivery="{{ $bid->type == 0 ? $bid->delivery_time.' Days' :  $bid->delivery_time.' Hours' }}"  class="popup-with-zoom-anim button ripple-effect acceptOffer"><i class="icon-material-outline-check"></i> {{ __('Accept Offer') }}</a>
                        <a href="#small-dialog-2" class="popup-with-zoom-anim button dark ripple-effect sendDirectMessage" data-name="{{ $bid->bidder->getFullName() }}" data-id="{{$bid->bidder->id}}"><i class="icon-feather-mail"></i> Send Message</a>
                        <a href="#!" class="button gray ripple-effect ic delete-Bidder" data-id="{{ $bid->id }}" title="Remove Bid" data-tippy-placement="top"><i class="icon-feather-trash-2"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
               @endforeach

            </ul>
          </div>
        </div>
      </div>

    </div>
    <!-- Row / End -->

      @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->
</div>
  <script>
      $('.delete-Bidder').on('click',function () {
          var bidId = $(this).data('id');
          $.confirm({
              title: '{{ __('Confirm!') }}',
              content: '{{ __('Are you sure you want to delete this bid?') }}',
              buttons: {
                  cancel: function () {
                  },
                  somethingElse: {
                      text: 'Yes',
                      btnClass: 'button ripple-effect',
                      keys: ['enter', 'shift'],
                      action: function(){
                          deleteForm('{{route('HandleDeleteBid')}}', {
                              bidId : bidId
                          }, 'post');
                      }
                  }
              }
          });
      });
  </script>

  <script>
      $('.acceptOffer').on('click',function () {

          $('#bidderName').text($(this).data('bidderName'));
          var bDelivery = 'For '+ $(this).data('bid-rate')+' TND In '+$(this).data('bid-delivery');
          $('#bidDelivery').text(bDelivery);
          $('#bid_id').val($(this).data('bid-id'));
          $('#task_id').val($(this).data('task-id'));
      });

      $('.sendDirectMessage').on('click', function () {
          $('#bidder_name').text($(this).data('name'));
          $('#receiverId').val($(this).data('id'));
      })
  </script>
@include('User::frontOffice.dashboard.modals.bidAcceptancePopup')
@include('User::frontOffice.dashboard.modals.sendDirectMessageBiddersPopup')
@endsection
