<div  class="col-xl-12" >


    <div>
        <div class="dashboard-box">

                        <div class="headline">
                <h3><i class="icon-material-outline-business"></i>{{ __('My Employments') }}</h3>
                    <a href="#!" class="accordionHandler ripple-effect slideEmps" style="top: 10px!important"><i class="icon-material-outline-add" style="color:#fff;"></i></a>

                        </div>
              <div  class="content">
            <ul id="empsList" class="dashboard-box-list">
                @foreach(Auth::user()->employments as $emp)
              <li id="emp-{{$emp->id}}" style=" border-bottom: 1px solid #ddd">
                <div class="job-listing">
                  <div class="job-listing-details">
                    <a href="#" class="job-listing-company-logo">
                      <img src="{{ asset('frontOffice/images/company-logo-placeholder.png') }}" alt="">
                    </a>
                    <div class="job-listing-description">
                      <h3 class="job-listing-title"><a href="#">{{ $emp->position }}</a></h3>
                      <div class="job-listing-footer">
                        <ul>
                          <li><i class="icon-material-outline-business"></i> {{$emp->company_name}}</li>
                          <li><i class="icon-material-outline-access-time"></i><span>From</span> {{ $emp->start_date->format('d/m/Y') }} To {{ $emp->present == 0? $emp->end_date->format('d/m/Y') : 'Present' }}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="buttons-to-right">
                  <a href="#" class="button red ripple-effect ico del-emp" title="Remove" data-id="{{ $emp->id }}" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                </div>
              </li>
        @endforeach

              </ul>
            </div>
            <div id="empsAccordion" style="display: none"  class="content with-padding">
             <form action="javascript:void(0)" method="post" id="save-emps-form">
                <div class="row">

                    <div class="col-xl-6">
                        <div class="submit-field">
                            <h5>{{ __('Company name') }}</h5>
                            <input name="cmpName" id="cmpName" type="text" class="with-border">
                           <small id="cmpName-error"></small>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="submit-field">
                            <h5>{{ __('Post title') }}</h5>
                            <input name="pTitle" id="pTitle" type="text" class="with-border">
                           <small id="pTitle-error"></small>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="submit-field">
                            <h5>{{ __('Start date') }}</h5>
                            <input name="start" id="start" type="date" class="with-border">
                         <small id="start-error"></small>
                        </div>
                        <div class="switches-list">
                      <div class="switch-container">
                          <label class="switch">
                          <input id="switchToCurrent" type="checkbox" >
                          <span class="switch-button"></span>Present?</label>
                      </div>
                      </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="submit-field">
                            <h5>{{ __('End date') }}</h5>
                            <input name="end" id="end" type="date" class="with-border">
                             <small id="end-error"></small>

                        </div>
                    </div>

                    <div class="col-xl-5">
                        <div class="submit-field">
                            <h5>{{ __('Describe your experience') }}</h5>
                            <textarea name="desc" id="desc" cols="30" rows="3"></textarea>
                             <small id="desc-error"></small>
                        </div>
                    </div>

                </div>
                <button type="submit" id="save-emps" form="save-emps-form" class="button ripple-effect big" style="margin-bottom: -20px">{{ __('Save Changes') }}</button>
                </form>
            </div>

        </div>

    </div>


</div>

<script>
    $( "#save-emps-form" ).submit(function( event ) {
        event.preventDefault();
        $('#save-emps').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('#save-emps').css('opacity','0.5');
        $('#save-emps').css('cursor','not-allowed');
        $("#save-emps").attr("disabled", true);
        $('#cmpName-error').text('')
        $('#pTitle-error').text('')
        $('#start-error').text('')
        $('#end-error').text('')
        $('#desc-error').text('')
        var data = {
            'cmpName' : $('#cmpName').val(),
            'pTitle' : $('#pTitle').val(),
            'start' : $('#start').val(),
            'desc' : $('#desc').val()
        }

        if($('#switchToCurrent').is(':checked')) {
            data.present = 1
        }else {
            data.present = 0,
            data.end = $('#end').val()
        }

        $.post('{{ route('apiHandleSaveEmployment') }}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                 $('#save-emps').html('Save Changes');
                 $('#save-emps').css('opacity','1');
                 $('#save-emps').css('cursor','pointer');
                 $("#save-emps").attr("disabled", false);
                 if(res.errors) {
                        if(res.errors.cmpName) {
                            $('#cmpName-error').text(res.errors.cmpName[0])
                        }
                        if(res.errors.pTitle) {
                            $('#pTitle-error').text(res.errors.pTitle[0])
                        }
                        if(res.errors.start) {
                            $('#start-error').text(res.errors.start[0])
                        }
                        if(res.errors.end) {
                            $('#end-error').text(res.errors.end[0])
                        }
                        if(res.errors.desc) {
                            $('#desc-error').text(res.errors.desc[0])
                        }
                 } else {
                     var newEmp = '  <li id="emp-'+res.emp.id+'" style=" border-bottom: 1px solid #ddd">\n' +
                         '                <div class="job-listing">\n' +
                         '                  <div class="job-listing-details">\n' +
                         '                    <a href="#" class="job-listing-company-logo">\n' +
                         '                      <img src="{{ asset('frontOffice/images/company-logo-placeholder.png') }}" alt="'+ res.emp.company +'">\n' +
                         '                    </a>\n' +
                         '                    <div class="job-listing-description">\n' +
                         '                      <h3 class="job-listing-title"><a href="#">' + res.emp.position + '</a></h3>\n' +
                         '                      <div class="job-listing-footer">\n' +
                         '                        <ul>\n' +
                         '                          <li><i class="icon-material-outline-business"></i>' + res.emp.company + '</li>\n' +
                         '                          <li><i class="icon-material-outline-access-time"></i>From '+ res.emp.start +' To '+ res.emp.end +' </li>\n' +
                         '                        </ul>\n' +
                         '                      </div>\n' +
                         '                    </div>\n' +
                         '                  </div>\n' +
                         '                </div>\n' +
                         '                <div class="buttons-to-right">\n' +
                         '                  <a href="#" class="button red ripple-effect ico del-emp" title="Remove" data-id="' + res.emp.id + '" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>\n' +
                         '                </div>\n' +
                         '              </li>';

                         $('#empsList').append(newEmp);
                          $('#cmpName').val('');
                          $('#pTitle').val('');
                          $('#start').val('');
                          $('#end').val('');
                          $('#desc').val('');
                          $('#switchToCurrent').prop('checked', false);
                          if($('#end').prop('disabled')) {
                              $('#end').removeAttr('disabled');
                              $('#end').css('opacity' , '1');
                          }
                     Snackbar.show({
                         text: 'Employment added',
                         pos: 'bottom-right',
                         showAction: false,
                         actionText: "Dismiss",
                         duration: 3000,
                         textColor: '#fff',
                         backgroundColor: '#80e4a1'
                     });
                 }

            })
            .fail(function (res) {

            })
    });

    $('#switchToCurrent').on('change', function () {
        if($(this).is(':checked')) {
             $('#end').attr('disabled' , 'disabled');
             $('#end').css('opacity' , '0.5');
        }else {
             $('#end').removeAttr('disabled');
             $('#end').css('opacity' , '1');
        }

    });

    $('.slideEmps').on('click', function (e) {
      e.preventDefault();
        if($('.slideEmps i').hasClass('icon-material-outline-add')) {
            $('.slideEmps i').removeClass('icon-material-outline-add');
            $('.slideEmps i').addClass('icon-line-awesome-minus');
        }else {
            $('.slideEmps i').addClass('icon-material-outline-add');
            $('.slideEmps i').removeClass('icon-line-awesome-minus');
        }
        $('#empsAccordion').slideToggle('slow');
    });

    $(document).on('click', '.del-emp', function () {
            var id = $(this).data('id');
            $.get('{{ route('apiHandleDeleteEmployment') }}?id='+id).done( function (res) {
                   if(res.error) {
                       Snackbar.show({
                           text: 'Employment not found!',
                           pos: 'bottom-right',
                           showAction: false,
                           actionText: "Dismiss",
                           duration: 3000,
                           textColor: '#fff',
                           backgroundColor: '#d9564c'
                       });
                   }
                  $('#emp-'+id).hide();

                    Snackbar.show({
                        text: 'Employment deleted',
                        pos: 'bottom-right',
                        showAction: false,
                        actionText: "Dismiss",
                        duration: 3000,
                        textColor: '#fff',
                        backgroundColor: '#80e4a1'
                    });


            })
    })
</script>
