@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Interviews')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

<style type="text/css">

.pending {
    background-color: #e8de5c;
    color: #fff;
    border-radius: 4px;
    line-height: 20px;
    padding: 4px 8px;
    font-size: 13px;
    position: relative;
    top: -1px;
}

</style>
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('Manage Interviews') }}</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('Manage Interviews') }}</li>
                        </ul>
                    </nav>
                </div>

                        <div class="col-xl-12">
                    <div class="dashboard-box">
                        <div class="headline">
                            <h3><i class="icon-feather-calendar"></i> {{ __('Interviews')}}</h3>
                        </div>
                        <div class="content">
                            <ul class="dashboard-box-list">
                                @if(Auth::user()->type == 0)
                                    @if(count($interviews) == 0)

                                        <li>{{ __("You haven't scheduled any interview yet!") }}</li>
                                    @else

                                        @foreach($interviews as $interview)
                                            <li>
                                                <div class="invoice-list-item">
                                                    <strong><a href="{{ route('showJobDetails', $interview->job->getJobParamTitle()) }}" style="color : black">{{ $interview->job->title }}</a></strong>
                                                    <ul>
                                                        <li><span class="{{ $interview->status == 0? 'pending' : 'paid' }}">{{ __($interview->status == 0? 'Pending' : 'Confirmed') }}</span></li>
                                                        <li><i class="icon-feather-user"></i><a style="color : #909090" href="{{ route('showFreelancerDetails', $interview->interviewedUser()->getFreelancerUsername() ) }}"> {{ $interview->interviewedUser()->getFullName() }}</a></li>
                                                        @if($interview->status == 0)
                                                            <li>
                                                                <a href="#" class="slideDates" data-id={{ $interview->id }}>{{ __('See dates') }}</a>
                                                            </li>
                                                                <div class="list-1-{{ $interview->id }}" style="display: none;margin-top: 20px">
                                                            <ul class="list-1" >
                                                                @foreach($interview->dates as $date)
                                                                    <li> {{ $date->date->format('d M, Y') }} {{ __('At')}} {{ $date->date->format('H:i').'H' }} </li>
                                                                @endforeach
                                                            </ul>
                                                                </div>
                                                        @else

                                                            <li>{{ __('Confirmed date:').' '. getConfirmedDate($interview) }}</li>
                                                            <li>
                                                                <div class="switch-container">
                                                                    <label class="switch"><input type="checkbox" id="emailAlerts"><span class="switch-button"></span><span class="switch-text">
                                {{ __('Enable e-mail notifications?') }}</span></label>
                                                                </div>
                                                            </li>

                                                        @endif

                                                    </ul>
                                                </div>
                                                <!-- Buttons -->
                                                @if($interview->status == 0)
                                                    <div class="buttons-to-right">
                                                        <a href="#" data-id="{{ $interview->id }}" class="button gray cancelInterview">{{ __('Cancel interview') }}</a>
                                                    </div>
                                                @endif
                                            </li>
                                        @endforeach
                                    @endif
                                    @else
                                    @if(count($interviewedInterviews) == 0)

                                        <li>{{ __("You haven't any interviews yet!") }}</li>
                                    @else
                                        @foreach($interviewedInterviews as $interview)
                                            <li>
                                                <div class="invoice-list-item">
                                                    <strong><a href="{{ route('showJobDetails', $interview->job->getJobParamTitle()) }}" style="color : black">{{ $interview->job->title }}</a></strong>
                                                    <ul>
                                                        <li><span class="{{ $interview->status == 0? 'pending' : 'paid' }}">{{ __($interview->status == 0? 'Pending' : 'Confirmed') }}</span></li>
                                                        <li><i class="icon-material-outline-business"></i><a style="color : #909090" href="{{ route('showCompanyDetails', $interview->job->company->getCompanyParamName() ) }}"> {{ $interview->job->company->name }}</a></li>
                                                        @if($interview->type == 1)
                                                            <li>
                                                               {{ __('Possible online')}}
                                                            </li>
                                                            @endif
                                                        @if($interview->status == 0)

                                                            <li>
                                                                <a href="#" class="slideDates" data-id={{ $interview->id }}>{{ __('See details') }}</a>
                                                            </li>
                                                        <div class="list-1-{{ $interview->id }} row"  style="display: none;margin-top: 20px">
                                                            <div class="col-xl-6">
                                                                <h4>{{ __('Suggested dates') }}</h4>
                                                                <ul class="list-1" style="margin-top: 20px">
                                                                    @foreach($interview->dates as $date)
                                                                        <li> {{ $date->date->format('d M, Y') }} {{ __('At')}} {{ $date->date->format('H:i').'H' }}
                                                                            <a href="{{ route('handleConfirmInterviewDate') }}?iid={{ $interview->id }}&did={{ $date->id }}">Confirm date</a> </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <h4>{{ __("Employer's notes") }}</h4>
                                                                <p style="margin-top: 20px">
                                                                    @if($interview->note)
                                                                        {{ $interview->note }}
                                                                        @else
                                                                        {{ __('No specified notes.') }}
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                        @else

                                                            <li>{{ __('Confirmed date:').' '. getConfirmedDate($interview) }}</li>
                                                            <li>
                                                                <div class="switch-container">
                                                                    <label class="switch"><input type="checkbox" id="emailAlerts"><span class="switch-button"></span><span class="switch-text">
                                {{ __('Enable e-mail notifications?') }}</span></label>
                                                                </div>
                                                            </li>

                                                        @endif

                                                    </ul>
                                                </div>
                                                <!-- Buttons -->
                                           {{--     @if($interview->status == 0)
                                                    <div class="buttons-to-right">
                                                        <a href="#" data-id="{{ $interview->id }}" class="button gray cancelInterview">{{ __('Cancel interview') }}</a>
                                                    </div>
                                                @endif--}}
                                            </li>
                                        @endforeach
                                    @endif

                                @endif
                            </ul>
                        </div>
                    </div>
                </div>


                @include('frontOffice.inc.small-footer')

            </div>
        </div>
        <!-- Dashboard Content / End -->
    </div>
<script type="text/javascript">

$('.slideDates').on('click', function(e) {
e.preventDefault();
var id = $(this).data('id');

$('.list-1-'+id).slideToggle('slow');
});

$('.cancelInterview').on('click', function(e) {
    e.preventDefault();
    var iId = $(this).data('id');
        $.confirm({
              title: 'Confirm!',
              content: 'Are you sure you want to delete this interview?',
              buttons: {
                  cancel: function () {
                  },
                  somethingElse: {
                      text: 'Yes',
                      btnClass: 'button ripple-effect',
                      keys: ['enter', 'shift'],
                      action: function(){
                        deleteForm('{{route('handleDeleteInterview')}}', {
                          iId : iId
                        }, 'post');
                    }
                  }
              }
          });

})

</script>
@endsection
