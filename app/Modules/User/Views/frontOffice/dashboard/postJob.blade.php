@extends('frontOffice.layout',['class' => 'gray', 'title' => __('Post New Job')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
  <div class="dashboard-content-inner" >

    <!-- Dashboard Headline -->
    <div class="dashboard-headline">
      <h3>{{ __('Post a Job') }}</h3>

      <!-- Breadcrumbs -->
      <nav id="breadcrumbs" class="dark">
        <ul>
            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
          <li>{{ __('Post a Job') }}</li>
        </ul>
      </nav>
    </div>
      <form action="{{route('handlePostJob')}}" method="post" enctype="multipart/form-data">
      @csrf
    <!-- Row -->
    <div class="row">

      <!-- Dashboard Box -->
      <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

          <!-- Headline -->
            <div class="headline">
            <div class="row">

                <div class="col-xl-3">
                        <h3><i class="icon-feather-folder-plus"></i> {{ __('Post a job') }}</h3>
                </div>
            </div>
            <div class="row">
                <div  class="col-xl-6 margin-top-30">
                    <h3 class="margin-bottom-10">Company:</h3>
                    <div class="submit-field">
                        <select name="company" class="selectpicker with-border" data-size="7" title="{{ __('Select company') }}">
                            @foreach (Auth::user()->companies as $company)
                                <option value="{{$company->id}}" selected>{{ $company->name}}</option>

                            @endforeach
                        </select>
                        @if ($errors->has('company'))
                            <small>{{ $errors->first('company') }}</small>
                        @endif
                    </div>
                </div>
            </div>
            </div>


          <div class="content with-padding padding-bottom-10">
            <div class="row">

              <div class="col-xl-12">
                <div class="submit-field">
                  <h5>{{ __('Job Title') }}</h5>
                  <input name="title" value="{{ old('title') }}" type="text" class="with-border">
                    @if ($errors->has('title'))
                        <small>{{ $errors->first('title') }}</small>
                    @endif
                </div>
              </div>

              <div class="col-xl-6">
                <div class="submit-field">
                  <h5>{{ __('Job Type') }}</h5>
                  <select id="typeSelect" name="type" class="selectpicker with-border" data-size="7" title="{{ __('Select Job Type') }}">
                    <option value="0">{{ __('Full Time') }}</option>
                    <option value="1">{{ __('Part Time') }}</option>
                    <option value="2">{{ __('Internship') }}</option>
                    <option value="3">{{ __('Temporary') }}</option>
                  </select>
                    @if ($errors->has('type'))
                        <small>{{ $errors->first('type') }}</small>
                    @endif
                </div>
              </div>

              <div class="col-xl-6">
                <div class="submit-field">
                  <h5>{{ __('Job Category') }}</h5>
                  <select id="catSelect" name="category" class="selectpicker with-border" data-size="7" title="{{ __('Select Category') }}">
                    @foreach (\App\Modules\General\Models\JobCategory::all() as $category)
                        <option value="{{$category->id}}">{{$category->label}}</option>
                    @endforeach
                  </select>
                    @if ($errors->has('category'))
                        <small>{{ $errors->first('category') }}</small>
                    @endif
                </div>
              </div>

                <div class="col-xl-6">
                    <div class="submit-field">
                        <h5>{{ __('Location') }}  <i class="help-icon" data-tippy-placement="right" title="{{ __("Leave blank if it's a remote job") }}"></i></h5>
                        <div class="input-with-icon">
                            <div id="autocomplete-container">
                                <input value="{{ old('address')}}" id="autocomplete-input" autocomplete="off" type="text" placeholder="{{ __("Leave blank if it's a remote job") }}" name="address">
                                <input value="{{ old('city') }} " type="hidden" name="city" id="city">
                                <input value="{{ old('province')}}" type="hidden" name="province" id="province">
                                <input value="{{ old('lat') }}" type="hidden" name="lat" id="lat">
                                <input value="{{ old('lon') }}" type="hidden" name="lon" id="lon">
                                 @if ($errors->has('address') || $errors->has('lon'))
                                  <small>{{ __('Please pick your address from the suggestions!') }}</small>
                                 @endif
                            </div>
                            <i class="icon-material-outline-location-on"></i>
                        </div>
                    </div>
                </div>

              <div class="col-xl-6">
                <div class="submit-field">
                  <h5>Salary</h5>
                  <div class="row">
                    <div class="col-xl-6">
                      <div class="input-with-icon">
                        <input name="min_salary" value="{{old('min_salary')}}" class="with-border" type="text" placeholder="{{ __('Min') }}">
                        <i class="currency">TND</i>
                      </div>
                        @if ($errors->has('min_salary'))
                            <small>{{ $errors->first('min_salary') }}</small>
                        @endif
                    </div>
                    <div class="col-xl-6">
                      <div class="input-with-icon">
                        <input name="max_salary" value="{{old('max_salary')}}" class="with-border" type="text" placeholder="{{ __('Max') }}">
                        <i class="currency">TND</i>
                      </div>
                        @if ($errors->has('max_salary'))
                            <small>{{ $errors->first('max_salary') }}</small>
                        @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-12">
                <div class="submit-field">
                  <h5>{{ __('Post Requirements') }} <span>({{ __('optional') }})</span>  <i class="help-icon" data-tippy-placement="right" title="{{ __('Maximum of 6 tags') }}"></i><small id="maxTags"></small></h5>
                  <div class="keywords-container">
                    <div class="keyword-input-container">
                      <input type="text" id="tagsInput" class="keyword-input with-border" placeholder="{{ __('e.g. you must be fluent with Adobe XD...') }}"/>
                        <input value="{{ old('tags') }}" type="hidden" id="jobTags" name="tags">
                          <a class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></a>
                    </div>

                      @php
                          $oldTags = null;
                          if(old('tags'))  $oldTags = explode(',',old('tags'));
                      @endphp
                      <div class="keywords-list">
                          @if($oldTags)
                              @foreach($oldTags as $tag)
                                  <span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>{{ $tag }}</span></span>
                              @endforeach
                          @endif
                      </div>
                    <div class="clearfix"></div>
                  </div>

                </div>
              </div>

              <div class="col-xl-12">
                <div class="submit-field">
                  <h5>{{ __('Job Description') }}</h5>
                  <textarea name="description" cols="30" rows="5" class="with-border">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                        <small>{{ $errors->first('description') }}</small>
                    @endif
                    <div class="uploadButton margin-top-30">
                        <input name="attachments[]" class="uploadButton-input" type="file" id="upload" accept=".pdf, .csv,.xls,.xlsx, image/*" multiple/>
                        <label class="uploadButton-button ripple-effect" for="upload">{{ __('Upload Files') }}</label>
                        @if(count($errors) > 0)
                            <span style="font-size: 18px" class="uploadButton-file-name">{{ __('Please check your files, you may need to re-upload them!') }}</span>
                        @else
                            <span class="uploadButton-file-name">{{ __('Images or documents that might be helpful in describing your project') }} <small id="uploadErrors"> &nbsp;({{ __('Each file size must be less than 10 MB') }})</small></span>
                        @endif
                    </div>
                    <div id="attachments" class="attachments-container margin-top-0 margin-bottom-0">
                    </div>
                    @if ($errors->has('attachments'))
                        <small>{{ $errors->first('attachments') }}</small>
                    @endif
                    @for($i = 0; $i<3 ; $i++ )
                        @if ($errors->has('attachments.'.$i))
                            <small>{{ 'File '.($i + 1).' :'. $errors->first('attachments.'.($i + 1)).' | ' }}</small>
                        @endif
                    @endfor
                    <button class="button red" id='clearFilesInput' style="display: none; padding: 5px 10px 5px 10px">Clear files</button>

                </div>

              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-12">
        <button type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> {{ __('Post a Job') }}</button>
      </div>

    </div>
    <!-- Row / End -->
      </form>

      @include('frontOffice.inc.small-footer')

  </div>
</div>
<!-- Dashboard Content / End -->

</div>


    <script>
        $(document).ready(function() {
            var oldCategory = '{{ old('category') }}';

            if(oldCategory !== '') {
                $('select #catSelect').val(oldCategory);
            }
        });

        $(document).ready(function() {
            var oldType = '{{ old('type') }}';

            if(oldType !== '') {
                $('select #typeSelect').val(oldType);
            }
        });

        $('#tagsInput').focus(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

        });

        $('#tagsInput').focusout(function() {
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    $('form').submit();

                }
            });
        });

        $(".keywords-container").each(function() {
            var tags = []  ;
            var oldTags;

            @if( old('tags'))
                oldTags = '{{  old('tags') }}'
                tags = oldTags.split(',');
                @endif


            var keywordInput = $(this).find(".keyword-input");
            var keywordsList = $(this).find(".keywords-list");

            function addKeyword() {
                if(tags.length < 6 ) {
                    var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
                    keywordsList.append($newKeyword).trigger('resizeContainer');
                    tags.push(keywordInput.val());
                    $('#jobTags').val(tags);
                }else {
                    $('#maxTags').text('You can just add 6 tags !').fadeIn('slow').delay(2000).fadeOut();
                }
                keywordInput.val("");
            }
            keywordInput.on('keyup', function(e) {
                if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $('.keyword-input-button').on('click', function() {
                if ((keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $(document).on("click", ".keyword-remove", function() {
                $(this).parent().addClass('keyword-removed');
                function removeFromMarkup() {
                    tags = $.grep(tags, function(value) {
                        return value !=  $(".keyword-removed").text();
                    });
                    $('#jobTags').val(tags);
                    $(".keyword-removed").remove()
                }
                setTimeout(removeFromMarkup, 500);
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            keywordsList.on('resizeContainer', function() {
                var heightnow = $(this).height();
                var heightfull = $(this).css({
                    'max-height': 'auto',
                    'height': 'auto'
                }).height();
                $(this).css({
                    'height': heightnow
                }).animate({
                    'height': heightfull
                }, 200);
            });
            $(window).on('resize', function() {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            $(window).on('load', function() {
                var keywordCount = $('.keywords-list').children("span").length;
                if (keywordCount > 0) {
                    keywordsList.css({
                        'height': 'auto'
                    }).height();
                }
            });
        });

         function clearFileInput(ctrl) {
        try {
            ctrl.value = null;
        } catch(ex) { }
        if (ctrl.value) {
            ctrl.parentNode.replaceChild(ctrl.cloneNode(true), ctrl);
        }
        }

        $('#clearFilesInput').on('click', function (e){
            e.preventDefault();
            $('#attachments').html('');
            clearFileInput(document.getElementById("upload"));
            $(this).css({'display':'none'})
        })

        $('#upload').on('change', function () {
          $('#attachments').html('');
            $('#clearFilesInput').css({'display':'block'})
            var files = $(this)[0].files;
            if(files.length > 3) {
                alert('Only 3 files are allowed!')
                $('#uploadErrors').text(' Only 3 files are allowed!').fadeIn('slow').delay(3000).fadeOut();
            } else
                $.each(files, function (i, file) {
                    $('#attachments').append('<div class="attachment-box ripple-effect">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t\t<span>'+ file.name +'</span> \n' +
                        '\t\t\t\t\t\t\t\t\t\t\t\t </div>')
                })
        })

    </script>
    <script type="text/javascript">
        function initAutocomplete() {
            var city, street, place, state, code, sublocality = ' ';
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var placeUser = autocomplete.getPlace();
                var lat = placeUser.geometry.location.lat();
                var lng = placeUser.geometry.location.lng();
                for (var i = 0; i < placeUser.address_components.length; i++) {
                    var addressType = placeUser.address_components[i]["types"][0];
                    console.log(addressType);
                    switch (addressType) {
                        case 'route':
                            street = placeUser.address_components[i]['long_name'];

                            break;
                        case 'administrative_area_level_1':
                            place = placeUser.address_components[i]['long_name'];
                            break;
                        case 'locality':
                            city = placeUser.address_components[i]['long_name'];
                            break;
                        case 'sublocality_level_1':
                            sublocality = placeUser.address_components[i]['long_name'];
                            break;
                        case 'country':
                            country = placeUser.address_components[i]['long_name'];
                            break;
                    }


                }
                $('#city').val(city);
                $('#province').val(sublocality);
                $('#lat').val(lat);
                $('#lon').val(lng);

                var myLatLng = {
                    lng: lng,
                    lat: lat,
                };
                var single_map = new google.maps.Map(document.getElementById('singleMap'), {
                    zoom: 18,
                    center: myLatLng,
                    scrollwheel: false,
                    zoomControl: true,
                    mapTypeControl: false,
                    scaleControl: false,
                    panControl: false,
                    navigationControl: false,
                    streetViewControl: false,
                    styles: [{
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [{
                            "color": "#f2f2f2"
                        }]
                    }]
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    draggable: true,
                    map: single_map,
                    title: 'Your location'
                });

                google.maps.event.addListener(single_map, 'click', function(event) {



                    marker.setPosition(event.latLng);
                    var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                    var location	= new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());		// turn coordinates into an object

                    geocoder.geocode({'latLng': location}, function (results, status) {


                        $('#autocomplete-input').val("");
                        $('#city').val("");
                        $('#province').val("");
                        $('#zip').val("");
                        $('#lat').val("");
                        $('#lon').val("");
                        $('#autocomplete-input').val(results[1].formatted_address);

                        if (results[0].address_components[3]) {
                            $('#city').val(results[0].address_components[3].long_name);
                        }else {
                            $('#city').val(results[0].address_components[2].long_name);
                        }

                        $('#province').val(results[0].address_components[2].long_name);
                        $('#lat').val(event.latLng.lat());
                        $('#lon').val(event.latLng.lng());
                    });

                });

                if (!placeUser.geometry) {
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
            });
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

@endsection
