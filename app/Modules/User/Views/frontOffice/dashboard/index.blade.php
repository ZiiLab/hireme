@extends('frontOffice.layout',['class' => 'gray','title' =>__('Dashboard')])
@section('header')
      @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')


  <!-- Dashboard Container -->
<div class="dashboard-container">
  <!-- Dashboard Sidebar
================================================== -->

  @include('User::frontOffice.dashboard.inc.sidebar')

<!-- Dashboard Sidebar / End -->

<!-- Dashboard Content
================================================== -->
<div class="dashboard-content-container" data-simplebar>
<div class="dashboard-content-inner">

<!-- Dashboard Headline -->
<div class="dashboard-headline">
  <h3>{{ __('Hello,') }} {{ Auth::user()->first_name ?? '' }}</h3>
  <span>{{ __('We are glad to see you again!') }}</span>

  <!-- Breadcrumbs -->
  <nav id="breadcrumbs" class="dark">
    <ul>
      <li><a href="{{ route('showHomePage') }}">{{__('Home')}}</a></li>
      <li>{{ __('Dashboard') }}</li>
    </ul>
  </nav>
</div>

<!-- Fun Facts Container -->
    @if(Auth::user()->type == 0)
        <div class="row">
        <div class="col-xl-12">
            <div class="fun-facts-container">
            <div class="fun-fact" data-fun-fact-color="#36bd78">
                <div class="fun-fact-text">
                    <span>{{ __('Bids') }}</span>
                    @php
                    $totalBids = 0;
                    foreach(Auth::user()->tasks as $task)
                      $totalBids += count($task->bids);
                    @endphp
                    <h4>{{ $totalBids }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>
            </div>
            <div class="fun-fact" data-fun-fact-color="#b81b7f">
                <div class="fun-fact-text">
                    <span>{{ __('Applications') }}</span>
                    @php
                        $totalApplications = 0;
                        foreach(Auth::user()->jobs as $job)
                          $totalApplications += count($job->applications);
                    @endphp
                    <h4>{{ $totalApplications }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
            </div>
            <div class="fun-fact" data-fun-fact-color="#efa80f">
                <div class="fun-fact-text">
                    <span>Reviews</span>
                    @php
                    $totalReviews = 0;
                    foreach(Auth::user()->companies as $company)
                          $totalReviews += count($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company'));
                    @endphp
                    <h4>{{ $totalReviews }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
            </div>

        </div>
        </div>
        </div>
    @else

       <div class="row">
          <div class="fun-facts-container">
            <div class="fun-fact" data-fun-fact-color="#36bd78">
                <div class="fun-fact-text">
                    <span>{{ __('Bids') }}</span>
                    <h4>{{ count(Auth::user()->bids) }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-gavel"></i></div>
            </div>
            <div class="fun-fact" data-fun-fact-color="#b81b7f">
                <div class="fun-fact-text">
                    <span>{{ __('Applications') }}</span>
                    <h4>{{ count(Auth::user()->candidates) }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
            </div>
            <div class="fun-fact" data-fun-fact-color="#efa80f">
                <div class="fun-fact-text">
                    <span>{{ __('Reviews') }}</span>
                    <h4>{{ count(Auth::user()->getAllRatings(Auth::id(),'desc','App\Modules\User\Models\User')) }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
            </div>

        </div>
       </div>
    @endif

<!-- Row -->
<div class="row">




        @if(Auth::user()->type == 1)
        <div class="col-xl-6">
            <div class="dashboard-box ">

                <!-- Headline -->
                <div class="headline">
                    <h3><i class="icon-material-outline-history"></i> {{ __('Applications History') }}</h3>
                </div>

                <div class="content">
                    <ul class="dashboard-box-list">
                        @if(count(Auth::user()->candidates) != 0)
                        @foreach (Auth::user()->candidates()->take(5)->get() as $application)
                            <li>
                                <!-- Job Listing -->
                                <div class="job-listing">

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Logo -->
                                        <a href="{{ route('showJobDetails', $application->job->getJobParamTitle()) }}" class="job-listing-company-logo">
                                            <img src="{{asset($application->job->company->medias()->where('type',0)->first() ? $application->job->company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png')}}" alt="{{$application->job->company->name}}">
                                        </a>

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title"><a href="{{ route('showJobDetails', $application->job->getJobParamTitle()) }}">{{ $application->job->title }}</a></h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-material-outline-location-on"></i>{{ $application->job->company->address->city }}</li>
                                                    <li><i class="icon-material-outline-business-center"></i> {{ jobTypeToString($application->job->type) }} </li>
                                                    <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($application->created_at)->diffForHumans() }} </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </li>
                        @endforeach
                            @else
                            <li>   <div class="job-listing"> <p> {{ __('No Application found yet!') }}</p> </div>  </li>
                           @endif
                    </ul>
                </div>
            </div>
        </div>
            @endif
    <div class="col-xl-6">
        <div class="dashboard-box">
            <div class="headline">
                <h3><i class="icon-material-baseline-notifications-none"></i> {{ __('Notifications') }}</h3>
                <button class="mark-as-read ripple-effect-dark" data-tippy-placement="left" title="{{ __('Mark all as read') }}">
                    <i class="icon-feather-check-square"></i>
                </button>
            </div>
            <div class="content">
                <ul class="dashboard-box-list NotificationsList">
                        @if(count(Auth::user()->notifications()->where('status',0)->get()) == 0)
                        <li> <p> {{ __('You have no new notifications!') }}</p></li>
                        @else
                        @foreach ( Auth::user()->notifications()->where('status',0)->orderBy('created_at','desc')->get() as $notif )
                            	<li>
									<span class="notification-icon"><i class="icon-material-baseline-notifications-none"></i></span>
									<span class="notification-text">
										<strong>{{ $notif->sender->getFullName() }}</strong> {{ $notif->content }} <a href="{{ $notif->route }}">{{ $notif->event }}</a>
									</span>
							
                                </li>
                                
                        @endforeach   
                        @endif       
                </ul>
            </div>
        </div>
    </div>
  <div class="col-xl-6">


    <div class="dashboard-box child-box-in-row">
      <div class="headline">
        <h3><i class="icon-material-outline-note-add"></i> {{ __('Notes') }}</h3>
      </div>

      <div  class="content with-padding notesList" style="overflow-y: scroll; height: 400px">
        <!-- Note -->
          @if(count(Auth::user()->notes) == 0)
              <div class="dashboard-note noNotes">
                  <p>{{ __('You have no notes yet! start adding some to keep things in order.') }}</p>
              </div>
          @else
              @foreach(Auth::user()->notes()->orderBy('created_at','desc')->take(10)->get() as $note)
        <div class="dashboard-note">
          <p>{{$note->body}}</p>
          <div class="note-footer">
            <span class="note-priority @if($note->priority == 0) low @elseif ($note->priority == 1) medium @else high @endif">@if($note->priority == 0) {{ __('Low priority') }} @elseif ($note->priority == 1) {{ __('Medium priority') }} @else {{ __('High priority') }} @endif</span>
            <div class="note-buttons">
              <a href="#" title="Remove" data-tippy-placement="top" class="deleteNote" data-id="{{ $note->id }}"><i class="icon-feather-trash-2"></i></a>
            </div>
          </div>
        </div>
              @endforeach
          @endif
        <!-- Note -->


      </div>
                <div class="add-note-button">
                    @if(count(Auth::user()->notes) < 10)

                    <a href="#small-dialog" class="popup-with-zoom-anim button full-width button-sliding-icon showAddNote">{{ __('Add Note') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                    <a class="button full-width button-sliding-icon maxNotesNotif" style="font-size: 13px; color: white; background-color: #de5959; display: none">{{ __('You have reached 10 notes, delete some to be able to add new one!') }}</a>
                    @else
                        <a style="display: none" href="#small-dialog" class="popup-with-zoom-anim button full-width button-sliding-icon showAddNote">{{ __('Add Note') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                        <a class="button full-width button-sliding-icon maxNotesNotif" style="font-size: 13px; color: white; background-color: #de5959">{{ __('You have reached 10 notes, delete some to be able to add new one!') }}</a>

                    @endif
                </div>




    </div>


    <!-- Dashboard Box / End -->
  </div>

</div>
<!-- Row / End -->

<!-- Row -->

  <!-- Dashboard Box -->



<!-- Row / End -->

@include('frontOffice.inc.small-footer')

</div>
</div>
<!-- Dashboard Content / End -->

</div>

  {{-- 

      var hiredNotification = localStorage.getItem("hiredNotificationContent");
      $(document).ready(function () {
       if(hiredNotification == null) {
           $('.NotificationsList').append(' <li> <p> {{ __('You have no new notifications!') }}</p></li>');
       }else {
       $('.NotificationsList').html(hiredNotification);
       }
    
      });--}}
<script>

      $('.mark-as-read').on('click', function () {
          // localStorage.removeItem('hiredNotificationContent');
          // localStorage.removeItem('hiredNotifications');
            $('.hiredNotifications').html('       <li class="notifications-not-read noHiredNotifs">\n' +
                '                        <a href="#">\n' +
                '                          <span class="notification-text">\n' +
                '                          No new notifications found!\n' +
                '                        </span>\n' +
                '                        </a>\n' +
                '                      </li>');
            $('.hiredNotificationsCount').data('count',0);
            $('.hiredNotificationsCount').text('0');
          $('.NotificationsList').html(' <li> <p> {{ __('You have no new notifications!') }}</p></li>');
          $.get('{{ route('apiReadNotifications') }}').done(function (res) {
              
          }).fail(function (err) {
              
          });
      })

  </script> 


<!-- Dashboard Container / End -->
  @include('User::frontOffice.dashboard.modals.addNote')

@endsection
