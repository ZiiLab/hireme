@extends('frontOffice.layout',['class' => 'gray','title' => __('My InProgress Tasks')])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        <!-- Dashboard Sidebar
      ================================================== -->

    @include('User::frontOffice.dashboard.inc.sidebar')

    <!-- Dashboard Sidebar / End -->

        <!-- Dashboard Content
            ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>{{ __('In Progress Tasks') }}</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                            <li><a href="{{ route('showDashboard') }}">{{ __('Dashboard') }}</a></li>
                            <li>{{ __('In Progress Tasks') }}</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-assignment"></i>{{ __('My In Progress Tasks') }}</h3>
                            </div>

                            <div class="content">
                                <ul class="dashboard-box-list">
                                    @foreach(Auth::user()->bids()->whereHas('accepted')->get() as $bid)
                                        @php
                                           $task = $bid->task;
                                            $endDate = Carbon\Carbon::parse($task->created_at)->addDays($bid->delivery_time);
                                            $left = $endDate->diffInDays(Carbon\Carbon::now());
                                            $leftInHours = null;
                                            if($left == 0 ) {
                                                 $leftInHours = $endDate->diffInHours(Carbon\Carbon::now());
                                            }
                                        @endphp
                                        <li>
                                            <!-- Job Listing -->
                                            <div class="job-listing width-adjustment">

                                                <!-- Job Listing Details -->
                                                <div class="job-listing-details">

                                                    <!-- Details -->
                                                    <div class="job-listing-description">
                                                        <h3 class="job-listing-title"><a href="{{ route('showTaskDetails', $bid->task->getTaskParamName()) }}">{{$task->name}}</a>  @if($leftInHours) <span class="dashboard-status-button yellow">{{ __('Expiring') }}</span> @endif </h3>
                                                        <div class="freelancer-name" style="margin-top: 0px">
                                                            @if($bid->task->company)
                                                            <a>
                                                                <i class="icon-material-outline-business"></i> <a href="{{ route('showCompanyDetails' , $bid->task->company->getCompanyParamName()) }}">{{$bid->task->company->name}}</a>
                                                            </a>
                                                             @else
 <a>
                                                                <i class="icon-line-awesome-user"></i> <a href="{{ route('showFreelancerDetails',$bid->task->user->getFreelancerUsername() ) }}">{{$bid->task->user->getFullName()}}</a>
                                                            </a>

                                                             @endif
                                                        </div>
                                                        <!-- Job Listing Footer -->
                                                        <div class="job-listing-footer">
                                                            <ul>
                                                                <li><i class="icon-material-outline-access-time"></i>   {{ $leftInHours ? $leftInHours.' Hours Left' : $left.' Days Left' }} </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Task Details -->
                                            <ul class="dashboard-task-info">
                                                <li><strong>{{ __('Budget') }}</strong><span>{{ $bid->rate.' TND' }}</span></li>
                                            </ul>

                                            <!-- Buttons -->
                                            <div class="buttons-to-right always-visible">
                                                <a href="#small-dialog-2" class="popup-with-zoom-anim button dark ripple-effect sendDirectMessage" data-name="{{ $bid->task->user->getFullName() }}" data-id="{{$bid->task->user->id}}"><i class="icon-feather-mail"></i> {{ __('Send message to employer') }}</a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row / End -->

                @include('frontOffice.inc.small-footer')
                @include('User::frontOffice.dashboard.modals.sendDirectMessageBiddersPopup')

            </div>
        </div>
        <!-- Dashboard Content / End -->

    </div>
     <script type="text/javascript">
         $('.sendDirectMessage').on('click', function () {
          $('#bidder_name').text($(this).data('name'));
          $('#receiverId').val($(this).data('id'));
      })
    </script>
@endsection


