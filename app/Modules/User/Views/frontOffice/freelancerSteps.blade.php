@extends('frontOffice.layout',['class' =>'gray','title' => __('Registration Steps')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')

    <style>
      .countrySelect >  .dropdown-menu{
            height: 350px;
            overflow: scroll;
        }
        a.btn_width{
            width: 330px !important;
        }

        @media screen and (max-width: 768px) {
            a.btn_width {
                width: 210px  !important;
            }
        }
        .error {
            color: red;
        }

        .input-with-icon-right i{
            position: absolute;
            right: 0;
            top: 0;
            color: #a0a0a0;
            text-align: center;
            line-height: 48px;
            width: 48px;
            height: 48px;
            font-size: 15px;
            background-color: #f8f8f8;
            border: 1px solid #e0e0e0;
            box-sizing: border-box;
            display: block;
            border-radius: 0px 4px 4px 0px;
        }
        #singleMap {
            height: 300px;
            width: 100%;
        }

        #msform {
            text-align: center;
            position: relative;
            margin-top: 20px
        }

        #msform .fieldSet {
            background: white;
            border: 0 none;
            border-radius: 0.5rem;
            box-sizing: border-box;
            width: 100%;
            margin: 0;
            padding-bottom: 20px;
            position: relative
        }

        .form-card {
            padding-bottom: 30px;
            text-align: left
        }
        .socialAddBtn {
            position: absolute;
            top: 0;
            right: 0;
            height: 36px;
            width: 36px;
            padding: 0;
            color: #fff;
            background-color: #2a41e8;
            border-radius: 4px;
            margin: 6px;
            font-size: 19px;
            text-align: center;
            line-height: 36px;
        }
        #msform .fieldSet:not(:first-of-type) {
            display: none
        }

        .fieldSet {
            padding: 30px;
            border-radius: 5px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.08);
        }


        #msform .action-button {

            padding: 10px 5px;
            margin: 10px 0px 10px 5px;
            float: right;
            width: 150px;
        }

        #msform .action-button:hover,
        #msform .action-button:focus {
            background-color: #311B92
        }

        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px 10px 0px;
            float: right
        }

        #msform .action-button-previous:hover,
        #msform .action-button-previous:focus {
            background-color: #000000
        }

        .card {
            z-index: 0;
            border: none;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #673AB7;
            margin-bottom: 15px;
            font-weight: normal;
            text-align: left
        }

        .purple-text {
            color: #673AB7;
            font-weight: normal
        }

        .steps {
            font-size: 25px;
            color: gray;
            margin-bottom: 10px;
            font-weight: normal;
            text-align: right
        }

        .fieldlabels {
            color: gray;
            text-align: left
        }

        #progressbar {
            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey;
            padding: 0px;
        }

        #progressbar .active {
            color: black
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 25%;
            float: left;
            position: relative;
            font-weight: 400
        }

        /*
                #progressbar #account:before {
                    font-family: FontAwesome;
                    content: "\f13e"
                }

                #progressbar #personal:before {
                    font-family: FontAwesome;
                    content: "\f007"
                }

                #progressbar #payment:before {
                    font-family: FontAwesome;
                    content: "\f030"
                }

                #progressbar #confirm:before {
                    font-family: FontAwesome;
                    content: "\f00c"
                }
        */

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #2a41e8
        }

        .progress {
            height: 20px
        }

        .progress-bar {
            background-color: #2a41e8
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        .logo-wrapper {
            position: relative;
            width: 150px;
            height: 150px;
            border-radius: 4px;
            overflow: hidden;
            box-shadow: none;
            transition: all .3s ease;
        }
        .logo-wrapper .profile-pic-logo {
            height: 100%;
            width: 100%;
            transition: all .3s ease;
            object-fit: cover;
        }
        .logo-wrapper .upload-button-logo {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
        }
        .logo-wrapper .file-upload-logo {
            opacity: 0;
            pointer-events: none;
            position: absolute;
        }
        .button {
            margin-top: -35px!important;
        }
    </style>



    <div class="col-xl-10 offset-xl-1" style="margin-top: 50px">

        <div class="login-register-page">
            <div class="welcome-text">
                <h3 style="font-size: 26px;">{{ __("Let's create your profile,") }} <strong>{{ __('STEP BY STEP!') }}</strong></h3>
                <span></span>
            </div>
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-xl-10" style="margin-bottom: 50px;">
                        <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                            <div id="msform">
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active icon-feather-settings" id="account"> <strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Profile Infos') }}</strong></li>
                                    <li id="personal" class="icon-feather-flag"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Preferences') }}</strong></li>
                                    <li id="payment" class="icon-material-outline-business"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Employments') }}</strong></li>
                                    <li id="confirm" class="icon-feather-check"><strong style="font-family: nunito,helveticaneue,helvetica neue,Helvetica,Arial,sans-serif;">{{ __('Finish') }}</strong></li>
                                </ul>

                                <div class="fieldSet">
                                    <form id="profileForm">
                                        <div class="form-card">

                                            <div  class="row">

                                                <div class="col-xl-6">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Tagline') }}</h5>
                                                        <input type="text" id="tagline" name="tagline" class="with-border" placeholder="iOS Expert + Node Dev">

                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Nationality') }}</h5>
                                                        <select name="country" id="country" class="selectpicker with-border countrySelect" data-size="7" title="Select your nationality" data-live-search="true">
                                                            <option value="Afghanistan">Afghanistan</option>
                                                            <option value="Åland Islands">Åland Islands</option>
                                                            <option value="Albania">Albania</option>
                                                            <option value="Algeria">Algeria</option>
                                                            <option value="American Samoa">American Samoa</option>
                                                            <option value="Andorra">Andorra</option>
                                                            <option value="Angola">Angola</option>
                                                            <option value="Anguilla">Anguilla</option>
                                                            <option value="Antarctica">Antarctica</option>
                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                            <option value="Argentina">Argentina</option>
                                                            <option value="Armenia">Armenia</option>
                                                            <option value="Aruba">Aruba</option>
                                                            <option value="Australia">Australia</option>
                                                            <option value="Austria">Austria</option>
                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                            <option value="Bahamas">Bahamas</option>
                                                            <option value="Bahrain">Bahrain</option>
                                                            <option value="Bangladesh">Bangladesh</option>
                                                            <option value="Barbados">Barbados</option>
                                                            <option value="Belarus">Belarus</option>
                                                            <option value="Belgium">Belgium</option>
                                                            <option value="Belize">Belize</option>
                                                            <option value="Benin">Benin</option>
                                                            <option value="Bermuda">Bermuda</option>
                                                            <option value="Bhutan">Bhutan</option>
                                                            <option value="Bolivia">Bolivia</option>
                                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                            <option value="Botswana">Botswana</option>
                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                            <option value="Brazil">Brazil</option>
                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                            <option value="Bulgaria">Bulgaria</option>
                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                            <option value="Burundi">Burundi</option>
                                                            <option value="Cambodia">Cambodia</option>
                                                            <option value="Cameroon">Cameroon</option>
                                                            <option value="Canada">Canada</option>
                                                            <option value="Cape Verde">Cape Verde</option>
                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                            <option value="Central African Republic">Central African Republic</option>
                                                            <option value="Chad">Chad</option>
                                                            <option value="Chile">Chile</option>
                                                            <option value="China">China</option>
                                                            <option value="Christmas Island">Christmas Island</option>
                                                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                            <option value="Colombia">Colombia</option>
                                                            <option value="Comoros">Comoros</option>
                                                            <option value="Congo">Congo</option>
                                                            <option value="Congo">Congo</option>
                                                            <option value="Cook Islands">Cook Islands</option>
                                                            <option value="Costa Rica">Costa Rica</option>
                                                            <option value="Cote D'ivoire">Cote D'ivoire</option>
                                                            <option value="Croatia">Croatia</option>
                                                            <option value="Cuba">Cuba</option>
                                                            <option value="Cyprus">Cyprus</option>
                                                            <option value="Czech Republic">Czech Republic</option>
                                                            <option value="Denmark">Denmark</option>
                                                            <option value="Djibouti">Djibouti</option>
                                                            <option value="Dominica">Dominica</option>
                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                            <option value="Ecuador">Ecuador</option>
                                                            <option value="Egypt">Egypt</option>
                                                            <option value="El Salvador">El Salvador</option>
                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                            <option value="Eritrea">Eritrea</option>
                                                            <option value="Estonia">Estonia</option>
                                                            <option value="Ethiopia">Ethiopia</option>
                                                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                            <option value="Fiji">Fiji</option>
                                                            <option value="Finland">Finland</option>
                                                            <option value="France">France</option>
                                                            <option value="French Guiana">French Guiana</option>
                                                            <option value="French Polynesia">French Polynesia</option>
                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                            <option value="Gabon">Gabon</option>
                                                            <option value="Gambia">Gambia</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Germany">Germany</option>
                                                            <option value="Ghana">Ghana</option>
                                                            <option value="Gibraltar">Gibraltar</option>
                                                            <option value="Greece">Greece</option>
                                                            <option value="Greenland">Greenland</option>
                                                            <option value="Grenada">Grenada</option>
                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                            <option value="Guam">Guam</option>
                                                            <option value="Guatemala">Guatemala</option>
                                                            <option value="Guernsey">Guernsey</option>
                                                            <option value="Guinea">Guinea</option>
                                                            <option value="Guinea-bissau">Guinea-bissau</option>
                                                            <option value="Guyana">Guyana</option>
                                                            <option value="Haiti">Haiti</option>
                                                            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                            <option value="Honduras">Honduras</option>
                                                            <option value="Hong Kong">Hong Kong</option>
                                                            <option value="Hungary">Hungary</option>
                                                            <option value="Iceland">Iceland</option>
                                                            <option value="India">India</option>
                                                            <option value="Indonesia">Indonesia</option>
                                                            <option value="Iran, Islamic Republic of">Iran</option>
                                                            <option value="Iraq">Iraq</option>
                                                            <option value="Ireland">Ireland</option>
                                                            <option value="Isle of Man">Isle of Man</option>
                                                            <option value="Israel">Israel</option>
                                                            <option value="Italy">Italy</option>
                                                            <option value="Jamaica">Jamaica</option>
                                                            <option value="Japan">Japan</option>
                                                            <option value="Jersey">Jersey</option>
                                                            <option value="Jordan">Jordan</option>
                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                            <option value="Kenya">Kenya</option>
                                                            <option value="Kiribati">Kiribati</option>
                                                            <option value="Korea, Democratic People's Republic of">Korea</option>
                                                            <option value="Korea, Republic of">Korea, Republic of</option>
                                                            <option value="Kuwait">Kuwait</option>
                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                            <option value="Latvia">Latvia</option>
                                                            <option value="Lebanon">Lebanon</option>
                                                            <option value="Lesotho">Lesotho</option>
                                                            <option value="Liberia">Liberia</option>
                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                            <option value="Lithuania">Lithuania</option>
                                                            <option value="Luxembourg">Luxembourg</option>
                                                            <option value="Macao">Macao</option>
                                                            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia</option>
                                                            <option value="Madagascar">Madagascar</option>
                                                            <option value="Malawi">Malawi</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Maldives">Maldives</option>
                                                            <option value="Mali">Mali</option>
                                                            <option value="Malta">Malta</option>
                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                            <option value="Martinique">Martinique</option>
                                                            <option value="Mauritania">Mauritania</option>
                                                            <option value="Mauritius">Mauritius</option>
                                                            <option value="Mayotte">Mayotte</option>
                                                            <option value="Mexico">Mexico</option>
                                                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                            <option value="Monaco">Monaco</option>
                                                            <option value="Mongolia">Mongolia</option>
                                                            <option value="Montenegro">Montenegro</option>
                                                            <option value="Montserrat">Montserrat</option>
                                                            <option value="Morocco">Morocco</option>
                                                            <option value="Mozambique">Mozambique</option>
                                                            <option value="Myanmar">Myanmar</option>
                                                            <option value="Namibia">Namibia</option>
                                                            <option value="Nauru">Nauru</option>
                                                            <option value="Nepal">Nepal</option>
                                                            <option value="Netherlands">Netherlands</option>
                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                            <option value="New Caledonia">New Caledonia</option>
                                                            <option value="New Zealand">New Zealand</option>
                                                            <option value="Nicaragua">Nicaragua</option>
                                                            <option value="Niger">Niger</option>
                                                            <option value="Nigeria">Nigeria</option>
                                                            <option value="Niue">Niue</option>
                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                            <option value="Norway">Norway</option>
                                                            <option value="Oman">Oman</option>
                                                            <option value="Pakistan">Pakistan</option>
                                                            <option value="Palau">Palau</option>
                                                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                            <option value="Panama">Panama</option>
                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                            <option value="Paraguay">Paraguay</option>
                                                            <option value="Peru">Peru</option>
                                                            <option value="Philippines">Philippines</option>
                                                            <option value="Pitcairn">Pitcairn</option>
                                                            <option value="Poland">Poland</option>
                                                            <option value="Portugal">Portugal</option>
                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                            <option value="Qatar">Qatar</option>
                                                            <option value="Reunion">Reunion</option>
                                                            <option value="Romania">Romania</option>
                                                            <option value="Russian Federation">Russian Federation</option>
                                                            <option value="Rwanda">Rwanda</option>
                                                            <option value="Saint Helena">Saint Helena</option>
                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                            <option value="Saint Lucia">Saint Lucia</option>
                                                            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                            <option value="Samoa">Samoa</option>
                                                            <option value="San Marino">San Marino</option>
                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                            <option value="Senegal">Senegal</option>
                                                            <option value="Serbia">Serbia</option>
                                                            <option value="Seychelles">Seychelles</option>
                                                            <option value="Sierra Leone">Sierra Leone</option>
                                                            <option value="Singapore">Singapore</option>
                                                            <option value="Slovakia">Slovakia</option>
                                                            <option value="Slovenia">Slovenia</option>
                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                            <option value="Somalia">Somalia</option>
                                                            <option value="South Africa">South Africa</option>
                                                            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                            <option value="Spain">Spain</option>
                                                            <option value="Sri Lanka">Sri Lanka</option>
                                                            <option value="Sudan">Sudan</option>
                                                            <option value="Suriname">Suriname</option>
                                                            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                            <option value="Swaziland">Swaziland</option>
                                                            <option value="Sweden">Sweden</option>
                                                            <option value="Switzerland">Switzerland</option>
                                                            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                            <option value="Taiwan, Province of China">Taiwan, Province of China</option>
                                                            <option value="Tajikistan">Tajikistan</option>
                                                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                            <option value="Thailand">Thailand</option>
                                                            <option value="Timor-leste">Timor-leste</option>
                                                            <option value="Togo">Togo</option>
                                                            <option value="Tokelau">Tokelau</option>
                                                            <option value="Tonga">Tonga</option>
                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                            <option value="Tunisia">Tunisia</option>
                                                            <option value="Turkey">Turkey</option>
                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                            <option value="Tuvalu">Tuvalu</option>
                                                            <option value="Uganda">Uganda</option>
                                                            <option value="Ukraine">Ukraine</option>
                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                            <option value="United Kingdom">United Kingdom</option>
                                                            <option value="United States">United States</option>
                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                            <option value="Uruguay">Uruguay</option>
                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                            <option value="Vanuatu">Vanuatu</option>
                                                            <option value="Venezuela">Venezuela</option>
                                                            <option value="Viet Nam">Viet Nam</option>
                                                            <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                            <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                            <option value="Western Sahara">Western Sahara</option>
                                                            <option value="Yemen">Yemen</option>
                                                            <option value="Zambia">Zambia</option>
                                                            <option value="Zimbabwe">Zimbabwe</option>
                                                        </select>
                                                        <label for="country" generated="true" class="error"></label>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Introduce Yourself') }}</h5>
                                                        <textarea name="abt_me" id="abt_me" cols="30" rows="5" class="with-border" placeholder="{{ __('With less than 500 characters try to introduce yourself.') }}"></textarea>

                                                    </div>
                                                </div>


                                                <div class="col-xl-8">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Location') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Please choose from dropdown list') }}"></i></h5>
                                                        <div >
                                                            <div id="autocomplete-container">
                                                            <div class="input-with-icon">
                                                                <input  id="autocomplete-input" type="text" placeholder="Type your full address" name="address">
                                                               <i class="icon-material-outline-location-on" style='top:25px'></i>
                                                            </div>

                                                                <input type="hidden" name="city" id="city">
                                                                <input type="hidden" name="province" id="province">
                                                                <input   type="hidden" name="lat"      id="lat">
                                                                <input  type="hidden" name="lon"      id="lon">
                                                            </div>
                                                        </div>
<span id="addrErr" style="display: none" class="error"> {{ __('Pick address from the suggestions or from the map below!') }} </span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Zip Code') }}</h5>
                                                        <input id="zip_code" type="text" name="zip_code" class="with-border" placeholder="Zip Code">

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                    </form>
                                    <button type="button" name="next" class="button next action-button button-sliding-icon ripple-effect profileInfos">{{ __('Save And Proceed') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

                                </div>
                                <div class="fieldSet">
                                    <div id="preferencesForm">
                                        <div class="form-card">
                                            <div class="row">

                                                 <div class="col-xl-4">
                                                     <div class="submit-field">
                                                         <div class="bidding-widget" style="background-color:white">
                                                             <!-- Headline -->
                                                             <span class="bidding-detail">{{ __('Set your') }} <strong>{{ __('minimal hourly rate') }} </strong> <span style="color: green" id="hourRateStatus"></span></span>

                                                             <div class="input-with-icon-right" style="position: relative">

                                                                 <i style="float: right;font-style: normal;">TND</i>
                                                                 <input id="hourRate"  class="input-text with-border margin-top-10"  type="number" min="" name="hour_rate" value="{{Auth::user()->preference->hour_rate ?? ""}}" placeholder="35" />
                                                                 <input type="hidden" name="" id="oldHourRate" value="{{Auth::user()->preference->hour_rate ?? ""}}">
                                                             </div>

                                                         </div>
                                                     </div>
                                                 </div>

                                                <div class="col-xl-8">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Skills') }} <i class="help-icon" data-tippy-placement="right" title="Add up to 6 skills"></i> <span style="color: red" id="skillsStatus"></span></h5>

                                                        <!-- Skills List -->
                                                        <div class="keywords-container">
                                                            <div class="keyword-input-container">
                                                                <input  type="text" class="keyword-input with-border" placeholder="e.g. Angular, Laravel"/>
                                                                <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
                                                            </div>
                                                            <div class="keywords-list">

                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Attachments') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Upload your cover lettre and cv, only pdf accepted') }}"></i><span id="uplaodSuccess" style="color: green"></span></h5>

                                                        <!-- Attachments -->
                                                        <div id="attachments" class="attachments-container margin-top-0 margin-bottom-0">

                                                        </div>
                                                        <!-- Upload Button -->
                                                        <div class="uploadButton margin-top-0">
                                                            <input name="attachments" class="uploadButton-input" type="file" accept="application/pdf" id="upload" multiple/>
                                                            <label class="uploadButton-button ripple-effect" for="upload">{{ __('Upload Files') }}</label>
                                                            <div style="width: 49%; display: none;" id="progress-wrp">
                                                                <div class="progress-bar"></div>
                                                                <div class="status">0%</div>
                                                            </div>

                                                        </div>
                                                        <span>{{ __('Maximum file size: 10 MB | 2 files are allowed') }}</span> <span style="color:red;" id="fileErrors"></span>

                                                    </div>
                                                </div>

                                                <div class="col-xl-2">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Socials') }} <i class="help-icon" data-tippy-placement="right" title="{{ __('Add your social accounts') }}"></i></h5>
                                                        <select id="socials" class="selectpicker with-border" title="Select Job Type" >
                                                     <option value="linkedin" selected>LinkedIn</option>
                                                <option value="facebook">Facebook</option>
                                                <option value="instagram">Instagram</option>
                                                <option value="github">Github</option>
                                                <option value="behance">Behance</option>
                                                <option value="twitter">Twitter</option>
                                                <option value="dribbble">Dribbble</option>

                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="col-xl-6">


                                                    <div class="submit-field">
                                                        <h5>{{ __('Links') }} <i class="help-icon" data-tippy-placement="right" title="Add your social accounts"></i> <span style="color: red" id="socialErrors"></span></h5>

                                                        <!-- Skills List -->
                                                        <div class="keywords-container">
                                                            <div class="keyword-input-container">

                                                                <input id="socialLinks" type="text" class=" with-border" value="https://linkedin.com/"/>
                                                                <button class="socialAddBtn ripple-effect"><i class="icon-material-outline-add"></i></button>

                                                            </div>


                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xl-4">
                                                    <div class="section-headline ">
                                                        <h5>{{ __('Your social accounts') }} <span style="color: green" id="socialSuccess"></span></h5>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-xl-12 col-md-12">
                                                            <ul class="list-3 color">
                                                                @if(Auth::user()->status == 2)
                                                                    <li> <span class="icon-line-awesome-legal"></span> <a style="margin-left: 5px;
margin-right: 30px;" href="{{ route('showFreelancerDetails', Auth::user()->getFreelancerUsername()) }}" target="_blank">Hired</a></li>
                                                                @endif

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <button type="button" name="next" class="button next previous action-button button-sliding-icon ripple-effect companySlide">Next<i class="icon-material-outline-arrow-right-alt"></i></button>
                                </div>
                                <div class="fieldSet">
                                    <div id="employmentsForm">
                                        <div class="form-card">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <ul id="empsList" class="dashboard-box-list">
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-xl-6">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Company name') }}</h5>
                                                        <input name="cmpName" id="cmpName" type="text" class="with-border">
                                                        <small id="cmpName-error"></small>
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Post title') }}</h5>
                                                        <input name="pTitle" id="pTitle" type="text" class="with-border">
                                                        <small id="pTitle-error"></small>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Start date') }}</h5>
                                                        <input name="start" id="start" type="date" class="with-border">
                                                        <small id="start-error"></small>
                                                    </div>
                                                    <div class="switches-list">
                                                        <div class="switch-container">
                                                            <label class="switch">
                                                                <input id="switchToCurrent" type="checkbox" >
                                                                <span class="switch-button"></span>{{ __('Present?') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-3">
                                                    <div class="submit-field">
                                                        <h5>{{ __('End date') }}</h5>
                                                        <input name="end" id="end" type="date" class="with-border">
                                                        <small id="end-error"></small>

                                                    </div>
                                                </div>

                                                <div class="col-xl-6">
                                                    <div class="submit-field">
                                                        <h5>{{ __('Describe your experience') }}</h5>
                                                        <textarea name="desc" id="desc" cols="30" rows="3"></textarea>
                                                        <small id="desc-error"></small>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>


                                    </div>
                                    <button type="submit" id="save-emps" form="save-emps-form" class="button ripple-effect big" style="padding: 10px 5px;
    margin: 10px 0px 10px 5px;
    float: right;
    margin-bottom: 40px;
    opacity: 1;
    cursor: pointer;
    width: 180px;
}">{{ __('Add Employment') }}</button>
                                    <button type="button" name="next" class="button next done previous action-button button-sliding-icon ripple-effect ">{{ __('Finish') }}<i class="icon-material-outline-arrow-right-alt"></i></button>

                                </div>
                                <div class="fieldSet">
                                    <div class="form-card">
                                        <div class="col-md-12">
                                            <div class="order-confirmation-page" style="padding-bottom: 25px;">
                                                <div class="breathing-icon"><i class="icon-feather-check"></i></div>
                                                <h2 class="margin-top-30 companyCreated" style="font-size: 38px; line-height: 2rem">{{ __('Done, your profile has been published!') }}</h2>
                                                <p>{{ __('You can now start applying for jobs, bidding or creating tasks, publishing your services and more.') }}</p>
                                                <div class="container">

                                                    <div class="row" style="display:block;margin-top:50px">
                                                        <a href="{{ route('showFreelancerDetails', Auth::user()->getFreelancerUsername()) }}" target="_blank" class="button ripple-effect-dark button-sliding-icon margin-top-30 companyProfile  btn_width" >{{ __('View Profile') }} <i class="icon-material-outline-arrow-right-alt"></i></a>

                                                    </div>
                                                    <div class="row" style="display:block;margin-top:50px">
                                                        <a href="{{ route('showPortfolio') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30 btn_width" >{{ __('Create Your Projects Portfolio') }} <i class="icon-material-outline-arrow-right-alt"></i></a>

                                                    </div>
                                                    <div class="row" style="display:block;margin-top:50px">
                                                        <a href="{{ route('showPostService') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30 btn_width" >{{ __('Add Your First Service') }} <i class="icon-material-outline-arrow-right-alt"></i></a>

                                                    </div>
                                                    <div class="row" style="display:block;margin-top:50px">
                                                        <a href="{{ route('showPostTask') }}" class="button ripple-effect-dark button-sliding-icon margin-top-30 btn_width" >{{ __('Post Your First Task') }} <i class="icon-material-outline-arrow-right-alt"></i></a>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ route('showSettings') }}" class="skipBtn">{{ __('Skip for now') }}</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        $(document).ready(function(){

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var current = 1;
            var steps = $(".fieldSet").length;


            $(".profileInfos").click(function(){

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                $("#progressbar li").eq($(".fieldSet").index(next_fs)).addClass("active");
                updateProfile($(this) , current_fs,next_fs)
            });



            $(".previous").click(function(){

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                $("#progressbar li").eq($(".fieldSet").index(next_fs)).addClass("active");

                next_fs.show();
                current_fs.animate({opacity: 0}, {
                    step: function(now) {
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({'opacity': opacity});
                    },
                    duration: 500
                });
            });


            $(".submit").click(function(){
                return false;
            })

        });

        function updateProfile(elm,current_fs,next_fs) {
            elm.html('Saving <span class="fa fa-spinner fa-spin fa fa-fw"></span>');
            elm.css('opacity','0.5');
            elm.css('cursor','not-allowed');
            elm.attr("disabled", true);
            $('#profileForm').validate({
                rules: {
                    tagline: {
                        required: true,
                        maxlength: 100
                    },
                    abt_me: {
                        required: false,
                        maxlength: 500
                    },
                    address : {
                        required : true,
                    },
                    city : {
                        required : true,
                    },
                    province : {
                        required : true,
                    },
                    lat : {
                        required : true,
                    },
                    lon : {
                        required : true,
                    },
                    zip_code : {
                        required : false,
                        digits: true,
                        minlength: 4,
                    },
                    country : {
                        required : true,
                    }
                }
            });

            if ((!$('#profileForm').valid())) {
                elm.html('Save And Proceed<i class="icon-material-outline-arrow-right-alt"></i>');
                elm.css('opacity','1');
                elm.css('cursor','pointer');
                elm.attr("disabled", false);
                return false;
                      }else if($('#lon').val() == '') {
                        $('#addrErr').fadeIn('slow').delay(2000).fadeOut();
                }
                else {
                var data = {
                    'tagline' : $('#tagline').val(),
                    'abt_me' : $('#abt_me').val(),
                    'country' : $('#country').val(),
                    'address' : $('#autocomplete-input').val(),
                    'city' : $('#city').val(),
                    'province' : $('#province').val(),
                    'lat' : $('#lat').val(),
                    'lon' : $('#lon').val(),
                    'zip_code' : $('#zip_code').val()
                }

                $.post('{{ route('apiHandleUpdateProfile') }}',
                    {
                        '_token': $('meta[name=csrf-token]').attr('content'), data : data

                    })
                    .done(function (res) {
                        next_fs.show();
                        current_fs.animate({opacity: 0}, {
                            step: function(now) {
                                opacity = 1 - now;

                                current_fs.css({
                                    'display': 'none',
                                    'position': 'relative'
                                });
                                next_fs.css({'opacity': opacity});
                            },
                            duration: 500
                        });
                        elm.html('Save And Proceed<i class="icon-material-outline-arrow-right-alt"></i>');
                        elm.css('opacity','1');
                        elm.css('cursor','pointer');
                        elm.attr("disabled", false);
                    })
                    .fail(function (res) {
                        elm.html('Save And Proceed<i class="icon-material-outline-arrow-right-alt"></i>');
                        elm.css('opacity','1');
                        elm.css('cursor','pointer');
                        elm.attr("disabled", false);
                        return false;
                    })


            }
        }

        $('.done').on('click', function () {
            $('.skipBtn').hide();
        });

        function initAutocomplete() {
            var city, street, place, state, code, sublocality = ' ';


            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var placeUser = autocomplete.getPlace();
                var lat = placeUser.geometry.location.lat();
                var lng = placeUser.geometry.location.lng();
                for (var i = 0; i < placeUser.address_components.length; i++) {
                    var addressType = placeUser.address_components[i]["types"][0];
                    switch (addressType) {
                        case 'route':
                            street = placeUser.address_components[i]['long_name'];

                            break;
                        case 'administrative_area_level_1':
                            place = placeUser.address_components[i]['long_name'];
                            break;
                        case 'locality':
                            city = placeUser.address_components[i]['long_name'];
                            break;
                        case 'sublocality_level_1':
                            sublocality = placeUser.address_components[i]['long_name'];
                            break;
                        case 'country':
                            country = placeUser.address_components[i]['long_name'];
                            break;
                    }


                }
                $('#city').val(city);
                $('#province').val(sublocality);
                $('#lat').val(lat);
                $('#lon').val(lng);


            });

        }


        $(document).on("focusout" , "#hourRate", function () {

            var InitialhourRate = $('#oldHourRate').val();
            var hourRate = $('#hourRate').val();

            if(parseInt(InitialhourRate) != parseInt(hourRate)) {

                $.get("{{ route('apiHandleSetHourRate')}}?hourRate="+hourRate).done(function (res) {
                    if(res.status == 'created') {
                        $('#hourRateStatus').text('Hour rate settled').fadeIn('slow').delay(1000).fadeOut();
                        $('#oldHourRate').val(parseInt(hourRate))
                    }
                    else if (res.status == 'updated') {
                        $('#hourRateStatus').text('Hour rate updated').fadeIn('slow').delay(1000).fadeOut();
                        $('#oldHourRate').val(parseInt(hourRate))

                    }else {
                        $('#hourRateStatus').text('Please enter a number').fadeIn('slow').delay(1000).fadeOut();
                    }
                });

            }

        });


        $(".keywords-container").each(function() {
            var keywordInput = $(this).find(".keyword-input");
            var keywordsList = $(this).find(".keywords-list");

            function addKeyword() {

                $.get("{{ route('apiHandleAddSkill')}}?label="+keywordInput.val()).done(function (res) {
                    if(res.status == 200) {
                        var $newKeyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + keywordInput.val() + "</span></span>");
                        keywordsList.append($newKeyword).trigger('resizeContainer');
                        keywordInput.val("");
                    }else {

                        if(res.status == 401) {
                            keywordInput.val("");
                            $('#skillsStatus').text('You have reached the maximum skills !').fadeIn('slow').delay(2000).fadeOut()
                        }else if(res.status == 400) {
                            $('#skillsStatus').text('Skill already exists !').fadeIn('slow').delay(2000).fadeOut()

                        }else if (res.status == 404) {
                            $('#skillsStatus').text('Something went wrong, try again.').fadeIn('slow').delay(2000).fadeOut()
                        }

                    }
                });

            }
            keywordInput.on('keyup', function(e) {
                if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $('.keyword-input-button').on('click', function() {
                if ((keywordInput.val() !== "")) {
                    addKeyword();
                }
            });
            $(document).on("click", ".keyword-remove", function() {
                $(this).parent().addClass('keyword-removed');
                var label = $(this).data('label');
                function removeFromMarkup() {
                    $.get("{{ route('apiHandleDeleteSkill')}}?label="+label).done(function (res) {
                        if(res.status == 200) {
                            $(".keyword-removed").remove();

                        }
                    });

                }
                setTimeout(removeFromMarkup, 500);
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            keywordsList.on('resizeContainer', function() {
                var heightnow = $(this).height();
                var heightfull = $(this).css({
                    'max-height': 'auto',
                    'height': 'auto'
                }).height();
                $(this).css({
                    'height': heightnow
                }).animate({
                    'height': heightfull
                }, 200);
            });
            $(window).on('resize', function() {
                keywordsList.css({
                    'height': 'auto'
                }).height();
            });
            $(window).on('load', function() {
                var keywordCount = $('.keywords-list').children("span").length;
                if (keywordCount > 0) {
                    keywordsList.css({
                        'height': 'auto'
                    }).height();
                }
            });
        });

        var elems = '{{ count(Auth::user()->medias) - 1 }}';
        $('#upload').on('change',function () {
            $('#progress-wrp').css('display', 'block');


            var files = $(this)[0].files;

            if(files.length > 2) {

                $('#fileErrors').text('Only 2 files are allowed !').fadeIn('slow').delay(3000).fadeOut();

            } else {
                $.each(files, function (i, file) {
                    var upload = new Upload(file);
                    var type_reg = /^image\/(jpg|png|jpeg|bmp|gif|ico)$/;
                    var size = upload.getSize();
                    if(type_reg.test(upload.getType())) {
                        $('#progress-wrp').css('display', 'none');
                        $('#fileErrors').text('Only PDF files are accepted').fadeIn('slow').delay(2000).fadeOut();
                    }
                    else if((size / 1048576).toFixed(2) > 10 ) {
                        $('#progress-wrp').css('display', 'none');
                        $('#fileErrors').text('Maximum file size is 10 MB').fadeIn('slow').delay(2000).fadeOut();
                    }

                    else {

                        upload.doUpload();
                        $('#progress-wrp').css('display', 'none');

                        $('#attachments').append('<div class="attachment-box ripple-effect">\n' +
                            '\t\t\t\t\t\t\t\t\t\t\t\t\t<span>'+ file.name +'</span> \n' +

                            '\t\t\t\t\t\t\t\t\t\t\t\t</div>')
                        elems++;
                        console.log(elems);

                        if (elems > 1) {
                            $('.uploadButton').css('display', 'none');
                        }

                    }
                })


            }


        });

        var Upload = function (file) {
            this.file = file;
        };

        Upload.prototype.getType = function() {
            return this.file.type;
        };
        Upload.prototype.getSize = function() {
            return this.file.size;
        };
        Upload.prototype.getName = function() {
            return this.file.name;
        };
        Upload.prototype.doUpload = function () {
            var that = this;
            var formData = new FormData();

            formData.append("file", this.file, this.getName());
            formData.append("upload_file", true);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: '{{ route('apiHandleUploadFreelancerFiles') }}',
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', that.progressHandling, false);
                    }
                    return myXhr;
                },
                success: function (data) {
                    $('#uplaodSuccess').text('File uploaded successfuly').fadeIn('slow').delay(2000).fadeOut();
                    $("#progress-wrp .progress-bar").css("width", "0%");
                    $("#progress-wrp .status").text("0%");
                },
                error: function (error) {

                },
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                timeout: 60000
            });
        };

        Upload.prototype.progressHandling = function (event) {
            var percent = 0;
            var position = event.loaded || event.position;
            var total = event.total;
            var progress_bar_id = "#progress-wrp";
            if (event.lengthComputable) {
                percent = Math.ceil(position / total * 100);
            }
            // update progressbars classes so it fits your code
            $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
            $(progress_bar_id + " .status").text(percent + "%");
        };


        $('.remove-attachment').on('click' ,  function () {
            var parent = $(this).parent()
            $.get("{{ route('apiHandleDeleteFreelancerFile')}}?id="+$(this).data('id')).done(function (res) {
                if(res.status == 200) {
                    parent.css('display', 'none');
                    $('#uplaodSuccess').text('File removed successfuly').fadeIn('slow').delay(2000).fadeOut();
                    elems--;
                    $('.uploadButton').css('display', 'block');
                }
                else {
                    $('#fileErrors').text('Something went wrong, please try again').fadeIn('slow').delay(2000).fadeOut();
                }
            });
        })

        $('#socials').on('change' , function () {
      switch($(this).children("option:selected").val()) {

        case "linkedin":
      $('#socialLinks').val('https://linkedin.com/')

        break;
      case "github":
      $('#socialLinks').val('https://github.com/')

        break;
      case "behance":
      $('#socialLinks').val('https://behance.com/')

        break;
      case "facebook":
      $('#socialLinks').val('https://facebook.com/')

        break;
      case "dribbble":
      $('#socialLinks').val('https://dribbble.com/')

      break;
      case "twitter":
      $('#socialLinks').val('https://twitter.com/')

       break;
      case "instagram":
      $('#socialLinks').val('https://instagram.com/')

       break;
      }
    })

        $('#socialLinks').on('keyup', function(e) {
            if ((e.keyCode == 13) && ($(this).val() !== "")) {
                addSocialLink();
            }
        });

        $('.socialAddBtn').on('click', function () {
            addSocialLink();
        })

        function addSocialLink() {

            var label = $('#socials').children("option:selected").val();
            var link = $('#socialLinks').val();

            $.get("{{ route('apiHandleAddSocialLink')}}?label="+label+"&link="+link).done(function (res) {
                if (res.status == 'created') {
                    $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                        '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                    $('#socialSuccess').text('Link added successfully !').fadeIn('slow').delay(2000).fadeOut();

                }else if (res.status == 'updated') {
                    $('.'+label).remove();
                    $('.list-3').append(' <li class="' + label + '"> <span class="fa fa-' + label + '"></span> <a style="margin-left: 5px;\n' +
                        '    margin-right: 30px;" href="' + link + '" target="_blank">' + label + '</a> </li>')

                    $('#socialSuccess').text('Link updated successfully !').fadeIn('slow').delay(2000).fadeOut();
                }
                else if(res.status == 400) {
                    $('#socialErrors').text('Social link already exists !').fadeIn('slow').delay(2000).fadeOut();
                }
                else {
                    $('#socialErrors').text('Something went wrong, please try again.').fadeIn('slow').delay(2000).fadeOut();
                }
            });
        }

        $('#uploadVideo').on('change' , function () {
            var size = this.files[0].size;
            if((size / 1048576).toFixed(2) > 50 ) {
                $('#videoErrors').text('Maximum video size is 50 MB').fadeIn('slow').delay(2000).fadeOut();
            } else {
                $('#uploadIntroForm').submit();

                $('.uploadButton-button-video').html('Please wait while we upload your video <span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
                $('.uploadButton-button-video').css('cursor','not-allowed');
                $(".uploadButton-button-video").attr("disabled", true);

            }
        })


        $('#updateIntoVideo').on('change' , function () {
            var size = this.files[0].size;
            if((size / 1048576).toFixed(2) > 50 ) {
                $('#videoErrors').text('Maximum video size is 50 MB').fadeIn('slow').delay(2000).fadeOut();
            } else {
                $('#updateIntoVideoForm').submit();

                $('.uploadButton-button-video').html('Please wait while we upload your video <span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
                $('.uploadButton-button-video').css('cursor','not-allowed');
                $(".uploadButton-button-video").attr("disabled", true);

            }
        });

        $( "#save-emps" ).on('click',function( event ) {
            $('#save-emps').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
            $('#save-emps').css('opacity','0.5');
            $('#save-emps').css('cursor','not-allowed');
            $("#save-emps").attr("disabled", true);
            $('#cmpName-error').text('')
            $('#pTitle-error').text('')
            $('#start-error').text('')
            $('#end-error').text('')
            $('#desc-error').text('')
            var data = {
                'cmpName' : $('#cmpName').val(),
                'pTitle' : $('#pTitle').val(),
                'start' : $('#start').val(),
                'desc' : $('#desc').val()
            }

            if($('#switchToCurrent').is(':checked')) {
                data.present = 1
            }else {
                data.present = 0,
                    data.end = $('#end').val()
            }

            $.post('{{ route('apiHandleSaveEmployment') }}',
                {
                    '_token': $('meta[name=csrf-token]').attr('content'), data : data

                })
                .done(function (res) {
                    $('#save-emps').html('Add Employment');
                    $('#save-emps').css('opacity','1');
                    $('#save-emps').css('cursor','pointer');
                    $("#save-emps").attr("disabled", false);
                    if(res.errors) {
                        if(res.errors.cmpName) {
                            $('#cmpName-error').text(res.errors.cmpName[0])
                        }
                        if(res.errors.pTitle) {
                            $('#pTitle-error').text(res.errors.pTitle[0])
                        }
                        if(res.errors.start) {
                            $('#start-error').text(res.errors.start[0])
                        }
                        if(res.errors.end) {
                            $('#end-error').text(res.errors.end[0])
                        }
                        if(res.errors.desc) {
                            $('#desc-error').text(res.errors.desc[0])
                        }
                    } else {
                        var newEmp = '  <li id="emp-'+res.emp.id+'"> <div class="col-xl-12">\n' +
                            '                <div class="job-listing">\n' +
                            '                  <div class="job-listing-details">\n' +
                            '                    <a href="#" class="job-listing-company-logo">\n' +
                            '                      <img src="{{ asset('frontOffice/images/company-logo-placeholder.png') }}" alt="'+ res.emp.company +'">\n' +
                            '                    </a>\n' +
                            '                    <div class="job-listing-description">\n' +
                            '                      <h3 class="job-listing-title"><a href="#">' + res.emp.position + '</a></h3>\n' +
                            '                      <div class="job-listing-footer">\n' +
                            '                        <ul>\n' +
                            '                          <li><i class="icon-material-outline-business"></i>' + res.emp.company + '</li>\n' +
                            '                          <li><i class="icon-material-outline-access-time"></i>From '+ res.emp.start +' To '+ res.emp.end +' </li>\n' +
                            '                        </ul>\n' +
                            '                      </div>\n' +
                            '                    </div>\n' +
                            '                  </div>\n' +
                            '                </div>\n' +
                            '                <div class="buttons-to-right">\n' +
                            '                  <a href="#" class="button gray ripple-effect ico del-emp" title="Remove" data-id="' + res.emp.id + '" data-tippy-placement="left" style="line-height : 25px!important"><i class="icon-feather-trash-2" style="font-size:20px!important"></i></a>\n' +
                            '                </div>\n' +
                            '              </div> </li> ';

                        $('#empsList').append(newEmp);
                        $('#cmpName').val('');
                        $('#pTitle').val('');
                        $('#start').val('');
                        $('#end').val('');
                        $('#desc').val('');
                        $('#switchToCurrent').prop('checked', false);
                        if($('#end').prop('disabled')) {
                            $('#end').removeAttr('disabled');
                            $('#end').css('opacity' , '1');
                        }
                        Snackbar.show({
                            text: 'Employment added',
                            pos: 'bottom-right',
                            showAction: false,
                            actionText: "Dismiss",
                            duration: 3000,
                            textColor: '#fff',
                            backgroundColor: '#80e4a1'
                        });
                    }

                })
                .fail(function (res) {

                })
        });
        $(document).on('click', '.del-emp', function () {
            var id = $(this).data('id');
            $.get('{{ route('apiHandleDeleteEmployment') }}?id='+id).done( function (res) {
                if(res.error) {
                    Snackbar.show({
                        text: 'Employment not found!',
                        pos: 'bottom-right',
                        showAction: false,
                        actionText: "Dismiss",
                        duration: 3000,
                        textColor: '#fff',
                        backgroundColor: '#d9564c'
                    });
                }
                $('#emp-'+id).hide();

                Snackbar.show({
                    text: 'Employment deleted',
                    pos: 'bottom-right',
                    showAction: false,
                    actionText: "Dismiss",
                    duration: 3000,
                    textColor: '#fff',
                    backgroundColor: '#80e4a1'
                });


            })
        });

        $('#switchToCurrent').on('change', function () {
            if($(this).is(':checked')) {
                $('#end').attr('disabled' , 'disabled');
                $('#end').css('opacity' , '0.5');
            }else {
                $('#end').removeAttr('disabled');
                $('#end').css('opacity' , '1');
            }

        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

@endsection

@section('footer')
    @include('frontOffice.inc.footer')
@endsection
