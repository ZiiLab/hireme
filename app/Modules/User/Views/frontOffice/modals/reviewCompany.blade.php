<!-- Leave a Review Popup
================================================== -->
<div id="small-dialog-1" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">{{ __('Leave a Review') }}</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content"  id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __('What is it like to work for') }} {{$company->name}}?</h3>
                @php
                    $review = null;
                   foreach ($company->getAllRatings($company->id,'desc','App\Modules\User\Models\Company') as $rating) {
                            if($rating->author_id == Auth::id()) {
                              $review = $rating;
                            }
                       }
                @endphp
                <!-- Form -->
                    <form action="javascript:void(0)" method="post" id="leave-review-form" >

                        <!-- Leave Rating -->
                        <div class="clearfix"></div>
                        <div class="leave-rating-container">
                            <div class="leave-rating margin-bottom-5">
                                @if($review)
                                    <input @if($review->rating == 5) checked @endif type="radio" name="rating" id="rating-1" value="5" required>
                                    <label for="rating-1" class="icon-material-outline-star"></label>
                                    <input @if($review->rating == 4)checked @endif  type="radio" name="rating" id="rating-2" value="4" required>
                                    <label for="rating-2" class="icon-material-outline-star"></label>
                                    <input @if($review->rating == 3) checked @endif type="radio" name="rating" id="rating-3" value="3" required>
                                    <label for="rating-3" class="icon-material-outline-star"></label>
                                    <input @if($review->rating == 2) checked @endif type="radio" name="rating" id="rating-4" value="2" required>
                                    <label for="rating-4" class="icon-material-outline-star"></label>
                                    <input @if($review->rating == 1) checked @endif type="radio" name="rating" id="rating-5" value="1" required>
                                    <label for="rating-5" class="icon-material-outline-star"></label>
                                @else
                                    <input type="radio" name="rating" id="rating-1" value="5" required>
                                    <label for="rating-1" class="icon-material-outline-star"></label>
                                    <input type="radio" name="rating" id="rating-2" value="4" required>
                                    <label for="rating-2" class="icon-material-outline-star"></label>
                                    <input type="radio" name="rating" id="rating-3" value="3" required>
                                    <label for="rating-3" class="icon-material-outline-star"></label>
                                    <input type="radio" name="rating" id="rating-4" value="2" required>
                                    <label for="rating-4" class="icon-material-outline-star"></label>
                                    <input type="radio" name="rating" id="rating-5" value="1" required>
                                    <label for="rating-5" class="icon-material-outline-star"></label>
                                @endif

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- Leave Rating / End-->

                </div>


                <div class="row">

                    <div class="col-xl-12">
                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-rate-review"></i>
                            <input type="text" value="{{ $review ? $review->title : '' }}" class="input-text with-border" name="reviewtitle" id="reviewtitle" placeholder="{{ __('Review Title') }}"  />
                        </div>
                    </div>
                </div>

                <textarea class="with-border" placeholder="{{ __('Review') }}" name="message" id="message" cols="7" >{{ $review ? $review->body : '' }}</textarea>

                </form>

                <!-- Button -->
                <button class="button margin-top-35 full-width button-sliding-icon ripple-effect submitReview" type="submit" form="leave-review-form">{{ __('Leave a Review') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Leave a Review Popup / End -->


<script>

    $('#leave-review-form').submit( function (event) {
        event.preventDefault();
        $('.submitReview').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('.submitReview').css('opacity','0.5');
        $('.submitReview').css('cursor','not-allowed');
        $(".submitReview").attr("disabled", true);

        var rating = $('.leave-rating input[name="rating"]:checked').val();
        var title = $('#reviewtitle').val();
        var message = $('#message').val();
        var companyId = '{{ $company->id }}';

        $.get('{{ route('handleSubmitReview') }}?rating='+rating+'&title='+title+'&message='+message+'&cId='+companyId).done(function (res) {

if(res.status == 404) {
    $('.noReviews').hide();
      Snackbar.show({
                text: '{{__('This company has been removed!')}}',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#5f9025'
            });
            $('.mfp-close').click();
}else {     $('.noReviews').hide();
            $('.reviewsList').prepend('   <li>\n' +
                '                        <div class="boxed-list-item">\n' +
                '                            <!-- Content -->\n' +
                '                            <div class="item-content">\n' +
                '                                <h4>'+ res.title +'<span>'+ res.name +'</span></h4>\n' +
                '                                <div class="item-details margin-top-10">\n' +
                '                                    <div class="star-rating" data-rating='+ res.rating +'></div>\n' +
                '                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i>'+ res.date +'</div>\n' +
                '                                </div>\n' +
                '                                <div class="item-description">\n' +
                '                                    <p>'+ res.body +'</p>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </li>');
            $('.leaveReviewBtn').html('Update Review <i class="icon-material-outline-arrow-right-alt"></i>');
            Snackbar.show({
                text: 'Review submitted successfully!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#5f9025'
            });
            $('.mfp-close').click();

            $('.submitReview').html('Leave a Review <i class="icon-material-outline-arrow-right-alt"></i>')
            $('.submitReview').css('opacity','1');
            $('.submitReview').css('cursor','pointer');
            $(".submitReview").attr("disabled", false);}

       
        }).fail(function (err) {

            Snackbar.show({
                text: 'Something went wrong, please try again!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#f00000'
            });
            $('.submitReview').html('Leave a Review <i class="icon-material-outline-arrow-right-alt"></i>')
            $('.submitReview').css('opacity','1');
            $('.submitReview').css('cursor','pointer');
            $(".submitReview").attr("disabled", false);
        })



    });
</script>
