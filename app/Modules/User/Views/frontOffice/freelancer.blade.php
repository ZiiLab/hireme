@extends('frontOffice.layout',['title' => 'Hired | '.$user->getFullName()])
@section('meta')
    <meta property="og:url" content="{{route('showFreelancerDetails',$user->getFreelancerUsername())}}">
    <meta property="og:description" content="{{$user->info ? $user->info->abt_me : 'Hired Candidate'}}">
    <meta property="og:site_name" content="{{$user->getFullName()}}">
    <meta property="og:image" content="{{asset($user->medias()->where('type',0)->first()->link ?? 'frontOffice/images/user-avatar-placeholder.png')}}">
    <meta property="og:image:type" content="image/png">
     <meta property="twitter:creator" content="{{$user->getFullName()}}">
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:title" content="{{$user->getFullName()}}">
    <meta property="twitter:description" content="{{$user->info ? $user->info->abt_me : 'Hired Candidate'}}">
    <meta property="twitter:image:src" content="{{asset($user->medias()->where('type',0)->first()->link ?? 'frontOffice/images/user-avatar-placeholder.png')}}">
    <meta property="twitter:image:width" content="1291">
    <meta property="twitter:image:height" content="315">
    <meta name="msapplication-TileColor" content="#2A5977">
    <meta name="theme-color" content="#2A5977">
    <meta name="description" content="{{$user->info ? $user->info->abt_me : 'Hired Candidate'}}">
    @if($user->info)
        <meta name="keywords" content="{{ $user->getFullName().','.$user->info->abt_me .','.$user->info->tagline }}">
    @else
        <meta name="keywords" content="{{ $user->getFullName() }}">

    @endif
@endsection
@section('header')
@include('frontOffice.inc.header')
@endsection

@section('content')

  <div class="single-page-header freelancer-header" data-background-image="{{asset('frontOffice/images/single-freelancer.jpg')}}">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">
  				<div class="single-page-header-inner">
  					<div class="left-side">
  						<div class="header-image freelancer-avatar"><img src="{{asset($user->medias()->where('type',0)->first()->link ?? 'frontOffice/images/user-avatar-placeholder.png')}}" alt="{{ $user->getFullName() }}"></div>
  						<div class="header-details">
  							<h3>{{ $user->getFullName() }}<span>{{ $user->info ? $user->info->tagline : __('Not specified') }}</span></h3>
  							<ul>
                                @if(count($user->getAllRatings($user->id,'desc','App\Modules\User\Models\User')) < 3)
                                    <li><span class="company-not-rated">{{ __('Minimum of 3 votes required') }}</span></li>

                                @else
                                    <li><div class="star-rating" data-rating="{{ $user->averageRating(1)[0] }}"></div></li>
                                @endif
   @if($user->address)
                                        <li> <img class="flag" src="{{ asset('frontOffice/images/flags/'.strtolower($user->address->country).'.svg') }}" alt="{{ $user->address->country }}"> </li>
                                    @endif
  								

  								<li>
                                    @if($user->medias()->where('type',2)->first())
                                        <a href="#" class="button ripple-effect margin-top-10 watchIntroVideo"
                                           data-url="{{asset($user->medias()->where('type',2)->first()->link)}}" data-ext="{{explode('.',$user->medias()->where('type',2)->first()->label)[1]}}"
                                           style="padding: 5px 10px; color: white; display: initial">Watch {{ $user->first_name."'s Intro " }}<i class="icon-line-awesome-video-camera"></i></a>
                                        @endif
                                </li>
                                    <li>
                                        @if(count($user->portfolios) > 0)
                                            <a href="#" id="seePortfolio" class="button ripple-effect margin-top-10 " style="padding: 5px 10px; color: white; display: initial">{{ __('Browse') }} {{ $user->first_name."'s Portfolio " }}<i class="icon-material-outline-folder-shared"></i></a>
                                            <a href="#" id="backToProfile" class="button ripple-effect margin-top-10 " style="padding: 5px 10px; color: white; display: none">{{ __('Back To Profile') }}</a>
                                        @else
                                                 @if(Auth::check() && Auth::id() == $user->id)
                                                <a href="{{ route('showPortfolio') }}"  class="button ripple-effect margin-top-10 " style="padding: 5px 10px; color: white; display: initial">{{ __('Create your portfolio') }}</a>
                                            @endif
                                        @endif
                                    </li>
  							</ul>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  </div>


  <!-- Page Content
  ================================================== -->
  <div class="container">
  	<div class="row">

  		<!-- Content -->
        <div class="portfolioContent col-md-12"></div>
  		<div class="col-xl-8 col-lg-8 content-right-offset profileContent">

  			<!-- Page Content -->
            
              @if ($user->info->abt_me)

              <div class="single-page-section">
                <h3 class="margin-bottom-25">{{ __('About Me') }}</h3>
                <p>{{ $user->info->abt_me }}</p>

            </div>
                  
              @endif


  			<div class="boxed-list margin-bottom-60">
  				<div class="boxed-list-headline">
  					<h3><i class="icon-material-outline-business"></i> {{ __('Employment History') }}</h3>
  				</div>
  				<ul class="boxed-list-ul">
            @if(count($user->employments) != 0)
                @foreach ($user->employments as $emp)
  					<li>
  						<div class="boxed-list-item">
  							<!-- Avatar -->
  							<div class="item-image">
  								<img src="{{ asset('frontOffice/images/company-logo-placeholder.png') }}" alt="{{$emp->company_name}}">
  							</div>

  							<!-- Content -->
  							<div class="item-content">
  								<h4>{{ $emp->position }}</h4>
  								<div class="item-details margin-top-7">
  									<div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> {{$emp->company_name}}</a></div>
  									<div class="detail-item"><i class="icon-material-outline-date-range"></i> {{$emp->start_date->format('F Y')}} - {{ $emp->end_date ? $emp->end_date->format('F Y') : 'Present'}}</div>
  								</div>
  								<div class="item-description">
  									<p>{{ $emp->description }}</p>
  								</div>
  							</div>
  						</div>
  					</li>

          @endforeach
        @else
          <li>
            	<div class="boxed-list-item">
                {{ __('No employments found!') }}
              </div>
          </li>
        @endif
  				</ul>
  			</div>
  			<!-- Boxed List / End -->


            <div class="boxed-list margin-bottom-60">
                <div class="boxed-list-headline">
                    <h3><i class="icon-material-outline-thumb-up"></i>{{ __(' Work History and Feedback') }}</h3>
                </div>
                <ul class="boxed-list-ul reviewsList">

                    @if(count($user->getAllRatings($user->id,'desc','App\Modules\User\Models\User')) == 0)
                        <li class="noReviews">
                            <div class="boxed-list-item ">
                                {{ __('No reviews found!') }}
                            </div>
                        </li>
                        @else
                        @foreach($user->getAllRatings($user->id, 'desc','App\Modules\User\Models\User') as $rating)
                    <li>
                        <div class="boxed-list-item">
                            <!-- Content -->
                            <div class="item-content">
                                <h4>{{ $rating->title }} <span>{{ $rating->author->getFullName() }}</span></h4>
                                <div class="item-details margin-top-10">
                                    <div class="star-rating" data-rating="{{ $rating->rating }}"></div>
                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i> {{ Carbon\Carbon::parse($rating->created_at)->format('d F Y') }} </div>
                                </div>
                                <div class="item-description">
                                    <p>{{ $rating->body }}</p>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
                @endif
                </ul>
                <div class="centered-button margin-top-35">
                @if(Auth::check())
                        @if(Auth::id() != $user->id)

                    @php
                        $authorsIds = [];
                       foreach ($user->getAllRatings($user->id,'desc','App\Modules\User\Models\User') as $rating) {
                            $authorsIds[] = $rating->author_id;
                           }
                    @endphp
                    @if(Auth::id() == $user->id)
                    @endif



                        @if(in_array(Auth::id(), $authorsIds))
                          
                        @else
                            <a href="#small-dialog-1" class="popup-with-zoom-anim button button-sliding-icon leaveReviewBtn">{{ __('Leave a Review') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                          @endif
                    @else @endif

                @else
                 <a href="#sign-in-dialog"  class="popup-with-zoom-anim button button-sliding-icon">{{ __('Leave a Review') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                @endif


            </div>
            </div>
        </div>



  		<!-- Sidebar -->
  		<div class="col-xl-4 col-lg-4 sideBarContent">
  			<div class="sidebar-container">
  				<!-- Profile Overview -->
  				<div class="profile-overview">
  					<div class="overview-item"><strong>{{ $user->preference ? $user->preference->hour_rate.' TND' : 'Not specified' }}</strong><span>{{ __('Hourly Rate') }}</span></div>
  					<div class="overview-item"><strong>{{ count($user->acceptedBids()) }}</strong><span>{{ __('Task Done') }}</span></div>
  				</div>

  				<!-- Button -->
                @if(Auth::id() == $user->id)
                    @else

                    @if(Auth::check())
                        <a href="#small-dialog-2" class="apply-now-button popup-with-zoom-anim margin-bottom-50 sendDirectMessage" data-name="{{ $user->getFullName() }}" data-id="{{$user->id}}">{{ __('Send Direct Message') }} <i class="icon-material-outline-arrow-right-alt"></i></a>
                    @else
                        <a href="#sign-in-dialog"  class="apply-now-button popup-with-zoom-anim margin-bottom-50">{{ __('Send Direct Message') }} <i class="icon-material-outline-arrow-right-alt"></i></a>

                    @endif
                    @endif
  				<!-- Freelancer Indicators -->
  				{{-- <div class="sidebar-widget">
  					<div class="freelancer-indicators">

  						<!-- Indicator -->
  						<div class="indicator">
  							<strong>88%</strong>
  							<div class="indicator-bar" data-indicator-percentage="88"><span></span></div>
  							<span>Job Success</span>
  						</div>

  						<!-- Indicator -->
  						<div class="indicator">
  							<strong>100%</strong>
  							<div class="indicator-bar" data-indicator-percentage="100"><span></span></div>
  							<span>Recommendation</span>
  						</div>

  						<!-- Indicator -->
  						<div class="indicator">
  							<strong>90%</strong>
  							<div class="indicator-bar" data-indicator-percentage="90"><span></span></div>
  							<span>On Time</span>
  						</div>

  						<!-- Indicator -->
  						<div class="indicator">
  							<strong>80%</strong>
  							<div class="indicator-bar" data-indicator-percentage="80"><span></span></div>
  							<span>On Budget</span>
  						</div>
  					</div>
  				</div> --}}

  				<!-- Widget -->


          @if(count($user->socials) != 0)
  				<div class="sidebar-widget">
  					<h3>{{ __('Social Profiles') }}</h3>
  					<div class="freelancer-socials margin-top-25">
  						<ul>
                            @foreach($user->socials as $social)
  							<li><a href="{{ $social->link }}" target="_blank" title="{{ $social->label }}" data-tippy-placement="top"><i class="icon-brand-{{$social->label}}"></i></a></li>
                                @endforeach
  						</ul>
  					</div>
  				</div>
        @endif



  				<!-- Widget -->
          @if(count($user->skills) != 0)
  				<div class="sidebar-widget">
  					<h3>{{ __('Skills') }}</h3>
  					<div class="task-tags">
                        @foreach($user->skills as $skill)
  						<span>{{ $skill->label }}</span>
                            @endforeach
  					</div>
  				</div>
        @endif

  				<!-- Widget -->

          @if(count($user->medias()->where('type',5)->get()) != 0)
  				<div class="sidebar-widget">
  					<h3>{{ __('Attachments') }}</h3>
  					<div class="attachments-container">
  					@foreach($user->medias()->where('type',5)->get() as $file)
              	<a href="{{ asset($file->link) }}" class="attachment-box ripple-effect" download><span>{{$file->label}}</span></a>
            @endforeach
  					</div>
  				</div>
        @endif

                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-business-center"></i>{{ __('Published Services') }}</h3>
                    </div>

                    <div class="listings-container compact-list-layout">
                        @if(count($user->services) != 0)
                            @foreach($user->services as $service)
                                <a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}" class="job-listing">

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title">{{ $service->name }}</h3>

                                            <!-- Job Listing Footer -->
                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li>
                                                        <i class="icon-material-outline-access-alarm"></i>
                                                        <span>In {{$service->delay.' Days'}}</span>
                                                    </li>

                                                    <li>
                                                        <i class="icon-material-outline-local-atm"></i>
                                                        <span>{{ $service->min.' TND - '.$service->max.' TND' }}</span>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    @if(Auth::check() && Auth::id() != $service->user->id)

                                        <span class="bookmark-icon bookmarkService {{ Auth::user()->hasBookmarked($service) ? 'bookmarked' : '' }}" data-id="{{ $service->id }}"></span>
                                    @endif
                                </a>
                            @endforeach
                        @else
                            @if(Auth::check() && Auth::id() == $user->id)
                                <div style="text-align: center"> <p style="margin-top: 15px">{{ __("You haven't any published services yet.") }}
                                        <a href="{{ route('showPostService') }}" class="button">{{ __('Add one now!') }}</a>  </p></div>

                            @else
                                <div style="text-align: center"> <p style="margin-top: 15px">{{ __('No published services.') }}</p></div>
                            @endif
                        @endif
                    </div>

                </div>
  				<!-- Sidebar Widget -->
  				<div class="sidebar-widget">
  					<h3>{{ __('Bookmark or Share') }}</h3>

  					<!-- Bookmark Button -->
                    @if(Auth::check() && Auth::id() != $user->id)
  					<button class="bookmark-button margin-bottom-25 {{ Auth::user()->hasBookmarked($user) ? 'bookmarked' : '' }}">
  						<span class="bookmark-icon"></span>
  						<span class="bookmark-text">{{ __('Bookmark') }}</span>
  						<span class="bookmarked-text">{{ __('Bookmarked') }}</span>
  					</button>
                    @endif
  					<!-- Copy URL -->
  					<div class="copy-url">
  						<input id="copy-url" type="text" value="" class="with-border">
  						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
  					</div>

  					<!-- Share Buttons -->
  					<div class="share-buttons margin-top-25">
  						<div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
  						<div class="share-buttons-content">
  							<span>{{ __('Interesting?') }} <strong>{{ __('Share It!') }}</strong></span>
  							<ul class="share-buttons-icons">
                                <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"
                                       data-sharer="facebook" data-title="{{$user->getFullName()}}" data-url="{{route('showFreelancerDetails',$user->getFreelancerUsername())}}"
                                    ><i class="icon-brand-facebook-f"></i></a></li>

  								<li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"
                                       data-sharer="twitter"  data-title="{{$user->getFullName()}}" data-url="{{route('showFreelancerDetails',$user->getFreelancerUsername())}}"
                                    ><i class="icon-brand-twitter"></i></a></li>

  								<li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"
                                       data-sharer="linkedin" data-title="{{$user->getFullName()}}"  data-url="{{route('showFreelancerDetails',$user->getFreelancerUsername())}}"
                                    ><i class="icon-brand-linkedin-in"></i></a></li>
  							</ul>
  						</div>
  					</div>
  				</div>

  			</div>
  		</div>

  	</div>
  </div>


  <!-- Spacer -->
  <div class="margin-top-15"></div>
  <!-- Spacer / End-->
  @include('User::frontOffice.modals.reviewPopup')
  @include('User::frontOffice.dashboard.modals.sendDirectMessageBiddersPopup')
  @include('User::frontOffice.auth.modals.login')

  {{--
    @include('User::frontOffice.dashboard.modals.watchIntro')
  --}}

  <script type="text/javascript">
  $('.bookmark-button').on('click', function () {
        $.get('{{ route('handleBookmark') }}?type=user&id='+{{ $user->id }}).done(function(res) {
            if(res.bookmarked) {
              $('.bookmark-button').addClass('bookmarked');
            }else {
              $('.bookmark-button').removeClass('bookmarked');
            }
        }).fail(function(error) {

        });
  });

  $('.bookmarkService').on('click', function (e) {
      e.preventDefault();
      var elm = $(this);
      $.get('{{ route('handleBookmark') }}?type=service&id='+$(this).data('id')).done(function(res) {
          if(res.bookmarked) {
              elm.addClass('bookmarked');
          }else {
              elm.removeClass('bookmarked');
          }
      }).fail(function(error) {

      });
  });


  $('.authCheckPopup').on('click' , function () {
      $.confirm({
          icon: 'fa fa-warning',
          title: 'Authentication is required!',
          type: 'orange',
          columnClass: 'col-xl-6 col-xl-offset-4',
          content: '       <h3 style="margin-top : 20px; margin-bottom: 10px"> You must be logged in to perform this action.</h3>\n' +
              '                            <a href="{{ route('showLogin') }}"> Already have an account? </a>\n' +
              '                            <a href="{{ route('showRegister') }}"> Register now! </a>',
          buttons: {
              cancel: function () {
              }
          }
      });
  });
  $('.sendDirectMessage').on('click', function () {
      $('#bidder_name').text($(this).data('name'));
      $('#receiverId').val($(this).data('id'));
  });

  $('#seePortfolio').on('click', function (e) {
       e.preventDefault();

      $(this).hide();
      $('#backToProfile').css('display', 'initial');
      $('.profileContent').hide();
      $('.sideBarContent').hide();
        $('.portfolioContent').html('<div class="loader"></div>');
        $.get('{{ route('apiGetUserPortfolios') }}?id='+{{ $user->id }}).done(function (res) {
            $('.portfolioContent').html(res.html);
        })

  });

  $('#backToProfile').on('click', function () {
      $(this).hide();
      $('.portfolioContent').html('');
      $('#seePortfolio').css('display', 'initial');
      $('.profileContent').show();
      $('.sideBarContent').show();



  });

  $('.watchIntroVideo').on('click', function (e) {
      e.preventDefault();
      var url = $(this).data('url');
      var ext = $(this).data('ext');
      $.confirm({
          title: 'Intro',
          columnClass: 'col-xl-6',
          containerFluid: true,
          content: ' <video style="width: 100%" controls autoplay>\n' +
              '                    <source src="'+ url +'" type="video/'+ext+'">\n' +
              '                    Your browser does not support the video tag.\n' +
              '                </video>',
          buttons: {
              close: function () {
              }
          }
      });
  })
  </script>
@endsection


@section('footer')
      @include('frontOffice.inc.footer')
@endsection
