<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#login">{{__('Log In')}}</a></li>
            <li><a href="#register">{{ __('Register') }}</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Login -->
            <div class="popup-tab-content" id="login">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __("We're glad to see you again!") }}</h3>
                    <span>{{ __("Don't have an account?") }} <a href="#" class="register-tab">{{ __("Sign Up!") }}</a></span>
                    <small id="invalid-logins"></small>
                </div>

                <!-- Form -->
                <form action="javascript:void(0)" method="post" id="login-form">
                    <div class="input-with-icon-left">
                        <i class="icon-feather-mail"></i>
                        <input type="text" class="input-text with-border" name="email" id="login-email" placeholder="{{ __('Email Address') }}"/>
                        <small id="login-email-error"></small>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-feather-lock"></i>
                        <input  type="password" class="input-text with-border" name="password" id="login-password" placeholder="{{ __('Password') }}"/>
                        <i class="fa fa-eye-slash toggle-password" aria-hidden="true" style="right: 0; cursor: pointer;"></i>
                        <small id="login-password-error"></small>
                    </div>
                    <a href="{{route('showRequestResetPassword')}}" class="forgot-password">{{ __('Forgot Password?') }}</a>
                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" id="login-submit-btn" form="login-form">{{ __('Log In') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

                <!-- Social Login -->
                <div class="social-login-separator"><span>or</span></div>
                <div class="social-login-buttons">
                    <button onclick="window.location.href='{{route('handleProviderRedirect', 'facebook')}}'" class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> {{ __('Log In via Facebook') }}</button>
                    <button onclick="window.location.href='{{route('handleProviderRedirect', 'google')}}'" class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> {{ __('Log In via Google') }}</button>
                </div>

            </div>

            <!-- Register -->
            <div class="popup-tab-content" id="register">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>{{ __("Let's create your account!") }}</h3>
                    <span>{{ __("Start with choosing your account type.") }}</span>
                    <small id="invalid-register"></small>
                </div>

                <!-- Account Type -->
                <div class="account-type">
                    <div>
                        <input type="radio" name="account-type-radio" id="freelancer-radio" class="account-type-radio" value="1"/>
                        <label for="freelancer-radio" class="ripple-effect-dark toogleTypeModal"><i class="icon-material-outline-account-circle"></i> {{ __('Candidate') }}</label>
                    </div>

                    <div>
                        <input type="radio" name="account-type-radio" id="employer-radio" class="account-type-radio" value="0"/>
                        <label for="employer-radio" class="ripple-effect-dark  toogleTypeModal"><i class="icon-material-outline-business-center"></i> {{ __('Employer') }}</label>
                    </div>
                </div>

                <!-- Form -->
                <form  method="post" action="javascript:void(0)" id="register-account-form">

                    <div class="input-with-icon-left">
                        <i class="icon-feather-user"></i>
                        <input  type="text" class="input-text with-border" name="first_name" id="register-firstname" placeholder="{{ __('First Name') }}" />
                        <small id="register-firstname-error"></small>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-feather-user"></i>
                        <input  type="text" class="input-text with-border" name="last_name" id="register-lastname" placeholder="{{ __('Last Name') }}" />
                        <small id="register-lastname-error"></small>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-feather-mail"></i>
                        <input  type="text" class="input-text with-border" name="email" id="register-email" placeholder="{{ __('Email Address') }}" />
                        <small id="register-email-error"></small>
                    </div>

                    <div class="input-with-icon-left" title="Should be at least 6 characters long" data-tippy-placement="bottom">
                        <i class="icon-feather-lock"></i>
                        <input type="password" class="input-text with-border" name="password" id="register-password" placeholder="{{ __('Password') }}" />
                        <small id="register-password-error"></small>
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-feather-lock"></i>
                        <input type="password" class="input-text with-border" name="z" id="register-password-repeat" placeholder="{{ __('Repeat Password') }}" />
                        <small id="register-password-repeat-error"></small>
                    </div>
                </form>

                <!-- Button -->
                <button class="margin-top-10 button full-width button-sliding-icon ripple-effect" id="submit-register-btn" type="submit" form="register-account-form">{{ __('Register') }} <i class="icon-material-outline-arrow-right-alt"></i></button>

                <!-- Social Login -->
                <div class="social-login-separator"><span>or</span></div>
                <div class="social-login-buttons">
                    <button onclick="window.location.href='{{route('handleProviderRedirect', 'facebook')}}'" id="facebook-register-btn" class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> {{ __('Register via Facebook') }}</button>
                    <button onclick="window.location.href='{{route('handleProviderRedirect', 'google')}}'" id="google-register-btn" class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> {{ __('Register via Google') }}</button>
                </div>

            </div>

        </div>
    </div>
</div>

<script>
    $( "#login-form" ).submit(function( event ) {
        event.preventDefault();
        $('#login-submit-btn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('#login-submit-btn').css('opacity','0.5');
        $('#login-submit-btn').css('cursor','not-allowed');
        $("#login-submit-btn").attr("disabled", true);
        $('#invalid-logins').text('')
        $('#login-email-error').text('')
        $('#login-password-error').text('')

        var data = {
            'email' : $('#login-email').val(),
            'password' : $('#login-password').val()
        }

        $.post('{{route('apiHandleLogin')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                $('#login-submit-btn').html('Register <i class="icon-material-outline-arrow-right-alt"></i>')
                $('#login-submit-btn').css('opacity','1');
                $('#login-submit-btn').css('cursor','pointer');
                $("#login-submit-btn").attr("disabled", false);
                $('#invalid-register').text('Something went wrong, please try again !')
                    if(res.errors) {
                        if(res.errors.email) {
                            $('#login-email-error').text(res.errors.email[0])
                        }
                        if(res.errors.password) {
                            $('#login-password-error').text(res.errors.password[0])
                        }
                    }else {
                    if(res.status === 401) {
                        $('#login-email-error').text('Email not verified, please check your mail box !')
                    }
                    if(res.status === 404) {
                        $('#invalid-logins').text('Wrong Credentials !')
                    }
                    if(res.status === 400) {
                        $('#invalid-logins').text('Something went wrong, please try again !')
                    }
                    if(res.status === 200 ) {
                        window.location = res.url
                    }
                    }
            })
            .fail(function (res) {
                $('#login-submit-btn').html('Register <i class="icon-material-outline-arrow-right-alt"></i>')
                $('#login-submit-btn').css('opacity','1');
                $('#login-submit-btn').css('cursor','pointer');
                $("#login-submit-btn").attr("disabled", false);
                $('#invalid-logins').text('Something went wrong, please try again !')
            })
    });

    $(document).ready(function() {
      $('#freelancer-radio').prop("checked", false);
      $('#employer-radio').prop("checked", false);
        $('#register-account-form').css('opacity','0.5');
        $("#register-account-form :input").prop("disabled", true);

        $('#submit-register-btn').css('opacity','0.5');
        $('#submit-register-btn').css('cursor','not-allowed');
        $("#submit-register-btn").attr("disabled", true);

    });

    $('.account-type-radio').on('click', function () {
        $('#register-account-form').css('opacity','1');
        $("#register-account-form :input").prop("disabled", false);

        $('#submit-register-btn').css('opacity','1');
        $('#submit-register-btn').css('cursor','pointer');
        $("#submit-register-btn").attr("disabled", false);

    })


    $( "#register-account-form" ).submit(function( event ) {
        event.preventDefault();
        $('#submit-register-btn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
        $('#submit-register-btn').css('opacity','0.5');
        $('#submit-register-btn').css('cursor','not-allowed');
        $("#submit-register-btn").attr("disabled", true);
        $('#register-firstname-error').text('')
        $('#register-lastname-error').text('')
        $('#register-email-error').text('')
        $('#register-password-error').text('')
        $('#register-password-repeat-error').text('')
        $('#invalid-register').text('')
        var data = {
            'email' : $('#register-email').val(),
            'password' : $('#register-password').val(),
            'password_confirmation' : $('#register-password-repeat').val(),
            'type' : $("input[name='account-type-radio']:checked").val(),
            'first_name' :$('#register-firstname').val(),
            'last_name' : $('#register-lastname').val()
        }

        $.post('{{route('apiHandleRegister')}}',
            {
                '_token': $('meta[name=csrf-token]').attr('content'), data : data

            })
            .done(function (res) {
                $('#submit-register-btn').html('Register <i class="icon-material-outline-arrow-right-alt"></i>')
                $('#submit-register-btn').css('opacity','1');
                $('#submit-register-btn').css('cursor','pointer');
                $("#submit-register-btn").attr("disabled", false);

                if(res.errors) {
                    if(res.errors.email) {

                        $('#register-email-error').text(res.errors.email[0])
                    }
                    if(res.errors.password) {
                        if(res.errors.password[0] === 'Password must be confirmed !') {
                            $('#register-password-repeat-error').text(res.errors.password[0])
                        } else  {
                            $('#register-password-error').text(res.errors.password[0])
                        }

                    }

                    if(res.errors.first_name) {

                        $('#register-firstname-error').text(res.errors.first_name[0])
                    }

                    if(res.errors.last_name) {

                        $('#register-lastname-error').text(res.errors.last_name[0])
                    }
                }else {
                    if(res.status === 400) {
                        $('#invalid-register').text('Something went wrong, please try again !')
                    }

                    if(res.status === 200) {
                        window.location = res.url;
                    }
                }
            })
            .fail(function (res) {
                $('#submit-register-btn').html('Register <i class="icon-material-outline-arrow-right-alt"></i>')
                $('#submit-register-btn').css('opacity','1');
                $('#submit-register-btn').css('cursor','pointer');
                $("#submit-register-btn").attr("disabled", false);
                $('#invalid-register').text('Something went wrong, please try again !')
            })
    });
</script>
