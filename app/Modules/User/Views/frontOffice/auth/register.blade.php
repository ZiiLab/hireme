@extends('frontOffice.layout',['title' => __('Hired | Registration')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('Register') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{__('Home')}}</a></li>
                            <li>{{ __('Register') }}</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-5 offset-xl-3">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">

                        @if(Session::has('user'))
                        <h3 style="font-size: 26px;">{{ __('Almost there!') }}</h3>
                        <span>{{ __('Choose your account type and enter your password.') }}</span>
                        @else
                            <h3>{{ __("Let's create your account!") }}</h3>
                            <span>{{ __("Already have an account?") }} <a href="{{route('showLogin')}}">{{ __('Log In!') }}</a></span>
                        @endif

                        @if ($errors->getBag('invalidCredentials')->first())
                            <small>
                                {{  __($errors->getBag('invalidCredentials')->first())  }}
                            </small>
                        @endif
                    </div>

                    <form method="post" action="{{route('handleRegister')}}" id="register-account-form-page">
                    <!-- Account Type -->
                    <div class="account-type">
                        <div>
                            <input value="1" @if(old('account-type-radio-page') != null && old('account-type-radio-page')  == 1 ) checked @endif type="radio" name="account-type-radio-page" id="freelancer-radio-page" class="account-type-radio account-type-radio-page" />
                            <label id="freelancerType" for="freelancer-radio" class="ripple-effect-dark toggleType"><i class="icon-material-outline-account-circle"></i> {{ __('Candidate') }}</label>
                        </div>

                        <div>
                            <input value="0" @if(old('account-type-radio-page')  != null && old('account-type-radio-page')  == 0 ) checked @endif type="radio" name="account-type-radio-page" id="employer-radio-page" class="account-type-radio account-type-radio-page"/>
                            <label id="employerType" for="employer-radio" class="ripple-effect-dark toggleType"><i class="icon-material-outline-business-center"></i> {{ __('Employer') }}</label>
                        </div>
                    </div>

                    <!-- Form -->

                        @csrf
                        @if(Session::has('user') && Session::get('user')['first_name'])
                            <input type="hidden" value="{{ Session::get('user')['first_name'] }}" name="first_name">
                            @else
                        <div class="input-with-icon-left register-input">
                            <i class="icon-feather-user"></i>
                            <input value="{{Session::has('user') ? Session::get('user')['first_name'] : old('first_name')}}" type="text" class="input-text with-border" name="first_name" placeholder="{{ __('First Name') }}" />
                            @if ($errors->has('first_name'))
                                <small>
                                    {{ __($errors->first('first_name')) }}
                                </small>
                            @endif
                        </div>
                        @endif
                        @if(Session::has('user') && Session::get('user')['last_name'])
                            <input type="hidden" value="{{ Session::get('user')['last_name'] }}" name="last_name">
                        @else
                        <div class="input-with-icon-left register-input">
                            <i class="icon-feather-user"></i>
                            <input value="{{Session::has('user') ? Session::get('user')['last_name'] : old('last_name')}}" type="text" class="input-text with-border" name="last_name" placeholder="{{ __('Last Name') }}" />
                            @if ($errors->has('last_name'))
                                <small>
                                    {{ __($errors->first('last_name')) }}
                                </small>
                            @endif
                        </div>
                        @endif
                        @if(Session::has('user') && Session::get('user')['email'])
                            <input type="hidden" value="{{ Session::get('user')['email'] }}" name="email">
                        @else
                        <div class="input-with-icon-left register-input">
                            <i class="icon-feather-mail"></i>
                            <input value="{{Session::has('user') ? Session::get('user')['email'] : old('email')}}" type="text" class="input-text with-border" name="email" placeholder="{{ __('Email Address') }}" />
                            @if ($errors->has('email'))
                                <small>
                                    {{ __($errors->first('email')) }}
                                </small>
                            @endif
                        </div>
                        @endif
                        <div class="input-with-icon-left register-input" title="{{__('Should be at least 6 characters long')}}" data-tippy-placement="bottom">
                            <i class="icon-feather-lock"></i>
                            <input value="{{old('password')}}" type="password" class="input-text with-border" name="password"  placeholder="{{ __('Password') }}" />
                            @if ($errors->has('password'))
                                <small>
                                    {{ __($errors->first('password')) }}
                                </small>
                            @endif
                        </div>

                        <div class="input-with-icon-left register-input">
                            <i class="icon-feather-lock"></i>
                            <input value="{{old('password_confirmation')}}"  type="password" class="input-text with-border" name="password_confirmation"  placeholder="{{ __('Repeat Password') }}" />
                            @if ($errors->has('password_confirmation'))
                                <small>
                                    {{ __($errors->first('password_confirmation')) }}
                                </small>
                            @endif
                        </div>


                        <!-- Button -->
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10" id="submit-register-page-btn" type="submit" >{{ __('Register') }} <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>
                    <!-- Social Login -->
                @if(Session::has('user'))
                    @else
                        <div class="social-login-separator"><span>or</span></div>
                        <div class="social-login-buttons">
                            <button onclick="window.location.href='{{route('handleProviderRedirect', 'facebook')}}'" id="facebook-register-page-btn" class="facebook-login ripple-effect"><i class="icon-brand-facebook-f"></i> {{ __('Register via Facebook') }}</button>
                            <button onclick="window.location.href='{{route('handleProviderRedirect', 'google')}}'" id="google-register-page-btn" class="google-login ripple-effect"><i class="icon-brand-google-plus-g"></i> {{ __('Register via Google') }}</button>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>


    <!-- Spacer -->
    <div class="margin-top-70"></div>
    <script>
        $(document).ready(function(){

            $('#freelancerType').on('click' , function () {
                $('#freelancer-radio-page').prop("checked", true);
            })

            $('#employerType').on('click' , function () {
                $('#employer-radio-page').prop("checked", true);
            })
            @if(count($errors) == 0)
            $('#freelancer-radio-page').prop("checked", false);
            $('#employer-radio-page').prop("checked", false);
            $('#register-account-form-page  .register-input').css('opacity','0.5');
            $("#register-account-form-page  .register-input :input").prop("disabled", true);

            $('#submit-register-page-btn').css('opacity','0.5');
            $('#submit-register-page-btn').css('cursor','not-allowed');
            $("#submit-register-page-btn").attr("disabled", true);
            @endif

        });

        $('.toggleType').on('click', function () {

            $('#register-account-form-page .register-input').css('opacity','1');
            $("#register-account-form-page .register-input :input ").prop("disabled", false);

            $('#submit-register-page-btn').css('opacity','1');
            $('#submit-register-page-btn').css('cursor','pointer');
            $("#submit-register-page-btn").attr("disabled", false);

        })
    </script>
@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
