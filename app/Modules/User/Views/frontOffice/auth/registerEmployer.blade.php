
@extends('frontOffice.layout',['title' => 'Hired | Registration'])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')

    <style media="screen">

        #validation {
            opacity: 0;
        }

        .selected {
            opacity: 0;
            animation: slide-in 0.8s forwards;
            -webkit-animation: slide-in 0.8s forwards;
        }

        .dismiss {
            animation: slide-out 0.5s forwards;
            -webkit-animation: slide-out 0.5s forwards;
        }

        @keyframes slide-in {
            0% {
                -webkit-transform: translateX(100%);
            }
            50% {
                -webkit-transform: translateX(100%);
                opacity: 0;
            }
            100% {
                -webkit-transform: translateX(0%);
                opacity: 1;
            }
        }

        @-webkit-keyframes slide-in {
            0% {
                transform: translateX(100%);
            }
            100% {
                transform: translateX(0%);
            }
        }

        @keyframes slide-out {
            0% {
                transform: translateX(0%);
            }
            100% {
                transform: translateX(-100%);
            }
        }

        @-webkit-keyframes slide-out {
            0% {
                -webkit-transform: translateX(0%);
            }
            100% {
                -webkit-transform: translateX(-100%);
            }
        }

        #data label{
            display: block;
            margin: 10px 0 5px 0;
            font-size: 11px;
            line-height: normal;
            font-weight: bold;
        }
        .text-error{
            color: red;
            font-size: 10px;
            line-height: normal;
        }

        .error{
            border: 1px solid red;
        }

        #data input[type="text"].error{
            border: 1px solid red;
        }
        #data h1{
            padding: 0 2%;
        }
        #data p{
            margin: 0;
            background: lightgray;
        }
        #data input[type="text"],
        #data input[type="password"]{
            width: 60px;
            height: 60px;
            margin-right: 10px;
            margin-bottom: 0;
            text-align: center;
        }
        #data input[type="text"]:not(:last-of-type),
        #data input[type="password"]:not(:last-of-type){
            margin-right: 5px;
        }
        #data input[type="text"].error,
        #data input[type="password"].error{
            border-color: #ff0000;
        }
        .col{
            float: left;
            margin: 2%;
            padding: 1% 2% 2% 2%;
            width: 42%;
            background: white;
        }

    </style>
    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>{{ __('Register As Employer') }}</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('showHomePage') }}">{{ __('Home') }}</a></li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">

                <div class="login-register-page">
                    <!-- Welcome Text -->
                    <div class="welcome-text">
                        <h3 style="font-size: 26px;">{{ __("Let's create your account!") }}</h3>
                        <span>{{ __('Please fill the required informations below.') }}</span>
                            <small id="invalid-employer-register">
                            </small>
                    </div>

                    <!-- Account Type -->
                    <!-- Form -->


                    <div id="#main-content">
                        <div class="section" id="employerDetails">
                            <form method="post" action="javascript:void(0)" id="register-employer-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-with-icon-left register-input">
                                        <i class="icon-feather-user"></i>
                                        <input type="text" class="input-text with-border" id="employer-firstName" placeholder="{{ __('First Name') }}" />
                                        <small id="register-employer-firstName-error"></small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="input-with-icon-left register-input">
                                        <i class="icon-feather-user"></i>
                                        <input type="text" class="input-text with-border" id="employer-lastName" placeholder="{{ __('Last Name') }}" />
                                        <small id="register-employer-lastName-error"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="input-with-icon-left register-input">
                                <i class="icon-material-baseline-mail-outline"></i>
                                <input type="text" class="input-text with-border" id="register-employer-email" placeholder="{{ __('Email Address') }}" />
                                <small id="register-employer-email-error"></small>
                            </div>

                            <div class="input-with-icon-left register-input" title="Should be at least 6 characters long" data-tippy-placement="bottom">
                                <i class="icon-material-outline-lock"></i>
                                <input  type="password" class="input-text with-border" id="password-employer" placeholder="{{ __('Password') }}" />
                                <small id="register-employer-password-error"></small>
                            </div>

                            <div class="input-with-icon-left register-input">
                                <i class="icon-material-outline-lock"></i>
                                <input type="password" class="input-text with-border" enabled id="password-confirmation" placeholder="{{ __('Repeat Password') }}" />
                                <small id="register-employer-password-repeat-error"></small>
                            </div>
                                <button id="register-employer-btn" type="submit" form="register-employer-form" style="float:right;width: 30%;"  class="button  button-sliding-icon ripple-effect validationSlide">{{ __('Next') }}<i class="icon-material-outline-arrow-right-alt"></i></button>

                            </form>

                        </div>


                        <div class="section" id="validation" style="margin-top: -30px;; text-align: center">
                                <div id="data" >
                                    <label>Enter the code here:</label>
                                    <div data-pin style="align-items: center;justify-content: center;display: flex;"></div>
                                    <span class="pin-error" style="color: red"></span>
                                    <input type="hidden" id="returnedEmployerEmail">
                                </div>

                                <button  type="button"  style="float:right;width: 33%!important;margin-top: 15px"  class="button  button-sliding-icon ripple-effect companySlide">Next<i class="icon-material-outline-arrow-right-alt"></i></button>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>



    <!-- Spacer -->
    <div class="margin-top-70"></div>

    <script type="text/javascript">
        (function ($, document) {
            $(document).ready(function () {
                $('[data-pin]').pin({
                    displayMessage: $('.pin-error'),
                    allowSequential: false,
                    allowRepeat: false,
                    count: 6
                });

                $('[name="next"]').on('click', function (e) {
                    e.preventDefault();
                    $('[data-pin]').data('plugin_pin').init(true);


                });
            });
        })(jQuery, document);
    </script>
    <script src="{{asset('frontOffice/js/pin.js')}}" type="text/javascript"></script>

    <script type="text/javascript">

            $("#register-employer-form").submit(function(event) {
                event.preventDefault();
                $('#register-employer-btn').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
                $('#register-employer-btn').css('opacity','0.5');
                $('#register-employer-btn').css('cursor','not-allowed');
                $("#register-employer-btn").attr("disabled", true);
                $('#register-employer-email-error').text('');
                $('#register-employer-password-error').text('');
                $('#register-employer-password-repeat-error').text('');
                $('#register-employer-firstName-error').text('');
                $('#register-employer-lastName-error').text('');
                $('#invalid-employer-register').text('');
                var data = {
                    'email' : $('#register-employer-email').val(),
                    'password' : $('#password-employer').val(),
                    'password_confirmation' : $('#password-confirmation').val(),
                    'first_name' : $('#employer-firstName').val(),
                    'last_name' : $('#employer-lastName').val()
                }

                $.post('{{route('apiHandleRegisterEmployer')}}',
                    {
                        '_token': $('meta[name=csrf-token]').attr('content'), data : data

                    })
                    .done(function (res) {
                        $('#register-employer-btn').html('Next <i class="icon-material-outline-arrow-right-alt"></i>')
                        $('#register-employer-btn').css('opacity','1');
                        $('#register-employer-btn').css('cursor','pointer');
                        $("#register-employer-btn").attr("disabled", false);

                        if(res.errors) {
                            if(res.errors.email) {

                                $('#register-employer-email-error').text(res.errors.email[0])
                            }
                            if(res.errors.password) {
                                if(res.errors.password[0] === 'Password must be confirmed !') {
                                    $('#register-employer-password-repeat-error').text(res.errors.password[0])
                                } else  {
                                    $('#register-employer-password-error').text(res.errors.password[0])
                                }

                            }

                            if(res.errors.first_name) {
                                $('#register-employer-firstName-error').text(res.errors.first_name[0])
                            }

                            if(res.errors.last_name) {
                                $('#register-employer-lastName-error').text(res.errors.last_name[0])
                            }
                        }else {

                            if(res.status === 200) {
                                $('#returnedEmployerEmail').val(res.user.email);
                                $('#employerDetails').addClass('dismiss').fadeOut();
                                $('#validation').css('margin-top', '0px');
                                $('.welcome-text h3').text('We have send you an email, please check your mail box!');
                                $('.welcome-text span').text('Copy the code you receiver here!');
                                $('#validation').addClass('selected');
                            }
                        }
                    })
                    .fail(function (res) {
                        $('#register-employer-btn').html('Next <i class="icon-material-outline-arrow-right-alt"></i>')
                        $('#register-employer-btn').css('opacity','1');
                        $('#register-employer-btn').css('cursor','pointer');
                        $("#register-employer-btn").attr("disabled", false);
                        $('#invalid-register-employer').text('Something went wrong, please try again !').fadeIn('slow').delay(2000).fadeOut();

                    })


            });

    </script>

    <script type="text/javascript">

        $(function() {

            $('.companySlide').on('click',function (event) {
                event.preventDefault();
                var elm = $(this);
                elm.html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
                elm.css('opacity','0.5');
                elm.css('cursor','not-allowed');
                elm.attr("disabled", true);

                var pin = $('[name="pin_0"]').val();
                pin += $('[name="pin_1"]').val();
                pin += $('[name="pin_2"]').val();
                pin += $('[name="pin_3"]').val();
                pin += $('[name="pin_4"]').val();
                pin += $('[name="pin_5"]').val();

                var data = {
                    'pin' : pin,
                    'email' : $('#returnedEmployerEmail').val()
                }

                $.post('{{route('apiHandleVerifyEmployer')}}',
                    {
                        '_token': $('meta[name=csrf-token]').attr('content'), data : data

                    })
                    .done(function (res) {
                        if(res.status == 400) {
                            $('#invalid-register-employer').text('The code you entered is incorrect!').fadeIn('slow').delay(2000).fadeOut();
                            $('.pin-error').html("The code you entered is incorrect");
                            $('#data input').addClass('error');
                             elm.html('Next <i class="icon-material-outline-arrow-right-alt"></i>')
                              elm.css('opacity','1');
                             elm.css('cursor','pointer');
                             elm.attr("disabled", false);
                        }else if(res.status == 200) {
                             window.location.href = res.url;
                        }else {
                             elm.html('Next <i class="icon-material-outline-arrow-right-alt"></i>')
                        elm.css('opacity','1');
                        elm.css('cursor','pointer');
                        elm.attr("disabled", false);
                            $('#invalid-register-employer').text('Something went wrong, please try again !').fadeIn('slow').delay(2000).fadeOut();

                        }
                        }).fail(function (res) {
                             elm.html('Next <i class="icon-material-outline-arrow-right-alt"></i>')
                        elm.css('opacity','1');
                        elm.css('cursor','pointer');
                        elm.attr("disabled", false);
                                  $('#invalid-register-employer').text('Something went wrong, please try again !').fadeIn('slow').delay(2000).fadeOut();

                        })

            })
        })

    </script>

@endsection




@section('footer')
    @include('frontOffice.inc.footer')
@endsection
