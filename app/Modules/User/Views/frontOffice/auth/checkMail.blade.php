@extends('frontOffice.layout', ['title' => __('Hired | Verification')])

@section('header')
    @include('frontOffice.inc.header')
@endsection

@section('content')


    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">

                <div class="login-register-page">

                    <div class="order-confirmation-page" style="padding-bottom: 25px!important;">
                        <div class="breathing-icon" style="font-size: 70px!important;"><i class="icon-feather-send"></i></div>
                        <h2 class="margin-top-30" style="    line-height: 3rem;">{{ __('Thank you for your registration!') }}</h2>
                        <p style="color: #2a41e8">{{ __('Check your mailbox to activate your account.') }}</p>
                        <p>Please note that verification mail can take up to three minutes to be delivered.</p>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="margin-top-70"></div>

@endsection


@section('footer')
    @include('frontOffice.inc.footer')
@endsection
