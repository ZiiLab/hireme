<?php

Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {

    Route::post('/api/login', 'UserController@apiHandleLogin')->name('apiHandleLogin');
    Route::post('/api/register', 'UserController@apiHandleRegister')->name('apiHandleRegister');
    Route::get('/social/{provider}', 'UserController@handleProviderRedirect')->name('handleProviderRedirect');
    Route::get('/social/{provider}/callback', 'UserController@handleProviderCallback')->name('handleProviderCallback');
    Route::get('/login', 'UserController@showLogin')->name('showLogin');
    Route::get('/register', 'UserController@showRegister')->name('showRegister');
    Route::get('/verification', 'UserController@showCheckMail')->name('showCheckMail');
    Route::post('/login', 'UserController@handleLogin')->name('handleLogin');
    Route::post('/register', 'UserController@handleRegister')->name('handleRegister');
    Route::get('/logout', 'UserController@handleLogout')->name('handleLogout');
    Route::get('/activate/{code}', 'UserController@handleMailValidation')->name('handleMailValidation');
    Route::get('/requestReset', 'UserController@showRequestResetPassword')->name('showRequestResetPassword');
    Route::post('/api/requestReset', 'UserController@apiResetPassword')->name('apiResetPassword');
    Route::get('/resetPassword/{code}', 'UserController@showResetPassword')->name('showResetPassword');
    Route::post('/resetPassword', 'UserController@handleResetPassword')->name('handleResetPassword');
    Route::post('/api/registerEmployer', 'UserController@apiHandleRegisterEmployer')->name('apiHandleRegisterEmployer');
    Route::post('/api/verifyEmployer', 'UserController@apiHandleVerifyEmployer')->name('apiHandleVerifyEmployer');
    Route::get('/registerEmployer', 'UserController@showRegisterEmployer')->name('showRegisterEmployer');
    Route::get('/loginProvider', 'UserController@showLoginProvider')->name('showLoginProvider');
    Route::get('/api/uploadFiles', 'UserController@apiHandleDeleteFreelancerFile')->name('apiHandleDeleteFreelancerFile');


    Route::get('/hu/{username}', 'UserController@showFreelancerDetails')->name('showFreelancerDetails');
    Route::get('/company/{companyName}', 'UserController@showCompanyDetails')->name('showCompanyDetails');
    Route::get('/api/get-portfolios', 'UserController@apiGetUserPortfolios')->name('apiGetUserPortfolios');


    Route::group(['module' => 'User', 'middleware' => ['user']], function() {


        Route::get('/start', 'UserController@showStartingInterface')->name('showStartingInterface');
        Route::get('/bidders/{id?}', 'UserController@showManageBidders')->name('showManageBidders');
        Route::get('/dashboard', 'UserController@showDashboard')->name('showDashboard');
        Route::get('/bids', 'UserController@showActiveBids')->name('showActiveBids');
        Route::get('/bookmarks', 'UserController@showBookmarks')->name('showBookmarks');
        Route::get('/messages', 'UserController@showMessages')->name('showMessages');
        Route::get('/post-task', 'UserController@showPostTask')->name('showPostTask');
        Route::get('/reviews', 'UserController@showReviews')->name('showReviews');
        Route::get('/settings', 'UserController@showSettings')->name('showSettings');
        Route::get('/manage-task', 'UserController@showManageTask')->name('showManageTask');
        Route::get('/inProgress', 'UserController@showInProgressTasks')->name('showInProgressTasks');
        Route::get('/manageInProgress', 'UserController@showManageInProgressTasks')->name('showManageInProgressTasks');
        Route::get('/interviews', 'UserController@showCalendar')->name('showCalendar');

        Route::post('/updateAccount', 'UserController@handleUpdateAccountInfos')->name('handleUpdateAccountInfos');
        Route::post('/updatePassword', 'UserController@handleUpdatePassword')->name('handleUpdatePassword');
        Route::post('/api/updateEmail', 'UserController@apiHandleUpdateEmail')->name('apiHandleUpdateEmail');
        Route::get('/updateEmail/{code}', 'UserController@handleUpdateEmail')->name('handleUpdateEmail');

         Route::get('/api/bookmark', 'UserController@handleBookmark')->name('handleBookmark');
         Route::get('/api/delete-bookmark', 'UserController@handleDeleteBookmark')->name('handleDeleteBookmark');

         Route::post('/api/updateProfile', 'UserController@apiHandleUpdateProfile')->name('apiHandleUpdateProfile');

                    Route::group(['module' => 'User', 'middleware' => ['employer']], function() {

                        Route::get('/createCompany', 'UserController@showAddCompanyAfterRegister')->name('showAddCompanyAfterRegister');
                        Route::post('/createCompany', 'UserController@apiHandleCreateFirstCompany')->name('apiHandleCreateFirstCompany');
                        Route::get('/myCompanies', 'UserController@showCompanies')->name('showCompanies');
                        Route::get('/post-job', 'UserController@showPostJob')->name('showPostJob');
                        Route::get('/manage-candidates/{id?}', 'UserController@showManageCandidates')->name('showManageCandidates');
                        Route::post('/settings/handleAddCompany', 'UserController@handleAddCompany')->name('handleAddCompany');
                        Route::post('/settings/DeleteCompany', 'UserController@HandleDeleteCompany')->name('HandleDeleteCompany');
                        Route::post('/settings/UpdateCompany/{companyId}', 'UserController@HandleUpdateCompany')->name('HandleUpdateCompany');
                        Route::get('/api/addSocialLinkCompany', 'UserController@apiHandleAddSocialLinkCompany')->name('apiHandleAddSocialLinkCompany');
                        Route::get('/api/addCompanySocialLink', 'UserController@apiHandleUpdateSocialLinkCompany')->name('apiHandleUpdateSocialLinkCompany');
                        Route::get('/mange-jobs', 'UserController@showManageJobs')->name('showManageJobs');
                        Route::post('/cancel-interview', 'UserController@handleDeleteInterview')->name('handleDeleteInterview');
                        Route::get('/redirect/interviews', 'UserController@redirectAfterScheduleInterview')->name('redirectAfterScheduleInterview');

                    });

        Route::group(['module' => 'User', 'middleware' => ['freelancer']], function() {
            Route::get('/profile', 'UserController@showProfile')->name('showProfile');
            Route::post('/api/save-employment', 'UserController@apiHandleSaveEmployment')->name('apiHandleSaveEmployment');
            Route::get('/api/delete-employment', 'UserController@apiHandleDeleteEmployment')->name('apiHandleDeleteEmployment');
            Route::post('/settings/profile', 'UserController@handleUpdateFreelancerInfo')->name('handleUpdateFreelancerInfo');
            Route::get('/api/addSkill', 'UserController@apiHandleAddSkill')->name('apiHandleAddSkill');
            Route::get('/api/removeSkill', 'UserController@apiHandleDeleteSkill')->name('apiHandleDeleteSkill');
            Route::get('/api/setHourRate', 'UserController@apiHandleSetHourRate')->name('apiHandleSetHourRate');
            Route::post('/api/uploadFiles', 'UserController@apiHandleUploadFreelancerFiles')->name('apiHandleUploadFreelancerFiles');
            Route::get('/api/addSocialLink', 'UserController@apiHandleAddSocialLink')->name('apiHandleAddSocialLink');
            Route::post('/uplaodIntro', 'UserController@handleUploadIntroVideo')->name('handleUploadIntroVideo');
            Route::post('/updateIntro', 'UserController@handleUpdateIntroVideo')->name('handleUpdateIntroVideo');
            Route::get('/register-steps', 'UserController@showFreelancerSteps')->name('showFreelancerSteps');
            Route::get('/confirm-interview-date', 'UserController@handleConfirmInterviewDate')->name('handleConfirmInterviewDate');
        });
        });
});
