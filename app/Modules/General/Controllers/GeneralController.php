<?php

namespace App\Modules\General\Controllers;

use App\Modules\Hired\Models\Job;
use App\Modules\Hired\Models\Service;
use App\Modules\Hired\Models\Task;
use App\Modules\General\Models\JobCategory;
use App\Modules\General\Models\TaskCategory;
use App\Modules\User\Models\Company;
use App\Modules\User\Models\User;
use App\Modules\General\Models\Note;
use App\Modules\General\Models\Contact;
use App\Modules\General\Models\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Spatie\Sitemap\SitemapGenerator;
use App\Modules\Blog\Models\Article;


class GeneralController extends Controller
{

    /**
     * Show Home Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showHomePage()
    {

        $fjRows =  json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Featured Jobs Items'] ;
        $rtRows =  json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Recent Tasks Items'] ;
        $fsRows =  json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Featured Services Items'] ;
        $hrf = json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Rated Candidates Items'] ;

        return view("General::index",[
            'featuredJobs' => Job::orderBy('created_at','desc')->take($fjRows)->get(),
            'recentTasks' => Task::orderBy('created_at','desc')->take($rtRows)->get(),
            'taskCategories' => TaskCategory::all(),
            'freelancers' => User::where('type',1)->where('status',2)->orderBy('created_at','asc')->take($hrf)->get(),
            'featuredServices' => Service::groupBy('user_id')->take($fsRows)->get(),
            'articles' => Article::where('status',0)->get()
        ]);
    }

    /**
     * Browse Tasks.
     *
     * @return \Illuminate\Http\Response
     */
    public function showTasks(Request $request)
    {

      $tr = json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Tasks Items'] ;

        if(Session::has('es')) {
            return view("General::browseTasks", [
                'tasks' => Session::get('tasks'),
                'count' => Session::get('count')
            ]);
        }
        if($request->has('offset')) {
            $tasks  = Task::where('status',1);

            if ($request->has('lat')) {
                $sqlDistance = DB::raw
                ('
              ( 6371 * acos
                  ( cos
                      ( radians
                          (' . $request->lat . ')
                      )
                  * cos
                      ( radians
                          ( lat )
                      )
                  * cos
                      ( radians
                          ( lon )
                      - radians
                          (' . $request->lon . ')
                      )
                  + sin
                      ( radians
                          (' . $request->lat  . ')
                      )
                  * sin
                      ( radians
                          ( lat )
                      )
                  )
              )
            ');
                $tasks->when($sqlDistance != null, function ($query) use ($sqlDistance,$request){
                    $query->whereHas('address', function ($subQuery) use ($sqlDistance,$request) {
                        $subQuery->addSelect(DB::raw("{$sqlDistance} AS distance"));
                        $subQuery->havingRaw("distance <= ?", [(int)$request->range]);
                    });
                })
                    ->with('user')
                    ->with('address');

            }

            if ($request->has('key')) {
                $tasks->where('name', 'like', '%' . $request->key . '%');
            }

            if ($request->has('cat')) {
                $cat = explode(',',$request->cat);

                $tasks->whereIn('category_id', $cat);
            }

            if ($request->has('type')) {
                $types = explode(',',$request->type);
                $tasks->whereHas('budget', function ($subQuery) use($request,$types) {
                   $subQuery->whereIn('type',$types);
                });

            }
               if ($request->has('remote')) {
               
                $tasks->whereNull('address_id');
            }


                 if($request->has('hs')) {

                return view("General::browseTasks", [
                    'tasks' => $tasks->orderBy('created_at','desc')->skip($request->offset * $tr)->take($tr)->get(),
                    'count' => count($tasks->get())
                ]);

            }

            $view = view('General::loaders.tasks', [
                'tasks' => $tasks->orderBy('created_at','desc')->skip($request->offset * $tr)->take($tr)->get()
            ])->render();

            return response()->json(['html' => $view , 'count' => count($tasks->get()) ]);

        }

        return view("General::browseTasks", [
            'tasks' => Task::where('status',1)->orderBy('created_at','desc')->take($tr)->get(),
            'count' => count(Task::where('status',1)->get())
        ]);
    }

    /**
     * Browse companies.
     *
     * @return \Illuminate\Http\Response
     */
    public function showBrowseCompanies(Request $request)
    {
       $cr = json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Companies Items'] ;
        if($request->has('letter')) {
            $companies = Company::where('name', 'LIKE', $request->get('letter').'%')->get();
            $view = view('General::loaders.companies', [
                'companies' => $companies
            ])->render();

            return response()->json(['html' => $view]);

        }
        return view("General::browseCompanies" , [
            'companies' => Company::take($cr)->get()
        ]);
    }

    /**
     * Browse Jobs.
     *
     * @return \Illuminate\Http\Response
     */
    public function showJobs(Request $request)
    {
      $jr = json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Jobs Items'] ;
         if(Session::has('es')) {
             return view("General::browseJobs", [
                 'jobs' => Session::get('jobs'),
                 'count' => Session::get('count')
             ]);
         }
        if($request->has('offset')) {

            $salary = explode(',',$request->salary);

            $jobs  = Job::where(function ($q) use ($salary) {
                $q->whereBetween('min_salary',$salary)->orWhereBetween('max_salary',$salary);
            });
            if ($request->has('lat')) {
                $sqlDistance = DB::raw
                ('
              ( 6371 * acos
                  ( cos
                      ( radians
                          (' . $request->lat . ')
                      )
                  * cos
                      ( radians
                          ( lat )
                      )
                  * cos
                      ( radians
                          ( lon )
                      - radians
                          (' . $request->lon . ')
                      )
                  + sin
                      ( radians
                          (' . $request->lat  . ')
                      )
                  * sin
                      ( radians
                          ( lat )
                      )
                  )
              )
            ');
                $jobs->when($sqlDistance != null, function ($query) use ($sqlDistance,$request){
                    $query->whereHas('address', function ($subQuery) use ($sqlDistance,$request) {
                        $subQuery->addSelect(DB::raw("{$sqlDistance} AS distance"));
                        $subQuery->havingRaw("distance <= ?", [(int)$request->range]);
                    });
                })
                    ->with('company')
                    ->with('address');


            }

            if ($request->has('key')) {
                $jobs->where('title', 'like', '%' . $request->key . '%');
            }

            if ($request->has('cat')) {
                $cat = explode(',',$request->cat);

                $jobs->whereIn('category_id', $cat);
            }

            if ($request->has('type')) {
                $type = explode(',',$request->type);

                $jobs->whereIn('type', $type);
            }

             if ($request->has('remote')) {
               
                $jobs->whereNull('address_id');
            }

            if($request->has('hs')) {

                return view("General::browseJobs", [
                    'jobs' => $jobs->orderBy('created_at','desc')->skip($request->offset * $jr)->take($jr)->get(),
                    'count' => count($jobs->get())
                ]);

            }

            $view = view('General::loaders.jobs', [
                'jobs' => $jobs->orderBy('created_at','desc')->skip($request->offset * $jr)->take($jr)->get()
            ])->render();

            return response()->json(['html' => $view , 'count' => count($jobs->get()) ]);

        }



        return view("General::browseJobs", [
            'jobs' => Job::orderBy('created_at','desc')->take($jr)->get(),
            'count' => count(Job::all())
        ]);
    }



    /**
     * Browse Jobs.
     *
     * @return \Illuminate\Http\Response
     */
    public function showFreelancers(Request $request)
    {
            $fr = json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Candidates Items'] ;

        if(Session::has('es')) {
            return view("General::browseFreelancers", [
                'freelancers' => Session::get('freelancers'),
                'count' => Session::get('count')
            ]);
        }

        if($request->has('offset')) {

            $freelancers  = User::where('type',1)->where('status',2);

            if ($request->has('lat')) {
                $sqlDistance = DB::raw
                ('
              ( 6371 * acos
                  ( cos
                      ( radians
                          (' . $request->lat . ')
                      )
                  * cos
                      ( radians
                          ( lat )
                      )
                  * cos
                      ( radians
                          ( lon )
                      - radians
                          (' . $request->lon . ')
                      )
                  + sin
                      ( radians
                          (' . $request->lat  . ')
                      )
                  * sin
                      ( radians
                          ( lat )
                      )
                  )
              )
            ');
                $freelancers->when($sqlDistance != null, function ($query) use ($sqlDistance,$request){
                    $query->whereHas('address', function ($subQuery) use ($sqlDistance,$request) {
                        $subQuery->addSelect(DB::raw("{$sqlDistance} AS distance"));
                        $subQuery->havingRaw("distance <= ?", [(int)$request->range]);
                    });
                })
                    ->with('address');


            }

            if ($request->has('key')) {
                $freelancers->whereHas('info', function ($subQuery) use ($request) {
                    $subQuery->where('tagline', 'like', '%' . $request->key . '%');
                });
            }

           /* if($request->has('rate')) {
                $rate = explode(',', $request->rate);
                $freelancers->whereHas('preference', function ($subQuery) use ($rate) {
                    $subQuery->whereBetween('hour_rate',[(int)$rate[0] , (int)$rate[1]]);
                });
            }*/

            if($request->has('skills')) {
                $skills = explode(',',$request->skills);
                $freelancers->whereHas('skills', function ($subQuery) use ($skills) {
                    $subQuery->whereIn('label',$skills);
                });
            }

            $view = view('General::loaders.freelancers', [
                'freelancers' => $freelancers->orderBy('created_at','desc')->skip($request->offset * $fr)->take($fr)->get()
            ])->render();

            return response()->json(['html' => $view , 'count' => count($freelancers->get()) ]);

        }

        return view("General::browseFreelancers", [
            'freelancers' => User::where('type',1)->where('status',2)->orderBy('created_at','desc')->take($fr)->get(),
            'count' => count(User::where('type',1)->where('status',2)->get())
        ]);
    }


    /**
     * Browse contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function showContact()
    {
      return view("General::contact");

    }

    public function apiSubscribeToNewsletter(Request $request) {
        $email = $request->data['email'];
        $newsletter = Newsletter::where('email',$email)->first();
        if($newsletter) {
            return response()->json(['exist' => 'Email already subscribed, thank you.']);
        }else {
            Newsletter::create([
               'email' => $email,
               'user_id' => Auth::check() ? Auth::id() : null
            ]);
            return response()->json(['success' => 'Thank you, you are now subscribed to our awesome newsletters.']);
        }
    }

    public function apiContactUs(Request $request) {
        $rules = [
            'email' => 'required|email',
            'name' => 'required',
            'subject' => 'required',
            'content' => 'required'
        ];

        $messages = [
            'email.required'          => 'Email is required!',
            'email.email'             => 'Invalid email!',
            'subject.required'       => 'Please, specify a subject!',
            'content.required'            => 'The message body is required!',
        ];

        $validation = Validator::make($request->data, $rules, $messages);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()]) ;
        }

        Contact::create([
            'email' => $request->data['email'],
            'name' => $request->data['name'],
            'subject' => $request->data['subject'],
            'content' => $request->data['content'],
            'user_id' => Auth::check() ? Auth::id() : null
        ]);

        return response()->json(['success' => 200]);


    }


    public function apiHandleAddNote(Request $request) {

       $note = Auth::user()->notes()->create([
            'priority' => $request->data['priority'],
            'body' => $request->data['body']
        ]);

       switch ($request->data['priority']) {
           case 0 :
               $class = 'low';
               $priority = 'Low priority';
               break;

           case 1 :
               $class = 'medium';
               $priority = 'Medium priority';
               break;

           case 2 :
               $class = 'high';
               $priority = 'High priority';
               break;

       }

        return response()->json(['body' => $note->body, 'class' => $class, 'priority' => $priority, 'id' => $note->id ]);
    }

    public function apiHandleUpdateNote(Request $request) {

        $note = Note::find($request->data['id']);
        if(!$note) {
            return response()->json(['notFound' => 404]);
        }
        $note->update([
            'priority' => $request->data['priority'],
            'body' => $request->data['body']
        ]);

        return response()->json(['updated' => 200]);
    }

    public function apiHandleDeleteNote(Request $request) {

        $note = Note::find($request->get('id'));
        if(!$note) {
            return response()->json(['notFound' => 404]);
        }

        if(count(Auth::user()->notes) > 10) {
            $nextNote = Auth::user()->notes()->orderBy('created_at','desc')->skip(10)->take(1)->first();
            switch ($nextNote->priority) {
                case 0 :
                    $class = 'low';
                    $priority = 'Low priority';
                    break;

                case 1 :
                    $class = 'medium';
                    $priority = 'Medium priority';
                    break;

                case 2 :
                    $class = 'high';
                    $priority = 'High priority';
                    break;

            }
            $note->delete();
            return response()->json(['deletedWithNextNote' => 200, 'nextNote' => ['body' => $nextNote->body, 'class' => $class, 'priority' => $priority, 'id' => $nextNote->id ]]);

        }else {
            $note->delete();

            return response()->json(['deleted' => 200]);
        }


    }

    public function showServices(Request $request) {

       $sr =  json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Services Items'] ;

        if(Session::has('es')) {
            return view("General::browseServices", [
                'services' => Session::get('services'),
                'count' => Session::get('count')
            ]);
        }

        if($request->has('offset')) {

            $cost = explode(',',$request->costRange);


            $services  = Service::where(function ($q) use ($cost) {
                $q->whereBetween('min',$cost)->orWhereBetween('max',$cost);
            });

            if ($request->has('key')) {

                $services->where('name', 'like', '%' . $request->key . '%');
            }

            if ($request->has('cat')) {

                $cat = explode(',',$request->cat);
                $services->whereIn('category_id', $cat);
            }

            if($request->has('skills')) {
                $skills = explode(',',$request->skills);
                $services->whereHas('skills', function ($subQuery) use ($skills) {
                    $subQuery->whereIn('label',$skills);
                });
            }

            $view = view('General::loaders.services', [
                'services' => $services->orderBy('created_at','desc')->skip($request->offset * $sr)->take($sr)->get()
            ])->render();

            return response()->json(['html' => $view , 'count' => count($services->get()) ]);

        }



        return view('General::browseServices', [
            'services' => Service::orderBy('created_at','desc')->take($sr)->get(),
            'count' => count(Service::all())
        ]);
    }

    public function handleSubscribeForEmailAlerts(Request $request) {


        Auth::user()->emailAlerts()->create([
            'type' => $request->get('type'),
            'query' => $request->get('query')
        ]);

        return response()->json(['status' => 200]);
    }

    public function handleUnsubscribeForEmailAlerts($id) {
        $eAlert = EmailAlert::find($id);

        if(!$eAlert) {
            //error
        }

        $eAlert->delete();
    }

    public function showSearchedData($list,$type) {
        $data = explode('_',$list);
        switch ($type) {
            case 'job' :
                $jobs = Job::whereIn('id',$data)->get();
                return redirect()->route('showJobs')->with( ['jobs' => $jobs,'count' => count($jobs) ,'es' => 'true' ] );
            case 'task' :
                $tasks = Task::whereIn('id',$data)->get();
                return redirect()->route('showTasks')->with( ['tasks' => $tasks,'count' => count($tasks) ,'es' => 'true' ] );
            case 'service' :
                $services = Service::whereIn('id',$data)->get();
                return redirect()->route('showServices')->with( ['services' => $services,'count' => count($services) ,'es' => 'true' ] );
            case 'freelancer' :
                $freelancers = User::whereIn('id',$data)->get();
                return redirect()->route('showFreelancers')->with( ['freelancers' => $freelancers,'count' => count($freelancers) ,'es' => 'true' ] );
        }
    }


    public function showAdminDashboard() {

        return view('General::admin.index');
    }

    public function showJobCategories() {
      return view('General::admin.jobCategories', [
        'categories' => JobCategory::all()
      ]);
    }

    public function handleAddJobCategory() {
      $data = Input::all();

      $rules = [
        'label' => 'required'
      ];

       $messages = [
        'label.required' => 'Category label is required'
      ];

       $validation = Validator::make($data, $rules, $messages);

          if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        JobCategory::create([
          'label' => $data['label']
        ]);

            Session::flash('message', 'Category created Successfully!');
            Session::flash('color', '#5f9025');
            return back();
    }

    public function handleDeleteJobCategory() {
      $data = Input::all();
      $jC = JobCategory::find($data['cId']);
      if(!$jC) {
        Session::flash('message', 'Some things went wrong, please try again!');
          Session::flash('color', '#de5959');
          return back();
      }

       $jC->delete();
        Session::flash('message', 'Category deleted Successfully!');
            Session::flash('color', '#5f9025');
            return back();

    }


    public function showTaskCategories() {
      return view('General::admin.taskCategories', [
        'categories' => TaskCategory::all()
      ]);
    }

    public function handleAddTaskCategory() {
      $data = Input::all();

      $rules = [
        'label' => 'required'
      ];

       $messages = [
        'label.required' => 'Category label is required'
      ];

       $validation = Validator::make($data, $rules, $messages);

          if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        TaskCategory::create([
          'label' => $data['label']
        ]);

            Session::flash('message', 'Category created Successfully!');
            Session::flash('color', '#5f9025');
            return back();
    }

    public function handleDeleteTaskCategory() {
      $data = Input::all();
      $tC = TaskCategory::find($data['cId']);
      if(!$tC) {
        Session::flash('message', 'Some things went wrong, please try again!');
          Session::flash('color', '#de5959');
          return back();
      }

       $tC->delete();
        Session::flash('message', 'Category deleted Successfully!');
            Session::flash('color', '#5f9025');
            return back();

    }

    public function showPrivacyPage() {

      return view('General::privacy');
    }

      public function showTermsPage() {

      return view('General::terms');
    }




}
