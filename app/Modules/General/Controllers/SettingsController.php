<?php

namespace App\Modules\General\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Session;


class SettingsController extends Controller
{

	public function showGeneralSettings()
	{
			return view('General::admin.settings');
	}

	public function apiGetSettings()
	{
			$settings = json_decode(file_get_contents(storage_path().'/settings/settings.json'), true);

			return response()->json($settings);
	}


	public function apiSaveSettings(Request $req)
	{
		 $settings = $req->get('settings');

  		 file_put_contents(storage_path().'/settings/settings.json', stripslashes($settings));

		 return response()->json(['status'=> 200]);

	}

	public function handleUpdateLogos() {
		$data = Input::all();

		if(isset($data['headerLogo'])) {
			    $media = \App\Modules\User\Models\Media::where('type' , 100)->first();
			    unlink(public_path($media->link));
                $link = uploadFile($data['headerLogo'], 'logo');
                $media->update([
                    'link' => $link
                ]);
		}

		if(isset($data['footerLogo'])) {
			$media = \App\Modules\User\Models\Media::where('type' , 101)->first();
			 unlink(public_path($media->link));
                $link = uploadFile($data['footerLogo'], 'logo');
                $media->update([
                    'link' => $link
                ]);
		}

		if(isset($data['svg'])) {
		$media = \App\Modules\User\Models\Media::where('type' , 102)->first();
			 unlink(public_path($media->link));
                $link = uploadFile($data['svg'], 'svg');
                $media->update([
                    'link' => $link
                ]);
		}
			Session::flash('message', 'Image updated successfully');
            Session::flash('color', '#5f9025');
		return back();

	}


}
