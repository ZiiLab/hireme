<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'priority',
        'body',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

}
