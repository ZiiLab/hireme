<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class TaskCategory extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'task_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label'
    ];

    public function tasks()
    {
        return $this->hasMany('App\Modules\Hired\Models\Task');
    }


}
