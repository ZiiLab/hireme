<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;

class EmailAlert extends Model {

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_alerts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'query',
        'url',
        'user_id'
    ];

    protected $casts = [
        'query' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

}
