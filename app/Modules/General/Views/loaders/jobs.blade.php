
@foreach($jobs as $job)
    <!-- Job Listing -->
    <a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}" class="job-listing">

        <!-- Job Listing Details -->
        <div class="job-listing-details">
            <!-- Logo -->
            <div class="job-listing-company-logo">
                <img src="{{ $job->company->medias()->where('type',0)->first() ? $job->company->medias()->where('type',0)->first()->link :'frontOffice/images/company-logo-placeholder.png' }}" alt="{{ $job->company->name }}">
            </div>

            <!-- Details -->
            <div class="job-listing-description">
                <h4 class="job-listing-company">{{ $job->company->name }} </h4>
                <h3 class="job-listing-title">{{ $job->title }}</h3>
            </div>
        </div>

        <!-- Job Listing Footer -->
        <div class="job-listing-footer">
            @if(Auth::check() && Auth::id() != $job->user->id)
                <span class="bookmark-icon {{ Auth::user()->hasBookmarked($job) ? 'bookmarked' : '' }}" data-id="{{ $job->id }}"></span>
            @endif
            <ul>
                <li><i class="icon-material-outline-location-{{ $job->address ? 'on' :'off' }}"></i> {{ $job->address ? $job->address->address :'Remote'}} </li>
                <li><i class="icon-material-outline-business-center"></i> {{ jobTypeToString($job->type) }} </li>
                <li><i class="icon-material-outline-account-balance-wallet"></i> {{ $job->min_salary.' TND' }} - {{ $job->max_salary.' TND' }}</li>
                <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($job->created_at)->diffForHumans() }} </li>
            </ul>
        </div>
    </a>
@endforeach
