@foreach($tasks as $task)
    <!-- Task -->
    <a href="{{ route('showTaskDetails', $task->getTaskParamName()) }}" class="task-listing">

        <!-- Job Listing Details -->
        <div class="task-listing-details">

            <!-- Details -->
            <div class="task-listing-description">
                <h3 class="task-listing-title">{{ $task->name }}</h3>
                <ul class="task-icons">
                    <li><i class="icon-material-outline-location-on"></i> {{ $task->address ? $task->address->address :'Remote'  }} </li>
                    <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</li>
                </ul>
            </div>

        </div>

        <div class="task-listing-bid">
            <div class="task-listing-bid-inner">
                <div class="task-offers">
                    <strong>{{ $task->budget->min.' TND' }} - {{ $task->budget->max.' TND' }}</strong>
                    <span>{{ $task->budget->type == 0 ? 'Fixed Price' : 'Hourly Price' }}</span>
                </div>
                <span class="button button-sliding-icon ripple-effect">{{__('Bid Now')}} <i class="icon-material-outline-arrow-right-alt"></i></span>
            </div>
        </div>
    </a>
@endforeach