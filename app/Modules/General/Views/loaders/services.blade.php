@foreach($services as $service)
    <!-- Task -->
    <a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}" class="task-listing">

        <!-- Job Listing Details -->
        <div class="task-listing-details">

            <!-- Details -->
            <div class="task-listing-description">
                <h3 class="task-listing-title">{{ $service->name }}</h3>
                <ul class="task-icons">
                    <li><i class="icon-feather-user"></i> {{ $service->user->getFullName() }} </li>
                    <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($service->created_at)->diffForHumans() }}</li>
                </ul>
            </div>

        </div>

        <div class="task-listing-bid">
            <div class="task-listing-bid-inner">
                <div class="task-offers">
                    <strong>{{ $service->min.' TND' }} - {{ $service->max.' TND' }}</strong>
                </div>
                <span class="button button-sliding-icon ripple-effect">{{__('Book Now')}} <i class="icon-material-outline-arrow-right-alt"></i></span>
            </div>
        </div>
    </a>
@endforeach
