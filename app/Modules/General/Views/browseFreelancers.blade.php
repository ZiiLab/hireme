@extends('frontOffice.layout',['class' => 'gray','title' =>  __('Browse Candidates')])

@section('header')
@include('frontOffice.inc.header',['headerClass' => 'not-sticky'])
@endsection

@section('content')
    <style>
        .sliderMargin > .slider {
                margin-top: 15px;
        }
    </style>

  <!-- Page Content
  ================================================== -->
  <div class="full-page-container">

    <div class="full-page-sidebar">
      <div class="full-page-sidebar-inner" data-simplebar>
        <div class="sidebar-container">

          <!-- Location -->
          <div class="sidebar-widget sliderMargin">
            <h3>{{__('Location')}}</h3>
            <div class="input-with-icon">
              <div id="autocomplete-container">
                <input id="autocomplete-input" type="text" placeholder="{{__('Location')}}">
                            <input type="hidden"  id="lat">
                            <input type="hidden"  id="lon">
              </div>
              <i class="icon-material-outline-location-on"></i>
            </div>
                    <small style="color: black">{{__('Raduis')}} (KM)</small>

                    <input class="range-slider-single" id="range" type="text" value="" data-slider-currency="Km"   data-slider-min="5" data-slider-max="50" data-slider-step="1" data-slider-value="20"/>
                </div>


          <!-- Keywords -->
          <div class="sidebar-widget">
            <h3>{{ __('Keyword') }}</h3>
            <div>
              <div class="keyword-input-container">
                <input type="text" id="key"  placeholder="{{ __('e.g. tagline') }}"/>
              </div>
              <div class="keywords-list"><!-- keywords go here --></div>
              <div class="clearfix"></div>
            </div>
          </div>


          <!-- Hourly Rate -->{{--
          <div class="sidebar-widget">
            <h3>Hourly Rate</h3>
            <div class="margin-top-55"></div>

            <!-- Range Slider -->
            <input class="range-slider" id="rate" type="text" value="" data-slider-currency="TND " data-slider-min="10" data-slider-max="250" data-slider-step="5" data-slider-value="[10,250]"/>
          </div>--}}

          <!-- Tags -->
          <div class="sidebar-widget">
            <h3>{{ __('Skills')}}</h3>

            <div class="tags-container">
           
              @foreach(\App\Modules\User\Models\Skill::whereNotNull('user_id')->groupBy('label')->inRandomOrder()->take(6)->get() as $key => $skill)
              <div class="tag">
                <input value="{{$skill->label}}" type="checkbox" id="tag{{$key+1}}"/>
                <label for="tag{{$key+1}}">{{$skill->label}}</label>
              </div>
              @endforeach
            </div>
            <div class="clearfix"></div>

            <!-- More Skills -->
            <div class="keywords-container margin-top-20">
              <div class="keyword-input-container">
                <input type="text" class="keyword-input" placeholder="{{ __('add more skills') }}"/>
                <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
              </div>
              <div class="keywords-list"><!-- keywords go here --></div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="margin-bottom-40"></div>

        </div>
        <!-- Sidebar Container / End -->

        <!-- Search Button -->
        <div class="sidebar-search-button-container">
          <button class="button ripple-effect handleFilterJobs">{{ __('Search') }}</button>
        </div>
        <!-- Search Button / End-->

      </div>
    </div>
    <!-- Full Page Sidebar / End -->

    <!-- Full Page Content -->
    <div class="full-page-content-container" data-simplebar>
      <div class="full-page-content-inner">

        <h3 class="page-title">{{ __('Search Results') }}</h3>

        <div class="notify-box margin-top-15">
                <div class="switch-container">
                    @if(Auth::check())
                        <label class="switch"><input type="checkbox" id="emailAlerts"><span class="switch-button"></span><span class="switch-text">
                                {{ __('Turn on email alerts for this search') }}</span></label>
                    @else
                        <label class="switch"><input type="checkbox" disabled><span class="switch-button"></span><span class="switch-text">
                                    <a href="#sign-in-dialog" class="popup-with-zoom-anim">{{ __('Log in') }}</a> {{ __('to be able to turn on email alerts for your searchs.')}}</span></label>
                    @endif
                </div>

        </div>

        <!-- Freelancers List Container -->
        <div class="freelancers-container listings-container freelancers-grid-layout margin-top-35">

                @foreach($freelancers as $freelancer)
          <!--Freelancer -->
          <div class="freelancer">

            <!-- Overview -->
            <div class="freelancer-overview">
              <div class="freelancer-overview-inner"> 

                <!-- Bookmark Icon -->
                @if(Auth::check() && Auth::id() != $freelancer->id)
                                <span class="bookmark-icon {{ Auth::user()->hasBookmarked($freelancer) ? 'bookmarked' : '' }}" data-id="{{ $freelancer->id }}"></span>
                            @endif
                <!-- Avatar -->
                <div class="freelancer-avatar">
                  <div class=""></div>
                  <a href="{{  route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}"><img src="{{asset( $freelancer->medias()->where('type',0)->first() ? $freelancer->medias()->where('type',0)->first()->link :  'frontOffice/images/user-avatar-placeholder.png')}}" alt=""></a>
                </div>

                <!-- Name -->
                <div class="freelancer-name">
                  <h4><a href="{{  route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}"> {{ $freelancer->getFullName() }}</a></h4>
                  <span>{{ $freelancer->info->tagline }}</span>
                </div>

                <!-- Rating -->
                <div class="freelancer-rating">
                                @if(count($freelancer->getAllRatings($freelancer->id,'desc','App\Modules\User\Models\User')) < 3)
                                    <span class="company-not-rated">Minimum of 3 votes required</span>
                                @else
                                    <div class="star-rating" data-rating="{{ $freelancer->averageRating(1)[0] }}"></div>
                                @endif
                </div>
              </div>
            </div>

            <!-- Details -->
            <div class="freelancer-details">
              <div class="freelancer-details-list">
                <ul>
                                <li>{{__('Address')}} <strong><i class="icon-material-outline-location-on"></i> {{ $freelancer->address->city ?? $freelancer->address->province }}</strong></li>
                                <li>{{__('Hourly Rate')}} <strong>{{ $freelancer->preference ? $freelancer->preference->hour_rate.' TND' : __('Not specified') }} / hr</strong></li>

                            </ul>
              </div>
              <a href="{{  route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}" class="button button-sliding-icon ripple-effect">{{__('View Profile')}} <i class="icon-material-outline-arrow-right-alt"></i></a>
            </div>
          </div>
          <!-- Freelancer / End -->
            @endforeach
        </div>
        <!-- Freelancers Container / End -->
            <div class="pagination-container margin-top-20 margin-bottom-20 ">
                <nav class="pagination">
                    <ul>
                        <button class="button ripple-effect resetFilters" style="display: none;">{{__('Reset filters')}}</button>
                    </ul>
                </nav>
            </div>
        <!-- Pagination -->
        <div class="clearfix"></div>
            @if($count > json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Candidates Items'])
                <div class="pagination-container margin-top-20 margin-bottom-20 ">
                    <nav class="pagination">
                        <ul>
                            <button class="button ripple-effect loadMore">{{__('Load more')}}</button>
                        </ul>
                    </nav>
                </div>
            @endif
        <div class="clearfix"></div>
        <!-- Pagination / End -->



      </div>
    </div>
    <!-- Full Page Content / End -->

  </div>

  <script>
      $(document).on('click','.bookmark-icon', function (e) {
          e.preventDefault();
          var elm = $(this);
          $.get('{{ route('handleBookmark') }}?type=user&id='+$(this).data('id')).done(function(res) {
              if(res.bookmarked) {
                  elm.addClass('bookmarked');
              }else {
                  elm.removeClass('bookmarked');
              }
          }).fail(function(error) {

          });
      });
      $(".keywords-container").each(function() {
          var keywordInput = $(this).find(".keyword-input");
          var keywordsList = $(this).find(".keywords-list");

          var count = $('.tags-container').find('.tag').length;

          function addKeyword() {
              count++;
                      var $newKeyword = $('\t<div class="tag">\n' +
                          '  \t\t\t\t\t\t\t<input value="'+ keywordInput.val() +'" type="checkbox" checked id="tag'+ count +'"/>\n' +
                          '  \t\t\t\t\t\t\t<label for="tag'+ count +'"> '+ keywordInput.val() +' </label>\n' +
                          '  \t\t\t\t\t\t</div>');
                      $('.tags-container').append($newKeyword).trigger('resizeContainer');
                      keywordInput.val("");

          }
          keywordInput.on('keyup', function(e) {
              if ((e.keyCode == 13) && (keywordInput.val() !== "")) {
                  addKeyword();
              }
          });
          $('.keyword-input-button').on('click', function() {
              if ((keywordInput.val() !== "")) {
                  addKeyword();
              }
          });
          $(document).on("click", ".keyword-remove", function() {
              $(this).parent().addClass('keyword-removed');
              function removeFromMarkup() {

                          $(".keyword-removed").remove();

                      }

              setTimeout(removeFromMarkup, 500);
              keywordsList.css({
                  'height': 'auto'
              }).height();
          });
          keywordsList.on('resizeContainer', function() {
              var heightnow = $(this).height();
              var heightfull = $(this).css({
                  'max-height': 'auto',
                  'height': 'auto'
              }).height();
              $(this).css({
                  'height': heightnow
              }).animate({
                  'height': heightfull
              }, 200);
          });
          $(window).on('resize', function() {
              keywordsList.css({
                  'height': 'auto'
              }).height();
          });
          $(window).on('load', function() {
              var keywordCount = $('.keywords-list').children("span").length;
              if (keywordCount > 0) {
                  keywordsList.css({
                      'height': 'auto'
                  }).height();
              }
          });
      });

$("#autocomplete-input").keyup(function() {

    if (!this.value) {
        $('#lat').val();
        $('#lon').val();
    }

});
      var offset = 0;
      $('.handleFilterJobs').on('click', function () {
          $('.listings-container').html('<div class="loader"></div>');
          $('.loadMore').hide();
          $('.resetFilters').hide();
          offset = 0;
          var skills = [];
          var key = $('#key').val();
          var lat = $('#lat').val();
          var lon =  $('#lon').val();
          var range = $('#range').val();

          $('.tag input:checkbox:checked').each( function (i,item) {
              skills[i]  = item.value;
          });

          var  url = '{{ route('showFreelancers') }}?offset='+offset;
          var query = {};

          if(lat != '') {
              url += '&lat='+lat+'&lon='+lon+'&range='+range;
              query.lat = lat;
              query.lon = lat;
              query.range = range;
          }

          if(key != '') {
              url += '&key='+key;
              query.key = key;
          }

          if(skills.length != 0) {
              url += '&skills='+skills;
              query.skills = skills;
          }

          if($('#emailAlerts').is(':checked')) {
              $.get('{{ route('handleSubscribeForEmailAlerts') }}?type=freelancer&query='+JSON.stringify(query)).done(function (res) {
                  console.log(res);
              });
          }

          $.get(url).done( function (res) {
              if(res.html.length !=0) {
                  $('.listings-container').html(res.html);
                  if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Candidates Items'] }}') {
                      $('.loadMore').show();
                  }
              }else {
                  $('.listings-container').html('<p> No jobs were found, please try to change the filters or reset them by clicking the button below. </p>');
                  $('.resetFilters').show();
              }


          }).fail(function (err) {
              $('.listings-container').html('<p> No jobs were found, please try to change the filters or reset them by clicking the button below. </p>');
              $('.resetFilters').show();
          });

          $("#emailAlerts").prop("checked", false);

      });

      $('.loadMore').on('click', function () {
            $('.loadMore').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>');
            offset++;
          var skills = [];
          var key = $('#key').val();
          var lat = $('#lat').val();
          var lon =  $('#lon').val();
          var range = $('#range').val();

          $('.tag input:checkbox:checked').each( function (i,item) {
              skills[i]  = item.value;
          });

          var  url = '{{ route('showFreelancers') }}?offset='+offset;

          if(lat != '') {
              url += '&lat='+lat+'&lon='+lon+'&range='+range;
             
          }

          if(key != '') {
              url += '&key='+key;
          }

          if(skills.length != 0) {
              url += '&skills='+skills;
          }

            $.get(url).done( function (res) {
                $('.listings-container').append(res.html);
                if(res.count < '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Candidates Items'] }}') {
                    $('.loadMore').hide();
                }
                $('.loadMore').html('{{__('Load more')}}');

            }).fail(function (err) {
                console.log(err);
            })
        });

      $('.resetFilters').on('click', function () {
          $('.listings-container').html('<div class="loader"></div>');
          $('.loadMore').hide();
          $('.resetFilters').hide();

          $('#key').val('');
          $(".tag input:checkbox").prop('checked',false);
          $('#lat').val('');
          $('#lon').val('');
          $('#autocomplete-input').val('');

          var  url = '{{ route('showFreelancers') }}?offset=0';
          $.get(url).done( function (res) {
              $('.listings-container').html(res.html);
              if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Candidates Items'] }}') {
                  $('.loadMore').show();
              }
          })

      })



  </script>
  <script type="text/javascript">
      function initAutocomplete() {
          var input = document.getElementById('autocomplete-input');
          var autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.addListener('place_changed', function() {
              var placeUser = autocomplete.getPlace();
              var lat = placeUser.geometry.location.lat();
              var lng = placeUser.geometry.location.lng();
              $('#lat').val(lat);
              $('#lon').val(lng);
          });
      }
  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

  <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>
@endsection
