@extends('frontOffice.layout',['class' => 'gray','title' => 'Task Categories'])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

    <div class="dashboard-container">
        @include('General::admin.sideBar')
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner">

                <div class="dashboard-headline">
                    <h3>Task Categories</h3>

                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>Categories</li>
                        </ul>
                    </nav>
                </div>
                <div class="row">

                    <div class="col-xl-6">
                        <div class="dashboard-box">
                            <div class="headline">
                                <h3><i class="icon-line-awesome-tags"></i> Categories</h3>
                            </div>
                            <div class="content">
                                <ul class="dashboard-box-list">
                                   @foreach($categories as $category)
                                        <li>
                                            <div class="invoice-list-item">
                                                <strong>{{ $category->label }}</strong>

                                            </div>
                                            <!-- Buttons -->
                                            <div class="buttons-to-right">
                                                <a href="#" class="button red ripple-effect ico deleteCategory" data-id="{{ $category->id }}" data-tippy-placement="left" data-tippy="" data-original-title="Remove"><i class="icon-feather-trash-2"></i></a>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="dashboard-box">
                            <div class="headline">
                                <h3><i class="icon-line-awesome-tags"></i> Add New</h3>
                            </div>
                            <div class="content">
                                <form action="{{ route('handleAddTaskCategory') }}" method="post">
                                    @csrf
                                    <div class="submit-field" style="padding: 25px;margin-bottom: 0px!important">
                                        <h5>Category Name</h5>
                                        <input type="text" name="label" class="with-border" placeholder="Category name">
                                        @if ($errors->has('name'))
                                            <small>{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                <div class="col-xl-12">
                                    <button type="submit" class="button ripple-effect big "><i class="icon-feather-plus"></i> Submit</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
    </div>
    </div>

    <script>
        $(document).on("click", ".deleteCategory", function(e) {
            e.preventDefault();
            var cId = $(this).data('id');
            $.confirm({
                title: 'Confirm!',
                content: 'Are you sure you want to delete this category ?',
                buttons: {
                    cancel: function () {
                    },
                    somethingElse: {
                        text: 'Yes',
                        btnClass: 'button ripple-effect',
                        keys: ['enter', 'shift'],
                        action: function(){
                            deleteForm('{{route('handleDeleteTaskCategory')}}', {
                                cId : cId
                            }, 'post');
                        }
                    }
                }
            });
        });

    </script>
@endsection
