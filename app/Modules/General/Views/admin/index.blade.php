@extends('frontOffice.layout',['class' => 'gray','title' => 'Admin Dashboard'])
@section('header')
    @include('frontOffice.inc.header',['headerClass' => 'dashboard-header not-sticky'])
@endsection

@section('content')

    <div class="dashboard-container">
    @include('General::admin.sideBar')
    </div>
@endsection
