@extends('frontOffice.layout',['title' => __('Hired Homepage')])
@section('meta')

    <meta name="description" content="Hire experts or be hired for any job, any time.
Hired is the best solution for small businesses to turn their ideas into reality.">
    <meta name="keywords" content="Freelance Tunisia, Work, Work Online, Find Job, Jobs, Find Tunisian Freelancer">
@endsection
@section('header')
@include('frontOffice.inc.header')
@endsection

@section('content')


    <!-- Intro Banner
    ================================================== -->
    <!-- add class "disable-gradient" to enable consistent background overlay -->
    <div class="intro-banner" data-background-image="{{asset('frontOffice/images/home-background.jpg')}}">

        <div class="container">
            @if(Session::has('loggedIn'))
                <div class="notification notice closeable">
                    <p>{{Session::get('loggedIn')}}</p>
                    <a class="close"></a>
                </div>
        @endif

                @if(Session::has('restricted'))
                    <div class="notification error closeable">
                        <p>{{Session::get('restricted')}}</p>
                        <a class="close"></a>
                    </div>
                @endif
            <!-- Intro Headline -->
            <div class="row">
                <div class="col-md-12">
                    <div class="banner-headline">
                        <h3>
                            <strong>{{ __('Hire experts or be hired for any job, any time.') }}</strong>
                            <br>
                            <span><strong class="color">Hired</strong> {{ __('is the best solution for small businesses to turn their ideas into reality.') }}</span>
                        </h3>
                    </div>
                </div>
            </div>

            <!-- Search Bar -->
            <div class="row">
                <div class="col-md-12">
                    <div class="intro-banner-search-form margin-top-95">

                        <!-- Search Field -->
                        <div class="intro-search-field with-autocomplete">
                            <label for="autocomplete-input" class="field-title ripple-effect">{{ __('Where?') }}</label>
                            <div class="input-with-icon">
                                <input id="autocomplete-input" type="text" placeholder="{{ __('Job Location') }}">
                                <input type="hidden"  id="lat">
                                <input type="hidden"  id="lon">
                                <i class="icon-material-outline-location-on"></i>
                            </div>
                        </div>

                        <!-- Search Field -->
                        <div class="intro-search-field">
                            <label for="intro-keywords" class="field-title ripple-effect">{{ __('What job you want?') }}</label>
                            <input id="intro-keywords" type="text" placeholder="{{ __('Job Title or Keywords') }}">
                        </div>

                        <!-- Button -->
                        <div class="intro-search-button">
                            <button class="button ripple-effect searchJobs" >{{ __('Search') }}</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Stats -->
            @if( json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Show Counters'] )
         <div class="row">
                <div class="col-md-12">
                    <ul class="intro-stats margin-top-45 hide-under-992px">
                        <li>
                            <strong class="counter">{{ count(\App\Modules\Hired\Models\Job::all()) }}</strong>
                            <span>{{ __('Jobs Posted') }}</span>
                        </li>
                        <li>
                            <strong class="counter">{{ count(\App\Modules\Hired\Models\Task::all()) }}</strong>
                            <span>{{ __('Tasks Posted') }}</span>
                        </li>
                        <li>
                            <strong class="counter">{{ count(\App\Modules\User\Models\User::where('type',1)->get()) }}</strong>
                            <span>{{__('Candidates')}}</span>
                        </li>
                    </ul>
                </div>
            </div>
@endif
        </div>
    </div>


    <!-- Content
    ================================================== -->
    <!-- Popular Job Categories -->
{{--    <div class="section margin-top-65 margin-bottom-30">
        <div class="container">
            <div class="row">

                <!-- Section Headline -->
                <div class="col-xl-12">
                    <div class="section-headline centered margin-top-0 margin-bottom-45">
                        <h3>Popular Categories</h3>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-list-layout-1.html" class="photo-box small" data-background-image="{{asset('frontOffice/images/job-category-01.jpg')}}">
                        <div class="photo-box-content">
                            <h3>Web / Software Dev</h3>
                            <span>612</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-list-layout-full-page-map.html" class="photo-box small" data-background-image="{{asset('frontOffice/images/job-category-02.jpg')}}">
                        <div class="photo-box-content">
                            <h3>Data Science / Analitycs</h3>
                            <span>113</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-grid-layout-full-page.html" class="photo-box small" data-background-image="{{asset('frontOffice/images/job-category-03.jpg')}}">
                        <div class="photo-box-content">
                            <h3>Accounting / Consulting</h3>
                            <span>186</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-list-layout-2.html" class="photo-box small" data-background-image="{{asset('frontOffice/images/job-category-04.jpg')}}">
                        <div class="photo-box-content">
                            <h3>Writing & Translations</h3>
                            <span>298</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-list-layout-1.html" class="photo-box small" data-background-image="{{asset('frontOffice/images/job-category-05.jpg')}}">
                        <div class="photo-box-content">
                            <h3>Sales & Marketing</h3>
                            <span>549</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-list-layout-full-page-map.html" class="photo-box small" data-background-image="{{asset('frontOffice/images/job-category-06.jpg')}}">
                        <div class="photo-box-content">
                            <h3>Graphics & Design</h3>
                            <span>873</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-grid-layout-full-page.html" class="photo-box small" data-background-image="images/job-category-07.jpg">
                        <div class="photo-box-content">
                            <h3>Digital Marketing</h3>
                            <span>125</span>
                        </div>
                    </a>
                </div>

                <div class="col-xl-3 col-md-6">
                    <!-- Photo Box -->
                    <a href="jobs-list-layout-2.html" class="photo-box small" data-background-image="images/job-category-08.jpg">
                        <div class="photo-box-content">
                            <h3>Education & Training</h3>
                            <span>445</span>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>--}}
    <!-- Features Cities / End -->


    <!-- Features Jobs -->
    <div class="section gray  padding-top-65 padding-bottom-75">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <!-- Section Headline -->
                    <div class="section-headline margin-top-0 margin-bottom-35">
                        <h3>{{ __('Featured Jobs') }}</h3>
                        <a href="{{ route('showJobs') }}" class="headline-link">{{ __('Browse All Jobs') }}</a>
                    </div>

                    <!-- Jobs Container -->
                    <div class="listings-container compact-list-layout margin-top-35">

                        <!-- Job Listing -->


                        <!-- Job Listing -->
                        @foreach($featuredJobs as $job)
                        <a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}" class="job-listing with-apply-button">

                            <!-- Job Listing Details -->
                            <div class="job-listing-details">

                                <!-- Logo -->
                                <div class="job-listing-company-logo">
                                    <img src="{{asset( $job->company->medias()->where('type',0)->first() ? $job->company->medias()->where('type',0)->first()->link : 'frontOffice/images/company-logo-placeholder.png' )}}" alt="">
                                </div>

                                <!-- Details -->
                                <div class="job-listing-description">
                                    <h3 class="job-listing-title">{{ $job->title }}</h3>

                                    <!-- Job Listing Footer -->
                                    <div class="job-listing-footer">
                                        <ul>
                                            <li><i class="icon-material-outline-business"></i> {{ $job->company->name }}
                                            </li>
                                            <li><i class="icon-material-outline-location-{{ $job->address ? 'on' : 'off' }}"></i>{{ $job->address ? $job->address->city : 'Remote' }}</li>
                                            <li><i class="icon-material-outline-business-center"></i> {{ jobTypeToString($job->type) }} </li>
                                            <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($job->created_at)->diffForHumans() }} </li>
                                        </ul>
                                    </div>
                                </div>

                                <!-- Apply Button -->
                                <span class="list-apply-button ripple-effect">Apply Now</span>
                            </div>
                        </a>
                        @endforeach

                    </div>
                    <!-- Jobs Container / End -->

                </div>
            </div>
        </div>
    </div>

    <!-- Featured Jobs / End -->

<div class="intro-banner dark-overlay" data-background-image="{{asset('frontOffice/images/home-background.jpg')}}" style="padding-top: 0px;
    padding-right: 0px;
    padding-bottom: 105px;
    padding-left: 0px;">

    <!-- Transparent Header Spacer -->
    <div class="transparent-header-spacer"></div>

    <div class="container">
        
        <!-- Intro Headline -->
        <div class="row">
            <div class="col-md-12">
                <div class="banner-headline">
                    <h3>
                        <strong>{{__('Hire experts freelancers for any job, any time.')}}</strong>
                        <br>
                        <span>{{__('Huge community of designers, developers and creatives ready for your project.')}}</span>
                    </h3>
                </div>
            </div>
        </div>
        
        <!-- Search Bar -->
        <div class="row">
            <div class="col-md-12">
                <div class="intro-banner-search-form margin-top-95">

                    <!-- Search Field -->
                 

                    <!-- Search Field -->
                    <div class="intro-search-field">
                        <label for ="intro-keywords" class="field-title ripple-effect">{{__('What you need done?')}}</label>
                        <input id="taskKey" type="text" placeholder="{{__('e.g. build me a website')}}">
                    </div>

                    <!-- Search Field -->
                    <div class="intro-search-field">
                    <label for ="intro-keywords" class="field-title ripple-effect">{{__('In which field?')}}</label>
                        <select class="selectpicker default taskCategory" multiple data-selected-text-format="count" data-size="7" title="{{__('All Categories')}}" >
                           @foreach($taskCategories as $category)
                             <option value="{{ $category->id }}">{{ $category->label }}</option>
                           @endforeach
                        </select>
                    </div>

                    <!-- Button -->
                    <div class="intro-search-button">
                        <button class="button ripple-effect searchTasks">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section gray padding-top-65 padding-bottom-75">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                
                <!-- Section Headline -->
                <div class="section-headline margin-top-0 margin-bottom-35">
                    <h3>{{ __('Recent Tasks') }}</h3>
                    <a href="{{ route('showTasks') }}" class="headline-link">{{__('Browse All Tasks')}}</a>
                </div>
                
                <!-- Jobs Container -->
                <div class="tasks-list-container compact-list margin-top-35">
                        
                    <!-- Task -->
                    @foreach($recentTasks as $task)
                    <a href="{{ route('showTaskDetails', $task->getTaskParamName()) }}" class="task-listing">

                        <!-- Job Listing Details -->
                        <div class="task-listing-details">

                            <!-- Details -->
                            <div class="task-listing-description">
                                <h3 class="task-listing-title">{{$task->name}}</h3>
                                <ul class="task-icons">
                                    <li><i class="icon-material-outline-location-{{ $task->address ? 'on' : 'off' }}"></i> {{ $task->address ? $task->address->city : 'Remote' }}</li>
                                    <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</li>
                                </ul>
                                <div class="task-tags margin-top-15">
                                    @foreach($task->skills as $tag)
                                    <span>{{$tag->label}}</span>
                                    @endforeach
                                </div>
                            </div>

                        </div>

                        <div class="task-listing-bid">
                            <div class="task-listing-bid-inner">
                                <div class="task-offers">
                                     <strong>{{ $task->budget->min.' TND' }} - {{ $task->budget->max.' TND' }}</strong>
                                    <span>{{ $task->budget->type == 0 ? 'Fixed Price' : 'Hourly Price' }}</span>
                                </div>
                                <span class="button button-sliding-icon ripple-effect">Bid Now <i class="icon-material-outline-arrow-right-alt"></i></span>
                            </div>
                        </div>
                    </a>
                    @endforeach

                </div>
                <!-- Jobs Container / End -->

            </div>
        </div>
    </div>
</div>

    <!-- Icon Boxes -->
    <div class="section padding-top-65 padding-bottom-65">
        <div class="container">
            <div class="row">

                <div class="col-xl-12">
                    <!-- Section Headline -->
                    <div class="section-headline centered margin-top-0 margin-bottom-5">
                        <h3>{{ __('How It Works?') }}</h3>
                    </div>
                </div>

                <div class="col-xl-4 col-md-4">
                    <!-- Icon Box -->
                    <div class="icon-box with-line">
                        <!-- Icon -->
                        <div class="icon-box-circle">
                            <div class="icon-box-circle-inner">
                                <i class="icon-line-awesome-lock"></i>
                                <div class="icon-box-check"><i class="icon-material-outline-check"></i></div>
                            </div>
                        </div>
                        <h3>{{__('Create an Account')}}</h3>
                        <p>{{__('We made an easy registration steps to help you create your account in few seconds!')}}</p>
                        @if(!Auth::check())
                        <a href="{{ route('showRegister') }}">Sign up now!</a>
                        @endif
                    </div>
                </div>

                <div class="col-xl-4 col-md-4">
                    <!-- Icon Box -->
                    <div class="icon-box with-line">
                        <!-- Icon -->
                        <div class="icon-box-circle">
                            <div class="icon-box-circle-inner">
                                <i class="icon-line-awesome-legal"></i>
                                <div class="icon-box-check"><i class="icon-material-outline-check"></i></div>
                            </div>
                        </div>
                        <h3>{{__('Post a Task')}}</h3>
                        <p>{{__('Easy and clear form, fill the required informations to well describe your project.')}}</p>
                    </div>
                </div>

                <div class="col-xl-4 col-md-4">
                    <!-- Icon Box -->
                    <div class="icon-box">
                        <!-- Icon -->
                        <div class="icon-box-circle">
                            <div class="icon-box-circle-inner">
                                <i class=" icon-line-awesome-trophy"></i>
                                <div class="icon-box-check"><i class="icon-material-outline-check"></i></div>
                            </div>
                        </div>
                        <h3>{{__('Choose an Expert')}} </h3>
                        <p>{{__('Your task will be published immediatly!, our experts will bid, and we will help to you choose the right one.')}}</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Icon Boxes / End -->



    <!-- Highest Rated Freelancers -->
    <div class="section gray padding-top-65 padding-bottom-70 full-width-carousel-fix">
        <div class="container">
            <div class="row">

                <div class="col-xl-12">
                    <!-- Section Headline -->
                    <div class="section-headline margin-top-0 margin-bottom-25">
                        <h3>{{ __('Highest Rated Candidates') }}</h3>
                        <a href="{{ route('showFreelancers') }}" class="headline-link">{{ __('Browse All Candidates') }}</a>
                    </div>
                </div>

                <div class="col-xl-12">
                    <div class="default-slick-carousel freelancers-container freelancers-grid-layout">

                        @foreach($freelancers as $freelancer)
                        <!--Freelancer -->
                        <div class="freelancer">

                            <!-- Overview -->
                            <div class="freelancer-overview">
                                <div class="freelancer-overview-inner">

                                    <!-- Bookmark Icon -->
                                  @if(Auth::check() && Auth::id() != $freelancer->id)
                                        <span class="bookmark-icon {{ Auth::user()->hasBookmarked($freelancer) ? 'bookmarked' : '' }}" data-id="{{ $freelancer->id }}"></span>
                                      @endif

                                    <!-- Avatar -->
                                    <div class="freelancer-avatar">
                                        <div class=""></div>
                                        <a href="{{ route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}"><img src="{{asset($freelancer->medias()->where('type',0)->first()? $freelancer->medias()->where('type',0)->first()->link :'frontOffice/images/user-avatar-placeholder.png')}}" alt="{{ $freelancer->getFullName() }}"></a>
                                    </div>

                                    <!-- Name -->
                                    <div class="freelancer-name">
                                        <h4><a href="{{ route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}">{{ $freelancer->getFullName() }} </a></h4>
                                        <span>{{ $freelancer->info->tagline }}</span>
                                    </div>

                                    <!-- Rating -->
                                    <div class="freelancer-rating">
                                        @if(count($freelancer->getAllRatings($freelancer->id,'desc','App\Modules\User\Models\User')) < 3)
                                            <span class="company-not-rated">{{ __('Minimum of 3 votes required') }}</span>
                                        @else
                                            <div class="star-rating" data-rating="{{ $freelancer->averageRating(1)[0] }}"></div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <!-- Details -->
                            <div class="freelancer-details">
                                <div class="freelancer-details-list">
                                    <ul style="text-align: center">
                                        <li>{{ __('Address') }} <strong><i class="icon-material-outline-location-on"></i> {{ $freelancer->address->city ?? $freelancer->address->province }}</strong></li>
                                        <li>{{ __('Hourly Rate')}} <strong>{{ $freelancer->preference ? $freelancer->preference->hour_rate.' TND' : '---' }} / hr</strong></li>

                                    </ul>
                                </div>
                                <a href="{{ route('showFreelancerDetails', $freelancer->getFreelancerUsername()) }}" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
                            </div>
                        </div>
                        <!-- Freelancer / End -->
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Highest Rated Freelancers / End-->
    <div class="section gray  padding-bottom-75">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                
                <!-- Section Headline -->
                <div class="section-headline margin-top-0 margin-bottom-35">
                    <h3>{{ __('Featured Services') }}</h3>
                    <a href="{{ route('showServices') }}" class="headline-link">{{__('Browse All Services')}}</a>
                </div>
                
                <!-- Jobs Container -->
                <div class="tasks-list-container compact-list margin-top-35">
                        
                    <!-- Task -->
                    @foreach($featuredServices as $service)
                    <a href="{{ route('showServiceDetails', $service->getServiceParamName()) }}" class="task-listing">

                        <!-- Job Listing Details -->
                        <div class="task-listing-details">

                            <!-- Details -->
                            <div class="task-listing-description">
                                <h3 class="task-listing-title"> {{$service->name}}</h3>
                                <ul class="task-icons">
                                    <li><i class="icon-feather-user"></i> {{ $service->user->getFullName() }}</li>
                                    <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($service->created_at)->diffForHumans() }}</li>
                                </ul>
                                <div class="task-tags margin-top-15">
                                    @foreach($service->skills as $tag)
                                    <span>{{$tag->label}}</span>
                                    @endforeach
                                </div>
                            </div>

                        </div>

                        <div class="task-listing-bid">
                            <div class="task-listing-bid-inner">
                                <div class="task-offers">
                                     <strong>{{ $service->min.' TND' }} - {{ $service->max.' TND' }}</strong>
                                     <span></span>
                                </div>
                                <span class="button button-sliding-icon ripple-effect">Book Now <i class="icon-material-outline-arrow-right-alt"></i></span>
                            </div>
                        </div>
                    </a>
                    @endforeach

                </div>
                <!-- Jobs Container / End -->

            </div>
        </div>
    </div>
</div>
@if( json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Show Blog'] )
    <div class="section padding-top-65 padding-bottom-50">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                
                <!-- Section Headline -->
                <div class="section-headline margin-top-0 margin-bottom-45">
                    <h3>{{ __('From The Blog') }}</h3>
                    <a href="{{route('showBlog')}}" class="headline-link">{{ __('View Blog') }}</a>
                </div>

                <div class="row">
                    @foreach($articles as $article)
                    <div class="col-xl-4">
                        <a href="{{ route('showPost', $article->getArticleParamTitle()) }}" class="blog-compact-item-container">
                            <div class="blog-compact-item">
                               <img src="{{ asset($article->medias()->where('type',10)->first()->link) }}" alt="{{ $article->title }}">
                                <span class="blog-item-tag">{{ $article->category->name }}</span>
                                <div class="blog-compact-item-content">
                                    <ul class="blog-post-tags">
                                        <li>{{ $article->created_at->format('d M, Y') }}</li>
                                    </ul>
                                    <h3>{{$article->title}}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
            
                </div>


            </div>
        </div>
    </div>
</div>
@endif
<div class="section border-top padding-top-45 padding-bottom-45">
    <!-- Logo Carousel -->
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <!-- Carousel -->
                <div class="col-md-12">
                    <div class="logo-carousel">
                        
                        <div class="carousel-item">
                            <a href="https://datadeep.tn" target="_blank" title="DataDeep"><img src="{{asset('frontOffice/images/datadeep-logo.png')}}" alt="DataDeep"></a>
                        </div>
                               <div class="carousel-item">
                            <a href="{{ route('showContact') }}" target="_blank" title="Be Our Next Sponsor"><img src="{{asset('frontOffice/images/next-sponsor.jpg')}}" alt="Be Our Next Sponsor"></a>
                        </div>
                        
                    </div>
                </div>
                <!-- Carousel / End -->
            </div>
        </div>
    </div>
</div>
    <!-- Membership Plans -->
{{--    <div class="section padding-top-60 padding-bottom-75">
        <div class="container">
            <div class="row">

                <div class="col-xl-12">
                    <!-- Section Headline -->
                    <div class="section-headline centered margin-top-0 margin-bottom-35">
                        <h3>Membership Plans</h3>
                    </div>
                </div>


                <div class="col-xl-12">

                    <!-- Billing Cycle  -->
                    <div class="billing-cycle-radios margin-bottom-70">
                        <div class="radio billed-monthly-radio">
                            <input id="radio-5" name="radio-payment-type" type="radio" checked>
                            <label for="radio-5"><span class="radio-label"></span> Billed Monthly</label>
                        </div>

                        <div class="radio billed-yearly-radio">
                            <input id="radio-6" name="radio-payment-type" type="radio">
                            <label for="radio-6"><span class="radio-label"></span> Billed Yearly <span class="small-label">Save 10%</span></label>
                        </div>
                    </div>

                    <!-- Pricing Plans Container -->
                    <div class="pricing-plans-container">

                        <!-- Plan -->
                        <div class="pricing-plan">
                            <h3>Basic Plan</h3>
                            <p class="margin-top-10">One time fee for one listing or task highlighted in search results.</p>
                            <div class="pricing-plan-label billed-monthly-label"><strong>$19</strong>/ monthly</div>
                            <div class="pricing-plan-label billed-yearly-label"><strong>$205</strong>/ yearly</div>
                            <div class="pricing-plan-features">
                                <strong>Features of Basic Plan</strong>
                                <ul>
                                    <li>1 Listing</li>
                                    <li>30 Days Visibility</li>
                                    <li>Highlighted in Search Results</li>
                                </ul>
                            </div>
                            <a href="pages-checkout-page.html" class="button full-width margin-top-20">Buy Now</a>
                        </div>

                        <!-- Plan -->
                        <div class="pricing-plan recommended">
                            <div class="recommended-badge">Recommended</div>
                            <h3>Standard Plan</h3>
                            <p class="margin-top-10">One time fee for one listing or task highlighted in search results.</p>
                            <div class="pricing-plan-label billed-monthly-label"><strong>$49</strong>/ monthly</div>
                            <div class="pricing-plan-label billed-yearly-label"><strong>$529</strong>/ yearly</div>
                            <div class="pricing-plan-features">
                                <strong>Features of Standard Plan</strong>
                                <ul>
                                    <li>5 Listings</li>
                                    <li>60 Days Visibility</li>
                                    <li>Highlighted in Search Results</li>
                                </ul>
                            </div>
                            <a href="pages-checkout-page.html" class="button full-width margin-top-20">Buy Now</a>
                        </div>

                        <!-- Plan -->
                        <div class="pricing-plan">
                            <h3>Extended Plan</h3>
                            <p class="margin-top-10">One time fee for one listing or task highlighted in search results.</p>
                            <div class="pricing-plan-label billed-monthly-label"><strong>$99</strong>/ monthly</div>
                            <div class="pricing-plan-label billed-yearly-label"><strong>$1069</strong>/ yearly</div>
                            <div class="pricing-plan-features">
                                <strong>Features of Extended Plan</strong>
                                <ul>
                                    <li>Unlimited Listings Listing</li>
                                    <li>90 Days Visibility</li>
                                    <li>Highlighted in Search Results</li>
                                </ul>
                            </div>
                            <a href="pages-checkout-page.html" class="button full-width margin-top-20">Buy Now</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>--}}
    <!-- Membership Plans / End-->


    <script type="text/javascript">
    $('.bookmark-icon').on('click', function () {
        var elm = $(this);
          $.get('{{ route('handleBookmark') }}?type=user&id='+$(this).data('id')).done(function(res) {
              if(res.bookmarked) {
                elm.addClass('bookmarked');
              }else {
                elm.removeClass('bookmarked');
              }
          }).fail(function(error) {

          });
    });

    $('.searchJobs').on('click', function () {
        var key = $('#intro-keywords').val();
        var lat = $('#lat').val();
        var lon =  $('#lon').val();

        var  url = '{{ route('showJobs') }}?offset=0&salary=700,3000';

        if(lat != '') {
            url += '&lat='+lat+'&lon='+lon+'&range=20';
        }

        if(key != '') {
            url += '&key='+key
        }

        url+='&hs=1';

        window.location.href = url;
    });

     $('.searchTasks').on('click', function () {
        var key = $('#taskKey').val();
         var categories = [];
         $(".taskCategory option:selected").each(function (i,item) {
              categories[i]  = item.value;
          });

        var  url = '{{ route('showTasks') }}?offset=0';


        if(key != '') {
            url += '&key='+key
        }

          if(categories.length != 0) {
              url += '&cat='+categories;
          }

        url+='&hs=1';

        window.location.href = url;
    });

    </script>
    <script type="text/javascript">
        function initAutocomplete() {
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var placeUser = autocomplete.getPlace();
                var lat = placeUser.geometry.location.lat();
                var lng = placeUser.geometry.location.lng();
                $('#lat').val(lat);
                $('#lon').val(lng);
            });
        }
    </script>

 <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>


    <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

@endsection


@section('footer')
      @include('frontOffice.inc.footer')
@endsection
