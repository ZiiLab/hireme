@extends('frontOffice.layout',['title' => __('Contact Us')])

@section('header')
@include('frontOffice.inc.header')
@endsection

@section('content')

    <style>
        #contact div input {
            margin-bottom: 0px;
        }
        .sentSuccess {
            color: green;
            text-align: center;
            font-size: 30px;
            margin-bottom: 35px;
        }
        .sentFail {
            color: red;
            text-align: center;
            font-size: 20px;
        } 
    </style>
  <!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>{{ __('Contact') }}</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="{{route('showHomePage')}}">{{ __('Home') }}</a></li>
						<li>{{ __('Contact') }}</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">

		<div class="col-xl-12">
			<div class="contact-location-info margin-bottom-50">
				<div class="contact-address">
					<ul>
						<li class="contact-address-headline">{{ __('Our Office') }}</li>
						<li>{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Address'] }}</li>
						<li>{{ __('Phone : ') }} {{json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Phone']}}</li>
						<li><a href="mailto:{{json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Email']}}"><span class="__cf_email__" data-cfemail="e885898184a88d90898598848dc68b8785">{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Email'] }}</span></a></li>
						<li>
							<div class="freelancer-socials">
								<ul>
									<li><a href="{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Facebook'] }}" title="Facebook" target="_blank" data-tippy-placement="top"><i class="icon-brand-facebook"></i></a></li>
									<li><a href="{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Contact Settings']['Linkedin'] }}" title="Linkedin" target="_blank" data-tippy-placement="top"><i class="icon-brand-linkedin"></i></a></li>

								</ul>
							</div>
						</li>
					</ul>

				</div>
				<div id="single-job-map-container">

                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3192.207918745488!2d10.191031714822014!3d36.86144177195051!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12e2cb358269b285%3A0xf34acd8728a92ecf!2sCENTRE%20AMINE!5e0!3m2!1sen!2stn!4v1584552473519!5m2!1sen!2stn" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>
		</div>

		<div class="col-xl-8 col-lg-8 offset-xl-2 offset-lg-2">

			<section id="contact" class="margin-bottom-60">
                <h3 id="submitStatus"></h3>
				<h3 class="headline margin-top-15 margin-bottom-35">{{ __('Any questions? Feel free to contact us!') }}</h3>
				<form action="javascript:void(0)" method="post" name="contactform" id="contactform" autocomplete="on">
					<div class="row">
						<div class="col-md-6">
							<div class="input-with-icon-left">
								<input class="with-border" name="name" type="text" id="name" placeholder="{{ __('Your Name') }}"  />
                                <small id="name-errors"></small>
								<i class="icon-material-outline-account-circle"></i>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-with-icon-left">
								<input class="with-border" name="email" type="text" id="email" placeholder="{{ __('Email Address') }}" />
                                <small id="email-errors"></small>
                                <i class="icon-material-outline-email"></i>
							</div>
						</div>
					</div>

					<div class="input-with-icon-left">
						<input class="with-border" name="subject" type="text" id="subject" placeholder="{{ __('Subject') }}"  />
                        <small id="subject-errors"></small>
                        <i class="icon-material-outline-assignment"></i>
					</div>

					<div>
						<textarea class="with-border" name="content" cols="40" rows="5" id="content" placeholder="{{ __('Message') }}" spellcheck="true"></textarea>
                        <small id="content-errors"></small>
                    </div>

                    <button type="submit" class="submit button margin-top-15" id="submit" form="contactform">{{ __('Submit Message') }}</button>

				</form>
			</section>

		</div>

	</div>
</div>

  <script>
      $( "#contactform" ).submit(function( event ) {
          event.preventDefault();
          $('#submit').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>')
          $('#submit').css('opacity','0.5');
          $('#submit').css('cursor','not-allowed');
          $("#submit").attr("disabled", true);

              var data = {
                  'email' : $('#email').val(),
                  'name' : $('#name').val(),
                  'subject' : $('#subject').val(),
                  'content' : $('#content').val()
              }

              $.post('{{route('apiContactUs')}}',
                  {
                      '_token': $('meta[name=csrf-token]').attr('content'), data : data

                  })
                  .done(function (res) {
                      $('#submit').html('Submit Message')
                      $('#submit').css('opacity','1');
                      $('#submit').css('cursor','pointer');
                      $("#submit").attr("disabled", false);

                      if(res.errors) {
                          if(res.errors.email) {
                              $('#email-errors').text(res.errors.email[0])
                          }
                          if(res.errors.name) {
                              $('#name-errors').text(res.errors.name[0])
                          }
                          if(res.errors.subject) {
                              $('#subject-errors').text(res.errors.subject[0])
                          }     if(res.errors.content) {
                              $('#content-errors').text(res.errors.content[0])
                          }

                      }else {
                          $('#email').val('');
                          $('#name').val('');
                          $('#subject').val('');
                          $('#content').val('');
                          $('#submitStatus').addClass('sentSuccess');
                          $('#submitStatus').text('Your message has been sent successfully, please feel free to contact us whenever you need help.').fadeIn('slow').delay(3000).fadeOut();

                      }

                  })
                  .fail(function (res) {
                      $('#submit').html('Submit Message')
                      $('#submit').css('opacity','1');
                      $('#submit').css('cursor','pointer');
                      $("#submit").attr("disabled", false);
                      $('#submitStatus').addClass('sentFail');
                      $('#submitStatus').text('Something went wrong, please try again!').fadeIn('slow').delay(2000).fadeOut();
                  })

      });
  </script>

  <!-- Container / End -->
@endsection



@section('footer')
	@include('frontOffice.inc.footer')
@endsection



