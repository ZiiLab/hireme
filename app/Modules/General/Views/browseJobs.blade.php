@extends('frontOffice.layout',['title' => __('Browse Jobs')])

@section('header')
@include('frontOffice.inc.header',['headerClass' => 'not-sticky'])
@endsection

@section('content')
    <style>
        .sliderMargin > .slider {
            margin-top: 15px;
        }
    </style>
	<div class="full-page-container">

		<div class="full-page-sidebar">
			<div class="full-page-sidebar-inner" data-simplebar>
				<div class="sidebar-container">

					<!-- Location -->
					<div class="sidebar-widget sliderMargin">
						<h3>{{__('Location')}}</h3>
						<div class="input-with-icon">
							<div id="autocomplete-container">
								<input id="autocomplete-input" type="text" placeholder="{{__('Location')}}">
                                <input type="hidden"  id="lat">
                                <input type="hidden"  id="lon">
							</div>
							<i class="icon-material-outline-location-on"></i>
						</div>
                        <small style="color: black">{{__('Raduis')}} (KM)</small>
						<input class="range-slider-single" id="range" type="text"  data-slider-min="5" data-slider-max="50" data-slider-step="1" data-slider-value="20"/>
					</div>
                            <div class="sidebar-widget">

                        <div class="switches-list">

                            <div class="switch-container">
                                <label class="switch"><input value="0" class="remoteFilter" type="checkbox"><span class="switch-button"></span> {{ __('Show only remote jobs') }}</label>
                            </div>
                        </div>

                    </div>

					<!-- Keywords -->
					<div class="sidebar-widget">
						<h3>{{__('Keyword')}}</h3>
						<div class="keywords-container">
							<div class="keyword-input-container">
								<input type="text" class="keyword-input" id="key" placeholder="{{ __('e.g. job title') }}"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

					<!-- Category -->
					<div class="sidebar-widget">
						<h3>{{__('Category')}}</h3>
						<select class="selectpicker default" id="categories" multiple data-selected-text-format="count" data-size="7" title="{{ __('All Categories') }}" >
                            @foreach (\App\Modules\General\Models\JobCategory::all() as $category)
                                <option value="{{$category->id}}">{{$category->label}}</option>
                            @endforeach
						</select>
					</div>

					<!-- Job Types -->
					<div class="sidebar-widget">
						<h3>{{ __('Job Type')}}</h3>

						<div class="switches-list">

							<div class="switch-container">
								<label class="switch"><input value="0" class="checkedType" type="checkbox"><span class="switch-button"></span> {{ __('Full Time') }}</label>
							</div>

							<div class="switch-container">
								<label class="switch"><input value="1" class="checkedType" type="checkbox"><span class="switch-button"></span> {{ __('Part Time') }}</label>
							</div>

							<div class="switch-container">
								<label class="switch"><input value="2" class="checkedType" type="checkbox"><span class="switch-button"></span> {{ __('Internship') }}</label>
							</div>
							<div class="switch-container">
								<label class="switch"><input value="3" class="checkedType" type="checkbox"><span class="switch-button"></span> {{ __('Temporary') }}</label>
							</div>
						</div>

					</div>

					<!-- Salary -->
					<div class="sidebar-widget">
						<h3>{{__('Salary')}}</h3>
                        <span>{{ __('This rang, represent the max and the min salaries that are currently present and specified by employers.') }}</span>
						<div class="margin-top-55"></div>

						<!-- Range Slider -->
						<input class="range-slider" id="salaryRange" type="text" value="" data-slider-currency="TND " data-slider-min="{{  \App\Modules\Hired\Models\Job::select('min_salary')->min('min_salary') }}" data-slider-max="{{  \App\Modules\Hired\Models\Job::select('max_salary')->max('max_salary') }}" data-slider-step="100" data-slider-value="{{ '['.\App\Modules\Hired\Models\Job::select('min_salary')->min('min_salary').','.\App\Modules\Hired\Models\Job::select('max_salary')->max('max_salary').']' }}"/>
					</div>

					<!-- Tags -->
					<div class="sidebar-widget">
					{{--	<h3>Tags</h3>

						<div class="tags-container">
							<div class="tag">
								<input value="front-end dev" class="checkedTag" type="checkbox" id="tag1"/>
								<label for="tag1">front-end dev</label>
							</div>
							<div class="tag">
								<input value="angular" class="checkedTag" type="checkbox" id="tag2"/>
								<label for="tag2">angular</label>
							</div>
							<div class="tag">
								<input value="react" class="checkedTag" type="checkbox" id="tag3"/>
								<label for="tag3">react</label>
							</div>
							<div class="tag">
								<input value="vue js" class="checkedTag" type="checkbox" id="tag4"/>
								<label for="tag4">vue js</label>
							</div>
							<div class="tag">
								<input value="web apps" class="checkedTag" type="checkbox" id="tag5"/>
								<label for="tag5">web apps</label>
							</div>
							<div class="tag">
								<input class="checkedTag" type="checkbox" id="tag6"/>
								<label for="tag6">design</label>
							</div>
							<div class="tag">
								<input class="checkedTag" type="checkbox" id="tag7"/>
								<label for="tag7">wordpress</label>
							</div>
						</div>--}}
						<div class="clearfix"></div>
					</div>

				</div>
				<!-- Sidebar Container / End -->

				<!-- Search Button -->
				<div class="sidebar-search-button-container">
					<button class="button ripple-effect handleFilterJobs">Search</button>
				</div>
				<!-- Search Button / End-->

			</div>
		</div>
		<!-- Full Page Sidebar / End -->

		<!-- Full Page Content -->
		<div class="full-page-content-container" data-simplebar>
			<div class="full-page-content-inner">

				<h3 class="page-title">{{ __('Search Results')}}</h3>

				<div class="notify-box margin-top-15">
					<div class="switch-container">
                        @if(Auth::check())
						<label class="switch"><input type="checkbox" id="emailAlerts"><span class="switch-button"></span><span class="switch-text">
                                {{ __('Turn on email alerts for this search') }}</span></label>
                        @else
                            <label class="switch"><input type="checkbox" disabled><span class="switch-button"></span><span class="switch-text">
                                    <a href="#sign-in-dialog" class="popup-with-zoom-anim">{{ __('Log in') }}</a> {{ __('to be able to turn on email alerts for your searchs.') }}</span></label>
                        @endif
                    </div>

			{{--		<div class="sort-by">
						<span>Sort by:</span>
						<select class="selectpicker hide-tick">
							<option>Relevance</option>
							<option>Newest</option>
							<option>Oldest</option>
							<option>Random</option>
						</select>
					</div>--}}
				</div>

				<div class="listings-container grid-layout margin-top-35">
                     @if(count($jobs) == 0)   
                     <p> {{ __('No jobs were found, please try to change the filters or reset them by clicking the button below.')}} </p>
                        @else
                    @foreach($jobs as $job)
					<!-- Job Listing -->
					<a href="{{ route('showJobDetails', $job->getJobParamTitle()) }}" class="job-listing">

						<!-- Job Listing Details -->
						<div class="job-listing-details">
							<!-- Logo -->
							<div class="job-listing-company-logo">
								<img src="{{ $job->company->medias()->where('type',0)->first() ? $job->company->medias()->where('type',0)->first()->link :'frontOffice/images/company-logo-placeholder.png' }}" alt="{{ $job->company->name }}">
							</div>

							<!-- Details -->
							<div class="job-listing-description">
								<h4 class="job-listing-company">{{ $job->company->name }}</h4>
								<h3 class="job-listing-title">{{ $job->title }}</h3>
							</div>
						</div>

						<!-- Job Listing Footer -->
						<div class="job-listing-footer">
                            @if(Auth::check() && Auth::id() != $job->user->id)
                                <span class="bookmark-icon {{ Auth::user()->hasBookmarked($job) ? 'bookmarked' : '' }}" data-id="{{ $job->id }}"></span>
                            @endif
							<ul>
								<li><i class="icon-material-outline-location-{{ $job->address ? 'on' :'off' }}"></i> {{ $job->address ? $job->address->address :'Remote'}} </li>
                                <li><i class="icon-material-outline-business-center"></i> {{ jobTypeToString($job->type) }} </li>
								<li><i class="icon-material-outline-account-balance-wallet"></i> {{ $job->min_salary.' TND' }} - {{ $job->max_salary.' TND' }}</li>
                                <li><i class="icon-material-outline-access-time"></i> {{ Carbon\Carbon::parse($job->created_at)->diffForHumans() }} </li>
							</ul>
						</div>
					</a>
                        @endforeach
                        @endif
				</div>

				<!-- Pagination -->
                <div class="pagination-container margin-top-20 margin-bottom-20 ">
                    <nav class="pagination">
                     @if(count($jobs) ==0) 
                        <ul>
                            <button class="button ripple-effect resetFilters" >{{ __('Reset filters') }}</button>
                        </ul>
                        @else
                           <ul>
                            <button class="button ripple-effect resetFilters" style="display: none;">{{ __('Reset filters') }}</button>
                        </ul>
                        @endif
                    </nav>
                </div>
				<div class="clearfix"></div>
			    @if($count > json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Jobs Items'])
                    <div class="pagination-container margin-top-20 margin-bottom-20 ">
                        <nav class="pagination">
                            <ul>
                                <button class="button ripple-effect loadMore">{{ __('Load more') }}</button>
                            </ul>
                        </nav>
                    </div>
                @endif
				<div class="clearfix"></div>
				<!-- Pagination / End -->



			</div>
		</div>
		<!-- Full Page Content / End -->

	</div>

    <script type="text/javascript">
        $('.bookmark-icon').on('click', function (e) {
            e.preventDefault();
            var elm = $(this);
            $.get('{{ route('handleBookmark') }}?type=job&id='+$(this).data('id')).done(function(res) {
                if(res.bookmarked) {
                    elm.addClass('bookmarked');
                }else {
                   elm.removeClass('bookmarked');
                }
            }).fail(function(error) {

            });
        });

        $("#autocomplete-input").keyup(function() {

    if (!this.value) {
        $('#lat').val();
        $('#lon').val();
    }

});
		var offset = 0;

        $('.handleFilterJobs').on('click', function () {
            $('.listings-container').html('<div class="loader"></div>');
			$('.loadMore').hide();
            $('.resetFilters').hide();

            offset = 0;
            var jobTypes = [];
            var categories = [];
                $('.checkedType:checkbox:checked').each( function (i,item) {
                    jobTypes[i]  = item.value;
                });

            var key = $('#key').val();
            var salaryRange = $('#salaryRange').val();
            var range = $('#range').val();
             $("#categories option:selected").each(function (i,item) {
                 categories[i]  = item.value;
            });
            var lat = $('#lat').val();
            var lon =  $('#lon').val();

            var  url = '{{ route('showJobs') }}?offset='+offset+'&salary='+salaryRange;
            var query = {
                'salaryRange' : salaryRange
            }


            if(lat != '') {
                url += '&lat='+lat+'&lon='+lon+'&range='+range;
                query.lat = lat;
                query.lon = lat;
                query.range = range;
            }

			if(key != '') {
                url += '&key='+key;
                query.key = key;
			}

			if(categories.length != 0) {
                url += '&cat='+categories;
               query.cat = categories;
			}

            if(jobTypes.length != 0) {
                url += '&type='+jobTypes;
               query.type = jobTypes;
            }

            if($('#emailAlerts').is(':checked')) {
                $.get('{{ route('handleSubscribeForEmailAlerts') }}?type=job&query='+JSON.stringify(query)).done(function (res) {
                });
            }

              if($('.remoteFilter').is(':checked')) {
                    url += '&remote=yes'; 
            }


            $.get(url).done( function (res) {
                if(res.html.length !=0) {
                    $('.listings-container').html(res.html);
                    if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Jobs Items'] }}') {
                        $('.loadMore').show();
                    }
                }else {
                    $('.listings-container').html('<p> {{ __('No jobs were found, please try to change the filters or reset them by clicking the button below.')}} </p>');
                    $('.resetFilters').show();
                }


            }).fail(function (err) {
                $('.listings-container').html('<p>  {{ __('No jobs were found, please try to change the filters or reset them by clicking the button below.')}} </p>');
                $('.resetFilters').show();
            });

            $("#emailAlerts").prop("checked", false);

		})

		$('.loadMore').on('click', function () {
            $('.loadMore').html('<span class="fa fa-spinner fa-spin fa-2x fa-fw"></span>');
            offset++;
            var jobTypes = [];
            var categories = [];
            $('.checkedType:checkbox:checked').each( function (i,item) {
                jobTypes[i]  = item.value;
            });

            var key = $('#key').val();
            var salaryRange = $('#salaryRange').val();
            var range = $('#range').val();
            $("#categories option:selected").each(function (i,item) {
                categories[i]  = item.value;
            });
            var lat = $('#lat').val();
            var lon =  $('#lon').val();
            var  url = '{{ route('showJobs') }}?offset='+offset+'&salary='+salaryRange;


            if(lat != '') {
                url += '&lat='+lat+'&lon='+lon+'&range='+range;
            }

            if(key != '') {
                url += '&key='+key
            }

            if(categories.length != 0) {
                url += '&cat='+categories;
            }

            if(jobTypes.length != 0) {
                url += '&type='+jobTypes;
            }

               if($('.remoteFilter').is(':checked')) {
                    url += '&remote=yes'; 
            }

            $.get(url).done( function (res) {
                $('.listings-container').append(res.html);
                if(res.count < '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Jobs Items'] }}') {
                    $('.loadMore').hide();
                }
                $('.loadMore').html('Load more');

            }).fail(function (err) {
            })
        });

        $('.resetFilters').on('click', function () {
            $('.listings-container').html('<div class="loader"></div>');
            $('.loadMore').hide();
            $('.resetFilters').hide();



            $('#key').val('');
            $("#categories").val('').change();
            $(".checkedType:checkbox").prop('checked',false);
            $(".remoteFilter:checkbox").prop('checked',false);
            $('#lat').val('');
            $('#lon').val('');
            $('#autocomplete-input').val('');

            var  url = '{{ route('showJobs') }}?offset=0&salary=0,10000';
            $.get(url).done( function (res) {
                    $('.listings-container').html(res.html);
                    if(res.count == '{{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['Preferences']['Jobs Items'] }}') {
                        $('.loadMore').show();
                    }
            })

        })
    </script>

    <script type="text/javascript">
        function initAutocomplete() {
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var placeUser = autocomplete.getPlace();
                var lat = placeUser.geometry.location.lat();
                var lng = placeUser.geometry.location.lng();
                $('#lat').val(lat);
                $('#lon').val(lng);
                });
        }
    </script>

  <script src="https://maps.googleapis.com/maps/api/js?key={{ json_decode(file_get_contents(storage_path().'/settings/settings.json'), true)['General Variables']['Api Key'] }}&amp;libraries=places&amp;callback=initAutocomplete"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/map_infobox.js')}}"></script>

    <script type="text/javascript" src="{{asset('frontOffice/js/markerclusterer.js')}}"></script>

@endsection


