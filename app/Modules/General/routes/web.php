<?php

Route::group(['module' => 'General', 'middleware' => ['web'], 'namespace' => 'App\Modules\General\Controllers'], function() {

    Route::get('/', 'GeneralController@showHomePage')->name('showHomePage');
    Route::get('/tasks', 'GeneralController@showTasks')->name('showTasks');
    Route::get('/companies', 'GeneralController@showBrowseCompanies')->name('showBrowseCompanies');
    Route::get('/jobs', 'GeneralController@showJobs')->name('showJobs');
    Route::get('/services', 'GeneralController@showServices')->name('showServices');
    Route::get('/contact', 'GeneralController@showContact')->name('showContact');
    Route::get('/candidates', 'GeneralController@showFreelancers')->name('showFreelancers');
    Route::post('/api/newsletter', 'GeneralController@apiSubscribeToNewsletter')->name('apiSubscribeToNewsletter');
    Route::post('/api/contactUs', 'GeneralController@apiContactUs')->name('apiContactUs');
    Route::get('/es/{data}/{type}', 'GeneralController@showSearchedData')->name('showSearchedData');
    Route::get('/privacy-policy', 'GeneralController@showPrivacyPage')->name('showPrivacyPage');
    Route::get('/terms&conditions', 'GeneralController@showTermsPage')->name('showTermsPage');

    Route::group(['module' => 'User', 'middleware' => ['user']], function() {

        Route::post('/api/addNote', 'GeneralController@apiHandleAddNote')->name('apiHandleAddNote');
        Route::post('/api/updateNote', 'GeneralController@apiHandleUpdateNote')->name('apiHandleUpdateNote');
        Route::get('/api/deleteNote', 'GeneralController@apiHandleDeleteNote')->name('apiHandleDeleteNote');
        Route::get('/api/subscribeEmailAlerts', 'GeneralController@handleSubscribeForEmailAlerts')->name('handleSubscribeForEmailAlerts');

    });

    Route::group(['module' => 'User', 'prefix' => 'admin', 'midsdleware' => ['admin']], function() {

        Route::get('/', 'GeneralController@showAdminDashboard')->name('showAdminDashboard');
        Route::get('/job-categories', 'GeneralController@showJobCategories')->name('showJobCategories');
        Route::post('/job-categories/add', 'GeneralController@handleAddJobCategory')->name('handleAddJobCategory');
        Route::post('/job-categories/delete', 'GeneralController@handleDeleteJobCategory')->name('handleDeleteJobCategory');

         Route::get('/task-categories', 'GeneralController@showTaskCategories')->name('showTaskCategories');
        Route::post('/task-categories/add', 'GeneralController@handleAddTaskCategory')->name('handleAddTaskCategory');
        Route::post('/task-categories/delete', 'GeneralController@handleDeleteTaskCategory')->name('handleDeleteTaskCategory');
        Route::get('/general-settings', 'SettingsController@showGeneralSettings')->name('showGeneralSettings');
        Route::get('/api/general-settings/get', 'SettingsController@apiGetSettings')->name('apiGetSettings');
        Route::get('/api/general-settings/save', 'SettingsController@apiSaveSettings')->name('apiSaveSettings');
        Route::post('/general-settings/updateLogos', 'SettingsController@handleUpdateLogos')->name('handleUpdateLogos');


    });


});
