<?php

/**
 *	General Helper
 */

if (! function_exists('jobTypeToString')) {


    function jobTypeToString($type)
    {
        switch($type) {

            case 0 :
                $stringType = 'Full Time';
                break;
            case 1 :
                $stringType = 'Part Time';
                break;
            case 2 :
                $stringType = 'Internship';
                break;
            case 3 :
                $stringType = 'Temporary';
                break;

            default : $stringType = 'Full Time';
        }

        return $stringType;

    }
}
