<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {

       if (Auth::check() && Auth::user()->type == 3) {
           return $next($request);
        }else  {

           return redirect()->route('showHomePage')->with(['restricted' => 'You dont have the right privileges to access the requested path.'] );

        }
    }


}
