<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::guard($guard)->check() && ( $request->route()->named('showLogin') || $request->route()->named('showRegister') || $request->route()->named('showRequestResetPassword') || $request->route()->named('showResetPassword') ||  $request->route()->named('showRegisterEmployer'))  ) {
            return redirect()->route('showHomePage')->with(['loggedIn' => 'You are already logged in.'] );
        }
        return $next($request);

    }
}
