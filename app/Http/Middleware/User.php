<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
        if(!Auth::guard($guard)->check()) {

            return redirect()->route('showLogin')->with(['restricted' => 'You need to log in in order to access the requested path.'] );
        }else {
            return $next($request);
        }
    }


}
