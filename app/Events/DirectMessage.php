<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DirectMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $img;
    public $username;
    public $sent_at;
    public $body;
    public $msgId;
    public $receriverId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $message,$receriverId)
    {

       $this->img = $user->medias()->where('type',0)->first() ? $user->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png';;
       $this->username = $user->getFullName();
       $this->sent_at = Carbon::parse($message->created_at)->diffForHumans();
       $this->body = $message->body;
       $this->msgId = $message->id;
       $this->receriverId = $receriverId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['direct-message'.$this->receriverId];
    }
}
