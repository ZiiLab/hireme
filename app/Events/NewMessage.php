<?php

namespace App\Events;

use App\Modules\Connect\Models\Message;
use App\Modules\User\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
       $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('messages.'.$this->message->receiver_id);
    }

    public function broadcastWith()
    {
        $messageToReturn = [
            'message' => [
                'id' => $this->message->id,
                'body' => $this->message->body ,
                'sent_at' => $this->message->created_at->format('Y/m/d H:i:s') ,
                'status' => $this->message->status
            ],
            'sender' => [
                'id' => $this->message->sender_id,
                'avatar' => User::where('id',$this->message->sender_id)->first()->medias()->where('type',0)->first() ? User::where('id',$this->message->sender_id)->first()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png',
                'fullName' => User::where('id',$this->message->sender_id)->first()->getFullName()
            ],
            'receiver' => [
                'id' => $this->message->receiver_id,
                'avatar' => User::where('id',$this->message->receiver_id)->first()->medias()->where('type',0)->first() ? User::where('id',$this->message->receiver_id)->first()->medias()->where('type',0)->first()->link : 'frontOffice/images/user-avatar-placeholder.png',
                'fullName' => User::where('id',$this->message->receiver_id)->first()->getFullName()
            ]
        ];
        return ['message' => $messageToReturn];
    }
}
