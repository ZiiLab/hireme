<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Crypt; 


class HiredEvents implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $username;
    public $eventType;
    public $body;
    public $event;
    public $receriverId;
    public $url;

    /**
     * Create a new event instance.
     *
     * @return void 
     */
    public function __construct($user, $eventType, $event, $receriverId,$id)
    {
       $this->username = $user->getFullName();
       $this->receriverId = $receriverId;

       switch ($eventType) {
         case 'application' :
              $this->body = 'Applied for a job';
              $this->url = route('showManageCandidates' , Crypt::encrypt($id));
              break;
         case 'bid' :
              $this->body = 'Placed a bid on';
              $this->url = route('showManageBidders',Crypt::encrypt($id));
              break;
           case 'booking' :
               $this->body = 'Booked your service';
               $this->url = route('showManageServices');
               break;
           case 'publish' :
               $this->body = 'Your Profile Has Been Published';
               $this->url = route('showFreelancerDetails', $id);
               break;
           case 'acceptOffer' :
               $this->body = 'Accepted your bid on';
               $this->url = route('showInProgressTasks');
               break;
           case 'bidUpdate' :
              $this->body = 'Updated his bid on';
              $this->url = route('showManageBidders',Crypt::encrypt($id));
              break;    
       }
       $this->event = $event;
       $this->eventType = $eventType;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['hired-events'.$this->receriverId];
    }
}
