<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Hired\Services\SearchAlerts;
use Illuminate\Support\Facades\Log;


class DailySearchAlerts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'searchAlerts:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily email alerts to notify users about new jobs, tasks, services or freelancers that match their search preferences';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Log::alert('crone running');
        return SearchAlerts::handleSendDailySearchAlerts();
    }
}
