<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'https://app.mailgun.com/app/sending/domains/mg.hired.tn',
        'secret' => 'pubkey-b2a5a38922b7ed8b496376cb9767fa04',
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID','229092588110599'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET','cd4c7609e3de8171466e8cbf28c63588'),
        'redirect' => env('FACEBOOK_PROVIDER_REDIRECT','https://hired.tn/social/facebook/callback'),
    ],

    'google' => [
        'client_id' => '225361944194-benjorer7vv6smig5al919fna3jsbc8c.apps.googleusercontent.com',
        'client_secret' => 'NEMo2h7WI5yN-pGq-bc4iIJ-',
        'redirect' => env('GOOGLE_PROVIDER_REDIRECT','https://hired.tn/social/google/callback'),
    ],


];
